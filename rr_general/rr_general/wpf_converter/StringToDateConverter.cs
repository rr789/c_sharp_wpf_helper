﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace rr_general.wpf_converter
{
    /// <summary>
    /// 
    /// </summary>
    /// <example>
    /// //In the xaml tag add this namespace: 
    /// xmlns:converters="clr-namespace:rr_general.wpf_converter;assembly=rr_general"
    /// 
    /// //Add to xaml resources (in this example we used this converter in a user control inheriting a "Grid" type): 
    /// <Grid.Resources>
    ///     <converters:StringToDateConverter x:Key="StringToDateConverter"/>
    /// </Grid.Resources>
    /// 
    /// //In datagrid->columns.  
    /// //This does two actions: 
    ///     - Converter turns string into date. 
    ///     - "StringFormat='yyyy-MM-dd'" tells WPF how to format the date.  
    /// <rr:DataGridTextColumnRR Header="Product $" Binding="{Binding Path=XXX, Converter={StaticResource StringToDateConverter}, StringFormat='yyyy-MM-dd'}"  >
    /// </example>
    /// Tested: 2019-07-16
    public class StringToDateConverter : IValueConverter
    {
        //From: https://www.codeproject.com/Questions/1184510/Format-number-string-to-a-certain-number-of-decima
        //Tested: 2019-07-16
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string temp = (string)value;
            
            //Safety: For heck of it.  
            if (temp == null || (string)temp == "")
                return null;

            return CString.ToDate((string)temp);

        }


        //Need this otherwise WPF will throw error when existing say a datagrid cell after editing.  
        //"value" is a string in this case (I tested).  Converting to a date (same as method above) works well since it returns a funky "0001-01-01" date if invalid value is entered.
        //Tested: 2019-07-16
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string temp = (string)value;
            
            //Safety: For heck of it.  
            if (temp == null || (string)temp == "")
                return null;

            return CString.ToDate((string)temp);
        }


    }
}