﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace rr_general.wpf_converter
{


    /// <summary>
    /// Use in xaml to get True/False if value is NULL.  
    /// From: http://stackoverflow.com/a/356690/722945
    /// </summary>
    /// <example>
    /// Add to namespace: 
    /// xmlns:converter="clr-namespace:rr_general.wpf_converter;assembly=rr_general"
    /// 
    /// Add to resources:
    /// <converter:IsNullConverter x:Key="isNullConverter"/>
    /// 
    /// Use in the control:
    /// <TextBlock>
    ///     <TextBlock.Style>
    ///         <Style>
    ///             <Style.Triggers>
    ///                 <DataTrigger Binding="{Binding SomeField, Converter={StaticResource isNullConverter}}" Value="False">
    ///                     <Setter Property="TextBlock.Text" Value="NOT NULL!"/>
    ///                 </DataTrigger>
    ///             </Style.Triggers>
    ///         </Style>
    ///     </TextBlock.Style>
    /// </TextBlock>
    /// </example>
    /// Tested: 2017-04-24 all passed.  
    public class IsNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value == null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("IsNullConverter can only be used OneWay.");
        }
    }
}
