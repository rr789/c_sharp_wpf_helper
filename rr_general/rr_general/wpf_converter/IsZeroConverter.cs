﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace rr_general.wpf_converter
{


    /// <summary>
    /// Used in xaml to determine if value = zero.  
    /// </summary>
    /// <example> 
    /// Add to namespace.  
    /// xmlns:converter="clr-namespace:rr_general.wpf_converter;assembly=rr_general"
    /// 
    /// Add to resources:
    /// <Grid.Resources>
    ///     <converter:IsZeroConverter x:Key="isZeroConverter" />
    /// </Grid.Resources>
    /// 
    /// Use in the control.  Here we show positive number green, negative red.  
    /// <TextBlock Text="{Binding ElementName=root, Path=SomeField}">
    /// 	<TextBlock.Style>
    /// 		<Style>
    /// 			<Style.Triggers>
    /// 				<DataTrigger Binding="{Binding ElementName=root, Path=SomeField, Converter={StaticResource isZeroConverter}}" Value="True">
    /// 					<Setter Property="TextBlock.Foreground" Value="Green" />
    /// 				</DataTrigger>
    /// 				<DataTrigger Binding="{Binding ElementName=root, Path=SomeField, Converter={StaticResource isZeroConverter}}" Value="False">
    /// 					<Setter Property="TextBlock.Foreground" Value="Red" />
    /// 				</DataTrigger>
    /// 			</Style.Triggers>
    /// 		</Style>
    /// 	</TextBlock.Style>
    /// </TextBlock>
    /// </example>
    /// Tested: 2020-08-02
    public class IsZeroConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal r = CGlobal.ErrorNumber_Int;

            //Safety: For heck of it. 
            if (value is null)
                return true;

            if (value is string)
                r = CStringNumber.stringToDecimal((string)value);

            if (value is double)
                r = ((decimal)value).RoundRR(8);

            if (value is int)
                r = (decimal)(int)value;

            if (value is long)
                r = (decimal)(long)value;

            if (value is decimal)
                r = (decimal)value;


            return (r == 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("IsNullConverter can only be used OneWay.");
        }
    }
}
