﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace rr_general.wpf_converter
{
    /// <summary>
    /// Used in xaml to determine if value > zero.  
    /// </summary>
    /// <example> 
    /// Add to namespace.  
    /// xmlns:converter="clr-namespace:rr_general.wpf_converter;assembly=rr_general"
    /// 
    /// Add to resources:
    /// <converter:IsPositiveConverter x:Key="isPositiveConverter"/>
    /// 
    /// Use in the control.  Here we show positive number green, negative red.  
    /// <TextBlock Text="{Binding ElementName=root, Path=SomeField}">
    /// 	<TextBlock.Style>
    /// 		<Style>
    /// 			<Style.Triggers>
    /// 				<DataTrigger Binding="{Binding ElementName=root, Path=SomeField, Converter={StaticResource isPositiveConverter}}" Value="True">
    /// 					<Setter Property="TextBlock.Foreground" Value="Green" />
    /// 				</DataTrigger>
    /// 				<DataTrigger Binding="{Binding ElementName=root, Path=SomeField, Converter={StaticResource isPositiveConverter}}" Value="False">
    /// 					<Setter Property="TextBlock.Foreground" Value="Red" />
    /// 				</DataTrigger>
    /// 			</Style.Triggers>
    /// 		</Style>
    /// 	</TextBlock.Style>
    /// </TextBlock>
    /// </example>
    /// Tested: 2019-08-29
    public class IsPositiveConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal r = CStringNumber.stringToDecimal(value.ToString());

            if (r != CGlobal.ErrorNumber_Int)
            {
                if (r > 0)
                    return true;
            }

            return false;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("IsPositiveConverter can only be used OneWay.");
        }
    }
}
