﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace rr_general.wpf_converter
{


    /// <summary>
    /// Use in xaml to get True/False if value is NOT NULL.  
    /// From: http://stackoverflow.com/a/356690/722945
    /// </summary>
    /// <example>
    /// Add to namespace: 
    /// xmlns:converter="clr-namespace:rr_general.converter"
    /// 
    /// Add to resources:
    /// <converter:HasValueConverter x:Key="hasValueConverter"/>
    /// 
    /// <TextBlock>
    ///     <TextBlock.Style>
    ///         <Style>
    ///             <Style.Triggers>
    ///                 <DataTrigger Binding="{Binding SomeField, Converter={StaticResource hasValueConverter}}" Value="True">
    ///                     <Setter Property="TextBlock.Text" Value="NOT NULL!"/>
    ///                 </DataTrigger>
    ///             </Style.Triggers>
    ///         </Style>
    ///     </TextBlock.Style>
    /// </TextBlock>
    /// </example>
    /// Tested: 2017-09-09 all passed.  
    class HasValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value != null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("HasValueConverter can only be used OneWay.");
        }
    }



}
