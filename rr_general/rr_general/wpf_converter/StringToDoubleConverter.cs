﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace rr_general.wpf_converter
{
    /// <summary>
    /// Converts string to number.  This enables xaml property "StringFormat=XXX" to apply formatting.
    /// Note that StringFormat=XXX only works when value is a "number" (not text).  At least for numeric format strings.  
    /// </summary>
    /// <example>
    /// //In the xaml tag add this namespace: 
    /// xmlns:converters="clr-namespace:rr_general.wpf_converter;assembly=rr_general"
    /// 
    /// //Add to xaml resources (in this example we used this converter in a user control inheriting a "Grid" type): 
    /// <Grid.Resources>
    ///     <converters:StringToDoubleConverter x:Key="StringToDoubleConverter"/>
    /// </Grid.Resources>
    /// 
    /// //In datagrid->columns.  
    /// //This does two actions: 
    ///     - Converter turns string into number. 
    ///     - "StringFormat=N" tells WPF to display number with two decimals.  
    /// <rr:DataGridTextColumnRR Header="Product $" Binding="{Binding Path=XXX, Converter={StaticResource StringToDoubleConverter}, StringFormat=N}"  >
    /// </example>
    /// Tested: 2019-07-15
    public class StringToDoubleConverter : IValueConverter
    {
        //From: https://www.codeproject.com/Questions/1184510/Format-number-string-to-a-certain-number-of-decima
        //Tested: 2019-07-16
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double result = 0;
            if (value != null && (double.TryParse(value.ToString(), out result)))
                return Math.Round(result, 4);
            else
                return value;
        }

        
        //Need this otherwise WPF will throw error when existing say a datagrid cell after editing. 
        //Tested: 2019-07-16
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //No need to do any fancy conversion.  "value" param is already a string.  
            return value;
        }






    }
}
