﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace class_helper_rr
{
    public static class CBool
    {
        
        /// <summary>
        /// Converts multiple param types into bool equivalent.  
        /// Handles string "TRUE", "TRUE_", "FALSE", "FALSE_". 
        /// Handles int/long/string 0 for false, 1 for true.  
        /// Handle boolean (of course).
        /// </summary>
        /// <example>
        ///     //These return true.
        ///     CMsgBox.Show(CBool.ConvertToBoolRR("TRUE") );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR("TRUE_") );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR(1) );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR("1") );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR(true));
        ///     
        ///     //These return false.
        ///     CMsgBox.Show(CBool.ConvertToBoolRR("FALSE") );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR("FALSE_") );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR(0) );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR("0") );
        ///     CMsgBox.Show(CBool.ConvertToBoolRR(false));
        /// </example>
        /// <param name="val"></param>
        /// <returns></returns>
        /// Tested: 2019-10-10
        public static bool ConvertToBoolRR(object val)
        {
            if (val == null)
                return false; 

            if (val is bool)
                return (bool)val; 


            if (val is string)
            {
                string tempStr = (string)val;

                if (tempStr == "TRUE")
                    return true;

                if (tempStr == "TRUE_")
                    return true;

                if (tempStr == "1")
                    return true;


                if (tempStr == "FALSE")
                    return false;

                if (tempStr == "FALSE_")
                    return false;

                if (tempStr == "0")
                    return false;

            }


            if (val is long)
            {
                long tempLong = (long)val;

                if (tempLong == 1)
                    return true;
                else 
                    return false;
            }


            if (val is int)
            {
                int tempInt = (int)val;

                if (tempInt == 1)
                    return true;
                else 
                    return false;
            }

            //To stay conservative.
            return false;


        }


        /// <summary>
        /// Converts multiple param types into bool (LONG) equivalent.  
        /// Handles string "TRUE", "TRUE_", "FALSE", "FALSE_". 
        /// Handles int/long/string 0 for false, 1 for true.  
        /// Handle boolean (of course).
        /// </summary>
        /// <example>
        ///     //These return 1.
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong("TRUE") );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong("TRUE_") );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong(1) );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong("1") );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong(true));
        ///     
        ///     //These return 0.
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong("FALSE") );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong("FALSE_") );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong(0) );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong("0") );
        ///     CMsgBox.Show(CBoolHelper.ConvertToLong(false));
        /// </example>
        /// <param name="val"></param>
        /// <returns></returns>
        /// Tested: 2019-10-10
        public static long ConvertToLong(object val)
        {
            if (ConvertToBoolRR(val))
                return 1;
            else 
                return 0;
        }




    }
}
