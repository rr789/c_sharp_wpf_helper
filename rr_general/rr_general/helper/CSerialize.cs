﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Specialized;

namespace class_helper_rr
{
    /* Saves and restores objects from hard disk (ex: an OrderedDictionary object).  
     * Used to store values quickly (where a full blown database w/b overkill).      
     * Modified from here: http://www.gamedev.net/topic/494225-c-how-to-save-an-object-into-a-fine-or-database/
     */
    public class CSerialize
    {
        /* Serializes/saves OrderedDictionary objects to hard disk.  
         * Example: 
                //Create Dictionary object.
                OrderedDictionary myDic = new OrderedDictionary();
                myDic.Add("key1", "val1");
                myDic.Add("key2", "val2");

                //Save object to program's root directory/saved_objects subfolder.  This is default.     
                CSerialize.Save_OrderedDictionary(myDic, "samplename.txt"); 
                //Optional: Save to custom directory "C:\" (have to use two slashes to escape).  
                CSerialize.Save_OrderedDictionary(myDic, "samplename.txt", "C:\\");

                //Clear all dictionary values. 
                myDic.Clear();

                //Open dictionary from hard disk (from default directory).
                myDic = CSerialize.Open_OrderedDictionary("samplename.txt");
                //Optional: Save to custom directory "C:\"
                myDic = CSerialize.Open_OrderedDictionary("samplename.txt", "C:\\");

                //Show dictionary value.  
                CMsgBox.Show(myDic["key1"].ToString());
         * Tested: 2012-01-08 all passed.
         */
        public static void Save_OrderedDictionary(OrderedDictionary myDic, string fileName, string filePathOptional = "")
        {
            //Build directory:
            //If no path passed-in, then set to program's root directory/saved_objects subfolder.  
            string filePath = filePathOptional == "" ? Directory.GetCurrentDirectory() + "\\saved_objects\\" : filePathOptional;
            //Create directory path.  If directory already exists then nothing happens (OK).  This can create multiple subdirectory levels OK (I tested).    
            Directory.CreateDirectory(filePath);
            //Compile full directory/filename string.  
            string filePathAndName = filePath + fileName;

            //Serialize & save object:
            //Create a filestream and name the xml file the object data will go into.
            Stream fileStream = File.Open(filePathAndName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);

            //Create binary formatter to serialise the object.
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            //Serialise the object.
            binaryFormatter.Serialize(fileStream, myDic);

            //Flush and close the filestream.
            fileStream.Flush();
            fileStream.Close();
        }


        /* Opens the serialized/saved OrderedDictionary object.  
         * See method above for example.  
         * Tested: 2012-01-08 all passed.
         */
        public static OrderedDictionary Open_OrderedDictionary(string fileName, string filePathOptional = "")
        {
            //Build directory/filename string.  
            string filePathAndName = filePathOptional == "" ? Directory.GetCurrentDirectory() + "\\saved_objects\\" : filePathOptional;
            filePathAndName += fileName;

            //Create an object to return.
            OrderedDictionary myDic = new OrderedDictionary();

            //Create a filestream and a binary formatter.
            Stream fileStream = null;
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            //Open the file.
            fileStream = File.Open(filePathAndName, FileMode.Open, FileAccess.Read, FileShare.None);

            //Deserialize the opened file into the object.
            myDic = (OrderedDictionary)binaryFormatter.Deserialize(fileStream); //Defining type OrderedDictionary for binaryFormatter is required.  

            //Close the stream.
            if (fileStream != null) { fileStream.Close(); }

            return myDic;

        }

    }
}
