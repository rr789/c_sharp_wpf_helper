﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



/// <summary>
/// Note: These "look" like extension methods but they are not.  The "T" params are there to return an array type that matches param.  
/// </summary>
namespace System //System so we can access the extension methods.  
{
    /// <summary>
    /// Creates a copy of array with new item added.  
    /// If null object is passed-in...This will create a new array object with one open slot, then fill the slot with param value.   
    /// Can add item either to first open slot, or resize/create a new slot at very end.  
    /// </summary>
        /// <example>
        ///     EXAMPLE 1: 
        ///     //Note we skipped "c" (aka position [2]);
        ///     string[] arr = new string[4];
        ///     arr[0] = "a";
        ///     arr[1] = "b";
        ///     arr[3] = "d";
        ///     
        ///     //Add value "c" to position [2].  Note the "true" parameter to insert into first empty slot.   For example sake since the param is "true" by default.  
        ///     arr = CArraySingle.AddItem(arr,"c", true);
        /// 
        ///     //Done.  Loop will show "c" is in position [2].  
        ///     for (var i=0;i<arr.Length;i++)
        ///     {
        ///         CMsgBox.Show("loop " + i + ": " + arr[i]);
        ///     }
        ///     
        /// 
        ///     EXAMPLE 2: 
        ///     //Note we skipped "c" (aka position [2]);
        ///     string[] arr = new string[4];
        ///     arr[0] = "a";
        ///     arr[1] = "b";
        ///     arr[3] = "d";
        ///     
        ///     //Add value "c" to position [2].  Note the "false" parameter to create a new slot and insert at very end.    
        ///     arr = CArraySingle.AddItem(arr,"c", false);
        /// 
        ///     //Done.  Loop will show "c" is in position [4].  
        ///     for (var i=0;i<arr.Length;i++)
        ///     {
        ///         CMsgBox.Show("loop " + i + ": " + arr[i]);
        ///     }
        ///     
        ///     EXAMPLE 3: 
        ///     //Pass a null list.  
        ///     string[] list = null;
        ///     list = CArraySingle.AddItem<string>(list, "new value");
        ///     CMsgBox.Show(list[0]); //Will show "new value".  
        /// </example>
    /// Tested: 2019-07-06.  
    public class CArraySingle
    {
        public static T[] AddItem<T>(T[] arr, T value, bool addToFirstEmptyPosition = true)
        {
            T[] arrCopy = arr;

            //Safety: If null, then create a new array with one open slot.  
            if (arr == null)
                arrCopy = new T[1];    

            int firstEmptyPosition = arrCopy.GetFirstEmptyPosition();

            if (firstEmptyPosition != CGlobal.ErrorNumber_Int && addToFirstEmptyPosition)
                arrCopy[firstEmptyPosition] = value;
            else
            {
                //Array.Resize does a head-fake.  It accepts a "ref" parameter, so one would think it modifies the original param.  However it actually replaces the original with a "copy" (which has a new reference pointer).  
                //Normally this is not an issue (for this non-extension method is it not an issue).  However, it is an issue for extension methods since extension methods want to return a reference to the "original" object.  So an extension method using Array.Resize will appear to do nothing.      
                //See https://stackoverflow.com/a/42411850/722945   
                Array.Resize<T>(ref arrCopy, arrCopy.Length+1); //One would think no need to add "+1" to the param, however the param starts at "1".  I tested, and other examples confirm it.  
                arrCopy[arrCopy.Length-1] = value;
                return arrCopy;
            }

            return arrCopy;
        }


        /// <summary>
        ///     Quicky add all items in a list to an array.  
        /// </summary>
        /// <example>
        ///     string[] b = new string[] { "A", "B" };
        ///     var myList = new List<string>() { "C", "D" };
        ///     b = CArraySingle.AddListToArray(b, myList);
        ///     b.EachRR(p => CMsgBox.Show(p)); //Shows "A","B","C","D".
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldArr"></param>
        /// <param name="newValues"></param>
        /// <param name="addToFirstEmptyPosition">Optional.  True = fill existing open slots.  False = add new slots to end.</param>
        /// <returns></returns>
        public static T[] AddListToArray<T>(T[] oldArr, List<T> newValues, bool addToFirstEmptyPosition = true)
        {
            T[] r = oldArr;

            newValues.EachRR(p =>
            {
                r = AddItem(r, p, addToFirstEmptyPosition);
            });

            return r;
        }





















    }



}
