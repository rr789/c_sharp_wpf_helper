﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using rr_general.helper;


namespace class_helper_rr
{
    public class CStringNumber
    {
        /// <summary>
        ///     Removes decimal, dollar signs, blank spaces.  
        ///     Replaces parenthesies "()" to negative dash "-".  
        /// </summary>
        /// <example>
        ///     CStringNumber.CleanString("$(1,000.009)"); //Returns string "-1000.009".
        /// </example>
        /// <param name="str"></param>
        /// <returns></returns>
        /// Tested: 2021-09-22
        public static string CleanString(string str)
        {
            string r = str;

            //Replace left parenthesies with negative sign.  
            //Keep this above the regex call to replace parenthesies.  
            r = r.RegexReplaceRR("[(]", "-"); //Method handles null strings OK.

            //Remove blanks spaces, $, commas, parenthesies etc.     
            r = r.RegexReplaceRR("[^[0-9-.]", ""); 

            return r;
        }


        /// <summary>
        ///     Converts string to decimal.
        ///     - If null/empty string "": Returns zero. 
        ///     - If error: Returns CGlobal.ErrorNumber_Int (ex: -999999999).
        ///     Handles: $ sign, Whitespace (including "&nbsp" strings), Empty Strings, Commas, Decimals, Negative number Parenthesis "(1200.00)" & hyphen "-$1200.00".  
        /// </summary>
        /// <example>
        ///     Example: CStringNumber.stringToNumber("($1,000.09)"); //Returns decimal -1000.09  
        /// </example>
        /// <param name="str"></param>
        /// <param name="showError">Optional.  Defaults to true.  </param>
        /// <returns></returns>
        /// Tested: 2021-09-22 on all format types, all passed. 
        public static decimal stringToDecimal(string str, bool showError = true)
        {
            decimal returnValue = CGlobal.ErrorNumber_Int; //This default value (ex: -999999999) is used to detect errors.  Fyi: Double type is the most flexible number type.  http://www.thebestcsharpprogrammerintheworld.com/fundamentals/datatypes.aspx

            //Remove blanks spaces, $, commas etc.  
            //Fyi: The "parse" method can handle some of these characters on its own.  Just being extra safe.    
            string tempStr = CStringNumber.CleanString(str); 

            //Safety
            //Convert.ToDouble will:
            //- Return zero if null passed-in.
            //- Throw error if empty string "" is passed-in.  Main reason for this check. 
            if (string.IsNullOrEmpty(tempStr))
	            return 0;

            //Do the conversion:
            try
            {
                //Convert string to number:
                //Add number formats for the parse() method to accept.  Fyi: This is not needed since we removed characters above already (just an extra safety). 
                //Adds exponent handling to convert strings like "6.0125E-05".  
                NumberStyles styles = NumberStyles.AllowParentheses | NumberStyles.Number | NumberStyles.AllowExponent; //"NumberStyles.Number" includes handling for whitespace, hyphen "-", comma, decimal point all-in-one.  From: http://msdn.microsoft.com/en-us/library/7yd1h1be.aspx

                returnValue = Decimal.Parse(tempStr, styles);
            }
            catch (FormatException)
            {
                //These are for debugging only.
                //showError param used in case this method is called in background thread.  Background threads don't like setting clipboard (OR) showing message boxes.  
                if (showError)
                    CMsgBox.Show("Error with stringToDecimal() method.  Could not convert this string: '" + tempStr + "'.  ");

            }

            return returnValue;
        }



        /// <summary>
        ///     Converts string to number.
        ///     - If null/empty string "": Returns zero. 
        ///     - If error: Returns CGlobal.ErrorNumber_Int (ex: -999999999).
        ///     Handles: $ sign, Whitespace (including "&nbsp" strings), Empty Strings, Commas, Decimals, Negative number Parenthesis "(1200.00)" & hyphen "-$1200.00".  
        /// </summary>
        /// <example>
        ///     Example: CStringNumber.stringToNumber("$12 00.12"); //Returns number 1200.12.  
        /// </example>
        /// <param name="str"></param>
        /// <param name="showError">Optional.  Defaults to true.  </param>
        /// <returns></returns>
        /// Tested: 2021-09-22 on all format types, all passed. 
        /// TODO 2021-09-22: Change name from stringToNumber to stringToDouble.
        public static double stringToDouble(string str, bool showError = true)
        {
            double returnValue = CGlobal.ErrorNumber_Int; //This default value (ex: -999999999) is used to detect errors.  Fyi: Double type is the most flexible number type.  http://www.thebestcsharpprogrammerintheworld.com/fundamentals/datatypes.aspx

            //Remove blanks spaces, $, commas etc.  
            //Fyi: The "parse" method can handle some of these characters on its own.  Just being extra safe.    
            string tempStr = CStringNumber.CleanString(str); 

            //Safety
            //Convert.ToDouble will:
            //- Return zero if null passed-in.
            //- Throw error if empty string "" is passed-in.  Main reason for this check. 
            if (string.IsNullOrEmpty(tempStr))
	            return 0;

            //Do the conversion:
            try
            {
                //Convert string to number:
                //Add number formats for the parse() method to accept.  Fyi: This is not needed since we removed characters above already (just an extra safety). 
                //Adds exponent handling to convert strings like "6.0125E-05".  
                NumberStyles styles = NumberStyles.AllowParentheses | NumberStyles.Number | NumberStyles.AllowExponent; //"NumberStyles.Number" includes handling for whitespace, hyphen "-", comma, decimal point all-in-one.  From: http://msdn.microsoft.com/en-us/library/7yd1h1be.aspx

                returnValue = Double.Parse(tempStr, styles);
            }
            catch (FormatException)
            {
                //These are for debugging only.
                //showError param used in case this method is called in background thread.  Background threads don't like setting clipboard (OR) showing message boxes.  
                if (showError)
                    CMsgBox.Show("Error with stringToNumber() method.  Could not convert this string: '" + tempStr + "'.  ");

            }

            return returnValue;

        }






    }
}
