﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace class_helper_rr
{
    public class CDynamic
    {
        /// <summary>
        ///     Determines in property exists.  This focuses on "dynamic" objects with two dimensional arrays.  Like Dictionary or Expandoobject. 
        ///     Handles properties with NULL values.  Returns true if property (with NULL value) exists. 
        /// </summary>
        /// <example>
        ///     //Example 1: 
        ///     dynamic a = new ExpandoObject();
        ///     a.my_property = null;                   //We only care about the property, not the value (NULL).    
        ///     DoesPropertyExist(a, "my_property");    //Returns true. 
        ///     DoesPropertyExist(a, "ZZZZ");           //Returns false. 
        ///     
        ///     //Example 2:
        ///     dynamic a = new Dictionary<string,object>();
        ///     a["my_property"] = null;                //We only care about the property, not the value (NULL).  
        ///     DoesPropertyExist(a, "my_property");    //Returns true. 
        ///     DoesPropertyExist(a, "ZZZZ");           //Returns false.
        /// </example>
        /// <param name="obj">Any "dynamic" object.</param>
        /// <param name="name">Property name</param>
        /// <returns></returns>
        /// <remarks>
        ///     I tried putting this into an "extension" class.  However C# won't allow extension methods that accept "Dynamic" properties as first parameter.  See error CS1103.  
        /// 
        ///     Tested 2019-07-03: 
        ///         -Dynamic object as new Dictionary. 
        ///         -Dynamic object as new ExpandoObject;
        ///         -Above having property with NULL value.  This method correctly returned TRUE since property existed (even if it had NULL value).  
        /// </remarks>
        //https://stackoverflow.com/a/48752086/722945
        public static bool DoesPropertyExist(dynamic obj, string name) 
        {
            if (obj == null) 
                return false;
            
            //Both Dictionary and ExpandoObject can get casted to IDictionary. 
            if (obj is IDictionary<string, object>) 
            {
                //Added this bounce line to handle older VisualStudio 2013.   Stackoverflow link above has more concise style tha works with later version.  
                IDictionary<string, object> temp = obj;
                return temp.ContainsKey(name);
            }

            //Added this type -RR.  Need this if object was converted from JSON string into JObject.    
            if (obj is Newtonsoft.Json.Linq.JObject)
            {
                //Added this bounce variable to handle older VisualStudio 2013.   Newer VisualStudio can use "return ((Newtonsoft.Json.Linq.JObject)obj).ContainsKey(name);"
                //JObject implements IDictionary.  See https://stackoverflow.com/a/7216953/722945
                IDictionary<string, JToken> temp = obj;
                return temp.ContainsKey(name);
            }

            return obj.GetType().GetProperty(name) != null;
        }



    }
}
