﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.helper
{
    public static class CReflection
    {
        /// <summary>
        /// Returns name of application the method is called from.  
        /// This method is in-expensive/fast. 
        /// </summary>
        /// <example>
        /// CReflection.GetApplicationName(); //Returns name of app that called this method.  Does not return "RR_General" where this method is stored.  
        /// </example>
        /// 
        /// <remarks>
        /// From: https://stackoverflow.com/a/27009377/722945
        /// </remarks>
        /// Tested: 2020-03-20
        public static string GetApplicationName()
        {
            return System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
        }




        /// <summary>
        /// Gets class name where this method was called.   
        /// ★★★ Note: This method is expensive.  Calls an expensive StackTrace method.    
        /// </summary>
        /// <example>
        /// class A
        /// {
        ///     public void Test()
        ///     {
        ///         CReflection.GetClassName(); //Returns "A".  
        ///     }
        /// }
        /// </example>
        /// <remarks>
        ///     Code from: https://stackoverflow.com/a/48570709/722945
        ///     Also see this comment.  StackTrace is the only way.  https://stackoverflow.com/a/48570725/722945
        /// </remarks>
        /// <param name="caller"></param>
        /// <param name="file"></param>
        /// Tested: 2020-03-20
        public static string GetClassName()
        {
            var methodInfo = new StackTrace().GetFrame(1).GetMethod();
            return methodInfo.ReflectedType.Name;
        }




        /// <summary>
        /// Returns method name this is called in.  
        /// This method is in-expensive/fast.  
        /// </summary>
        /// <example>
        /// function void TestMe()
        /// {
        ///     CReflection.GetMethodName(); //Returns "TestMe".  
        /// }
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/9621581/722945
        /// </remarks>
        /// <param name="methodname">Do not pass anything in for the method name.</param>
        /// Tested: 2020-03-20
        public static string GetMethodName([CallerMemberName] string methodname = null)
        {
            return methodname;
        }



    }
}
