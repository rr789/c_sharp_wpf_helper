﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using class_helper_rr;
using PropertyChanged;

namespace rr_general.helper
{
    /// <summary>
    /// Faster performance for ObservableCollection.  Has a method to suppress OnCollectionChanged when loading bulk lists.  
    /// </summary>
    /// <remarks>
    /// Modified from: https://peteohanlon.wordpress.com/2008/10/22/bulk-loading-in-observablecollection/
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    [SuppressPropertyChangedWarnings] //2024-08-08 Suppresses warning "Warning Fody/PropertyChanged: Unsupported signature for a On_PropertyName_Changed method" when using this collection with Fody->PropertyChanged.  For whatever reason Fody->PropertyChanged doesn't like dealing with property changes with this class.   
    public class CObservableCollection<T>: ObservableCollection<T>
    {
        /// <summary>
        /// Used to temporarily suppress events.  Mainly used in RemoveAllRR() and ImportListRR().  
        /// True: Notifications will bubble-up.
        /// False: Notifications are suppressed.  
        /// </summary>
        public bool AllowNotification = true;
 
        /// <summary>
        /// Has ability to suppress notification.  
        /// </summary>
        /// Tested: 2024-08-29
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (AllowNotification)
                base.OnCollectionChanged(e);
        }
 

        /// <summary>
        /// Imports items from a List object.  
        /// - Performance boost...This suppresses event OnCollectionChanged until the "last" item is added.  Ex: Adding 1,000 items will only fire OnCollectionChanged once, after the 1,000th item is added. 
        /// </summary>
        /// <example>
        /// //Incorrect example: 
        /// myObservableCollection = new ObservableCollection<some type>()
        /// myObservableCollection = new ObservableCollection(listToImport); //☆☆☆ This will break any custom event handlers attached to myObservableCollection.
        /// 
        /// //Correct example: 
        /// //Instead, use this extension.    
        /// myObservableCollection = new ObservableCollection<some type>()
        /// myObservableCollection.ImportListRR(listToImport);  //☆☆☆ Previously attached event handlers remain in-tact.  Also performance boost from temporarily suppressing OnCollectionChanged.  
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="listToImport"></param>
        /// <param name="removeOldRecords">
        /// False: Append to existing collection.    
        /// True: Remove all records, then import list.  
        /// </param>
        /// Tested: 2024-08-29
        public void ImportListRR(List<T> listToImport, bool removeOldRecords = true)
        {
            //Keep above the safety count check.  We often want to clear the existing list whether or not the new list has records.    
            if (removeOldRecords)
                //RemoveAllRR() will fire an OnCollectionChangedEvent "once" when the last item is removed.
                //This means OnCollectionChangedEvent could fire "twice" with this ImportListRR method (once for RemoveAllRR() + once for adding the last item below) but that's OK. 
                this.RemoveAllRR();  


            //Safety to only set "AllowNotification = false" if we are guaranteed to also hit
            //"AllowNotification = true" inside the loop.  Otherwise AllowNotification would be stuck at "false" forever.  
            if (listToImport.CountRR() > 0)
            {
                AllowNotification = false;
                
                //Import items
                for (var i=0; i<listToImport.CountRR(); i++)
                {
                    //Only trigger event on last item.  
                    if (i==listToImport.CountRR()-1)
                        AllowNotification = true;

                    //Add() method will trigger the event OK.  
                    this.Add(listToImport[i]);
                }
            }


        }



        /// <summary>
        /// Removes all items from collection.  
        /// Similar to List->RemoveAll().  
        /// - Performance boost...This suppresses event OnCollectionChanged until the last item is removed. 
        /// </summary>
        /// Tested: 2024-08-29
        public void RemoveAllRR()
        {
            //Safety to only set "AllowNotification = false" if we are guaranteed to also hit
            //"AllowNotification = true" inside the loop.  Otherwise AllowNotification would be stuck at "false" forever.  
            if (this.CountRR() > 0)
            {
                //Disable OnCollectionChanged event. 
                AllowNotification = false;

                //Remove records.
                for (var i=this.CountRR()-1; i>=0; i--)
                {
                    //Activate event before deleting the very last record.  
                    if (i==0)
                        AllowNotification = true;

                    //RemoveAt() method will trigger the event OK.  
                    this.RemoveAt((int)i);
                }
            }


        }

    }
}
