﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace class_helper_rr
{
    public static class CType
    {
        /// <summary>
        /// Converts an object to another type.  
        /// </summary>
        /// <example>
        /// object a = new object();
        /// a = 1;
        /// var s = CType.ConvertType<string>(a);
        /// CMsgBox.Show(s.GetType()); //Returns "System.String"
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/899636/722945
        /// </remarks>
        /// <typeparam name="T">Type to convert to.</typeparam>
        /// <param name="obj">Object to convert.</param>
        /// <returns></returns>
        /// Tested: 2017-06-17.
        public static T ConvertType<T>(object obj)
        {
            T r = default(T); //Default(T) returns empty value depending on type.  Ex: Null for controls/strings, 0 for integers.  I tested. 

            if (obj == null) return r;

            if (obj is T)
                r = (T)obj;
            else
            {
                try
                {
                    r = (T)Convert.ChangeType(obj, typeof(T));
                }
                catch (InvalidCastException)
                {
                    Debug.WriteLine("Error with CType.ConvertType: Cannot cast from " + obj.GetType() + " to " + typeof(T) + ".");
                }
            }

            return r;

        }









    }
}
