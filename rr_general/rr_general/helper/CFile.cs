﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace rr_general.helper
{
    public static class CFile
    {

        /// <summary>
        /// Gets filename (with extension) from full file path.
        /// Handles both forward and backward slashes.  
        /// </summary>
        /// <example>
        /// var a = @"C:\Users\randy\AppData\Roaming\my_app_name\myfile.txt";
        /// CFile.GetFilenameFromPath(a); //returns "myfile.txt"
        /// </example>
        /// <param name="fullpath"></param>
        /// <returns></returns>
        /// Tested: 2017-06-19
        public static string GetFilenameFromPath(string fullpath)
        {
            string r = null;

            if (fullpath == null) return null;

            var delimiters = new List<string> { @"\", "/" };
            var myList = CString.ParseWithDelimiters(fullpath, delimiters);
            r = myList.Last<string>();
            
            return r;
        }


        /// <summary>
        /// Returns file extension from full directory path. 
        /// </summary>
        /// <example>
        /// CFile.GetFileExtensionFromPath(@"\ddd\ddd\ddd.aaa.bbb.ccc"); Returns "ccc" without the period.  
        /// </example>
        /// <param name="fullpath"></param>
        /// <returns></returns>
        /// Tested: 2019-09-28
        public static string GetFileExtensionFromPath(string fullpath)
        {
            string r = null;

            if (fullpath == null) return null;

            var delimiters = new List<string> { "."};
            var myList = CString.ParseWithDelimiters(fullpath, delimiters);
            r = myList.Last<string>();
            
            return r;
        }


        /// <summary>
        /// Removes characters that pevent a file from saving to hard drive.  
        /// </summary>
        /// <example>
        /// CFile.SanitizeFileName(@"\/hello123_-.dll"); //Returns "hello123_-.dll".
        /// </example>
        /// <param name="_file"></param>
        /// <returns></returns>
        /// Tested: 2019-09-28  
        public static string SanitizeFileName(string _file)
        {
            //I tested all of these regex characters in Windows file explorer.  
            //Note the "^" means "not in". 
            var regex = new Regex(@"[^A-Za-z0-9_\s\-\.\,\)\(\$\!\[\]\{\}]{0,}");
            return regex.Replace(_file, "");

        }


        /// <summary>
        ///     Opens file similar to double clicking file in Windows explorer.  
        /// </summary>
        /// <example>
        ///     CFile.OpenFile("C:\myfile.txt");
        /// </example>
        /// <param name="filePath">Full path to file, including extension.  Ex: "C:\myfile.txt" </param>
        /// Tested: 2019-07-21
        public static void OpenFile(string filePath)
        {
            if (!File.Exists(filePath) )
            {
                Clipboard.SetText(filePath);
                CMsgBox.Show("Sorry, file path does not exist.  The file path is copied to clipboard for debugging.");
                return;
            }

            //Opens file as if opening in file explorer.  
            ProcessStartInfo startInfo = new ProcessStartInfo(filePath);
            Process.Start(startInfo);

        }




        /// <summary>
        /// Saves text to disk.  
        /// Suppresses exceptions.  Instead, returns bool to know if file save was successful.   
        /// </summary>
        /// <example>
        /// CFile.SaveTextFile(@"C:\my_file.txt", "text to save", CFile.SAVE_FILE_OVERWRITE); 
        /// </example>
        /// <param name="fullPath"></param>
        /// <param name="textToSave"></param>
        /// <param name="saveFileConstant">
        /// Use constants in this class: 
        /// - SAVE_FILE_OVERWRITE: Make new file or overwrite existing. 
        /// - SAVE_FILE_APPEND_TEXT: Append text to existing file.  
        /// </param>
        /// <returns>
        /// True: File save successful.
        /// False: File save had problem.  
        /// </returns>
        /// Tested: 2020-06-16
        public static bool SaveTextFile(string fullPath, string textToSave, string saveFileConstant = SAVE_FILE_OVERWRITE)
        {
            bool r = false;  

            string dirPath = CDirectory.GetDirectoryFromPath(fullPath);

            CTryCatchWrapper.wrap(delegate ()
            {
                //Creates directory, or does nothing if already exists.  
                System.IO.Directory.CreateDirectory(dirPath);

                switch (saveFileConstant)
                {
                    case SAVE_FILE_OVERWRITE: 
                        using (StreamWriter w = File.CreateText(fullPath))
                        {
                            w.Write(textToSave);
                            r = true;
                        }
                        break;

                    case SAVE_FILE_APPEND_TEXT: 
                        using (StreamWriter w = File.AppendText(fullPath))
                        {
                            w.Write(textToSave);
                            r = true;
                        }
                        break;

                }
            }, false,false,false); //No need to draw attention if writing log file failed.  

            return r;
        }

        /// <summary>
        /// Used with method SaveTextFile().
        /// </summary>
        public const string SAVE_FILE_OVERWRITE = "SAVE_FILE_OVERWRITE";

        /// <summary>
        /// Used with method SaveTextFile().
        /// </summary>
        public const string SAVE_FILE_APPEND_TEXT = "SAVE_FILE_APPEND_TEXT";















        /// <summary>
        /// 1. Opens file.  2. Replaces text in file.  3. Saves modified file.  
        /// Returns true if file was opened/saved successfully.       
        /// </summary>
        /// <example>
        /// CFile.TextFile_ReplaceSubstring(@"C:\DeleteMe.txt", "string to find", "replacement value");
        /// </example>
        /// <param name="filePath"></param>
        /// Full path, including file extension.  
        /// <param name="findString"></param>
        /// <param name="replaceString"></param>
        /// <returns></returns>
        public static bool TextFile_ReplaceSubstring(string filePath, string findStringRegex, string replaceString)
        {
            //Safety: 
            if (!File.Exists(filePath))
                return false;

            //Proceed to open file, replace substring, save file.  
            //From https://stackoverflow.com/a/13509665
            string text = File.ReadAllText(filePath);
            text = text.RegexReplaceRR(findStringRegex, replaceString);
            File.WriteAllText(filePath, text);

            return true;
        }







    }
}
