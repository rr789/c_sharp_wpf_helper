﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.helper
{
    public class CZip
    {
        /// <summary>
        /// Downloads zip file from website.  Then extracts to directory.  
        /// Throws exception log with helpful popup if cannot download zip file (OR) cannot save to disk.  
        /// </summary>
        /// <example>
        /// CZip.DownloadZipThenExtract("http://mysite.com/zip_file.zip, @"C:\TempExtractDir"); 
        /// </example>
        /// <param name="urlToZip"></param>
        /// <param name="dirExtractDestination"></param>
        /// Tested: 2020-06-21
        public static void DownloadZipThenExtract(string urlToZip, string dirExtractDestination)
        {
            string fileName = CFile.GetFilenameFromPath(urlToZip);
            string zipSaveFilePath = CDirectory.ConcatDirectoryAndFilePath(CDirectory.GetDirTemp_StartupProject(), Guid.NewGuid() + "__" + fileName);

            //2020-08-25 Prevents very rare error "Could not create ssl/tls secure channel".  See OneNote for this error.  
            //https://stackoverflow.com/a/62998805/722945
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            using (WebClient fileDownloader = new WebClient())
            {
                fileDownloader.DownloadFile(urlToZip, zipSaveFilePath);
            }

            if (File.Exists(zipSaveFilePath))
            {
                ZipFile.ExtractToDirectory(zipSaveFilePath, dirExtractDestination);
                File.Delete(zipSaveFilePath);
            }
            else
            {
                CLogger.CreateLogEntry("Error with CZip->DownloadZipThenExtract().  File not downloaded so could not extract.  Fyi: URL to download " + urlToZip, true, true);
            }

        }

    }
}
