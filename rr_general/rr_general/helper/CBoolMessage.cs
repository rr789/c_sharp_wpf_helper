﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using class_helper_rr;
using Newtonsoft.Json;

namespace rr_general.contract
{
    /// <summary>
    /// Used by functions to return a tiny object containing bool value, string description, and color message.
    /// Very handy for multi-threaded validation functions.  
    /// </summary>
    public class CBoolMessage
    {
        /// <summary>
        /// Boolean result to return.  
        /// </summary>
        public bool BoolValueRR { get; set; }
        
        /// <summary>
        /// Optional message.  
        /// </summary>
        public string MessageRR { get; set; }

        /// <summary>
        /// Optional.  Value equals one of the constants in this class.  Ex: SEVERITY_COLOR_GREEN, SEVERITY_COLOR_BLUE etc. 
        /// Often used with the Toast Notifications library.  
        /// </summary>
        public String SeverityColorRR { get; set;}


        /// <summary>
        /// True: Deletes existing messages to show new message.  
        /// </summary>
        public bool OverwriteExistingMessages = true;


        //-------------------------
        //OPTIONAL PROPERTIES.  
        //-------------------------
        /// <summary>
        /// Optional property.  Used when a method returns both: 
        ///     1. Validation true/false.
        ///     2. Completed transaction recid (or some other value).  
        ///     
        /// Default value: NULL
        /// </summary>
        public string OptionalString01 { get; set; }

        /// <summary>
        /// Optional property.  Used when a method returns both: 
        ///     1. Validation true/false.
        ///     2. Completed transaction recid (or some other value). 
        ///     
        /// Default value: CGlobal.ErrorNumber_Long
        /// </summary>
        public long OptionalLong01 { get; set; } = CGlobal.ErrorNumber_Long;
        
        /// <summary>
        /// Optional property.  Used when a method returns both: 
        ///     1. Validation true/false.
        ///     2. Completed transaction recid (or some other value).  
        ///     
        /// Default value: CGlobal.ErrorNumber_Decimal
        /// </summary>
        public decimal OptionalDecimal01 { get; set; } = CGlobal.ErrorNumber_Decimal;

        /// <summary>
        /// Optional property.  Used when a method returns both: 
        ///     1. Validation true/false.
        ///     2. Completed transaction recid (or some other value).  
        ///     
        ///     
        /// Default value: CGlobal.ErrorDateTime
        /// </summary>
        public DateTime OptionalDateTime01 { get; set; }= CGlobal.ErrorDateTime;


        //--------------------------------
        //SEVERITY COLOR CONSTANTS: 
        //These are often used with the Toast Notification library.  
        //--------------------------------
        /// <summary>
        /// Aka task completed.  
        /// </summary>
        public const string SEVERITY_COLOR_GREEN = "SEVERITY_COLOR_GREEN";

        /// <summary>
        /// Aka not an error, but not expected results.
        /// Ex: Often used to tell user "query ran but no records returned".  
        /// </summary>
        public const string SEVERITY_COLOR_BLUE = "SEVERITY_COLOR_BLUE";

        /// <summary>
        /// For minor error.  
        /// </summary>
        public const string SEVERITY_COLOR_ORANGE = "SEVERITY_COLOR_ORANGE";

        /// <summary>
        /// For major error.
        /// </summary>
        public const string SEVERITY_COLOR_RED = "SEVERITY_COLOR_RED";
        
        //--------------------------------

        /// <summary>
        /// Quick way to create an object with one line of code.
        /// </summary>
        /// <param name="booleanValue"></param>
        /// <param name="description"></param>
        /// <param name="severityConstant">Use one of the constants in this class.  Ex: SEVERITY_COLOR_GREEN, SEVERITY_COLOR_BLUE etc.</param>
        /// <returns></returns>
        /// Tested: 2019-10-21
        public static CBoolMessage CreateRR(bool booleanValue, string description = "", string severityConstant = "", bool overwriteExistingMsg = true)
        {
            return new CBoolMessage() { BoolValueRR = booleanValue, MessageRR = description, SeverityColorRR = severityConstant, OverwriteExistingMessages = overwriteExistingMsg };
        }


        /// <summary>
        /// Fast method to create false/red message.  Initial setup.  
        /// </summary>
        /// Tested: 2019-12-21
        public static CBoolMessage CreateFalseRedObj(string msg = "")
        {
            return new CBoolMessage() { BoolValueRR = false, MessageRR = msg, SeverityColorRR = CBoolMessage.SEVERITY_COLOR_RED, OverwriteExistingMessages = true };
        }

        /// <summary>
        /// Fast method to create true/green message.  Initial setup.  
        /// </summary>
        /// Tested: 2019-12-21
        public static CBoolMessage CreateTrueGreenObj(string msg = "")
        {
            return new CBoolMessage() { BoolValueRR = true, MessageRR = msg, SeverityColorRR = CBoolMessage.SEVERITY_COLOR_GREEN, OverwriteExistingMessages = true };
        }

        /// <summary>
        /// Create new CBoolMessage object from json string. 
        /// Useful for external applications that want to return a C# CBoolMessage, but can only return object as json string.    
        /// </summary>
        /// <example>
        ///     var str = "{\"BoolValueRR\":false,\"MessageRR\":\"Hello\",\"SeverityColorRR\":\"SEVERITY_COLOR_RED\",\"OverwriteExistingMessages\":true} ";
        ///     CBoolMessage msgObj = CBoolMessage.DeserializeJson(str);
        ///     CMsgBox.Show("cbool: " + msgObj.MessageRR);
        /// </example>
        /// <param name="jsonStr"></param>
        /// Tested: 2021-04-26
        public static CBoolMessage DeserializeJson(string jsonStr = "")
        {
            return JsonConvert.DeserializeObject<CBoolMessage>(jsonStr);
        }



    }
}
