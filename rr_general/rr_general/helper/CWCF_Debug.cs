﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.helper
{



    /// <summary>
    /// Adds a debug log to inspect: 
    /// - Request sent. 
    /// - Response received. 
    /// 
    /// Handles logging the "body" very well.  Does not (yet) trap header parameters (cookies, content type etc).  
    /// Note that the body does contain an xml section called "header" but that's really part of the body (aka the body's header).  
    /// </summary>
    /// <example>
    ///     //Instantiate AIF/WCF service.  
    ///     var wcfObj = new SRV_CREX_AIF_QueryAXManagementRRClient(myBinding, myEndpoint);
    ///     
    ///     //Instantiate WCF debug object.
    ///     //This contains two events we can use (Before/After request).  
    ///     var wcfDebugObj = new CWCF_Debug();
    ///     wcfDebugObj.MessageInspectorRR.BeforeSendRequestRR += (object a, EventArgs args) => 
    ///     { 
    ///         if (DebugLog_Enabled)
    ///         {
    ///             //Convert type, to get property MessageRR.  
    ///             var _args = args as CWCF_Debug_EventArgs;
    ///             CDirectoryHelper.SaveTextFile(DebugLog_SaveDirectory, _args.MessageRR, CDirectoryHelper.SAVE_FILE_APPEND_TEXT);
    ///         }
    ///     };
    ///
    ///     //Optional...Do similar for "after receive reply". 
    ///     wcfDebugObj.MessageInspectorRR.AfterReceiveReplyRR......
    ///
    ///     //Add debugging to the AIF/WCF object.  
    ///     obj.Endpoint.EndpointBehaviors.Add(wcfDebugObj);
    /// </example>
    /// <remarks>
    ///     From: 
    ///         - https://stackoverflow.com/a/49446604/722945
    ///         - https://dotnetkeep.com/intercepting-raw-soap-request-and-response-wcf/
    /// </remarks>
    /// Tested: 2022-02-15
    public class CWCF_Debug : IEndpointBehavior
    {   
        /// <summary>
        /// Child class object contains the two events to hook into.  
        /// - BeforeSendRequestRR
        /// - AfterReceiveReplyRR
        /// </summary>
        public ChildClass_MyMessageInspector MessageInspectorRR { get; set; }

        //Constructor 
        public CWCF_Debug()
        {
            MessageInspectorRR = new ChildClass_MyMessageInspector();
        }

        /* 
        ----------------------------------
        STEP 01: ADD MESSAGE INSPECTOR TO WCF SERVICE.  
        ----------------------------------
        */
        //Four boilerplate methods are required since we inherit IEndpointBehavior.  
            //- AddBindingParameters  (not used)
            //- ApplyClientBehavior ★(only care about this one)
            //- ApplyDispatchBehavior (not used)
            //- Validate              (not used)
        
        //Boilerplate placeholder.  
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {}

        //★ Main goal.
        //Attach our child class (with before/after message events) to this parent class.  
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(MessageInspectorRR);
        }

        //Boilerplate placeholder. 
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {}

        //Boilerplate placeholder. 
        public void Validate(ServiceEndpoint endpoint)
        {}  
    




        /* 
        ----------------------------------
        STEP 02: DEFINE OUR TWO CUSTOM EVENTS.  
        Basically piggy-backing onto the native method calls.      
        ----------------------------------
        */

        /// <summary>
        /// Child class.  
        /// Handles the native before/after method calls.  
        /// Enables our custom before/after events.    
        /// </summary>
        public class ChildClass_MyMessageInspector : IClientMessageInspector 
        {
            /// <summary>
            /// Fired just before sending the request to server.  EventArgs contains the message sent.  
            /// </summary>
            public event EventHandler BeforeSendRequestRR;

            private void FireEvent_BeforeSendRequestRR(string debugString)
            {
                var args            = new CWCF_Debug_EventArgs();
                args.MessageRR      = debugString;

                BeforeSendRequestRR?.Invoke(this, args);
            }

            //---------------------------
            /// <summary>
            /// Fired after receiving response from server.  EventArgs contains the message received.  
            /// </summary>
            public event EventHandler AfterReceiveReplyRR;

            private void FireEvent_AfterReceiveReplyRR(string debugString)
            {
                var args            = new CWCF_Debug_EventArgs();
                args.MessageRR      = debugString;

                AfterReceiveReplyRR?.Invoke(this, args);
            }

            //---------------------------
            //Don't change method name.  This method name is required by interface IClientMessageInspector. 
            public object BeforeSendRequest(ref Message request, IClientChannel channel)
            {
                FireEvent_BeforeSendRequestRR(request.ToString());
                return null;
            }

            //Don't change method name.  This method name is required by interface IClientMessageInspector. 
            public void AfterReceiveReply(ref Message reply, object correlationState)
            {
                FireEvent_AfterReceiveReplyRR(reply.ToString());
            }
        }   


    }

    /// <summary>
    /// Used with class CWCF_Debug.  
    /// </summary>
    public class CWCF_Debug_EventArgs: EventArgs
    {
        public string MessageRR { get; set; }
    }

}
