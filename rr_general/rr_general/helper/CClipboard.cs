﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using System.Windows;

namespace rr_general.helper
{
    public class CClipboard
    {
        /// <summary>
        /// Appends new text to existing clipboard value.  Puts a linefeed between old/new strings.  
        /// </summary>
        /// <example>
        /// Clipboard.SetText("old value");
        /// CClipboard.AppendTextToClipboard("new value"); //Clipboard now has this string: "old value" [linefeed here] "new value".
        /// </example>
        /// Tested: 2018-07-23
        public static void AppendTextToClipboard(string newText)
        {
            string oldText = Clipboard.GetText();

            Clipboard.SetText(oldText + Environment.NewLine + newText);
        }



        /// <summary>
        /// Basic try/catch wrapper to prevent errors if called from webapp.  
        /// From desktop: Will copy to clipboard.
        /// From webapp: Write console line.  
        /// </summary>
        /// <param name="newText"></param>
        public static void SetTextRR(string newText)
        {
            try
            {
                Clipboard.SetText(newText);
            }
            catch(Exception ex)
            {

                Console.WriteLine(newText);
            }



        }


    }
}
