﻿using rr_general.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace class_helper_rr
{
    public class CTryCatchWrapper
    {
        /// <summary>
        /// Consice way to run Try/Catch when calling a method.  Will also log message to disk.  <para/>
        /// </summary>
        /// <example>
        /// Option 1 (if no return value):
        ///     CTryCatchWrapper.wrap(dbConnection.Open ,true); //This open() attempt is now wrapped in one line!  True will throw exception (optional).
        /// Option 2:
        ///     CTryCatchWrapper.wrap(delegate{ //delegate = anonymous function.  
        ///         rowsAffected = command.ExecuteNonQuery(); //Can use rowsAffected outside of this scope OK.  
        ///     },true); //True will throw exception (optional).  
        /// </example>
        /// <param name="method">For option 1...pass-in method WITHOUT "()" AT END.  Defining with "()" will make it execute twice (once when setting the parameter, again in the wrapper below).  </param>
        /// <param name="showPopup">True: Will display pop-up msg. </param>
        /// <param name="throwException">True: Will throw an exception error and halt the program. </param>
        /// <param name="createLogEntry">True: Will write log record to hard disk. </param>
        /// Tested: 2018-09-10 all passed. 
        public static void wrap(Action method, bool showPopup = true, bool throwException = false, bool createLogEntry = true)
        {
            try
            {
                method();
            }
            catch (Exception ex)
            {
                if (createLogEntry)
                    CLogger.CreateLogEntry("ERROR: " + ex.ToString(), showPopup, throwException);
                else
                {
                    if (showPopup)
                        CMsgBox.Show("ERROR: " + ex.ToString());

                    if (throwException)
                        throw new Exception("ERROR: " + ex.ToString());

                }
            }

        }


    }
}
