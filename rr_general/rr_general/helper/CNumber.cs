﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace class_helper_rr
{
    public class CNumber
    {
        //Used in function below.  From: http://stackoverflow.com/a/768001/722945
        private static readonly Random random = new Random(); //Generates random numbers.  
        private static readonly object syncLock = new object(); //Helps sync the "random" variable to main thread.  Read Stackoverflow for more info.  

        /// <summary>
        /// Returns random integer between min/max values.    
        /// </summary>
        /// <remarks>
        /// Tested: 2012-03-11 all passed.  
        /// </remarks>
        public static int randomNumberInt(int min, int max)
        {
            lock (syncLock) //Synchronize to main thread (to ensure number is truly random).  See Stackoverflow link above for more info.  
            {
                return random.Next(min, max);
            }
        }

        /// <summary>
        /// Returns random double number type between min/max values.  Can handle decimals/fractions etc.    
        /// </summary>
        /// <remarks>
        /// Tested: 2012-03-11 all passed.  
        /// </remarks>
        public static double randomNumberDouble(double min, double max)
        {
            //Used to convert decimal to integer.  1,000,000 handles up to six decimal places.    
            int multipyFactor = 1000000;

            int minAsInt = (int)(min * multipyFactor);
            int maxAsInt = (int)(max * multipyFactor);

            int randomInt = randomNumberInt(minAsInt, maxAsInt);

            return (double)randomInt / (double)multipyFactor; //Need to cast these as Double types to return decimal places (I forget the reason why but it is needed).  

        }


        //----------------------
        //POSITIVE/NEGATIVE SIGN.
        //----------------------
        /// <summary>
        /// Gets sign from double.  Returns one of the CONSTANT sign values in class CNumber.  Ex: "POSITIVE", "NEGATIVE", "ZERO".
        /// </summary>
        /// <param name="_number"></param>
        /// <returns></returns>
        /// Tested: 2019-12-28
        public static string GetSignRR(double _number)
        {
            if (_number.IsZeroRR(5))
                return SIGN_ZERO;

            else if (_number < 0 )
                return SIGN_NEGATIVE;

            else return SIGN_POSITIVE;
        }



        /// <summary>
        /// Gets sign from decimal.  Returns one of the CONSTANT sign values in class CNumber.  Ex: "POSITIVE", "NEGATIVE", "ZERO".
        /// </summary>
        /// <param name="_number"></param>
        /// <returns></returns>
        /// Tested: 2019-12-28
        public static string GetSignRR(decimal _number)
        {
            if (_number.IsZeroRR(5))
                return SIGN_ZERO;

            else if (_number < 0 )
                return SIGN_NEGATIVE;

            else return SIGN_POSITIVE;
        }





        /// <summary>
        /// Gets sign from integer.  Returns one of the CONSTANT sign values in class CNumber.  Ex: "POSITIVE", "NEGATIVE", "ZERO".
        /// </summary>
        /// <param name="_number"></param>
        /// <returns></returns>
        /// Tested: 2019-12-28
        public static string GetSignRR(int _number)
        {
            if (_number == 0)
                return SIGN_ZERO;

            else if (_number < 0 )
                return SIGN_NEGATIVE;

            else return SIGN_POSITIVE;
        }

        /// <summary>
        /// Gets sign from long.  Returns one of the CONSTANT sign values in class CNumber.  Ex: "POSITIVE", "NEGATIVE", "ZERO".
        /// </summary>
        /// <param name="_number"></param>
        /// <returns></returns>
        /// Tested: 2019-12-28
        public static string GetSignRR(long _number)
        {
            if (_number == 0)
                return SIGN_ZERO;

            else if (_number < 0 )
                return SIGN_NEGATIVE;

            else return SIGN_POSITIVE;
        }



        //----------------------
        //CONSTANTS.
        //----------------------
        //For misc use.  Ex: If wanted to remember if original value was pos/neg.  
        public const string SIGN_NEGATIVE    = "NEGATIVE";
        public const string SIGN_POSITIVE    = "POSITIVE"; 
        public const string SIGN_ZERO        = "ZERO";








    }
}
