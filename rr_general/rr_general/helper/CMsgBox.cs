﻿using rr_general.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace class_helper_rr
{
    public class CMsgBox
    {
        /// <summary>
        /// More flexible MessageBox.  No need to use toString() on args.  
        /// </summary>
        /// <param name="msg">
        /// Text to show. 
        /// </param>
        /// <param name="copyToClipboard">
        /// Optional.  If want to copy text to clipboard.  Mainly for debugging.  
        /// </param>
        /// <remarks>
        /// Tested: 2023-08-12
        /// </remarks>
        /// <param name="msg"></param>
        static public void Show(object msg, bool copyToClipboard = false)
        {
            if (msg != null)
            {
                MessageBox.Show(msg.ToString());

                if (copyToClipboard) 
                    CClipboard.SetTextRR(msg.ToString());

            }

            
        }





    }
}
