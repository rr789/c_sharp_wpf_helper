﻿using rr_general.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace class_helper_rr
{
    public class CTime
    {
        /* Returns seconds based on string "HH:MM:SS" passed-in.
         * If error: Returns CGlobal.ErrorNumber_Int (ex: -999999999).
         * Example: CTime.getSecondsFromStringHHMMSS("20:10:01"); //Returns 72601 seconds (which are the seconds in 20hours + 10minutes + 01seconds).  
         * Tested: 2012-01-15 all passed.
         */
        public static int getSecondsFromStringHHMMSS(string timeString)
        {
            int returnValue = CGlobal.ErrorNumber_Int; //This default value (ex: -999999999) is used to detect errors.

            try
            {
                int hours = Int32.Parse(timeString.Substring(0, 2));
                int minutes = Int32.Parse(timeString.Substring(3, 2));
                int seconds = Int32.Parse(timeString.Substring(6, 2));
                returnValue = (hours * 3600) + (minutes * 60) + seconds;
            }
            catch (FormatException)
            {
                //Debug message: CMsgBox.Show("Error with CTime.getSecondsFromStringHHMMSS() method.  Could not convert this string: '" + timeString + "'.  Copied string to clipboard.");
                CClipboard.SetTextRR(timeString);
            }

            return returnValue;

        }



    }
}
