﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Specialized;
using System.Collections;
using System.Reflection;

namespace class_helper_rr
{

    /**
     * --------------------------------------------------------------
     * 2017-06-10.  
     * DATATABLE IS A WINFORMS TECHNOLOGY.  DATATABLE HAS NO "COLUMN TYPE".  
     * KEEPING IT SINCE CDatabaseQuery uses this for PullRecords.  OTHERWISE CAN DELETE.  
     * --------------------------------------------------------------
     */

    /* 
     * Class simplifies adding/deleting rows.  
     * Example:
            //Instantiate class (optional: can define new column names in parameters): 
                CDataTable classObj = new CDataTable("fieldname1", "fieldname2"); 
            //Optional: Another way to add columns to DataTable.
                classObj.addColumns("fieldname1", "fieldname2");
            //Optional: Change column type (must do this here before populating with data).  Fyi: addColumns() creates String type columns by default.    
                classObj.table.Columns["fieldname1"].DataType = typeof(double); //Make it a number column.  
            //Define field values for new record.  
                classObj.fieldNameValues.Add("fieldname1", "value1");
                classObj.fieldNameValues.Add("fieldname2", "value2");
            //Insert the record.
                classObj.addRow();//Fyi: fieldNameValues gets cleared automatically to set another row.  
            //Bind GridView to DataTable:
                dataGridViewObj.dataSourceRR(classObj); //My custom DGV class works closely with this class.  
     * Reviewed/Tested all code throughly 2012-03-11.   
    */
    public class CDataTable
    {
        //Custom Events.  Gets called before/after setting the DataTable.        
        public Action eventHandler_dTableBeforeSet = null;
        public Action eventHandler_dTableAfterSet = null;
        //_table & table are the same.  Need internal/external properties to prevent a StackOverFlow "circular reference" error (since we use Get/Set below). 
        private DataTable _table; //For internal use.   
        public DataTable table //For external use.  
        {
            get
            {
                return this._table;
            }
            set
            {
                if (this.eventHandler_dTableBeforeSet != null) { this.eventHandler_dTableBeforeSet(); }
                this._table = value;
                if (this.eventHandler_dTableAfterSet != null) { this.eventHandler_dTableAfterSet(); }
            }
        }

        //Used to define field names & values.  
        //Example: classObj.fieldNameValues.Add("field_name", "field_value");
        public OrderedDictionary fieldNameValues;


        //Constructor
        public CDataTable(params string[] columnNames)
        {
            //Create new DataTable.
            this._table = new DataTable();

            //Optional: Add column names if passed-in.  
            this.addColumns(columnNames);//No need to handle "nothing passed-in", works OK as-is.    

            //This is a reusable object.  
            this.fieldNameValues = new OrderedDictionary();

        }

        /* Adds columns to DataTable.
         * Example: classObj.addColumns("fieldname1", "fieldname2");
         * Tested: 2011-11-25 all passed.  
         */
        public void addColumns(params string[] columnNames) //Handles unlimited variables. 
        {
            foreach (string colName in columnNames)
            {
                DataColumn col = new DataColumn();
                col.ColumnName = colName;
                this._table.Columns.Add(col);
            }
        }


        /* Adds one record to DataTable.
         * Example:
                //Define field values for new record.     
                    classObj.fieldNameValues.Add("fieldname1", "value1");
                    classObj.fieldNameValues.Add("fieldname2", "value2");
                //Insert the record.
                    classObj.addRow();//Fyi: fieldNameValues object gets cleared automatically to set another row.  
         * Tested: 2011-11-25 all passed.
         */
        public Action eventHandler_AddRow; //Triggered after row is added (optional).  
        public void addRow()
        {
            //Loop thru the dictionary object & add fieldnames/values to row.  
            IDictionaryEnumerator myEnumerator = this.fieldNameValues.GetEnumerator(); //Need this for the loop.  See http://sources68.com/use-idictionaryenumerator-to-loop-through-ordereddictionary-10e72b6c.html
            DataRow row = this._table.NewRow();
            while (myEnumerator.MoveNext())
            {
                row[myEnumerator.Key.ToString()] = myEnumerator.Value.ToString();
            }

            this._table.Rows.Add(row); //Attach row to DataTable.  
            if (this.eventHandler_AddRow != null) { this.eventHandler_AddRow(); } //Call event handler.  Keep above fieldNameValues.Clear() so we can access them.
            this.fieldNameValues.Clear(); //Convenience: For next row definition.    

        }



        /* Returns count of records in DataTable (returned from query on it).
         * Example: classObj.countRecordsQuery("ColumnName = 'value to find'"); //Returns count of rows returned from that query.  See http://msdn.microsoft.com/en-us/library/det4aw50.aspx for example queries.  
         * Fyi: Queries the DataTable object, doesn't send a query to an external database (c# is very nice).  
         * Tested: 2011-12-04 all passed.
         */
        public int countRecordsQuery(string queryString)
        {
            DataRow[] foundRows; //Stores rows

            // Use the Select method to query the DataTable object.  
            foundRows = this._table.Select(queryString);

            return foundRows.Count();

        }



        /* Converts LINQ query resultset into a DataTable type object.  
         * Helps when attaching a LINQ resultset to a DataGridView (since DataGridView.DataSource requires a DataTable type).  
         * Fyi: 
         *      LINQ returns an anonymous datatype (the structure is similar to DataTable but it is not a "true" DataTable type object).  
         *      Also converts column names defined in the LINQ query.  This is very powerful.  
         * Example: 
             * Do LINQ query to get result recordset (as anonymous data type).  
         *     //Attach LINQ recordset to DataGridView.
             * myDataGridView.DataSource = CDataTable.LINQToDataTable(LINQ_Returned_Recordset);
         * From here: http://www.c-sharpcorner.com/uploadfile/VIMAL.LAKHERA/convert-a-linq-query-resultset-to-a-datatable/
         * Tested: 2011-12-11 all passed.
         */
        public static DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            // column names 
            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                // Use reflection to get property names, to create table, Only first time, others will follow 
                if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }


        /* Updates this class's Internal DataTable with values from an External Foreign Table.  Has option to delete Internal DataTable rows that do not exist in Foreign Table.  
         * Updates all column values on the matching row(s).  
         * Converts imported values to the Internal DataTable's Datatype.  Very nice especially when merging with a LINQ resultset (since LINQ query resultsets are always an Anonymous/String data type).   
         * Fyi: Very flexible method.  Can use to update a whole table OR just a single row.  
         * Example:
         *      this.classObj.mergeTables(foreignDataTable, "PrimaryKeyColumnNameThatExistsInBothTables", true); //"true" means "delete values in Internal Datatable that do not exist in Foreign Table".  False leaves non-matching Internal Datatable rows untouched.  
         * Tested: 2012-01-08 all passed.  Also tested conversion for Double, Integer, DateTime types (all converted ok from a LINQ query with an anonymous type resultset).   
         */
        public void mergeTables(DataTable foreignTable, string primaryKeyColumnName, bool deleteRowsThatDoNotExistInForeignTable)
        {
            //Loop thru foreign table.  
            foreach (DataRow foreignTableRow in foreignTable.Rows)
            {
                //We look for this value in the internal table.  
                var pkForeignValueToFind = foreignTableRow[primaryKeyColumnName];

                //Add row to internal table if foreign row does not exist.  
                if (this.countRecordsQuery(primaryKeyColumnName + "= '" + pkForeignValueToFind + "'") == 0) //Value Type does not matter for this Select Query OK.
                {
                    //Just insert a row with primary key value only (the other column values for this row get updated below).  
                    //Fyi: It's ok if this value is an Anonymous string type (say from a LINQ query).  All column values of the row get overwritten in section below.  
                    this.fieldNameValues.Add(primaryKeyColumnName, pkForeignValueToFind);
                    this.addRow();
                }

                //Subloop thru the internal table.  
                foreach (DataRow dTableRow in this._table.Rows)
                {
                    var pkDTableValue = dTableRow[primaryKeyColumnName];

                    //If the Primary Key value matches, then update all column values for the row.   
                    if (pkDTableValue.ToString() == pkForeignValueToFind.ToString())//toString() allows comparing different datatypes OK (ex: Double number vs String).  Ran into an issue with different column datatypes being skipped here (this fixes it OK).         
                    {
                        foreach (DataColumn dTableColumn in this._table.Columns)
                        {
                            switch (dTableColumn.DataType.ToString())
                            {
                                case "System.Decimal":
                                    dTableRow[dTableColumn.ColumnName] = CStringNumber.stringToDecimal(foreignTableRow[dTableColumn.ColumnName].ToString()); 
                                    break;

                                case "System.Double":
                                    dTableRow[dTableColumn.ColumnName] = CStringNumber.stringToDouble(foreignTableRow[dTableColumn.ColumnName].ToString()); //Works for both Double & Integer types.
                                    break;

                                case "System.Int32":
                                    dTableRow[dTableColumn.ColumnName] = CStringNumber.stringToDouble(foreignTableRow[dTableColumn.ColumnName].ToString()); //Works for both Double & Integer types.  
                                    break;

                                case "System.DateTime":
                                    dTableRow[dTableColumn.ColumnName] = DateTime.Parse(foreignTableRow[dTableColumn.ColumnName].ToString()); //This parses any string without needing an input mask.  http://msdn.microsoft.com/en-us/library/1k1skd40.aspx
                                    break;

                                default:
                                    dTableRow[dTableColumn.ColumnName] = foreignTableRow[dTableColumn.ColumnName]; //Will return a string type value by default for anonymous types (I tested this).
                                    break;
                            }


                        }
                    }
                }
            }


            //Delete Internal Table rows that do not exist in the Foreign Table.  
            if (deleteRowsThatDoNotExistInForeignTable)
            {
                //Loop backwards since deleting.  
                for (int i = this._table.Rows.Count - 1; i >= 0; i--)
                {
                    var tempPkValue = this._table.Rows[i][primaryKeyColumnName];

                    DataRow[] foundRows; //Stores rows
                    foundRows = foreignTable.Select(primaryKeyColumnName + "= '" + tempPkValue + "'"); //Value Type does not matter for this Select Query OK. 
                    if (foundRows.Count() == 0)
                    {
                        this._table.Rows[i].Delete();
                    }
                }
            }

        }


        /* Deletes rows where Column Value = Value Passed-in.  
         * Example: 
         *      this.classObj.deleteRowsWhereFieldValueEquals("fieldname", "valueToFind"); //Deletes all rows in table where fieldname equals "valueToFind".  
         * Tested: 2012-01-21 all passed
         */
        public void deleteRowsWhereFieldValueEquals(string columnName, string valueToFind)
        {
            for (int i = this._table.Rows.Count - 1; i >= 0; i--)
            {
                if (this._table.Rows[i][columnName].ToString() == valueToFind) //Convert to string so any column type will match OK.    
                {
                    //Delete row. 
                    this._table.Rows[i].Delete();
                }
            }
        }



        /* Fast way to delete all rows where field value = max value.    
         * Example:
         *      this.classObj.deleteRowsEqualingMaxFieldValue("fieldname"); //Deletes all rows where fieldname column value = max value;
         * Tested: 2012-01-21 all passed.
         */
        public void deleteRowsEqualingMaxFieldValue(string columnName)
        {
            //Get max field value.  
            DataRow[] row = this._table.Select(columnName + " = Max(" + columnName + ")");
            string maxValue = row[0][columnName].ToString(); //Casting to string is ok.  The deleteRows...() method below does a generic search with a string value (works with all datatypes ok).  

            this.deleteRowsWhereFieldValueEquals(columnName, maxValue);
        }





    }
}

