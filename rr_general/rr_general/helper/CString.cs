﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace class_helper_rr
{
    public class CString
    {
        /// <summary>
        /// Returns true if haystack contains needle.
        /// Handles null values OK.
        /// </summary>
        /// <example>
        /// var haystack = "hello there";
        /// var needle = "THE";
        /// CMsgBox.Show(CString.Contains(haystack, needle, true)); //Returns False.
        /// CMsgBox.Show(CString.Contains(haystack, needle, false)); //Returns True.
        /// </example>
        /// <param name="haystack">String to search.</param>
        /// <param name="needle">String to find.</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <returns></returns>
        /// Tested: 2017-05-07 all passed.
        public static bool Contains(string haystack, string needle, bool caseSensitive = false) 
        {
            bool r = false;

            if (haystack != null && needle != null)
            {
                if (caseSensitive)
                {
                    r = haystack.Contains(needle);
                }
                else
                {
                    r = haystack.ToLower().Contains(needle.ToLower());
                }
            }

            return r;
        }

        /// <summary>
        /// Returns true if haystack starts with needle. False if not (or null haystack).   
        /// </summary>
        /// <example>
        /// var haystack = "hello there";
        /// var needle = "HEL";
        /// CMsgBox.Show(CString.StartsWith(haystack, needle, true)); //Returns False.
        /// CMsgBox.Show(CString.StartsWith(haystack, needle, false)); //Returns True.
        /// </example>
        /// <param name="haystack">String to search.</param>
        /// <param name="needle">String to find.</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <returns></returns>
        /// Tested: 2017-05-07 all passed.
        public static bool StartsWith(string haystack, string needle, bool caseSensitive = false)
        {
            bool r = false;

            if (haystack != null)
            {
                if (caseSensitive)
                {
                    r = haystack.StartsWith(needle);
                }
                else
                {
                    r = haystack.ToLower().StartsWith(needle.ToLower());
                }
            }

            return r;
        }


        /// <summary>
        /// Returns true if haystack ends with needle. False if not (or null haystack).   
        /// </summary>
        /// <example>
        /// var haystack = "hello there";
        /// var needle = "THERE";
        /// CMsgBox.Show(CString.EndsWith(haystack, needle, true)); //Returns False.
        /// CMsgBox.Show(CString.EndsWith(haystack, needle, false)); //Returns True.
        /// </example>
        /// <param name="haystack">String to search.</param>
        /// <param name="needle">String to find.</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <returns></returns>
        /// Tested: 2017-07-30 all passed.
        public static bool EndsWith(string haystack, string needle, bool caseSensitive = false)
        {
            bool r = false;

            if (haystack != null)
            {
                if (caseSensitive)
                    r = haystack.EndsWith(needle);
                else
                    r = haystack.ToLower().EndsWith(needle.ToLower());
            }

            return r;
        }


        


        /// <summary>
        /// Removes starting string at left-hand side if any needle(s) match.  
        /// </summary>
        /// <example>
        /// CMsgBox.Show(CString.removeLeft("aabc", new []{"aa"}, true));   //Returns "bc"
        /// CMsgBox.Show(CString.removeLeft("aabc", new []{"a","a"}, true));   //Returns "abc" (just one "a" removed).
        /// CMsgBox.Show(CString.removeLeft("aabc", new []{"AA"}) );        //Returns "bc"
        /// </example>
        /// <param name="haystack_">String to search.</param>
        /// <param name="needleArr">String(s) to find.  Use new []{"a","a"}</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <returns>String with needle removed, or haystack if needle not found.  </returns>
        /// Tested: 2017-06-11
        public static string removeLeft(string haystack_, string[] needleArr, bool caseSensitive = false)
        {
            //Safety.
            if (haystack_ == null || needleArr == null) return null;

            string r = haystack_; //Original haystack returned if no match.  
            string haystack = haystack_;

            //Loop thru parameter array & remove all the substrings. 
            for (int i = 0; i < needleArr.Length; i++)
            {
                string needle = needleArr[i];

                if (caseSensitive == false)
                {
                    haystack = haystack.ToUpper();
                    needle = needle.ToUpper();
                }

                if (haystack.StartsWith(needle))
                {
                    r = haystack_.Substring(needle.Length); //Use original haystack to keep original case returned.  
                }

            }

            return r;
        }


        /// <summary>
        /// Removes ending string at right-hand side if any needle(s) match.  
        /// </summary>
        /// <example>
        /// CMsgBox.Show(CString.removeRight("aabc", new[] { "bc" }) );        //Returns "aa"
        /// CMsgBox.Show(CString.removeRight("aabc", new[] { "BC" },true));   //Returns "aabc"
        /// </example>
        /// <param name="haystack_">String to search.</param>
        /// <param name="needle_">String(s) to find.  Use new []{"a","a"}</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <returns>String with needle removed, or haystack if needle not found.  </returns>
        /// Tested: 2017-06-11
        public static string removeRight(string haystack_, string[] needleArr, bool caseSensitive = false)
        {
            //Safety.
            if (haystack_ == null || needleArr == null) return null;

            string r = haystack_; //Original haystack returned if no match.  
            string haystack = haystack_;

            //Loop thru parameter array & remove all the substrings. 
            for (int i = 0; i < needleArr.Length; i++)
            {
                string needle = needleArr[i];

                if (caseSensitive == false)
                {
                    haystack = haystack.ToUpper();
                    needle = needle.ToUpper();
                }

                if (haystack.EndsWith(needle))
                {
                    r = haystack_.Substring(0, haystack_.Length - needle.Length); //Use original haystack to keep original case returned. 
                }

            }

            return r;
        }


        /// <summary>
        /// Used to test if string CONTAINS, or STARTS WITH.  
        /// </summary>
        /// <example>
        /// var haystack = "hello there sammy";
        /// var needle = "the";
        /// CMsgBox.Show(CString.MatchesCriteria(haystack, needle, CString.CONTAINS, false)); //Returns true.
        /// CMsgBox.Show(CString.MatchesCriteria(haystack, needle, CString.STARTS_WITH, false));//Returns false.
        /// </example>
        /// <param name="haystack"></param>
        /// <param name="needle"></param>
        /// <param name="optionConstant"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        public static bool MatchesCriteria(string haystack, string needle, string optionConstant, bool caseSensitive = false)
        {
            bool r = false;

            switch (optionConstant)
            {
                case CONTAINS:
                    r = Contains(haystack, needle, caseSensitive);
                    break;

                case STARTS_WITH:
                    r = StartsWith(haystack, needle, caseSensitive);
                    break;
            }

            return r;

        }




        /// <summary>
        /// Returns true if haystack contains ANY needle.
        /// Handles null OK. 
        /// </summary>
        /// <example>
        /// var haystack = "hello there sammy";
        /// var needles = new List<string> { "zzz", "sam" };
        /// CMsgBox.Show(CString.ContainsAny(haystack, needles,  true)); //Returns True.
        /// </example>
        /// <param name="haystack">String to search.</param>
        /// <param name="needles">Strings to find.</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <returns></returns>
        /// Tested: 2017-05-07 all passed.
        public static bool ContainsAny(string haystack, List<string> needles, bool caseSensitive = false)
        {
            bool r = false;

                for (int i = 0; i < needles.Count; i++)
                {
                    if (needles != null)
                        if (Contains(haystack, needles[i], caseSensitive))
                        {
                            r = true;
                            break;
                        }
                }

            return r;
        }


        /// <summary>
        /// Determines if at least one needle in list equals haystack. 
        /// </summary>
        /// <example>
        ///     CString.EqualsAny("TEST_B", new List<string>() { "Test_A", "Test_B", "Test_C" }); //Returns true.  Not case-sensitive. 
        ///     CString.EqualsAny("TEST_B", new List<string>() { "Test_A", "Test_B", "Test_C" }, true); //Returns false.  Case sensitive.  
        /// </example>
        /// <param name="haystack">String to match.</param>
        /// <param name="needles">List of candidate strings to try and match.</param>
        /// <param name="caseSensitive">Default false.</param>
        /// <returns></returns>
        /// Tested: 2019-09-13
        public static bool EqualsAny(string haystack, List<string>needles, bool caseSensitive = false)
        {
            //Safety: For the heck of it.
            if (haystack == null || needles == null)
                return false;
            
            for (int i=0; i<needles.Count; i++)
            {
                string needleSingle = needles[i];
                
                if (caseSensitive)
                {
                    if (haystack == needleSingle)
                        return true;
                }
                else
                {
                    if (haystack.ToUpper() == needleSingle.ToUpper())
                        return true;
                }
            }

            return false;
        }



        /// <summary>
        /// (Overload alternative method).  Determines if at least one needle in list equals haystack. Case in-sensitive.
        /// </summary>
        /// <example>
        ///     CString.EqualsAny("TEST_C", "Test_A", "Test_B", "Test_C"); //Returns true.
        ///     CString.EqualsAny("TEST_C", "Z", "ZZ", "ZZZ"); //Returns false. 
        /// </example>
        /// <param name="haystack">String to match.</param>
        /// <param name="needles">List of candidate strings to try and match.</param>
        /// <returns></returns>
        /// Tested: 2019-09-13
        public static bool EqualsAny(string haystack, params string[] needles)
        {
            //Safety: For the heck of it.
            if (haystack == null || needles == null)
                return false;

            for (int i = 0; i < needles.Length; i++)
            {
                string needleSingle = needles[i];

                if (haystack.ToUpper() == needleSingle.ToUpper())
                    return true;
            }

            return false;
        }







        /// <summary>
        /// Formats any type of value to the specified format. 
        /// Just simplifies string.Format() so don't have to use the "{0:" before the format string. 
        /// </summary>
        /// <example>
        /// CMsgBox.Show(CString.Format("#,##0.00", 1234)); //Returns "1,234.00".
        /// CMsgBox.Show(CString.Format("MM/dd/yyyy", DateTime.Now)); //Returns "01/21/2012".
        /// </example>
        /// <param name="formatString"></param>
        /// <param name="arg0"></param>
        /// <returns></returns>
        /// Tested: 2012-01-21 all passed.
        public static string Format(string formatString, object arg0) //arg0 means "any type of value".
        {
            
            //Safety: For heck of it.  
            if (arg0 == null)
                return "";
      
            return string.Format("{0:" + formatString + "}", arg0); //"{0:" specifies the first element in the object (array) to format.  We're only passing one value so it's always zero.  See http://www.csharp-examples.net/string-format-datetime/
        }


        /// <summary>
        /// Easy way to remove unlimited strings from a string. Case sensitive.
        /// </summary>
        /// <example>
        /// string str = "a b c d";
        /// str = CString.RemoveStrings(str, "b", "d"); //Remove "b" & "d" from string.  
        /// CMsgBox.Show(str); //Shows "a c"
        /// </example>
        /// <param name="haystack"></param>
        /// <param name="stringsToRemove"></param>
        /// <returns></returns>
        /// Tested: 2012-01-30 all passed.
        public static string RemoveStrings(string haystack, params string[] stringsToRemove)
        {
            string r = haystack;

            if (haystack != null)
            {
                //Loop thru parameter array & remove all the substrings. 
                for (int i = 0; i < stringsToRemove.Length; i++)
                {
                    r = r.Replace(stringsToRemove[i], "");
                }
            }
            return r;

        }


        /// <summary>
        /// Converts string into list.  Uses array of delimiters to split string.  Handles null OK.
        /// </summary>
        /// <example>
        /// var haystack = "hello there sammy";
        /// var delimiters = new List<string> { " ", "  ", ",", ".", ":" };
        /// var myList = CString.ParseWithDelimiters(haystack, delimiters);
        /// foreach (var a in myList) { CMsgBox.Show(a); }
        /// CMsgBox.Show(myList.Count);
        /// </example>
        /// <param name="haystack"></param>
        /// <param name="delimiters"></param>
        /// <returns></returns>
        /// Tested: 2017-05-07 all passed.
        public static List<string> ParseWithDelimiters(string haystack, List<string> delimiters)
        {
            List<string> r = null; //Returned.

            if (haystack != null && delimiters != null)
            {

                //Builds regex pattern for below.  From: http://stackoverflow.com/a/2485044/722945
                string delimiterPattern = "(" + String.Join("|", delimiters.Select(d => Regex.Escape(d))
                                                                  .ToArray())
                                        + ")";

                r = Regex.Split(haystack, delimiterPattern).ToList<string>();

                //Remove the delimiters entries that get included in the array when split.  Cleanup...
                for (int i = 0; i < r.Count; i++)
                {
                    if (delimiters.Contains(r[i]))
                    {
                        r.RemoveAt(i);
                    }
                }

            }

            return r;
        }





        public const string CONTAINS = "CONTAINS";
        public const string STARTS_WITH = "STARTS_WITH";

        /// <summary>
        /// Parses both haystack and needle into arrays of words.  Returns true if all needles exist in haystack.  
        /// CONTAINS and STARTS_WITH.  
        /// </summary>
        /// <example>
        /// var haystack = "hello there there sammy";
        /// var delimiters = new List<string> { " ", "  ", ",", ".", ":" };
        /// 
        /// var needle = "THE SAM";
        /// CMsgBox.Show(CString.HaystackMatchMultipleNeedles(haystack, needle, delimiters, CString.STARTS_WITH, false)); //Returns True (case insensitive).  Matched THE and SAM. 
        /// CMsgBox.Show(CString.HaystackMatchMultipleNeedles(haystack, needle, delimiters, CString.STARTS_WITH, true)); //Returns False (case sensitive).
        /// 
        /// var needle = "ere mmy";
        /// CMsgBox.Show(CString.HaystackMatchMultipleNeedles(haystack, needle, delimiters, CString.STARTS_WITH, true)); //Returns False (zero needle matches since "STARTS_WITH").
        /// CMsgBox.Show(CString.HaystackMatchMultipleNeedles(haystack, needle, delimiters, CString.CONTAINS, true)); //Returns True (since "CONTAINS").
        /// </example>
        /// <param name="haystack">String to search.</param>
        /// <param name="needle">Strings to find.  Pass-in as complete single string.  This method will parse it.</param>
        /// <param name="delimiters">Used to break up the haystack and needle strings.</param>
        /// <param name="optionConstant">Use CString.CONTAINS or CString.STARTS_WITH. </param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <returns></returns>
        /// Tested: 2017-05-07 all passed.
        public static bool HaystackMatchMultipleNeedles(string haystack, string needle, List<string> delimiters, string optionConstant, bool caseSensitive = false)
        {
            bool r = false;

            var needleWords = CString.ParseWithDelimiters(needle, delimiters);
            var haystackWords = CString.ParseWithDelimiters(haystack, delimiters);

            int countNeedleWords = needleWords.Count();
            int countMatches = 0;

            //Safety if null. 
            if (haystack == null || needle == null) { return false; }

            //If needle value exists in haystack array, then increment count +1.  
            foreach (string needleWord in needleWords)
            {
                foreach (string haystackWord in haystackWords)
                {
                    if (optionConstant == STARTS_WITH)
                    {
                        if (StartsWith(haystackWord, needleWord, caseSensitive))
                        {
                            countMatches++;
                            break;
                        }
                    }
                    else if (optionConstant == CONTAINS)
                    {
                        if (Contains(haystackWord, needleWord, caseSensitive))
                        {
                            countMatches++;
                            break;
                        }
                    }
                }
                
            }


            if (countNeedleWords == countMatches) { r = true; }

            return r;

        }




        /// <summary>
        /// Converts any object into a properly formatted string.  
        /// Also used in AX Dynamics X++ to convert all CLR object types into a string (especially date).  
        /// </summary>
        /// <param name="a">Any object</param>
        /// <example>
        /// CMsgBox.Show(CString.ToString(DateTime.Now)); //Returns date in YYYY-MM-DD HH:MM:SS format.  
        /// </example>
        /// <returns>
        /// Other types are converted to strings as-is.  
        /// </returns>
        /// Tested: 2019-08-31
        public static string ToString(object a)
        {
            string r = "";

            //C# has no "Date" property.  So have to treat all as DateTime.  
            if (a is DateTime)
                r = ((DateTime)a).ToString("yyyy-MM-dd HH:mm:ss");

            else if (a is int || a is Int32 || a is Int64 || a is long || a is double || a is decimal)
                r = a.ToString();

            else if (a is string)
                r = a as string;
            else
            {
                CTryCatchWrapper.wrap(delegate()
                {
                    r = a.ToString();
                });
            }

            return r;
        }



        /// <summary>
        /// Returns the position of a substring. 
        /// </summary>
        /// <param name="haystack">String to search in. </param>
        /// <param name="needle">String to find.</param>
        /// <param name="occurence">Occurance of needle.  Starts at 1. </param>
        /// <param name="caseSensitive">False is default (not case sensitive).</param>
        /// <example>
        /// CMsgBox.Show(CString.SubstringPositionAtOccurence("hi hi there", "hi", 1)); //Returns 0 (start of string, OK).
        /// CMsgBox.Show(CString.SubstringPositionAtOccurence("hi hi there", "hi", 2)); //Returns 3.
        /// CMsgBox.Show(CString.SubstringPositionAtOccurence("hi hi there", "hi", 3)); //Returns -1 (not found, OK).
        /// </example>
        /// <returns>Position of string (starting at zero).  -1 if not found. </returns>
        /// Tested: 2018-09-11
        public static int SubstringPositionAtOccurence(string haystack, string needle, int occurence, bool caseSensitive = false)
        {
            int r = -1; //-1 means not found.  

            //Ongoing counter keeps increasing with each iteration.  
            int tempPosition = 0;

            string haystack_, needle_;

            if (caseSensitive)
            {
                haystack_ = haystack;
                needle_   = needle;
            }
            else
            {
                haystack_ = haystack.ToUpper();
                needle_   = needle.ToUpper();
            }

            //i is essentially the occurance counter.  
            for (var i=0; i<haystack_.Length; i++)
            {
                //Each iteration starts searching from last found position.  
                tempPosition = haystack_.IndexOf(needle_, tempPosition);

                if (tempPosition >= 0)
                {
                    if ((i + 1) == occurence)
                    {
                        r = tempPosition;
                        break;
                    }
                }
                else
                    break;

                tempPosition++;
            }

            return r;
        }


        /// <summary>
        ///     Removes both types of linefeeds "\r\n" and "\n".  
        /// </summary>
        /// <example>
        ///     string r = "line 1 \r" + "line2"; //Added a linefeed.  
        ///     r = CString.RemoveLinefeeds(r);  
        /// </example>
        /// <param name="str">String to remove linefeeds.</param>
        /// <param name="replaceWith">Optional.  Enter string to replace linefeeds with.  Ex: Period "." or a couple spaces "  ".</param>
        /// Tested: 2019-04-06
        public static string RemoveLinefeeds(string str, string replaceWith = "")
        {
            string r = str;

            //Safety: Regex.Replace will error out if NULL is passed-in.  Regex.Replace can handle empty strings "".  
            if (r == null)
                r = "";

            //From: https://stackoverflow.com/a/8196219/722945
            r = Regex.Replace(r, @"\r\n?|\n", replaceWith);

            return r;

        }



        /// <summary>
        /// Converts string to date.  Handles various date formats.  Meant to act as a "universal" converter.  
        /// </summary>
        /// <example>
        ///     CMsgBox.Show(CString.ToDate("2010-01-10").ToString("yyyy-MM-dd") );           //Returns date as "2010-01-10".
        ///     CMsgBox.Show(CString.ToDate("2010/01/10").ToString("yyyy-MM-dd") );           //Returns date as "2010-01-10".
        ///     CMsgBox.Show(CString.ToDate("1/2/2010").ToString("yyyy-MM-dd") );             //Returns date as "2010-01-02".
        ///     CMsgBox.Show(CString.ToDate("1/10/2010").ToString("yyyy-MM-dd") );            //Returns date as "2010-01-10".  
        ///     CMsgBox.Show(CString.ToDate("01/10/2010").ToString("yyyy-MM-dd") );           //Returns date as "2010-01-10".
        ///            
        ///     CMsgBox.Show(CString.ToDate("2-JAN-10").ToString("yyyy-MM-dd") );             //Returns date as "2010-01-02".
        ///     CMsgBox.Show(CString.ToDate("02-JAN-10").ToString("yyyy-MM-dd") );            //Returns date as "2010-01-02".
        ///     CMsgBox.Show(CString.ToDate("02-JAN-2010").ToString("yyyy-MM-dd") );          //Returns date as "2010-01-02".
        ///     CMsgBox.Show(CString.ToDate("02-JANUARY-10").ToString("yyyy-MM-dd") );        //Returns date as "2010-01-02".
        ///     CMsgBox.Show(CString.ToDate("02-JANUARY-2010").ToString("yyyy-MM-dd") );      //Returns date as "2010-01-02".
        ///     
        ///     //Can handle two digit "year" at very end.  
        ///     CMsgBox.Show(CString.ToDate("1/2/10").ToString("yyyy-MM-dd") );                //Returns date as "2010-01-02".
        ///     CMsgBox.Show(CString.ToDate("1/02/10").ToString("yyyy-MM-dd") );               //Returns date as "2010-01-02".
        ///     CMsgBox.Show(CString.ToDate("01/02/10").ToString("yyyy-MM-dd") );              //Returns date as "2010-01-02".
        /// </example>
        /// <remarks>
        ///     Modified from: https://social.msdn.microsoft.com/Forums/vstudio/en-US/85f3b876-7e8d-4510-aef9-f49c974cb807/convert-different-formats-of-dates-in-ddmmyyyy-format-in-c?forum=csharpgeneral
        /// </remarks>
        /// <param name="str">String in any date format.  </param>
        /// Tested: 2019-07-18
        public static DateTime ToDate(string str)
        {
            DateTime d;

            string str_ = str;

            //Remove the "time" portion of the string.  
            //Fyi: The formats in next section below could handle time, but we don't want to assume we will always use a 12 or 24 hour format.
            str_ = CString.ExtractDateSubstring(str_);

            //C# is picky about year and month.  However one "d" for day will handle one or two characters.  I tested.   
            string[] formats = { "yyyy-MM-dd", "yyyy/MM/dd", "M/d/yyyy",  "MM/d/yyyy", "d-MMM-yy", "d-MMM-yyyy",  "d-MMMM-yy", "d-MMMM-yyyy", "M/d/yy"};
            DateTime.TryParseExact(str_, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out d);

            return d;
        }


        /// <summary>
        /// Removes "time" portion from a date/time string.  Returns only the "date" portion of the string. 
        /// </summary>
        /// <example>
        ///     CMsgBox.Show(ExtractDateSubstring("2019-05-31T00:00:00")); //Returns "2019-05-31".
        ///     CMsgBox.Show(ExtractDateSubstring("7/13/2019 3:16:34 PM")); //Returns "7/13/2019".
        /// </example>
        /// <param name="str"></param>
        /// <returns></returns>
        /// Tested: 2019-07-16
        public static string ExtractDateSubstring(string str)
        {
            int endPosition;

            if (str == "")
                return null;

            //--------------------
            //First attempt, if string has "T" to start the time portion of string.  Ex: "2019-05-31T00:00:00".
            //--------------------
            endPosition = CString.SubstringPositionAtOccurence(str, "T", 1);

            if (endPosition > 0)
                return str.Substring(0, endPosition);

            //--------------------
            //Second attempt, if string has space " " to start the time portion of string.  Ex: "7/13/2019 3:16:34 PM".
            //--------------------
            endPosition = CString.SubstringPositionAtOccurence(str, " ", 1);

            if (endPosition > 0)
                return str.Substring(0, endPosition);

            //Default.  Return original string since it had no time separator.  
            return str;

        }



        /// <summary>
        /// Just busts up a comma separated string into a typed List. 
        /// </summary>
        /// <example>
        /// string s = "1,2,3";
        /// List<int> myList = CString.ParseStringCSV<int>(s);
        /// CMsgBox.Show(myList[1]); //Returns "2" as int. 
        /// </example>
        /// <param name="str">CSV string.</param>
        /// Tested: 2020-08-15
        public static List<T> ParseStringCSV<T>(string str)
        {
            //Safety
            if (!str.HasValueRR())
                return null;
            
            //Get array of strings.  
            var arr = str.Split(',');
            
            //Convert array to typed list.  
            //Fyi: Typical use will convert to basic types like string, int, decimal, double.  
            List<T> r = new List<T>();

            arr.EachRR(p =>
            {
                r.Add(CType.ConvertType<T>(p));
            });

            return r;
        }


    }
}
