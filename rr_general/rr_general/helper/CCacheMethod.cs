﻿using class_helper_rr;
using FirebirdSql.Data.FirebirdClient;
using Newtonsoft.Json;
using rr_general.data_contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace rr_general.helper
{
    /// <summary>
    /// Handles caching returned method data to disk.  Geared toward caching methods that return large recordsets.
    /// Uses Firebird db to store results.  
    /// Note: 
    ///     This class implements a singleton pattern so all cache operations get pushed thru a single Firebird instance.  
    ///     Only use on methods that have no parameters, or where the parameters passed-in do not change.  This cachine class uses identifiers CacheKey01/02 to find cached record.  CacheKey01/02 are set at same value regardless of params passed-into cached method.  Ex: Method like PopulateAllRecords("all companies") is perfect choice since params do not change.  
    /// </summary>
    /// <example>
    /// Pretend expensive method.    
    /// public string ExpensiveMethod(string a)
    /// {
    /// 	Thread.Sleep(5000);
    /// 	return "hi " + a;
    /// }
    /// 
    /// //Step 01: Optional.  Set additional lookup key set at class level.  Say dev/prod server so we keep records separate.  Typically set when app starts.  
    /// CCacheMethod.InstanceRR.CacheKey02 = "PROD SERVER ADDRESS";
    /// 
    /// //Step 02: Call method with wrapper.  
    /// //  Note that CacheKey01 is required, and passed-into the method below.  Value passed is typically "class name + method name".  
    /// //  No need to worry about setting-up a Firebird db.  This class creates the cache lookup table automatically.     
    /// var cachedResult = CCacheMethod.InstanceRR.CallMethodRR(() => ExpensiveMethod("sample value"), "MyClass->ExpensiveMethod", 24);
    /// </example>
    /// <remarks>
    /// Wrapper class from: https://stackoverflow.com/a/53339109/722945    
    /// </remarks>
    /// TODO: 
    ///     Handle params passed into cached method being called.  Then I could remove the note above about "only use on methods with no parameters".
    ///     Add a janitor cleanup utility to cleanup old cached JSON files that are not in db.  Mainly for when we upgrade cache library to new version (since prior version cached JSON files will never get deleted).  Basically, start from cached directory, then ensure file exists in Firebird db.  If not exist in Firebird db then delete from local disk.   
    public class CCacheMethod
    {
        //-------------------------
        //PROPERTIES:
        //-------------------------
        public string DirectoryPathDb           = CDirectory.GetDirLocal_StartupProject() + @"\CacheMethod";
        //Increment version when structural changes are made to this class (OR) we update to a new Firebird version. 
        public string DbName                    = "CACHE_DATABASE_VERSION_02.fdb";  //2023-09-20 Updated name to "version 02" for new Firebird db 4.0.3.  4.0.3 engine cannot open 3.0 db files.  
        public string FullPathDb;
        public string DirectoryPathJsonFiles    = CDirectory.GetDirLocal_StartupProject() + @"\CacheMethod\CachedJsonObject";
        public CDatabaseConnection DbConnection;

        /// <summary>
        /// Optional:  Provides a secondary look-up key.  Set at class level.  
        /// CallMethodRR() does not have a param for this.  However, it does reference this property.  
        /// </summary>
        /// <example>
        /// Set to dev/prod server address.  This will keep cached records for dev/prod separate.  
        /// </example>
        public string CacheKey02        = "";



        //-------------------------
        //SINGLETON SETUP:
        //-------------------------
        /// <summary>
        /// [Part 01 of Singleton setup.]
        /// Constructor. 
        /// Private for singleton so users can't instantiate on their own.  Preventing instantiation helps prevent stackoverflow error.  https://stackoverflow.com/q/1431810/722945
        /// </summary>
        private CCacheMethod()
        {
            FullPathDb = CDirectory.ConcatDirectoryAndFilePath(DirectoryPathDb, DbName);

            //OK to set this even if db does not exist.  We will create db file (if needed) below.  
            DbConnection = new CDatabaseConnection(
                      _serverHost: null //Always null for embedded.  
                    , _dbName: FullPathDb
                    , _username: "SYSDBA"
                    , _password: ""
                    , _dbType: CDatabaseConnection.DB_TYPE_FIREBIRD_EMBEDED_SERVER
                    , _port: null //No port with embedded server.
                    );

            //Create db if needed.  
            if (!File.Exists(FullPathDb))
            {
                FbConnection.CreateDatabase(DbConnection.conn_str);

                string sql = @"
                    CREATE TABLE MAIN_T (
                        REC_ID BIGINT GENERATED BY DEFAULT AS IDENTITY (START WITH 1) NOT NULL,
                        CACHE_KEY_01 VARCHAR(400) NOT NULL,
                        CACHE_KEY_02 VARCHAR(400) NOT NULL,
                        JSON_DATA_FILE_PATH VARCHAR(400) NOT NULL,
                        DATE_TIME_CREATED TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL)
                ";
                DbConnection.executeNonQuery(sql);

                //Need to execute as separate command to avoid error.  
                sql = @"ALTER TABLE MAIN_T ADD PRIMARY KEY (REC_ID);";
                DbConnection.executeNonQuery(sql);
            }

        }
        
        /// <summary>
        /// [Part 02 of Singleton setup.]
        /// Alternate constructor.  Called once the first time the static "Instance" object is accessed (note statically "accessed" not object "instantiated").
        /// Need to use this lambda technique since constructor above is private.  
        /// Remember that the "private" constructor will never get called since we never instantiate this class.  Instance object is static.   
        /// </summary>
        private static readonly Lazy<CCacheMethod> lazy = new Lazy<CCacheMethod>
        (
            () => new CCacheMethod()
        );
 
        /// <summary>
        /// [Part 03 of Singleton setup.]
        /// Singleton instance of this class. 
        /// Note this is a static property.  Static properties do remember their updated values. 
        /// </summary>
        public static CCacheMethod InstanceRR { get { return lazy.Value; } }


        //-------------------------
        //CUSTOM CLASS:
        //-------------------------
        /// <summary>
        /// Populates the re-usable cache. 
        /// </summary>
        /// <param name="showErrors"> Setting to false will suppress error message boxes from ExecuteQueryGeneric() call.</param>
        /// Tested: 2020-06-15
        public List<DC_CACHE_METHOD> GetAllRecords(bool showErrors = true)
        {
            string sql = @"
                SELECT 
	                 T.REC_ID
                    ,T.CACHE_KEY_01
                    ,T.CACHE_KEY_02
                    ,T.JSON_DATA_FILE_PATH
                    ,T.DATE_TIME_CREATED
                FROM 
                    MAIN_T	T 
                ORDER BY 
                    T.DATE_TIME_CREATED DESC
            ";

             return InstanceRR.DbConnection.ExecuteQueryGeneric<DC_CACHE_METHOD>(sql, showErrors);  
        }

        /// <summary>
        /// Saves cached method and data to hard drive.
        /// 1. Inserts record into db (that points to cached json file).  
        /// 2. Serializes result from called method, and stores serialized file to disk.  
        /// </summary>
        /// <param name="cacheKey01">Unique value to identify which cache to pull from.  Ex: Typical value is "Class name + method name".</param>
        /// <param name="objToSerialize">Pass any returned object in.  This function will serialize to json, then save json file to disk.  </param>
        /// Tested: 2020-06-16
        public void InsertRecordAndCreateJsonFile(string cacheKey01, object objToSerialize)
        {
            string cacheKeySanitized    = CFile.SanitizeFileName(cacheKey01);
            string fileName             = cacheKeySanitized + " " + Guid.NewGuid() + ".json";  
            string fullPathJsonFile     = CDirectory.ConcatDirectoryAndFilePath(DirectoryPathJsonFiles, fileName);
            
            //-------------------
            //Step 01: Save serialized obj to disk.  
            //-------------------
            CFile.SaveTextFile(fullPathJsonFile, objToSerialize.ToJsonRR(), CFile.SAVE_FILE_OVERWRITE);

            //-------------------
            //Step 02: Insert db record. 
            //-------------------
            var query = new CDatabaseQuery(InstanceRR.DbConnection); //connectionObject is from CDatabaseConnection.
            query.tableName = "MAIN_T";
            query.valueArr = new Dictionary<string, string>() {   //Or can use the std syntax: valueArr["fieldname"] = "value";
                { DC_CACHE_METHOD.C_CACHE_KEY_01, cacheKeySanitized },
                {DC_CACHE_METHOD.C_CACHE_KEY_02, CacheKey02 },
                { DC_CACHE_METHOD.C_JSON_DATA_FILE_PATH, fullPathJsonFile  }
                //The other columns REC_ID and DATE_TIME_CREATED automatically populate.
            };
            query.insertRecord(); //Returns "1" for inserting one row.  

        }

        /// <summary>
        /// Queries db for all records having cacheKey01 (and cacheKey02 set at class level). 
        /// Records ordered by newest to oldest.  
        /// </summary>
        /// <example>
        /// CCacheMethod.InstanceRR.GetRecordsByCacheKey("sample key")[0].JSON_DATA_FILE_PATH; //
        /// </example>
        /// <param name="cacheKey01">Unique value to identify which cache to pull from.  Ex: Typical value is "Class name + method name".</param>
        /// <param name="showErrors"> Setting to false will suppress error message boxes from ExecuteQueryGeneric() call.</param>
        /// Tested: 2020-06-16
        public List<DC_CACHE_METHOD> GetRecordsByCacheKey(string cacheKey01, bool showErrors = true)
        {
            //TODO: REPLACE THE SELECT METHOD WITH THE CDATABASE QUERY.  THEN NO NEED TO SANITZE.  
            string cacheKey01_Sanitized = CDatabaseQuery.sanitizeString(cacheKey01);
            cacheKey01_Sanitized = CFile.SanitizeFileName(cacheKey01_Sanitized); //Not really needed.  
            
            string sql = @"
                SELECT 
	                 T.REC_ID
                    ,T.CACHE_KEY_01
                    ,T.CACHE_KEY_02
                    ,T.JSON_DATA_FILE_PATH
                    ,T.DATE_TIME_CREATED
                FROM 
                    MAIN_T	T 
                WHERE 
                    1=1 
                    AND CACHE_KEY_01 = '" + cacheKey01_Sanitized + @" ' 
                    AND CACHE_KEY_02 = '" + CacheKey02 + @" '  
                ORDER BY 
                    T.DATE_TIME_CREATED DESC
            ";

            return InstanceRR.DbConnection.ExecuteQueryGeneric<DC_CACHE_METHOD>(sql, showErrors);  
        }





        /// <summary>
        /// Deletes records starting with oldest.  
        /// </summary>
        /// <example>
        /// CCacheMethod.InstanceRR.DeleteOldCacheRecord("sample key", 2); //Keep 2 records, delete the rest.  
        /// </example>
        /// <param name="cacheKey01">Unique value to identify which cache to pull from.  Ex: Typical value is "Class name + method name".</param>
        /// <param name="recordsToDelete"></param>
        /// Tested: 2020-06-16
        public void DeleteOldCacheRecord(string cacheKey01, int recordsToKeep)
        {
            var list_ = GetRecordsByCacheKey(cacheKey01);

            //Safety: 
            if (list_.CountRR() < recordsToKeep)
                return;

            for (int i=recordsToKeep; i<list_.CountRR(); i++)
            {
                var rec = list_[i];
                
                //Step 01: Delete from hard drive.
                File.Delete(rec.JSON_DATA_FILE_PATH);

                //Step 02: Delete from db.
                var query = new CDatabaseQuery(InstanceRR.DbConnection); //connectionObject is from CDatabaseConnection.
                query.tableName = "MAIN_T";
                query.criteriaArr = new Dictionary<string, string>() {
                    { "REC_ID", rec.REC_ID.ToString() }
                };
                query.deleteRecords(); 
            }
        }



        /// <summary>
        /// Gets most recent cached value for CacheKey01 (and CacheKey02 set at class level).  This is the value deserialized into original type.  
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey01">Unique value to identify which cache to pull from.  Ex: Typical value is "Class name + method name".</param>
        /// Tested: 2020-06-16
        public T GetCachedValue<T>(string cacheKey01)
        {
            var list_ = InstanceRR.GetRecordsByCacheKey(cacheKey01);
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(list_[0].JSON_DATA_FILE_PATH)); //GetRecordsByCacheKey() sorts newest record on top.  
        }



        /// <summary>
        /// ★★★ Main goal of class. ★★★
        /// Checks cache for saved value.  
        /// -   If no cache exists, then call method.  
        /// -   If cache has expired, then refresh cache in main thread.  App will wait until refreshed.      
        /// </summary>
        /// <example>
        /// See class header notes.  
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodToCache"></param>
        /// <param name="cacheKey01">Unique value to identify which cache to pull from.  Ex: Typical value is "Class name + method name".</param>
        /// <param name="hoursToCache"></param>
        /// Tested: 2020-06-28
        public T CallMethodRR<T>(Func<T> methodToCache, string cacheKey01 = "", int hoursToCache = 24) where T : class
        {
            //Situation 01: 
            //Explicit request to NOT cache records.  
            //We will call the method fresh, and save a cache behind the scenes (in case want to use a cache the next call).  
            if (hoursToCache <= 0)
            {
                //Have two refresh methods (one async, one synchronous).  We call the synchronous method freeze app until finished.    
                InstanceRR.RefreshCache(cacheKey01, methodToCache);
                return InstanceRR.GetCachedValue<T>(cacheKey01);
            }

            //Keep under "situation 01" but above "situation 02/03".  
            var list_ = InstanceRR.GetRecordsByCacheKey(cacheKey01);

            //Situation 02: 
            //We do not have a cached record.  So call method, serialize to json, store json to disk, then return value.    
            if (list_.CountRR() == 0)
            {
                Debug.WriteLine("Fyi: Cache key " + cacheKey01 + " does not exist.  Querying database to refresh.");
                InstanceRR.InsertRecordAndCreateJsonFile(cacheKey01, methodToCache());
                return InstanceRR.GetCachedValue<T>(cacheKey01);
            }

            //Situation 03: 
            //We have cached record.  So check if disk cache needs a refresh. 
            //Note: 
            //  - Refresh happens in background "after" the value is returned.    
            //  - CallMethodRR returns the cached value no matter how old it is.  
            if ((DateTime.Now - list_[0].DATE_TIME_CREATED).TotalHours > hoursToCache)
                InstanceRR.RefreshCacheAsync(cacheKey01, methodToCache);

            //Deserialize JSON cache from "situation 03".   
            return InstanceRR.GetCachedValue<T>(cacheKey01);
        }

               
        /// <summary>
        /// Refreshes disk cache in background.  
        /// Has housekeeping to keep max of two records related to same CacheKey01/CacheKey02.  So user drive will not fill-up.  
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodToCache"></param>
        /// <param name="cacheKey01">Unique value to identify which cache to pull from.  Ex: Typical value is "Class name + method name".</param>
        /// Tested: 2020-06-28
        private async void RefreshCacheAsync<T>(string cacheKey01, Func<T> methodToCache)
        {
            Debug.WriteLine("CCacheMethod->RefreshCacheAsync called.  For CacheKey01: " + cacheKey01);
            
            await Task.Run(() =>
            {
                InstanceRR.InsertRecordAndCreateJsonFile(cacheKey01, methodToCache());

                //Could reduce to 1.  Including 2 records for heck of it.  
                InstanceRR.DeleteOldCacheRecord(cacheKey01, 2);
                
            });

            Debug.WriteLine("CCacheMethod->RefreshCacheAsync finished.  For CacheKey01: " + cacheKey01);
        }



        /// <summary>
        /// Same as RefreshCacheAsync but runs synchronously.  
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey01"></param>
        /// <param name="methodToCache"></param>
		/// Tested: 2021-09-09
        private void RefreshCache<T>(string cacheKey01, Func<T> methodToCache)
        {
            Debug.WriteLine("CCacheMethod->RefreshCache (synchronous) called.  For CacheKey01: " + cacheKey01);
 
            InstanceRR.InsertRecordAndCreateJsonFile(cacheKey01, methodToCache());

            //Could reduce to 1.  Including 2 records for heck of it.  
            InstanceRR.DeleteOldCacheRecord(cacheKey01, 2);
    

            Debug.WriteLine("CCacheMethod->RefreshCache (synchronous) finished.  For CacheKey01: " + cacheKey01);
        }





    }
}
