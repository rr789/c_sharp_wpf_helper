﻿using rr_general.wpf_custom_control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace class_helper_rr
{
    /// <summary>
    /// Provides an Loaded event that only fires once.  
    /// Native control Loaded events often fire multiple times.  See: https://social.msdn.microsoft.com/Forums/vstudio/en-US/97e1256c-02c6-48dc-a8f8-c00bd1931b89/loaded-event-fires-twice-in-usercontrol?forum=wpf
    /// </summary>
    /// <example>
    /// //Use as class obj.  
    /// var load = new CLoadedEvent(this); //this refers to a control.
    /// load.loadedRR += onLoad;
    /// 
    /// //Or use static for quick setup.  
    /// CLoadedEvent.LoadedHandler(this, onLoad);
    /// 
    /// //onLoad handler.
    /// private void onload(object sender, EventArgs e)
    /// { }
    /// 
    /// 
    /// </example>
    public class CLoadedEvent
    {
        /// <summary>
        /// True = loaded once.  False = not yet loaded.    
        /// </summary>
        private bool hasLoaded;

        /// <summary>
        /// Replaces native "loaded" event.  
        /// This only fires once guaranteed.  
        /// </summary>
        public event EventHandler loadedRR;

        //Constructor.  
        public CLoadedEvent(object c) //Typed as object to handle DataGrid (VS cannot convert DataGrid into FrameworkElement).  
        {
            //This handles most of them.  
            if (typeof(FrameworkElement).IsAssignableFrom(c.GetType()))
                ((FrameworkElement)c).Loaded += _onLoad;

            //DataGrid does not inherit from Control.  It inherits from MultiSelector.  
            if (typeof(DataGrid).IsAssignableFrom(c.GetType()))
                ((DataGrid)c).Loaded += _onLoad;

            //Add more types as needed.  
        }

        /// <summary>
        /// Fires with the control.  Has safety to check if previously loaded.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// Tested: 2017-05-21 all passed.  
        private void _onLoad(object sender, EventArgs e)
        {
            //Fixes bogus Visual Studio XAML errors about "object reference not set to an instance of an object".     See: https://stackoverflow.com/a/25003268/722945
            //Halts Visual Studio designer screen from executing code further.  Does not affect running the app.   
            if (IsInDesignMode) { return; }
            
            if (hasLoaded == false)
            {
                if (this.loadedRR != null) { this.loadedRR(sender, e); }
                hasLoaded = true;
            }
        }


        //--------------------------------------------------
        /// <summary>
        /// Quick way to setup Loaded handler.  See class for example.  
        /// </summary>
        /// <param name="c">Control</param>
        /// <param name="f">Event handler function.</param>
        /// Tested: 2017-05-21 all passed.  
        public static void LoadedHandler(object c, EventHandler f)
        {
            var obj = new CLoadedEvent(c);
            obj.loadedRR += f;
        }

        /// <summary>
        /// Used with OnLoad method.  Fixes bogus Visual Studio XAML errors about "object reference not set to an instance of an object".    
        /// Fyi: The bogus error happens because Visual Studio tries too hard to execute application code to make the "designer" screen more accurate.  Using this method tells AX to take it easy.  Future versions of Visual Studio might resolve this issue.  
        /// See: https://stackoverflow.com/a/25003268/722945
        /// </summary>
        /// Tested: 2018-09-26
        public bool IsInDesignMode
        {
            get
            {
                var prop = DesignerProperties.IsInDesignModeProperty;
                return (bool)DependencyPropertyDescriptor
                    .FromProperty(prop, typeof(FrameworkElement))
                    .Metadata.DefaultValue;
            }
        }


    }
}
