﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace class_helper_rr
{

    /// <summary>
    /// Fast way to save settings.  Uses json to serialize data objects, so object is restored to original type.    
    /// Stores data in user's AppData\Roaming directory.  Ex: "C:\Users\randy\AppData\Roaming\my_app_name\".    
    /// </summary>
    /// <example>
    /// //Misc data object class.  
    /// public class myClass
    /// {
    ///     public string a;
    ///     public bool b;
    /// }
    /// 
    /// //Create object, assign values. 
    /// var obj = new myClass();
    /// obj.a = "hi"; 
    /// obj.b = true; 
    /// 
    /// //Serialize object into json, write to disk.  
    /// var store = new CDataStore<myClass>("sample", CDirectoryHelper.roamAppDir + @"\my_app_name\"); 
    /// store.writeToDisk(obj);
    /// 
    /// --------------------------------------------------
    /// //Read json file, deserialize into original object.  
    /// var store = new CDataStore<myClass>("sample"), CDirectoryHelper.roamAppDir + @"\my_app_name\");
    /// myClass obj = store.readFromDisk();
    /// CMsgBox.Show(obj.a);
    /// CMsgBox.Show(obj.b); //Returns boolean true (not string).  OK.  
    /// </example>
    /// Tested: 2017-06-11 all passed.  
    public class CDataStore<T>
    {
        /// <summary>
        /// Main directory.  Used to store data files.  
        /// Ex: "C:\Users\randy\AppData\Roaming\my_app_name\"
        /// </summary>
        public string dirStore;

        /// <summary>
        /// Full path to file.  
        /// Ex: "C:\Users\randy\AppData\Roaming\my_app_name\my_file.txt"
        /// </summary>
        public string filePath;

        /// <summary>
        /// Access data without reading from disk.  Updated on writeToDisk() and readFromDisk().  
        /// </summary>
        public T localDataset;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="fileName">Name without file extension.</param>
        /// <param name="dir_">Path to folder.  Typically pass-in "CDirectoryHelper.roamAppDir".  Ending slash optional.  Slashes can go forward or backward (OK).    Ex: "C:/Users/randy/AppData/Roaming/derp_app/sub1/sub2/sub3" </param>
        /// Tested: 2017-06-20
        public CDataStore(string fileName, string dir_)
        {
            if (fileName == null || dir_ == null) return;

            //Remove "\" & "/" from right side.  So user doesn't have to worry about entering slashes.  
            string dir = CString.removeRight(dir_, new[] { @"\", @"/" });

            filePath = dir + @"\" + fileName + ".txt";

            //For future sake...creates directory, or does nothing if already exists.  
            System.IO.Directory.CreateDirectory(dir);
        }

        /// <summary>
        /// Write data.  Done in background thread.  
        /// </summary>
        /// <param name="data"></param>
        /// Tested: 2017-07-01
        public void writeToDisk(T data)
        {
            //Safety: Needed when say DataGridRR has no config info defined.  
            if (filePath == null)
                return;
            
            //Save local copy.  
            localDataset = data;

            //Write to disk.  
            var txt = JsonConvert.SerializeObject(data);

            CBackgroundWorker.runTaskStatic(delegate
            {
                //Wrap in-case file is currently in-use.  Happens occasionally. 
                CTryCatchWrapper.wrap(delegate
                {
                    File.WriteAllText(filePath, txt);
                }, false, false, true);
            });

        }

        /// <summary>
        /// Read data from file on disk.   
        /// </summary>
        /// Tested: 2018-09-27
        public T readFromDisk()
        {
            T r = default(T);
            string txt = "";

            //2018-09-27 have had a few times where file was in-use (on disk) so it threw error.  This prevents it from throwing error.  
            CTryCatchWrapper.wrap(delegate
            {
                txt = File.ReadAllText(filePath);
            });
            
            if (txt != "")
            {
                //Local copy.  
                localDataset = JsonConvert.DeserializeObject<T>(txt);

                //Read from disk.  Deserialize (again) to keep two copies separate.  
                r = JsonConvert.DeserializeObject<T>(txt);
            }

            return r;

        }

        /// <summary>
        /// Simple method.  
        /// </summary>
        /// <returns></returns>
        /// Tested: 2017-06-18.
        public bool doesFileExist()
        {
            return File.Exists(filePath);
        }





    }





}
