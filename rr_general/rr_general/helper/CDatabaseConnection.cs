﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirebirdSql.Data.FirebirdClient;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Dapper;
using rr_general.helper;
using System.Windows.Documents;

namespace class_helper_rr
{
    /// <summary>
    /// Purpose: Connect to database and execute sql.  <para/>
    ///     Handles multiple database vendors using a common api. <para/>
    /// Note: Can reuse this object to execute multiple queries safely.  It creases a new connection for each query.  
    /// </summary>
    /// <example>
    /// Setup connection.  
    ///     //Firebird embedded server.
    ///     //Note: This CDatabaseConnection class will download/extract all required DLL's to directory AppData->Local->Firebird.  All automatic.  No special Windows permissions required.    
    ///     CDatabaseConnection conn = new CDatabaseConnection(
    ///           _serverHost:  null //Always null for embedded.  
    ///         , _dbName:      @"E:\sample_firebird_database.FDB" 
    ///         , _username:    "SYSDBA" 
    ///         , _password:    "my_password" 
    ///         , _dbType:      CDatabaseConnection.DB_TYPE_FIREBIRD_EMBEDED_SERVER 
    ///         , _port:        null //No port with embedded server.
    ///         );
    ///         
    ///     //Firebird connecting to external server.    
    ///     CDatabaseConnection conn = new CDatabaseConnection(
    ///           _serverHost:  "localhost" //Always null for embedded.  
    ///         , _dbName:      @"E:\sample_firebird_database.FDB" 
    ///         , _username:    "SYSDBA" 
    ///         , _password:    "my_password" 
    ///         , _dbType:      CDatabaseConnection.DB_TYPE_FIREBIRD_EXTERNAL_SERVER
    ///         , _port:        3050
    ///         );
    /// 
    ///     //Mysql.
    ///     CDatabaseConnection conn = new CDatabaseConnection(
    ///           _serverHost:  "localhost" 
    ///         , _dbName:      "db_name" 
    ///         , _username:    "username" 
    ///         , _password:    "my_password" 
    ///         , _dbType:      CDatabaseConnection.DB_TYPE_MYSQL 
    ///         , _port:        3306
    ///         );
    ///         
    /// 	//Microsoft SQL with trusted connection.  
    ///		CDatabaseConnection conn = new CDatabaseConnection(
    ///		    _serverHost:  "RM-DC1-DEV3"
    ///		    , _dbName:    "AxManagement" 
    ///		    , _dbType:    CDatabaseConnection.DB_TYPE_MICROSOFT_SQL_TRUSTED_CONNECTION
    ///		    , _port:      "1433" //Optional
    ///		    //Ps. No need for user/pass when using trusted connection.
    ///		);
    ///		
    ///     //Oracle.
    ///     CDatabaseConnection conn2 = new CDatabaseConnection(
    ///           _serverHost:  "192.168.1.12"
    ///         , _dbName:      "orcl" 
    ///         , _username:    "erp"
    ///         , _password:    "my_password"
    ///         , _dbType:      CDatabaseConnection.DB_TYPE_ORACLE
    ///         , _port:        "1521"
    ///         );
    /// 
    ///Insert one record into db.  
    ///     conn.executeNonQuery("INSERT INTO TABLE1 (FIRSTNAME,LASTNAME) VALUES ('F SAMPLE', 'L SAMPLE')");
    ///
    ///Query records from db.  
    ///     var dt = conn.executeQuery("SELECT * FROM table1"); //Runs query, puts into CDataTable.  
    ///     dataGridView1.dataSourceRR(dt); //Links CDataGridView to CDataTable. 
    ///     CMsgBox.Show("records found: " + dt.table.Rows.Count.ToString());
    ///     
    /// Get scalar value (aka value in first row, first column).  Also used for getting value from a function.  
    ///     var oValue = conn.executeScalar("SELECT FIRSTNAME FROM table1 WHERE ID='2'"); //Query only returns one column, one row.    
    ///     CMsgBox.Show(oValue.ToString()); //Shows the value (ex: "Jane").
    /// 
    /// </example>
    /// Tested: 2017-03-12 on both Firebird, Mysql, Oracle all passed.  
    public class CDatabaseConnection
    {
        /*
         * --------------------------------------------------------------------------------------
         * USER DEFINED PROPERTIES. 
         * --------------------------------------------------------------------------------------
         */
        /// <summary>
        /// Name or IP address of server hosting db.  <para/>
        /// User defines value in constructor.  
        /// </summary>
        public string serverHost;

        /// <summary>
        /// Name of database connecting to.  <para/>
        /// User defines value in constructor.  
        /// </summary>
        public string dbName;

        /// <summary>
        /// User defines value in constructor.  <para/>
        /// </summary>
        public string username;

        /// <summary>
        /// User defines value in constructor.  <para/>   
        /// </summary>
        public string password;

        /// <summary>
        /// Assign value to one of the static strings DB_TYPE_FIREBIRD_EMBEDED_SERVER, DB_TYPE_MYSQL etc.  <para/>   
        /// User defines value in constructor.  
        /// </summary>
        public string dbType;

        /// <summary>
        /// User defines value in constructor.  Optional, class will use default ports if null. <para/>   
        /// </summary>
        string port;


        /*
         * --------------------------------------------------------------------------------------
         * DATABASE TYPES.  
         * --------------------------------------------------------------------------------------
         */
        /// <summary> 
        /// Used in constructor for dbType parameter.  
        /// For Embedded firebird setup.  No need for outside server.  
        /// </summary>
        public const string DB_TYPE_FIREBIRD_EMBEDED_SERVER = "DB_TYPE_FIREBIRD_EMBEDED_SERVER";

        /// <summary> 
        /// Used in constructor for dbType parameter.  
        /// For Firebird setup that connects to external server.    
        /// </summary>
        public const string DB_TYPE_FIREBIRD_EXTERNAL_SERVER = "DB_TYPE_FIREBIRD_EXTERNAL_SERVER";


        /// <summary>
        /// Used in constructor for dbType parameter. 
        /// </summary>
        public const string DB_TYPE_MYSQL = "MYSQL";


        /// <summary>
        /// Used in constructor for dbType parameter. 
        /// </summary>
        public const string DB_TYPE_ORACLE = "ORACLE";


        /// <summary>
        /// Used in constructor for dbType parameter.  
        /// Uses Windows user authentication.  No need to pass user/pass.  
        /// </summary>
        public const string DB_TYPE_MICROSOFT_SQL_TRUSTED_CONNECTION = "MICROSOFT_SQL_TRUSTED_CONNECTION";

        /*
         * --------------------------------------------------------------------------------------
         * AUTOMATICALLY GENERATED.  
         * --------------------------------------------------------------------------------------
         */
        /// <summary>
        /// Connection string.  
        /// </summary>
        public string conn_str;

        /// <summary> 
        /// Path to fbclient.dll in BIN Debug/Release directory.  
        /// Class sets the value automatically.  For both 32/64bit.     
        /// </summary>
        public string firebird_embedded_server_path;



        /*
         * --------------------------------------------------------------------------------------
         * FUNCTIONS
         * --------------------------------------------------------------------------------------
         */
        /// <summary>
        /// Constructor.  All parameters are required.   
        /// </summary>
        /// <param name="_serverHost">For embedded Firebird use null.    </param>
        /// <param name="_dbName">For both embedded & standard Firebird point to location of file, ex: "E:\\sample_firebird_database.FDB".   </param>
        /// <param name="_username"></param>
        /// <param name="_password"></param>
        /// <param name="_dbType">Ex: CDatabaseConnection.DB_TYPE_FIREBIRD_EMBEDED_SERVER, CDatabaseConnection.DB_TYPE_MYSQL etc</param>
        /// <param name="_port">Optional: Port number.  If null then will use default port for each db vendor.</param>
        /// Tested: 2020-06-15 all passed.  
        public CDatabaseConnection(string _serverHost, string _dbName, string _username = null, string _password = null, string _dbType = null, string _port = null)
        {
            serverHost = _serverHost;
            dbName = _dbName;
            username = _username;
            password = _password;
            dbType = _dbType;
            port = _port;

            if (_dbType == DB_TYPE_FIREBIRD_EMBEDED_SERVER)
                SetupFirebirdEmbeddedServer();

            createConnectionString();
        }


        /// <summary>
        /// Installs Firebird embedded server (DLL's) onto local user hard drive.  
        /// No special permission required.  DLL's are downloaded to app's AppDirectory/Local/App name/Database XXX.  
        /// </summary>
        /// Tested: 2020-06-28
        public void SetupFirebirdEmbeddedServer()
        {
            string firebirdDir = "";

            //Fyi on embedded server: 
            //- The DLL's for embedded server can exist anywhere on the local drive (or even on a network).   
            //- DLL location for embedded server is defined in the connection string.  
            //- Below we download embedded server ZIP, then extract to directory AppData->Local->Firebird.    
            //- Including resource files in a class library project is difficult.  Downloading from Firebird.org and extracting is much easier.  See https://social.msdn.microsoft.com/Forums/windows/en-US/722bc990-af15-4144-8414-3ee2e3a86d3c/referenced-dll-content-files-not-being-published-by-clickonce?forum=winformssetup

            //If app running 32 bit.  
            if (IntPtr.Size == 4)
            {
                firebirdDir = CDirectory.ConcatDirectory(CDirectory.GetDirLocal(), @"Firebird\Embedded_server_32bit\4.0.3");
                firebird_embedded_server_path = CDirectory.ConcatDirectoryAndFilePath(firebirdDir, "fbclient.dll");

                if (!File.Exists(firebird_embedded_server_path))
                {
                    //★★★ 2022-01-11 Comment-out msgbox.  For client it works fine, but for Webapp and running as DLL in another app (ex: Dynamics AX) the msgbox causes exception. 
                    //CMsgBox.Show("I need to download the Firebird 32bit database to speed-up this app's caching ability.  Takes 1 min.  Will prompt when done.");
                    CZip.DownloadZipThenExtract(@"https://github.com/FirebirdSQL/firebird/releases/download/v4.0.3/Firebird-4.0.3.2975-0-Win32.zip", firebirdDir);
                    //CMsgBox.Show("Firebird install completed OK.");

                    /* Update Firebird.conf to run Firebird in "Super Classic" mode.  This prevents error when multiple app instances access the same embedded db simultaneously.  
                     * Comparison of modes: 
                     *      - Super: 
                     *          - One app gets "exclusive" access to the embedded db.  Other apps trying to access the embedded db will throw error. 
                     *      - Super classic: 
                     *          - Shared.  Multiple apps can access the same embedded db.  
                     *          - All connections share one executing process.   
                     *      - Classic: 
                     *          - Shared.  Multiple apps can access the same embedded db.  
                     *          - Each connection spans a new executing process.    
                     *         https://firebirdsql.org/manual/qsg25-classic-or-super.html
                     */
                    string pathToConfigFile = CDirectory.ConcatDirectoryAndFilePath(firebirdDir, "firebird.conf");

                    CFile.TextFile_ReplaceSubstring(
                        pathToConfigFile
                        , "#ServerMode = Super" 
                        ,    "#2023-09-20 Updated from default \"Super\" to \"SuperClassic\" so multiple C# apps can connect to the same db.  -RR\n" //Added a little note to the config file.  
                           + "ServerMode = SuperClassic\n" 
                           + "\n"
                           + "#Original -RR\n"
                           + "#ServerMode = Super"
                          );



                }
                
            }
            //64 bit.
            else if (IntPtr.Size == 8)
            {
                firebirdDir = CDirectory.ConcatDirectory(CDirectory.GetDirLocal(), @"Firebird\Embedded_server_64bit\4.0.3");
                firebird_embedded_server_path = CDirectory.ConcatDirectoryAndFilePath(firebirdDir, "fbclient.dll");

                if (!File.Exists(firebird_embedded_server_path))
                {
                    //★★★ 2022-01-11 Comment-out msgbox.  For client it works fine, but for Webapp and running as DLL in another app (ex: Dynamics AX) the msgbox causes exception. 
                    //CMsgBox.Show("I need to download the Firebird 64bit database to speed-up this app's caching ability.  Takes 1 min.  Will prompt when done.");
                    CZip.DownloadZipThenExtract(@"https://github.com/FirebirdSQL/firebird/releases/download/v4.0.3/Firebird-4.0.3.2975-0-x64.zip", firebirdDir);
                    //CMsgBox.Show("Firebird install completed OK.");


                    //Update Firebird.conf to run Firebird in "Super Classic" mode.  More notes are in 32bit section above.  
                    string pathToConfigFile = CDirectory.ConcatDirectoryAndFilePath(firebirdDir, "firebird.conf");

                    CFile.TextFile_ReplaceSubstring(
                        pathToConfigFile
                        , "#ServerMode = Super" 
                        ,    "#2023-09-20 Updated from default \"Super\" to \"SuperClassic\" so multiple C# apps can connect to the same db.  -RR\n" //Added a little note to the config file.  
                           + "ServerMode = SuperClassic\n" 
                           + "\n"
                           + "#Original -RR\n"
                           + "#ServerMode = Super"
                          );

                }
            }



        }















        /// <summary>
        /// Generates the db connection string.  Sets value to connectionString property.  <para/>
        /// Constructor calls this method.  <para/>
        /// </summary>
        /// Tested: 2017-03-12 all passed.  
        public void createConnectionString()
        {
            switch (this.dbType) //dbType is set in the Constructor.  
            {
                case DB_TYPE_FIREBIRD_EMBEDED_SERVER:
                    //Check if dll exists in BIN Debug/Release directory.  See OneNote section "Firebird Embedded in Visual Studio" for how to setup a project.  
                    if (!System.IO.File.Exists(firebird_embedded_server_path))
                    {
                        CMsgBox.Show("CDatabaseConnection error: Path to Firebird fbclient.dll not found.  Path copied to clipboard. ");
                        CClipboard.SetTextRR(firebird_embedded_server_path);
                    }

                    //Safety: 
                    //Ensure directory exists that holds database file.  Firebird is OK making a new database.  However, Firebird will throw error if it has to create a child subdirectory.  
                    string dir_ = CDirectory.GetDirectoryFromPath(dbName);
                    Directory.CreateDirectory(dir_);

                    conn_str = ""
                        //serverHost not needed (since running embedded).          
                        + "Database=" + dbName + ";" //Since Firebird is embedded, dbName = file location.  Ex: "E:\\sample_firebird_database.fdb;".  Firebird stores one database per file.  So picking the file = picking the database.
                        + "User=" + username + ";"
                        + "Password=" + password + ";"
                        + "ServerType=1;" //0 = external server.  1 = embedded server.  
                        + "Charset=UTF8;"
                        //This is required for embedded server.  It s/b called "ServerLibrary" since it is exclusively defined for embedded server.      
                        + "ClientLibrary=" + firebird_embedded_server_path + ";" //Points to BIN Release/Debug output directory.  
                        + "Pooling=true"; //2023-09-20 Prevents error "lock manager error...inconsistent lock table type/version".  I tested.  To recreate bug, remove this line, then open two "different/unrelated" apps simultaneously.  Opening two instances of the "same" app simultaneously will not trigger the bug (little unususal).  Have to open two "different/unrelated" apps that both use the same firebird embedded db.  
                        ; 
                    break;

                case DB_TYPE_FIREBIRD_EXTERNAL_SERVER:
                    conn_str = ""
                        + "DataSource=" + serverHost + ";" //Ex: "localhost"
                        + "Database=" + dbName + ";" //Ex: "E:\\sample_firebird_database.fdb;"  File locations are used even if server is external (or setup external server with "alias" name).                         
                        + "User=" + username + ";"
                        + "Password=" + password + ";"
                        + "ServerType=0;" //0 = external server.  1 = embedded server.     
                        + "Charset=UTF8;"
                        + "Port=" + (port == null ? "3050" : port + ";") //3050 is default Firebird port.  
                        ;                         
                    break;

                case DB_TYPE_MYSQL:
                    //From: https://www.connectionstrings.com/mysql/
                    conn_str = ""
                        + "Server=" + serverHost + ";"
                        + "Database=" + dbName + ";"
                        + "Uid=" + username + ";"
                        + "Pwd=" + password + ";"
                        + "Port=" + (port == null ? "3306" : port + ";"); //3306 is default MySql port. 
                                                                        
                    break;

                case DB_TYPE_ORACLE:
                    port = port == null ? "1521" : port; //1521 is default Oracle port. 

                    //Forming a string like: "data source=192.168.1.12:1521/orcl; user id=xxx; password=xxx; pooling=false; ";  From: https://www.connectionstrings.com/oracle-data-provider-for-net-odp-net/
                    conn_str = "data source=" + serverHost + ":" + port + "/" + dbName + "; user id=" + username + "; password=" + password + ";"; //"data source" and "user id" must have space between words (I tested without and it throws error);
                    break;


                case DB_TYPE_MICROSOFT_SQL_TRUSTED_CONNECTION:
                    //Has a safety in to "not" generate port param if default port 1433 is passed-in.  It is dumb but MSSQL server will throw error if any port (even the default port) is defined in connection string but TCP/IP is not activated on the server.  See: https://stackoverflow.com/a/32633660/722945
                    if (port == null || port == "1433")
                        conn_str = ""
                            + "Server=" + serverHost + ";"
                            + "Database=" + dbName + ";"
                            + "Trusted_Connection=True;";
                    else 
                        conn_str = ""
                            + "Server=" + serverHost + ";"
                            + "Database=" + dbName + "," + port + ";" //MSSQL does not have a named "port" param.  Instead to use a non-standard port, pass it via db name then ",MyPortNumber".
                            + "Trusted_Connection=True;";
                    break;


                default:
                    CMsgBox.Show("Error: CDatabaseConnection->createConnectionString->Database Type not defined."); //Help debug.     
                    break;


            }

        }


        /// <summary>
        /// Creates a NEW connection object. Also opens the connection.   
        /// </summary>
        /// Tested: 2015-04-12 all passed.  
        public IDbConnection createConnection()
        {
            IDbConnection conn = null; //IDbConnection is a generic db interface.  All third party db libraries implement idb methods.  

            switch (dbType)
            {
                case DB_TYPE_FIREBIRD_EMBEDED_SERVER:
                    conn = new FbConnection(conn_str);
                    break;

                case DB_TYPE_FIREBIRD_EXTERNAL_SERVER:
                    conn = new FbConnection(conn_str);
                    break;

                case DB_TYPE_MYSQL:
                    conn = new MySqlConnection(conn_str);
                    break;

                case DB_TYPE_ORACLE:
                    conn = new OracleConnection(conn_str);
                    break;

                case DB_TYPE_MICROSOFT_SQL_TRUSTED_CONNECTION:
                    conn = new SqlConnection(conn_str);
                    break;

            }

            CTryCatchWrapper.wrap(conn.Open); //Open connection.  
            return conn;
        }



        /// <summary>
        /// Use for INSERT, UPDATE, DELETE statements.  Not for SELECT.  <para/>
        /// Closes the connection immediately after executing. 
        /// </summary>
        /// <param name="sql">Full sql string.</param>
        /// <param name="showErrors">Can set to false to suppress error message boxes.</param>
        /// <returns>Number of rows affected/updated.  Misc statements will return -1 (note: -1 does not mean an error).  https://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqlcommand.executenonquery%28v=vs.110%29.aspx</returns>
        /// Tested: 2015-04-12 all passed.  
        public int executeNonQuery(string sql, bool showErrors = true)
        {
            int rowsAffected = 0;

            using (var connection = createConnection())  //USING will close/dispose of the connection & command objects automatically.  Example here: http://stackoverflow.com/a/4002709/722945
            {
                using (var command = connection.CreateCommand())
                {
                    if (showErrors == true)
                    {
                        try
                        {
                            command.CommandText = sql;
                            rowsAffected = command.ExecuteNonQuery(); //Returns integer for number of rows affected.   
                        }
                        catch (Exception e)
                        {
                            CMsgBox.Show(e.ToString());
                        }
                    }
                    else
                    {
                        command.CommandText = sql;
                        rowsAffected = command.ExecuteNonQuery(); //Returns integer for number of rows affected.   
                    }
                }
            }

            return rowsAffected;
        }



        /// <summary>
        /// ★★★ 2020-06-15 This works but will get depreciated in favor of new method ExecuteQueryGeneric.  
        /// Use for SELECT statements.  Runs query and puts results into CDataTable object.  Then closes connection.      
        /// </summary>
        /// <param name="sql">Full sql string.</param>
        /// <param name="showErrors">Can set to false to suppress error message boxes.</param>
        /// <returns>Returns a CDataTable object.  </returns>
        /// Tested: 2015-04-12 all passed.  
        public CDataTable executeQuery(string sql, bool showErrors = true)
        {
            CDataTable dt = new CDataTable();

            using (var connection = createConnection())  //USING will close/dispose of the Connection, Command, DataReader objects automatically.  Used example here: http://stackoverflow.com/a/4002709/722945
            {
                using (var command = connection.CreateCommand())
                {
                    if (showErrors == true)
                    {
                        try
                        {
                            command.CommandText = sql;
                            dt.table.Load(command.ExecuteReader(CommandBehavior.CloseConnection)); //CommandBehavior.CloseConnection tells C# to close the Connection when DataReader is closed.  It's not totally required.  http://stackoverflow.com/a/5689765/722945
                        }
                        catch (Exception e)
                        {
                            CMsgBox.Show(e.ToString());
                        }
                    }
                    else
                    {
                        command.CommandText = sql;
                        dt.table.Load(command.ExecuteReader(CommandBehavior.CloseConnection)); //CommandBehavior.CloseConnection tells C# to close the Connection when DataReader is closed.  It's not totally required.  http://stackoverflow.com/a/5689765/722945
                    }
                }
            }

            return dt;
        }

        /// <summary>
        /// 2020-06-15 This method is superior and will replace executeQuery().  
        /// Executes query.  Returns strongly typed list.  
        /// </summary>
        /// <example>
        /// string sql = "SELECT * FROM MY_TABLE";
        /// var myList = DbConnectionObject.ExecuteQueryGeneric<MY_DATA_CONTRACT>(sql); 
        /// </example>
        /// <param name="sql">Full sql string.</param>
        /// <param name="showErrors">Can set to false to suppress error message boxes.</param>
        /// <remarks>
        /// Uses NuGet Dapper package.  Very handy.  https://github.com/StackExchange/Dapper
        /// </remarks>
        /// Tested: 2020-06-15
        /// <returns></returns>
        public List<T> ExecuteQueryGeneric <T>(string sql, bool showErrors = true)
        {
            var r = new List<T>();

            using (var connection = createConnection())  //USING will close/dispose of the Connection, Command, DataReader objects automatically.  Used example here: http://stackoverflow.com/a/4002709/722945
            {
                using (var command = connection.CreateCommand())
                {
                    if (showErrors == true)
                    {
                        try
                        {
                            //This Query<T>() method is an extension from NuGet library Dapper.  
                            r = connection.Query<T>(sql).ToList();
                        }
                        catch (Exception e)
                        {
                            CMsgBox.Show(e.ToString());
                        }
                    }
                    else
                    {
                        //This Query<T>() method is an extension from NuGet library Dapper.  
                        r = connection.Query<T>(sql).ToList();
                    }
                }
            }

            return r;
        }




        /// <summary>
        /// For calling queries that return only one value.  Mainly for calling UDF (User Defined Functions).  
        /// Returns generic "Object" type to keep flexible.         
        /// </summary>
        /// <param name="sql">Full sql string.</param>
        /// <param name="showErrors">Can set to false to suppress error message boxes.</param>
        /// <returns>The one value in first column, first row.  Need to cast as value expected (to string/int etc).</returns>
        public object executeScalar(string sql, bool showErrors = true)
        {
            object oReturn = null;

            using (var connection = createConnection())  //USING will close/dispose of the connection & command objects automatically.  Example here: http://stackoverflow.com/a/4002709/722945
            {

                using (var command = connection.CreateCommand())
                {
                    if (showErrors == true)
                    {
                        try
                        {
                            command.CommandText = sql;
                            oReturn = command.ExecuteScalar(); //Returns only one value from the first row, first column.    
                        }
                        catch (Exception e)
                        {
                            CMsgBox.Show(e.ToString());
                        }
                    }
                    else
                    {
                        command.CommandText = sql;
                        oReturn = command.ExecuteScalar(); //Returns only one value from the first row, first column.
                    }
                }
            }

            return oReturn;
        }


        /// <summary>
        /// Creates a new database.  Typically used with embedded Firebird database.  
        /// </summary>
        /// Tested: 2024-08-27
        public void CreateDatabase()
        {
            switch (dbType)
            {
                //Most common.  
                case DB_TYPE_FIREBIRD_EMBEDED_SERVER:
                    //Firebird just needs the connection string to know where to create the database.  Pretty easy.
                    FbConnection.CreateDatabase(conn_str); 
                    break;

                //Probably will not use this since we could just send an SQL statement to the server.  
                case DB_TYPE_FIREBIRD_EXTERNAL_SERVER:
                    //Firebird just needs the connection string to know where to create the database.  Pretty easy.
                    FbConnection.CreateDatabase(conn_str); 
                    break;

            }

        }























    }
}
