﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace class_helper_rr
{

    /// <summary>
    /// Purpose: Prepare the sql strings, and execute query.  
    /// </summary>
    public class CDatabaseQuery
    {
        /*
        * --------------------------------------------------------------------------------------
        * USER DEFINED PROPERTIES. 
        * --------------------------------------------------------------------------------------
        */

        /// <summary>
        /// Table name to query.    
        /// </summary>
        public string tableName = null;

        /// <summary>
        /// Field names to pull in SELECT statements.  
        /// </summary>
        public string[] fieldsToPull = null;

        /// <summary>
        /// For all queries.  Defines criteria for selecting data.  Key=Fieldname, Value=Value.    
        /// </summary>
        public Dictionary<string, string> criteriaArr = new Dictionary<string, string>(); //Using String type for both key/value.  It's OK since sql is a string anyhow. 


        /// <summary>
        /// For INSERT/UPDATE queries.  Key=Fieldname, Value=Value.  
        /// </summary>
        public Dictionary<string, string> valueArr = new Dictionary<string, string>(); //Using String type for both key/value.  It's OK since sql is a string anyhow.      



        /*
        * --------------------------------------------------------------------------------------
        * SYSTEM GENERATED PROPERTIES. 
        * --------------------------------------------------------------------------------------
        */

        /// <summary>
        /// Stores the db connection object.  
        /// </summary>
        public CDatabaseConnection connection;

        /// <summary>
        /// For debugging.  View the sql that ran.  
        /// </summary>
        public string sqlDebug = null;


        /*
        * --------------------------------------------------------------------------------------
        * FUNCTIONS
        * --------------------------------------------------------------------------------------
        */

        /// <summary>
        /// Constructor.  Passes in connection object.  This class extends the CDatabaseConnection.    
        /// </summary>
        /// <param name="_connection"></param>
        public CDatabaseQuery(CDatabaseConnection _connection)
        {
            connection = _connection;
        }



        /// <summary>
        /// ★★★ 2020-06-15 This is a legacy method.  It works but will eventually get replaced by superior method PullRecordsGeneric().
        /// Executes query, returns recordset (as CDataTable). Sanitizes criteria values OK.<para/>
        /// Required: tableName, fieldsToPull,criteriaArr.  
        /// </summary>
        /// <example>
        /// var query = new CDatabaseQuery(connectionObject); //connectionObject is from CDatabaseConnection. 
        /// query.tableName = "TABLE1";
        /// query.fieldsToPull = new[] { "ID", "FIRSTNAME", "LASTNAME" }; //Or pass nothing in to grab all columns.
        /// query.criteriaArr["FIRSTNAME"] = "Jane";
        /// query.criteriaArr["LASTNAME"] = "Doe";
        /// dataGridView1.dataSourceRR(query.pullRecords()); //Execute query, attach recordset to DataGridView.  
        /// </example>
        /// <param name="showErrors"> Setting to false will suppress error message boxes from executeQuery() call.</param>
        /// <returns>Result set in CDataTable.  </returns>
        /// Tested: 2020-06-16 all passed.  
        public CDataTable pullRecords(bool showErrors = true)
        {
            string sql = "";

            //Setup sql. 
            if (criteriaArr.CountRR() == 0 || fieldsToPull == null  )
                sql = "" +
                    "SELECT * " + 
                    "FROM " + tableName + " " +
                    "WHERE 1=1 ";
            else
                sql = "" +
                    "SELECT " + string.Join(",", fieldsToPull) + " " +
                    "FROM " + tableName + " " +
                    "WHERE 1=1 ";
            
            foreach (KeyValuePair<string, string> obj in criteriaArr)
            {
                sql += "AND " + obj.Key + "='" + sanitizeString(obj.Value) + "' ";
            }

            sqlDebug = sql; //For debugging.  

            return connection.executeQuery(sql, showErrors);
        }




        /// <summary>
        /// ★★★ 2020-06-15 This is a superior method.  Replaces (soon to be) deprecated method pullRecords().
        /// Executes query, returns recordset (as CDataTable). Sanitizes criteria values OK.
        /// </summary>
        /// <example>
        /// var query = new CDatabaseQuery(connectionObject); //connectionObject is from CDatabaseConnection. 
        /// query.tableName = "TABLE1";
        /// query.fieldsToPull = new[] { "ID", "FIRSTNAME", "LASTNAME" };
        /// query.criteriaArr["FIRSTNAME"] = "Jane";
        /// query.criteriaArr["LASTNAME"] = "Doe";
        /// var myList = query.pullRecords<MY_DATA_CONTRACT>(); 
        /// </example>
        /// <param name="showErrors"> Setting to false will suppress error message boxes from ExecuteQueryGeneric() call.</param>
        /// Tested: 2020-06-16
        public List<T> PullRecordsGeneric<T>(bool showErrors = true)
        {
            string sql = "";
            
            //Setup sql. 
            if (criteriaArr.CountRR() == 0 || fieldsToPull == null  )
                sql = "" +
                    "SELECT * " + 
                    "FROM " + tableName + " " +
                    "WHERE 1=1 ";
            else
                sql = "" +
                    "SELECT " + string.Join(",", fieldsToPull) + " " +
                    "FROM " + tableName + " " +
                    "WHERE 1=1 ";
            
            foreach (KeyValuePair<string, string> obj in criteriaArr)
            {
                sql += "AND " + obj.Key + "='" + sanitizeString(obj.Value) + "' ";
            }

            sqlDebug = sql; //For debugging.  

            return connection.ExecuteQueryGeneric<T>(sql, showErrors);
        }





        /// <summary>
        /// Executes INSERT statement.  Returns number of rows inserted.  Sanitizes criteria values OK.<para/>
        /// Required: tableName, valueArr.  
        /// </summary>
        /// <example> 
        /// var query = new CDatabaseQuery(connectionObject); //connectionObject is from CDatabaseConnection.
        /// query.tableName = "TABLE1";
        /// query.valueArr = new Dictionary<string, string>() {   //Or can use the std syntax: valueArr["fieldname"] = "value";
        ///     { "FIELDNAME1", "VALUE1" },   
        ///     { "FIELDNAME2", "VALUE2" },
        ///     { "FIELDNAME3", "VALUE3" }
        /// };
        /// query.insertRecord(); //Returns "1" for inserting one row.  
        /// </example>
        /// <param name="showErrors"> Setting to false will suppress error message boxes from executeNonQuery() call.</param>
        /// <returns>Number of rows inserted.   </returns>
        /// Tested: 2015-04-26 all passed.  
        public int insertRecord(bool showErrors = true)
        {
            //Setup sql. 
            //Final query will be "INSERT INTO Table (Column1, Column2) VALUES ('Value1', 'Value2')"
            string sql1 = "INSERT INTO " + tableName + " (";
            string sql2 = "VALUES (";

            foreach (KeyValuePair<string, string> obj in valueArr)
            {
                sql1 += obj.Key + ",";

                if (obj.Value == null)
                    //Preserve null value.  Not turning into empty string ''.  
                    sql2 += "null,";
                else
                    sql2 += "'" + sanitizeString(obj.Value) + "',";
            }

            //Remove trailing commas & cap off each stmt.  
            sql1 = sql1.TrimEnd(',') + ") ";
            sql2 = sql2.TrimEnd(',') + ") ";

            string sqlCombined = sql1 + sql2;

            sqlDebug = sqlCombined; //For debugging.  
            return connection.executeNonQuery(sqlCombined, showErrors);
        }


        /// <summary>
        /// Executes UPDATE statement.  Returns number of rows updated.  Sanitizes criteria values OK.<para/>
        /// Required: tableName, valueArr, criteriaArr.  
        /// </summary>
        /// <example> 
        /// var query = new CDatabaseQuery(connectionObject); //connectionObject is from CDatabaseConnection.
        /// query.tableName = "TABLE1";
        /// query.valueArr = new Dictionary<string, string>() {
        ///     { "FIELDNAME1", "VALUE1" },   
        ///     { "FIELDNAME2", "VALUE2" },
        ///     { "FIELDNAME3", "VALUE3" }
        /// };
        /// query.criteriaArr = new Dictionary<string, string>() {
        ///     { "FIELDNAME1", "VALUE1" },   
        ///     { "FIELDNAME2", "VALUE2" },
        ///     { "FIELDNAME3", "VALUE3" }
        /// };
        /// query.updateRecords();
        /// </example>
        /// <param name="showErrors"> Setting to false will suppress error message boxes from executeNonQuery() call.</param>
        /// <returns>Number of rows updated.   </returns>
        /// Tested: 2015-04-26 all passed.  
        public int updateRecords(bool showErrors = true)
        {
            //Setup sql. 
            //Final query will be: UPDATE TABLENAME SET FIELD1 = 'VALUE1', FIELD2 = 'VALUE2' WHERE CRITERIA_FIELDNAME = 'CRITERIA VALUE' ...
            string sql1 = "UPDATE " + tableName + " SET ";
            string sql2 = "WHERE 1=1 ";

            foreach (KeyValuePair<string, string> obj in valueArr)
            {
                sql1 += obj.Key + "='" + sanitizeString(obj.Value) + "',";
            }

            //Remove trailing comma.  
            sql1 = sql1.TrimEnd(',');

            foreach (KeyValuePair<string, string> obj in criteriaArr)
            {
                sql2 += "AND " + obj.Key + "='" + sanitizeString(obj.Value) + "' ";
            }

            string sqlCombined = sql1 + " " + sql2;


            sqlDebug = sqlCombined; //For debugging.  
            return connection.executeNonQuery(sqlCombined, showErrors);
        }




        /// <summary>
        /// Executes DELETE statement.  Returns number of rows deleted.  Sanitizes criteria values OK.<para/>
        /// Required: tableName, criteriaArr.  
        /// </summary>
        /// <example> 
        /// var query = new CDatabaseQuery(connectionObject); //connectionObject is from CDatabaseConnection.
        /// query.tableName = "TABLE1";
        /// query.criteriaArr = new Dictionary<string, string>() {
        ///     { "ID", "5" },   
        ///     { "FIRSTNAME", "val2" }
        /// };
        /// query.deleteRecords(); 
        /// </example>
        /// <param name="showErrors"> Setting to false will suppress error message boxes from executeNonQuery() call.</param>
        /// <returns>Number of rows deleted.   </returns>
        /// Tested: 2015-04-26 all passed.  
        public int deleteRecords(bool showErrors = true)
        {
            //Setup sql. 
            //Final query will be: DELETE FROM TABLENAME WHERE COLUMN1 = 'VALUE1' AND COLUMN2 = 'VALUE2'...
            string sql = "DELETE FROM " + tableName + " WHERE 1=1 ";
            
            foreach (KeyValuePair<string, string> obj in criteriaArr)
            {
                sql += "AND " + obj.Key + "='" + sanitizeString(obj.Value) + "' ";
            }

            sqlDebug = sql; //For debugging.  
            return connection.executeNonQuery(sql, showErrors);
        }




        /// <summary>
        /// Sanitizes string from SQL injection.
        /// Basically just replaces one single quote "'" with two "''".  Both MySQL and Oracle accept this method.
        /// Copied from my PHP function.  
        /// </summary>
        /// <example>
        /// string str = CDatabaseQuery.sqlSanitize("Jane’s ball");
        /// CMsgBox.Show(str); //Shows "Jane''s ball".  
        /// </example>
        /// <param name="sql">Full sql string.</param>
        /// <returns></returns>
        /// Tested: 2015-04-19 all passed.  
        public static string sanitizeString(string sql)
        {
            if (sql == null)
                return null;
            
            string oReturn = sql;

            //Optional: Convert these special slanted quote characters into regular quote characters.  These are from Microsoft Outlook/Word.  If a database's encoding is set to UTF8 then this is not needed.  Oracle database can store most UTF8 characters ok (ex: "ø" is stored OK) but these three slanted quotes it has trouble with.
            oReturn = oReturn.Replace("’", "'"); //Slanted single quote.  
            oReturn = oReturn.Replace('“', '"'); //Slanted double quote (beginning).  
            oReturn = oReturn.Replace('”', '"'); //Slanted double quote (ending).  

            //Sanitize String: Replace any single quote with two single quotes.  This is all that is needed to sanitize a string (for both MySQL and Oracle).
            oReturn = oReturn.Replace("'", "''");

            return oReturn;
        }

        /// <summary>
        /// Sanitizes all elements in basic string array.  Loops thru all elements in array and runs sqlSanitize on them.     
        /// </summary>
        /// <example>
        /// string[] a = { "test'1", "test'2",}; //Testing if single quotes are escaped.  <para/>
        /// a = CDatabaseQuery.sanitizeStringArray(a);  <para/>
        /// CMsgBox.Show(string.Join(",",a)); //Returns "test''1, test''2" (escaped OK).  <para/>
        /// </example>
        /// <param name="arr">string array</param>
        /// <returns>Array with sanitized string values.  </returns>
        /// Tested: 2015-04-26 all passed.
        public static string[] sanitizeStringArray(string[] arr)
        {
            List<string> oReturn = new List<string>(); //Using List since it can dynamically add values to array.  

            arr.EachRR(p =>
            {
                oReturn.Add(sanitizeString(p));
            });

            return oReturn.ToArray<string>(); //Converts List to basic string array.  
        }


    }
}
