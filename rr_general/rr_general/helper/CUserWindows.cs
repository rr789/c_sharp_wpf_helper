﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using class_helper_rr;
using System.DirectoryServices.AccountManagement;

namespace class_helper_rr
{
    public static class CUserWindows
    {

        /// <summary>
        /// Returns user email address from active directory. 
        /// Returns currently logged-in user if param is missing.  
        /// </summary>
        /// <example>
        ///     //Current logged-in user.  Two options.  
        ///     - CUserWindows.GetEmail();
        ///     - CUserWindows.GetEmail(Environment.UserName); 
        ///     //Other user
        ///     - CUserWindows.GetEmail("Windows username");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/785617/722945
        /// </remarks>
        /// Tested: 2022-01-17
        public static string GetEmail(string userName = null)
        {
            string r = "";
            string _userName = userName.HasValueRR() ? userName : Environment.UserName;

            //Get a DirectorySearcher object.
            DirectorySearcher search = new DirectorySearcher();

            //Specify the search filter
            search.Filter = "(&(objectClass=user)(anr=" + _userName + "))";

            //Specify which property values to return in the search
            search.PropertiesToLoad.Add("mail");        // smtp mail address

            // perform the search
            SearchResult result = search.FindOne();

            try
            {
                //2022-01-18 Null check for rare situation user has no email.  Ex: Running in Dynamics AX server where the main AX master user has no email setup.  Returning null/empty string in this situation is OK.      
                if (result != null)
                    r = result.Properties["mail"][0].ToString(); //See the <remarks> URL above for fyi.  

            }
            catch { }
            
            return r;
        }


        /// <summary>
        /// Returns user first name from active directory.    
        /// Returns currently logged-in user if param is missing.  
        /// </summary>
        /// <example>
        ///     //Current logged-in user.  Two options.  
        ///     - CUserWindows.GetFirstName();
        ///     - CUserWindows.GetFirstName(Environment.UserName); 
        ///     //Other user
        ///     - CUserWindows.GetFirstName("Windows username");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/785617/722945
        /// </remarks>
        /// Tested: 2018-07-24
        public static string GetFirstName(string userName = null)
        {
            string r = "";
            string _userName = userName.HasValueRR() ? userName : Environment.UserName;

            //Get a DirectorySearcher object.
            DirectorySearcher search = new DirectorySearcher();

            //Specify the search filter
            search.Filter = "(&(objectClass=user)(anr=" + _userName + "))";

            //Specify which property values to return in the search
            search.PropertiesToLoad.Add("givenName");        // smtp mail address

            // perform the search
            SearchResult result = search.FindOne();

            try
            {
                //2022-01-18 Null check for rare situation user has no email.  Ex: Running in Dynamics AX server where the main AX master user has no email setup.  Returning null/empty string in this situation is OK.      
                if (result != null)
                    r = result.Properties["givenName"][0].ToString(); //See the <remarks> URL above for fyi.  
            }
            catch { }

            return r;
        }




        /// <summary>
        /// Returns user last name from active directory.  
        /// Returns currently logged-in user if param is missing.  
        /// </summary>
        /// <example>
        ///     //Current logged-in user.  Two options.  
        ///     - CUserWindows.GetLastName();
        ///     - CUserWindows.GetLastName(Environment.UserName); 
        ///     //Other user
        ///     - CUserWindows.GetLastName("Windows username");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/785617/722945
        /// </remarks>
        /// Tested: 2018-07-24
        public static string GetLastName(string userName = null)
        {
            string r = "";
            string _userName = userName.HasValueRR() ? userName : Environment.UserName;

            //Get a DirectorySearcher object.
            DirectorySearcher search = new DirectorySearcher();

            //Specify the search filter
            search.Filter = "(&(objectClass=user)(anr=" + _userName + "))";

            //Specify which property values to return in the search
            search.PropertiesToLoad.Add("sn");        // smtp mail address

            //Perform the search
            SearchResult result = search.FindOne();

            try
            {
                //2022-01-18 Null check for rare situation user has no email.  Ex: Running in Dynamics AX server where the main AX master user has no email setup.  Returning null/empty string in this situation is OK.      
                if (result != null)
                    r = result.Properties["sn"][0].ToString(); //See the <remarks> URL above for fyi.  
            }
            catch { }

            return r;
        }

        /// <summary>
        /// Returns email "domain" from active directory.
        /// Ex: Returns "emaildoman.com" from email "randy@emaildomain.com".  
        /// Returns currently logged-in user if param is missing.  
        /// </summary>
        /// <example>
        ///     //Current logged-in user.  Two options.  
        ///     - CUserWindows.getEmailDomain();
        ///     - CUserWindows.getEmailDomain(Environment.UserName); 
        ///     //Other user
        ///     - CUserWindows.GetEmail("Windows username");
        /// </example>
        /// Tested: 2021-01-17
        public static string getEmailDomain(string userName = null)
        {
            string r = "";
            string fullEmail = CUserWindows.GetEmail(userName); //GetEmail defaults to logged-in user if nothing is passed-in.   
            
            //2022-01-18 Null check for rare situation user has no email.  Ex: Running in Dynamics AX server where the main AX master user has no email setup.  Returning null/empty string in this situation is OK. 
            if (fullEmail.HasValueRR())
            {
                int startPosition = fullEmail.IndexOf("@") + 1;
                int remainingLength = fullEmail.Length - startPosition;
                
                r = fullEmail.Substring(startPosition, remainingLength);
            }

            return r;
        }



        /// <summary>
        /// Determines if current user belongs to a Windows Active Directory group on the company's network.
        /// </summary>
        /// <example>
        /// CUserWindows.DoesUserBelongToGroup("sample group name");
        /// 
        /// </example>
        /// <remarks>
        /// 
        /// To get list of groups current user belongs to: https://serverfault.com/a/835855
        /// 
        /// To get list of groups an "other" person belongs to: https://superuser.com/a/611843
        ///     Example: "net user randy.refsland" /domain"
        /// </remarks>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public static bool DoesUserBelongToGroup(string groupName)
        {
            var groups = UserPrincipal.Current.GetGroups().ToList();

            //From limited research and testing..."SamAccountName" is preferred over "Name".
            //UserPrincipalName only works with users (not user groups).  I tested.  
            List<Principal> groupsFiltered = groups.FindAll(p => p.SamAccountName.ToUpper() == groupName.ToUpper());

            if (groupsFiltered.CountRR() > 0)
                return true;
            return false;
        }










    }
}
