﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.helper
{
    /// <summary>
    /// Helper properties/methods related to the application.  
    /// </summary>
    public class CApp
    {
        /// <summary>
        /// Returns name of "startup project".  
        /// Useful inside RR_General to determine which project/app originated the call.      
        /// </summary>
        /// Tested: 2023-09-21
        public static string StartUpProjectName()
        {
            return System.Reflection.Assembly.GetEntryAssembly().GetName().Name; 

            //Fyi: Alternative method below works to get the original library but only looks at the immediate preceeding
            //caller project.  Calling below after multiple calls inside RR_General will return "RR_General" (we would
            //rather it return the startup project name (not RR_General)..
            //Assembly.GetCallingAssembly().GetName().Name

        }




    }
}
