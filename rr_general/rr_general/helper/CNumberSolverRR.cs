﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rr_general;

namespace class_helper_rr
{
    /// <summary>
    /// Finds all value combinations that sum to a desired value.  
    /// Can handle negative values OK.  
    /// </summary>
    /// <example>
    ///     //Populate a list of potential values to match.  
    ///     //Need to wrap each potential value inside this NumberComboItemRR class.  It provides a couple breadcrumb properties to help back-track after getting results.  
    ///     var c = new CNumberSolverRR();
    ///     var list = new List<NumberComboItemRR>()
    ///     	{
    ///     		  new NumberComboItemRR{Value = 1, Breadcrumb_String_01  = "A" }
    ///     		, new NumberComboItemRR{Value =-2, Breadcrumb_String_01  = "B" } //Notice this value is negative.  This method can handle negative numbers.  
    ///     		, new NumberComboItemRR{Value = 3, Breadcrumb_String_01  = "C" }
    ///     		, new NumberComboItemRR{Value = 4, Breadcrumb_String_01  = "D" }
    ///     		, new NumberComboItemRR{Value = 5, Breadcrumb_String_01  = "E" }
    ///     		, new NumberComboItemRR{Value = 6, Breadcrumb_String_01  = "F" }
    ///     	};
    ///
    ///     //Get every posible combination that sums to "6" with a +/-1 tolerance.  So it will accept combinations that sum from 5 thru 7.   
    ///     //Fyi: See section "DATA CONTRACT CLASSES" for returned value structure.
    ///     c.GetCombinationsWrapper(list, 6, 1).EachRR(p =>
    ///     {
    ///     	CMsgBox.Show(c.ConcatListValues(p.ReturnedList));
    ///     });
    /// </example>
    /// <remarks>
    ///     Modified from: https://stackoverflow.com/a/10739219/722945
    /// </remarks>
    /// Tested all: 2019-09-09
    public class CNumberSolverRR
    {
        /// <summary>
        /// ☆☆☆ Helpful wrapper to assign unique "ID" values to every MatchComboRR record.  Also sorts returned list in order from smallest difference to largest.  
        /// </summary>
        /// <param name="set">See GetCombinations().</param>
        /// <param name="amountGoal">See GetCombinations().</param>
        /// <param name="amountThreshold">See GetCombinations().</param>
        /// <returns></returns>
        /// Tested: 2019-09-09.
        public List<MatchComboRR> GetCombinationsWrapper(List<NumberComboItemRR> set, decimal amountGoal, decimal amountThreshold = 0)
        {
            var r = new List<MatchComboRR>();

            //Sort diff smallest to largest.  
            r = this.GetCombinations(set, amountGoal, amountThreshold)
                    .OrderBy(p => p.UnmatchedAmountAbs).ToList();

            //Just populate the ID field.  
            for (var i = 0; i < r.Count; i++)
            {
                r[i].ID = i;
            }

            return r;
        }



        /// <summary>
        /// Finds all value combinations that sum to a desired value. 
        /// </summary>
        /// <example>
        /// See class notes above.  
        /// </example>
        /// <param name="set">List of values to attempt combo matching on.  </param>
        /// <param name="amountGoal">Desired net value.  Sum of each combo should match this amount.  </param>
        /// <param name="amountThreshold">Optional: Allow for tolerance +/-. Enter positive number.  Ex: Passing amountThreshold = "1", and amountGoal ="5" will allow combinations that equal 4 thru 6.</param>
        /// <param name="_values">Always pass null or just ignore.  This is used internally for recursion.</param>
        /// <returns>List of combinations whose sum matches amountGoal (and threshold).</returns>
        /// Tested: 2019-08-11.  Positive goal, negative goal, lists containing mix of positive/negative numbers, all passed OK.  
        public IEnumerable<MatchComboRR> GetCombinations(List<NumberComboItemRR> set, decimal amountGoal, decimal amountThreshold = 0, List<NumberComboItemRR> _values = null)
        {
            //Setup the internal property.  
            if (_values == null)
                _values = new List<NumberComboItemRR>();

            //Safety: Mainly for visual sake.  
            if (amountThreshold < 0)
                CMsgBox.Show("Error with CNumberSolverRR->GetCombinations().  Param amountThreshold must zero or a positive number.  ");

            //Find all matching combos.  
            for (int i = 0; i < set.Count; i++)
            {
                //Step 1: Subtract the candidate value from our remainingAmount tally.  
                decimal remainingAmount;
                remainingAmount = amountGoal - set[i].Value;

                //Step 2: Add the candidate value to the internal "valueList" (aka "_values" param).  Keeps track of values tried.    
                List<NumberComboItemRR> valueList = _values.ToList(); //☆☆☆ Must call ".ToList()" here.  ToList() prevents "valueList" from remembering prior "for loop" values.  Very strange behavior (especially since we re-create the variable here).  Issue has to do with using yield.  Anyhow, this fixes the wierd issue.  
                valueList.Add(new NumberComboItemRR()
                {
                    Value = set[i].Value
                    , Breadcrumb_Object_01 = set[i].Breadcrumb_Object_01
                    , Breadcrumb_String_01 = set[i].Breadcrumb_String_01
                    , Breadcrumb_String_02 = set[i].Breadcrumb_String_02
                    , Breadcrumb_Long_01 = set[i].Breadcrumb_Long_01
                    , Breadcrumb_Long_02 = set[i].Breadcrumb_Long_02
                });

                //Debug: 
                //CMsgBox.Show("valueList: " + ConcatListValues(valueList) + "  remainingChild: " + remainingAmount + "   set[i].Value: " + set[i].Value);
                //CMsgBox.Show("sum: " + SumValuesMatchComboRR(valueList));

                //Add qualified combo to our return list.  
                if (Math.Abs(remainingAmount) <= amountThreshold)
                {
                    //Debug: 
                    //CMsgBox.Show("remainingChild: " + remainingAmount + "    set[i].value: " + set[i].Value + "   concat valueList: " + ConcatListValues(valueList));  

                    yield return new MatchComboRR() { ReturnedList = valueList, UnmatchedAmountAbs = remainingAmount };
                }

                //Proceed to test all other possible combinations.  
                //Take() grabs 0,1,2,3... values on each respective for loop.  
                //Note: 
                //Modified to look at all values.  Original author had "possible = set.Take(i).Where(n => n.Value <= amountGoal).ToList()" but this only works when passing-in all positive values.
                //Modified to remove the "else".  This allows the method to find more (valid) combinations. Does not duplicate combos either, I did a bunch of tests.   
                List<NumberComboItemRR> possible = set.Take(i).ToList();

                if (possible.Count > 0)
                    foreach (MatchComboRR s in GetCombinations(possible, remainingAmount, amountThreshold, valueList))
                    {
                        yield return s;
                    }


            }

        }


        //--------------------------------
        //DEBUGGING METHODS:
        //--------------------------------
        /// <summary>
        ///     Mainly for debugging.  Concatenates all "Value" properties.  
        /// </summary>
        /// <example>
        ///     See class example above.    
        /// </example>
        /// <param name="values"></param>
        /// <returns></returns>
        /// Tested: 2019-08-11
        public string ConcatListValues(List<NumberComboItemRR> values)
        {
            string r = "";

            values.EachRR(p =>
            {
                r += p.Value + ",";
            });

            return CString.removeRight(r, new[] { "," });
        }

        /// <summary>
        ///     Mainly for debugging.  Sums up all "Value" properties.  
        /// </summary>
        /// <example>
        ///     See "debugging" lines in method GetCombinations() above.    
        /// </example>
        /// <param name="list"></param>
        /// <returns></returns>
        /// Tested: 2019-08-11
        public decimal SumValuesMatchComboRR(List<NumberComboItemRR> list)
        {
            decimal r = 0;

            list.EachRR(p =>
            {
                r += p.Value;
            });

            return r;
        }



    }


    //--------------------------------
    //DATA CONTRACT CLASSES (used above).
    //--------------------------------
    //Overall structure: 
    //  GetCombinations() returns a list of MatchComboRR objects.
    //      Each MatchComboRR object has a list of NumberComboItemRR objects.  Aka a list of values that equal the goal.  
    //          Each NumberComboItemRR has an optional "Breadcrumb_XXX" properties to back-track to the original object.  


    /// <summary>
    ///     Every individual value that is tested for a combo match gets wrapped in this class.       
    ///     Provides breadcrumbs to find original objects related to each tested value.   
    /// </summary>
    /// Tested: 2019-08-11
    public class NumberComboItemRR
    {
        //Value candidate to match.  
        public decimal Value { get; set; }

        //Optional.  Additional references.   
        public object Breadcrumb_Object_01 { get; set; }

        public string Breadcrumb_String_01 { get; set; }
        public string Breadcrumb_String_02 { get; set; }

        public long Breadcrumb_Long_01 { get; set; }
        public long Breadcrumb_Long_02 { get; set; }



    }



    /// <summary>
    /// Each instantiated object contains: 
    ///     1. A list of matched items, whose sum equals the goal. 
    ///     2. The un-matched difference amount (optional, if tolerance is passed-into method GetCombinations()).  
    /// Instantiate one object per match combination found.  
    /// </summary>
    /// Tested: 2019-08-11
    public class MatchComboRR
    {
        public List<NumberComboItemRR> ReturnedList { get; set; }

        /// <summary>
        /// Remaining unmatched amount after netting goal against all items in ReturnedList.    
        /// </summary>
        public decimal UnmatchedAmountAbs { get; set; }

        /// <summary>
        /// Just a primary key if need to reference later.  
        /// </summary>
        public long ID { get; set; }
    }









}


