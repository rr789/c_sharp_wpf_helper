﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows;
using rr_general.helper;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace class_helper_rr
{
    public static class CJsonRR
    {
        /// <summary>
        ///     Extracts a specifc value from json array.  Can return any type.  Returns null if cannot convert type.
        ///     Can traverse both json curly brackets "{}" for objects, and square brackets "[]" for arrays.  When accessing arrays, pass the key integer as string (this method will convert the string to int if needed).
        ///     Note: To extract a list, use GetListFromJsonString().  
        /// </summary>
        /// <typeparam name="T">Type to return.</typeparam>
        /// <param name="jsonStr">Json string.</param>
        /// <param name="keyLevel01">Key for first level in json tree.</param>
        /// <param name="keyLevel02">Key for second level in json tree (optional).</param>
        /// <param name="keyLevel03">Key for third level in json tree (optional).</param>
        /// <param name="keyLevel04">Key for fourth level in json tree (optional).</param>
        /// <param name="keyLevel05">Key for fifth level in json tree (optional).</param>
        /// <returns>Typed value.  </returns>
        /// <example>
        ///     //JSON string five dimensions deep.  Contains objects and arrays.  
        ///     string s = "{\"key1A\":\"val1A\",\"key1B\":\"val1B\",\"key1C\":  {\"key2A\":\"val2A\", \"key2B\": {\"key3A\": \"val3A\",\"key3B\":[\"ARRAY VALUE 1\",\"ARRAY VALUE 2\",\"ARRAY VALUE 3\",\"ARRAY VALUE 4\"],\"key3C\": {\"key4A\":\"val4A\", \"key4B\": {\"key5A\":\"val5A\"} }}}}";
        ///     string r;
        ///     r = CJsonRR.GetValueFromJsonString<string>(s, "key1A"); //Returns "val1A". 
        ///     r = CJsonRR.GetValueFromJsonString<string>(s, "key1C", "key2A"); //Returns "val2A"
        ///     r = CJsonRR.GetValueFromJsonString<string>(s, "key1C", "key2B", "key3A"); //Returns "val3A".  
        ///     r = CJsonRR.GetValueFromJsonString<string>(s, "key1C", "key2B", "key3B", "0"); // Returns "ARRAY VALUE 1".   
        ///     r = CJsonRR.GetValueFromJsonString<string>(s, "key1C", "key2B", "key3C", "key4B", "key5A"); //Returns "val5A".  
        /// </example>
        /// Tested: 2018-07-14
        public static T GetValueFromJsonString<T>(string jsonStr, string keyLevel01 = "", string keyLevel02 = "", string keyLevel03 = "", string keyLevel04 = "", string keyLevel05 = "")
        {
            T r;
            //Use dynamic since json square brackets "[]" parse as "array", curly brackets "{}" parse as "object".  
            dynamic j = JsonConvert.DeserializeObject(jsonStr);

            //General types since we may change string to int.  Depends if square brackets or curly bracets are used.  
            dynamic keyLevel01_ = keyLevel01;
            dynamic keyLevel02_ = keyLevel02;
            dynamic keyLevel03_ = keyLevel03;
            dynamic keyLevel04_ = keyLevel04;
            dynamic keyLevel05_ = keyLevel05;

            //--------------------------------------
            //CONVERT KEY PARAMS TO INTEGERS (IF NEEDED).  
            //-------------------------------------- 
            //Below allows handling square bracket json strings (for "arrays").  Json arrays have KEY = integer (starts at zero), VALUE can be anything.  For json arrays, must use "integer" type (I tried string "0" etc but that did not work).     
            //Fyi: Curly bracket json strings (for "objects") requires no special preparation below.   
            if (j is JArray)
                keyLevel01_ = (int)CStringNumber.stringToDouble(keyLevel01_);

            //"try" suppresses errors if dimension(s) do not exist.    
            try
            {
                if (j[keyLevel01_] is JArray)
                    keyLevel02_ = (int)CStringNumber.stringToDouble(keyLevel02_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_] is JArray)
                    keyLevel03_ = (int)CStringNumber.stringToDouble(keyLevel03_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_][keyLevel03_] is JArray)
                    keyLevel04_ = (int)CStringNumber.stringToDouble(keyLevel04_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_] is JArray)
                    keyLevel05_ = (int)CStringNumber.stringToDouble(keyLevel05_);
            }
            catch { };

            //--------------------------------------
            //EXTRACT VALUE:
            //--------------------------------------
            //If user did not pass param for next level, then OK to return current level value.  
            if (keyLevel02 == "")
            {
                r = CType.ConvertType<T>(j[keyLevel01_]);
            }
            else
            {
                if (keyLevel03 == "")
                {
                    r = CType.ConvertType<T>(j[keyLevel01_][keyLevel02_]);
                }
                else
                {
                    if (keyLevel04 == "")
                    {
                        r = CType.ConvertType<T>(j[keyLevel01_][keyLevel02_][keyLevel03_]);
                    }
                    else
                    {
                        if (keyLevel05 == "")
                            r = CType.ConvertType<T>(j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_]);
                        else
                            r = CType.ConvertType<T>(j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_][keyLevel05_]);
                    }
                }
            }

            //For date, convert json string value into date type.  
            //String-to-date conversion needs a little extra help beyond just casting to date.  This does it OK.  
            if (r is DateTime)
            {
                r = CType.ConvertType<T>(Convert.ToDateTime(r.ToString()));
            }


            return r;
        }



        /// <summary>
        ///     Extracts a list from json array.  Returns null if cannot convert type.
        ///     Can traverse both json curly brackets "{}" for objects, and square brackets "[]" for arrays.  When accessing arrays, pass the key integer as string (this method will convert the string to int if needed).
        /// </summary>
        /// <typeparam name="T">Type to return.</typeparam>
        /// <param name="jsonStr">Json string.</param>
        /// <param name="keyLevel01">Key for first level in json tree.</param>
        /// <param name="keyLevel02">Key for second level in json tree (optional).</param>
        /// <param name="keyLevel03">Key for third level in json tree (optional).</param>
        /// <param name="keyLevel04">Key for fourth level in json tree (optional).</param>
        /// <param name="keyLevel05">Key for fifth level in json tree (optional).</param>
        /// <returns>Typed value.  </returns>
        /// <example>
        /// //Fyi: Examples use this json string.   
        /// {"data":
        ///     [
        ///          {"KEY":"VAL_01"}
        ///         ,{"KEY":
        /// 			[
        /// 				 {"KEY":"VAL_02"}
        /// 				,{"KEY":"VAL_03"}
        /// 			]
        /// 		 }
        ///     ]
        /// }
        /// 
        /// Fyi: We convert into this data contract.
        /// public class DC_TEST
        /// {
        ///     public string KEY { get; set;}
        /// }
        /// 
        /// //Now test: 
        /// string s = "{\"DATA\":[{\"KEY\":\"VAL_01\"},{\"KEY\":[{\"KEY\":\"VAL_02\"},{\"KEY\":\"VAL_03\"}]}]}";
        /// var a = CJsonRR.GetListFromJsonString<DC_TEST>(s, "DATA","1", "KEY"); //Remember that array starts at zero, so "1" is the second item.  
        /// 
        /// CMsgBox.Show(a[0].KEY); //Returns "VAL_02".  
        /// CMsgBox.Show(a[0].KEY); //Returns "VAL_03".  
        /// </example>
        /// Tested: 2020-05-28
        public static List<T> GetListFromJsonString<T>(string jsonStr, string keyLevel01 = "", string keyLevel02 = "", string keyLevel03 = "", string keyLevel04 = "", string keyLevel05 = "")
        {
            List<T> r;
            //Use dynamic since json square brackets "[]" parse as "array", curly brackets "{}" parse as "object".  
            dynamic j = JsonConvert.DeserializeObject(jsonStr);

            //General types since we may change string to int.  Depends if square brackets or curly bracets are used.  
            dynamic keyLevel01_ = keyLevel01;
            dynamic keyLevel02_ = keyLevel02;
            dynamic keyLevel03_ = keyLevel03;
            dynamic keyLevel04_ = keyLevel04;
            dynamic keyLevel05_ = keyLevel05;

            //--------------------------------------
            //CONVERT KEY PARAMS TO INTEGERS (IF NEEDED).  
            //-------------------------------------- 
            //Below allows handling square bracket json strings (for "arrays").  Json arrays have KEY = integer (starts at zero), VALUE can be anything.  For json arrays, must use "integer" type (I tried string "0" etc but that did not work).     
            //Fyi: Curly bracket json strings (for "objects") requires no special preparation below.   
            if (j is JArray)
                keyLevel01_ = (int)CStringNumber.stringToDouble(keyLevel01_);

            //"try" suppresses errors if dimension(s) do not exist.    
            try
            {
                if (j[keyLevel01_] is JArray)
                    keyLevel02_ = (int)CStringNumber.stringToDouble(keyLevel02_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_] is JArray)
                    keyLevel03_ = (int)CStringNumber.stringToDouble(keyLevel03_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_][keyLevel03_] is JArray)
                    keyLevel04_ = (int)CStringNumber.stringToDouble(keyLevel04_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_] is JArray)
                    keyLevel05_ = (int)CStringNumber.stringToDouble(keyLevel05_);
            }
            catch { };

            //--------------------------------------
            //EXTRACT LIST:
            //--------------------------------------
            //Fyi: Do not use these two methods when converting to list.  They do not work (spent a lot of time figuring this out): 
            //  CType.ConvertType<List<T>>(j[keyLevel01_]).   This only works to convert values, not lists.
            //  tempJArray.ToList<T>().  Wierd but VS throws error about "JArray does not contain a definition for ToList() even though code hinting shows the ToList() method.

            //If user did not pass param for next level, then OK to return list at current level.  
            if (keyLevel02 == "")
            {
                //Tricks VS into code hinting properties for the dynamic object.  
                JArray tempJArray = j[keyLevel01_];
                r = tempJArray.ToObject<List<T>>(); //https://gist.github.com/deostroll/9969331
            }
            else
            {
                if (keyLevel03 == "")
                {
                    JArray tempJArray = j[keyLevel01_][keyLevel02_];
                    r = tempJArray.ToObject<List<T>>(); 
                }
                else
                {
                    if (keyLevel04 == "")
                    {
                        JArray tempJArray = j[keyLevel01_][keyLevel02_][keyLevel03_];
                        r = tempJArray.ToObject<List<T>>(); 
                    }
                    else
                    {
                        if (keyLevel05 == "")
                        {
                            JArray tempJArray = j;
                            r = tempJArray.ToObject<List<T>>(j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_]); 
                        }
                        else
                        {
                            JArray tempJArray = j;
                            r = tempJArray.ToObject<List<T>>(j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_][keyLevel05_]); 
                        }
                    }
                }
            }


            return r;
        }




        /// <summary>
        /// Similar to GetListFromJsonString, just returns an ObservableCollection instead.  
        /// Fyi: ObservableCollection does a better job (compared to List) in notifying the UI when rows are added/deleted.  
        /// </summary>
        /// <typeparam name="T">Type to return.</typeparam>
        /// <param name="jsonStr">Json string.</param>
        /// <param name="keyLevel01">Key for first level in json tree.</param>
        /// <param name="keyLevel02">Key for second level in json tree (optional).</param>
        /// <param name="keyLevel03">Key for third level in json tree (optional).</param>
        /// <param name="keyLevel04">Key for fourth level in json tree (optional).</param>
        /// <param name="keyLevel05">Key for fifth level in json tree (optional).</param>
        /// <returns>Typed value.  </returns>
        /// Tested: 2020-07-29
        public static ObservableCollection<T> GetObservableCollectionFromJsonString<T>(string jsonStr, string keyLevel01 = "", string keyLevel02 = "", string keyLevel03 = "", string keyLevel04 = "", string keyLevel05 = "")
        {
            var r = GetListFromJsonString<T>
            (
                  jsonStr: jsonStr
                , keyLevel01: keyLevel01
                , keyLevel02: keyLevel02
                , keyLevel03: keyLevel03
                , keyLevel04: keyLevel04
                , keyLevel05: keyLevel05
            );

            //I tested, ObservableCollection has a constructor (used below) that will convert lists.  However, the constructor will throw error if NULL is passed-in.
            if (r == null)
                return null;

            return new ObservableCollection<T>(r);
        }
        


        /// <summary>
        ///     Counts number of child elements for a key.  
        ///     Can go up to five levels deep.   
        /// </summary>
        /// <param name="jsonStr">Json string. </param>
        /// <param name="keyLevel01">Identifier. </param>
        /// <param name="keyLevel02">Identifier (optional).</param>
        /// <param name="keyLevel03">Identifier (optional).</param>
        /// <param name="keyLevel04">Identifier (optional).</param>
        /// <param name="keyLevel05">Identifier (optional).</param>
        /// <example>
        ///     string s = "{\"key1A\":\"val1A\",\"key1B\":\"val1B\",\"key1C\":  {\"key2A\":\"val2A\", \"key2B\": {\"key3A\": \"val3A\",\"key3B\":[\"ARRAY VALUE 1\",\"ARRAY VALUE 2\",\"ARRAY VALUE 3\",\"ARRAY VALUE 4\"],\"key3C\": {\"key4A\":\"val4A\", \"key4B\": {\"key5A\":\"val5A\",\"key5B\": {\"key6A\":\"val6A\"}}}}}}";
        ///     CJsonRR.CountElements(s, "");                                           //Returns 3, all items at first level.  
        ///     CJsonRR.CountElements(s, "key1A");                                      //Returns zero since key1A has no object/array with items.  
        ///     CJsonRR.CountElements(s, "key1C");                                      //Returns 2 since key1C has an "object" with two items.  
        ///     CJsonRR.CountElements(s, "key1C", "key2B");                             //Returns 3 since key2B has has an "object" with three items.  
        ///     CJsonRR.CountElements(s, "key1C", "key2B", "key3B");                    //Returns 4 since key3B has an "array" with four items.  
        ///     CJsonRR.CountElements(s, "key1C", "key2B", "key3C", "key4B");           //Returns 2 since key4B has an "object" with one item. 
        ///     CJsonRR.CountElements(s, "key1C", "key2B", "key3C", "key4B", "key5B");  //Returns 1 since key5B has an "object" with one item.  
        /// </example>
        /// <returns>See summary.</returns>
        /// Tested: 2018-09-10 all levels, passed OK.  
        public static int CountElements(string jsonStr, string keyLevel01 = "", string keyLevel02 = "", string keyLevel03 = "", string keyLevel04 = "", string keyLevel05 = "")
        {
            int r;

            //JToken is very handy for counting JSON strings unlimited levels deep.  Can also handle mixed (object/array) JSON strings.  I tested a bunch.    
            ////https://stackoverflow.com/a/20620510/722945
            var token = JToken.Parse(jsonStr);


            //General types since we may change string to int.  Depends if square brackets or curly bracets are used.  
            dynamic keyLevel01_ = keyLevel01;
            dynamic keyLevel02_ = keyLevel02;
            dynamic keyLevel03_ = keyLevel03;
            dynamic keyLevel04_ = keyLevel04;
            dynamic keyLevel05_ = keyLevel05;

            //--------------------------------------
            //CONVERT PARAMS TO INTEGERS (IF NEEDED).  
            //--------------------------------------
            //Below allows handling square bracket json strings (for "arrays").  Json arrays have KEY = integer (starts at zero), VALUE can be anything.  For json arrays, must use "integer" type (I tried string "0" etc but that did not work).     
            //Fyi: Curly bracket json strings (for "objects") requires no special preparation below.   
            if (token is JArray)
                keyLevel01_ = (int)CStringNumber.stringToDouble(keyLevel01_);

            //"try" suppresses errors if dimension(s) do not exist.    
            try
            {
                if (token[keyLevel01_] is JArray)
                    keyLevel02_ = (int)CStringNumber.stringToDouble(keyLevel02_);
            }
            catch { };

            try
            {
                if (token[keyLevel01_][keyLevel02_] is JArray)
                    keyLevel03_ = (int)CStringNumber.stringToDouble(keyLevel03_);
            }
            catch { };

            try
            {
                if (token[keyLevel01_][keyLevel02_][keyLevel03_] is JArray)
                    keyLevel04_ = (int)CStringNumber.stringToDouble(keyLevel04_);
            }
            catch { };

            try
            {
                if (token[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_] is JArray)
                    keyLevel05_ = (int)CStringNumber.stringToDouble(keyLevel05_);
            }
            catch { };


            //--------------------------------------
            //EXTRACT/COUNT VALUE:
            //--------------------------------------
            //If user did not pass param for next level, then OK to return current level count.  
            if (keyLevel01 == "")
            {
                r = ((JToken)token).Count(); //Just count all elements at first level.
            }
            else
            {
                if (keyLevel02 == "")
                    r = ((JToken)token[keyLevel01_]).Count();
                else
                {
                    if (keyLevel03 == "")
                        r = ((JToken)token[keyLevel01_][keyLevel02_]).Count();
                    else
                    {
                        if (keyLevel04 == "")
                            r = ((JToken)token[keyLevel01_][keyLevel02_][keyLevel03_]).Count();
                        else
                        {
                            if (keyLevel05 == "")
                                r = ((JToken)token[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_]).Count();
                            else
                                r = ((JToken)token[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_][keyLevel05_]).Count();
                        }
                    }
                }
            }

            return r;
        }


        /// <summary>
        /// Simple check to ensure JSON string is valid.  
        /// </summary>
        /// <param name="jsonStr">JSON string</param>
        /// <example>
        ///     //Valid json string.  
        ///     string s = @"{
        /// 		    		""A"":{ ""_ROW_NUMBER_"":""0"",""CREATED_DATE_TIME"":""2017 - 12 - 18T00: 00:00""},
        /// 		    		""B"":{ ""_ROW_NUMBER_"":""1"",""CREATED_DATE_TIME"":""2018 - 03 - 15T00: 00:00""}
        /// 		    	}";
        ///     
        ///     CMsgBox.Show(CREX_Json_RR.IsValidJson(s)); //Returns true.
        /// </example>
        /// <returns></returns>
        /// <remarks>
        /// From: https://stackoverflow.com/questions/14977848/how-to-make-sure-that-string-is-valid-json-using-json-net
        /// </remarks>
        /// Tested: 2018-09-04
        public static bool IsValidJson(string jsonStr)
        {
            jsonStr = jsonStr.Trim();
            //Stackoverflow post mentions why this is needed.  
            if ((jsonStr.StartsWith("{") && jsonStr.EndsWith("}")) || //For object
                (jsonStr.StartsWith("[") && jsonStr.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(jsonStr);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Debug.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Debug.WriteLine(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        ///     Converts JSON string into a format Excel JSON formulas can use.  Excel JSON formulas like one object (aka array) per line.  
        ///     Basically strips-off the array square brackets "[]" and adds a line feed after each object (aka array).     
        ///     Can only handle a basic array containing objects.   
        /// </summary>
        /// <example>
        ///     String passed-in: 
        ///         [{"name1":"val1"}, {"name2":"val2"}]
        ///     String returned: 
        ///         {"name1":"val1"}
        ///         {"name2":"val2"}
        ///         Notice no square brackets "[]".  Added a line feed after each value set.  
        /// 
        /// </example>
        /// <param name="s">JSON string.</param>
        /// Tested: 2019-04-06
        public static string ConvertToExcelFormat(string s)
        {
            string r = "";

            //Safety
            if (s == null || s == "")
                return "";
            
            var arr = JsonConvert.DeserializeObject<List<dynamic>>(s);
            
            arr.EachRR(p =>
            {
                r += CString.RemoveLinefeeds(p.ToString()) + "\r";
            });
            
            return r;
        }



        /// <summary>
        ///     Determines if json string has property.  Handles up to five levels deep.  
        ///     Also handles json curly brackets "{}" for objects, and square brackets "[]" for arrays.  When accessing arrays, pass the key integer as string (this method will convert the string to int if needed).
        ///     Also handles if property exists but value is NULL.  In this case it will return true (which is correct). 
        /// </summary>
        /// <param name="jsonStr">Json string.</param>
        /// <param name="keyLevel01">Key for first level in json tree.</param>
        /// <param name="keyLevel02">Key for second level in json tree (optional).</param>
        /// <param name="keyLevel03">Key for third level in json tree (optional).</param>
        /// <param name="keyLevel04">Key for fourth level in json tree (optional).</param>
        /// <param name="keyLevel05">Key for fifth level in json tree (optional).</param>
        /// <returns>Bool</returns>
        /// <example>
        ///     //JSON string five dimensions deep.  Contains objects and arrays.  
        ///     string s = "{\"key1A\":\"val1A\",\"key1B\":\"val1B\",\"key1C\":  {\"key2A\":\"val2A\", \"key2B\": {\"key3A\": \"val3A\",\"key3B\":[\"ARRAY VALUE 1\",\"ARRAY VALUE 2\",\"ARRAY VALUE 3\",\"ARRAY VALUE 4\"],\"key3C\": {\"key4A\":\"val4A\", \"key4B\": {\"key5A\":\"val5A\"} }}}}";  
        ///     DoesPropertyExist(s, "key1C","key2B","key3C","key4B", "key5A")
        /// </example>
        /// Tested: 2019-07-03 individually testeed 1,2,3,4,5 dimension params.    
        public static bool DoesPropertyExist(string jsonStr, string keyLevel01 = "", string keyLevel02 = "", string keyLevel03 = "", string keyLevel04 = "", string keyLevel05 = "")
        {
            bool r;
            //Use dynamic since json square brackets "[]" parse as "array", curly brackets "{}" parse as "object".  
            dynamic j = JsonConvert.DeserializeObject(jsonStr);

            //General types since we may change string to int.  Depends if square brackets or curly bracets are used.  
            dynamic keyLevel01_ = keyLevel01;
            dynamic keyLevel02_ = keyLevel02;
            dynamic keyLevel03_ = keyLevel03;
            dynamic keyLevel04_ = keyLevel04;
            dynamic keyLevel05_ = keyLevel05;

            //--------------------------------------
            //CONVERT PARAMS TO INTEGERS (IF NEEDED).  
            //--------------------------------------
            //Below allows handling square bracket json strings (for "arrays").  Json arrays have KEY = integer (starts at zero), VALUE can be anything.  For json arrays, must use "integer" type (I tried string "0" etc but that did not work).     
            //Fyi: Curly bracket json strings (for "objects") requires no special preparation below.   
            if (j is JArray)
                keyLevel01_ = (int)CStringNumber.stringToDouble(keyLevel01_);

            //"try" suppresses errors if dimension(s) do not exist.    
            try
            {
                if (j[keyLevel01_] is JArray)
                    keyLevel02_ = (int)CStringNumber.stringToDouble(keyLevel02_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_] is JArray)
                    keyLevel03_ = (int)CStringNumber.stringToDouble(keyLevel03_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_][keyLevel03_] is JArray)
                    keyLevel04_ = (int)CStringNumber.stringToDouble(keyLevel04_);
            }
            catch { };

            try
            {
                if (j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_] is JArray)
                    keyLevel05_ = (int)CStringNumber.stringToDouble(keyLevel05_);
            }
            catch { };

            //--------------------------------------
            //EXTRACT VALUE:
            //--------------------------------------
            //If user did not pass param for next level, then OK to return current level value.  
            if (keyLevel02 == "")
            {
                r = CDynamic.DoesPropertyExist(j, keyLevel01_);
            }
            else
            {
                if (keyLevel03 == "")
                {
                    r = CDynamic.DoesPropertyExist(j[keyLevel01_], keyLevel02_);
                }
                else
                {
                    if (keyLevel04 == "")
                    {
                        r = CDynamic.DoesPropertyExist(j[keyLevel01_][keyLevel02_], keyLevel03_);
                    }
                    else
                    {
                        if (keyLevel05 == "")
                            r = CDynamic.DoesPropertyExist(j[keyLevel01_][keyLevel02_][keyLevel03_], keyLevel04_);
                        else
                            r =  CDynamic.DoesPropertyExist(j[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_], keyLevel05_);
                    }
                }
            }

            return r;
        }




        /// <summary>
        ///     Explodes node (in list [] format) into one string.  Ex: "[100,110]" for integers or '["VALUE_01","VALUE_02"] for strings.  
        ///     Handles lists containing all types (ex: list of strings, list of int, list of dates, list of doubles);
        ///     Can go up to five levels deep.   
        /// </summary>
        /// <param name="jsonStr">Json string. </param>
        /// <param name="keyLevel01">Identifier. </param>
        /// <param name="keyLevel02">Identifier (optional).</param>
        /// <param name="keyLevel03">Identifier (optional).</param>
        /// <param name="keyLevel04">Identifier (optional).</param>
        /// <param name="keyLevel05">Identifier (optional).</param>
        /// <example>
        ///     //TEST LIST OF INTEGERS.  
        ///     string s = "{\"key1A\":[100,110],\"key1B\":\"val1B\",\"key1C\":  {\"key2A\":[200,210], \"key2B\": {\"key3A\":[300,310],\"key3B\":[\"ARRAY VALUE 1\",\"ARRAY VALUE 2\",\"ARRAY VALUE 3\",\"ARRAY VALUE 4\"],\"key3C\": {\"key4A\":[400,410], \"key4B\": {\"key5A\":[500,510],\"key5B\": {\"key6A\":\"val6A\"}}}}}}";
        ///     JsonListToString(s, "key1A");                                 //Returns string "[100,110]".    
        ///     JsonListToString(s, "key1C","key2A");                         //Returns string "[200,210]".
        ///     JsonListToString(s, "key1C","key2B","key3A");                 //Returns string "[300,310]".
        ///     JsonListToString(s, "key1C","key2B","key3C","key4A");         //Returns string "[400,410]".
        ///     JsonListToString(s, "key1C","key2B","key3C","key4B","key5A"); //Returns string "[500,510]".  
        ///     
        ///     //TEST LIST OF STRINGS (just to show it double quotes each value).  
        ///     string s = "{\"KEY_01\":[\"SAMPLE_IN_STRING_01\",\"SAMPLE_IN_STRING_02\"]}";
        ///     JsonListToString(s, "KEY_01"); //Returns string '["SAMPLE_IN_STRING_01","SAMPLE_IN_STRING_02"]'
        /// </example>
        /// <returns>See summary.</returns>
        /// Tested: 2019-07-04 all levels, passed OK.  
        public static string JsonListToString(string jsonStr, string keyLevel01 = "", string keyLevel02 = "", string keyLevel03 = "", string keyLevel04 = "", string keyLevel05 = "")
        {
            string r;

            //JToken is very handy for counting JSON strings unlimited levels deep.  Can also handle mixed (object/array) JSON strings.  I tested a bunch.    
            ////https://stackoverflow.com/a/20620510/722945
            var token = JToken.Parse(jsonStr);


            //General types since we may change string to int.  Depends if square brackets or curly bracets are used.  
            dynamic keyLevel01_ = keyLevel01;
            dynamic keyLevel02_ = keyLevel02;
            dynamic keyLevel03_ = keyLevel03;
            dynamic keyLevel04_ = keyLevel04;
            dynamic keyLevel05_ = keyLevel05;

            //--------------------------------------
            //CONVERT PARAMS TO INTEGERS (IF NEEDED).  
            //--------------------------------------
            //Below allows handling square bracket json strings (for "arrays").  Json arrays have KEY = integer (starts at zero), VALUE can be anything.  For json arrays, must use "integer" type (I tried string "0" etc but that did not work).     
            //Fyi: Curly bracket json strings (for "objects") requires no special preparation below.   
            if (token is JArray)
                keyLevel01_ = (int)CStringNumber.stringToDouble(keyLevel01_);

            //"try" suppresses errors if dimension(s) do not exist.    
            try
            {
                if (token[keyLevel01_] is JArray)
                    keyLevel02_ = (int)CStringNumber.stringToDouble(keyLevel02_);
            }
            catch { };

            try
            {
                if (token[keyLevel01_][keyLevel02_] is JArray)
                    keyLevel03_ = (int)CStringNumber.stringToDouble(keyLevel03_);
            }
            catch { };

            try
            {
                if (token[keyLevel01_][keyLevel02_][keyLevel03_] is JArray)
                    keyLevel04_ = (int)CStringNumber.stringToDouble(keyLevel04_);
            }
            catch { };

            try
            {
                if (token[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_] is JArray)
                    keyLevel05_ = (int)CStringNumber.stringToDouble(keyLevel05_);
            }
            catch { };


            //--------------------------------------
            //EXTRACT/COUNT VALUE:
            //--------------------------------------
            //If user did not pass param for next level, then OK to return current level count.  
            if (keyLevel01 == "")
            {
                r = ((JToken)token).ToString(); 
            }
            else
            {
                if (keyLevel02 == "")
                    r = ((JToken)token[keyLevel01_]).ToString()  ;
                else
                {
                    if (keyLevel03 == "")
                        r = ((JToken)token[keyLevel01_][keyLevel02_]).ToString();
                    else
                    {
                        if (keyLevel04 == "")
                            r = ((JToken)token[keyLevel01_][keyLevel02_][keyLevel03_]).ToString();
                        else
                        {
                            if (keyLevel05 == "")
                                r = ((JToken)token[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_]).ToString();
                            else
                                r = ((JToken)token[keyLevel01_][keyLevel02_][keyLevel03_][keyLevel04_][keyLevel05_]).ToString();
                        }
                    }
                }
            }

            return r;
        }














    }
}
