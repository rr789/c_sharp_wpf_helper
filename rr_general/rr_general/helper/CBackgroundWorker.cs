﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;


/* Simplifies using a background worker.  
 * Has safety to ignore running the same job again if prior job is not finished.    
 * Tested: 2012-02-08 all passed.  
 * EXAMPLE:
        CBackgroundWorker bw = new CBackgroundWorker(); 

        //-------------------------------
        //DEFINE TASKS TO RUN IN BACKGROUND.
        //-------------------------------
        //Can "read/access" (but not modify) objects on main thread. 
        bw.work = delegate()
        {
            //Works OK ("reading" value from main thread).
            CMsgBox.Show(this.timeLeftLabel.Text); 

            //Does NOT work (can't "modify" objects on main thread).  
            this.timeLeftLabel.Text = "set while working"; //Won't work because you can't modify another thread.  

            //Optional: Passed to onComplete method.  
            bw.returnValue = "something to return";

            //Can delete this (just pretends it takes a long time to finish).    
            System.Threading.Thread.Sleep(2000);
        };

        //-------------------------------
        //WHEN WORKER IS FINISHED.  
        //-------------------------------
        //This has full access to the main thread.  It can "read/access/modify" main thread objects OK.  
        bw.onComplete = delegate()
        {
            //Modify a label that is on the main GUI thread.
            this.timeLeftLabel.Text = "set after working";

            //Optional: Show the returned value passed from Worker.  
            CMsgBox.Show(bw.returnValue.ToString());
        };

        //Start the background worker (to do steps above).  
        bw.run();
*/
namespace class_helper_rr
{
    public class CBackgroundWorker
    {
        public BackgroundWorker bw;
        public Action work; //Custom "anonymous method" I define.  All of the stuff to "work on" goes here.  Operates on a separate thread.    
        public Action onComplete; //Custom "anonymous method" I define.  Gets called when "work()" is finished.  Operates on main thread.

        public object returnValue; //Optional: Allows passing values between work() and onComplete().  
        private bool isWorkerFinished = true;


        //Constructor
        public CBackgroundWorker()
        {
            this.bw = new BackgroundWorker();
            this.bw.WorkerReportsProgress = true; //Not needed.
            this.bw.DoWork += new DoWorkEventHandler(this.bw_DoWork);
            this.bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.bw_RunWorkerCompleted);
        }

        //Just calls our anonymous custom function.  
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            this.isWorkerFinished = false; //Prevents running worker if prior job is not finished.  Gets set to True when done.  
            this.work();
        }


        //Just calls our anonymous custom function.  
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.onComplete != null)
                this.onComplete();

            this.isWorkerFinished = true;
        }

        //Runs the background worker.
        public void run()
        {
            if (this.isWorkerFinished)
            {
                this.bw.RunWorkerAsync(null); //null param since we don't need to pass arguments to the worker (worker thread can already "see" all objects on the main thread anyhow).  
            }
        }



        //------------------------------------------
        //STATIC OPTIONAL METHODS.  
        //------------------------------------------

        /// <summary>
        /// Quick way to run a background job.  
        /// </summary>
        /// <example>
        /// CBackgroundWorker.runTaskStatic(
        ///     //Define tasks.  
        ///     delegate
        ///     {
        ///         File.WriteAllText(filePath, txt);
        ///     },
        ///     //Optional, when finished.  
        ///     delegate 
        ///     {
        ///        CMsgBox.Show(filePath);
        ///     }
        /// );
        /// </example>
        /// <param name="taskToRun"></param>
        /// <param name="taskCompleted"></param>
        /// Tested: 2017-07-01.
        public static void runTaskStatic(Action taskToRun, Action taskCompleted = null)
        {
            CBackgroundWorker bw = new CBackgroundWorker();

            bw.work = taskToRun;

            if (taskCompleted != null)
                bw.onComplete = taskCompleted;

            bw.run();
        }






    }
}
