﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace class_helper_rr
{
    public class CDate
    {
        /// <summary>
        /// Returns last day (date) of the month.  
        /// </summary>
        /// <example>
        /// CDate.GetLastDayOfMonth(Convert.ToDateTime("2019-09-15")); //Returns date "2019-09-30".
        /// </example>
        /// <param name="_date"></param>
        /// <returns></returns>
        /// Tested: 2019-10-01
        public static DateTime GetLastDayOfMonth(DateTime _date)
        {
            //https://stackoverflow.com/a/4655207/722945
            return new DateTime(_date.Year, 
                                _date.Month, 
                                DateTime.DaysInMonth(_date.Year, _date.Month));
        }



    }
}
