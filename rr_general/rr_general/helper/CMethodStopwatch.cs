﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace class_helper_rr
{
    public class CMethodStopwatch
    {

        /// <summary>
        /// Simple way to time how long a method takes to execute.  
        /// </summary>
        /// <remarks>
        /// CMsgBox.Show(CMethodStopwatch.getTimeToExecuteInMs(this.myMethod).ToString());
        /// Tested: 2012-03-17 all passed.
        static public double getTimeToExecuteInMs(Action myMethod)
        {
            DateTime startTime = DateTime.Now;
            myMethod();
            return (DateTime.Now - startTime).TotalMilliseconds;
        }

    }
}