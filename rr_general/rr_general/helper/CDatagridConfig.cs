﻿using rr_general.config;
using rr_general.wpf_custom_control;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace class_helper_rr
{
    /// <summary>
    /// Applies user profile preferences to DataGridRR.   
    /// </summary>
    public class CDatagridConfig
    {
        /// <summary>
        /// Grid that owns the config file.  Aka grid shown to user.  
        /// </summary>
        public DataGridRR grid;

        /// <summary>
        /// Config info from hard drive file.  
        /// To access collection, use "myObj.disk.readDataTyped()".
        /// </summary>
        public CDataStore<List<DC_ConfigRecord>> disk;

        /// <summary>
        /// Constructor.  
        /// </summary>
        /// <param name="grid_">Grid that owns the config file. </param>
        public CDatagridConfig(DataGridRR grid_)
        {
            grid = grid_;

            //Path to directory storing CDataStore file.
            string dir = ConfigUserProfile.getUserProfileDataGridDirectory();

            //Load from hard drive.  
            disk = new CDataStore<List<DC_ConfigRecord>>(grid.profileGridIdentifierID, dir);
            if (disk.doesFileExist())
                copyDiskToGrid();
            else
                copyGridToDisk();
            
           


            //------------------------------
            //Grid Event: Save setting when user moves columns.  
            //------------------------------
            this.grid.ColumnReordered += delegate
            {
                copyGridToDisk();
            };

            //------------------------------
            //Grid Event: Save setting when column width changes.  
            //------------------------------
            this.grid.ColumnWidthChangedRR += delegate
            {
                copyGridToDisk();
            };



        }

        /// <summary>
        /// Copies grid settings to hard drive.  
        /// Typically used first time a grid is created.  
        /// </summary>
        /// Tested: 2017-07-01.
        public void copyGridToDisk()
        {
            //Gets written to disk.   
            var data = new List<DC_ConfigRecord>();

            grid.Columns.EachRR(col =>
            {
                //Safety: Cancel loading settings if this method is called too early. 
                //Sometimes (and it happens randomly) but when app first loads, this method could get called "before" the datagrid has set DisplayIndex properties on all columns.  
                //So we want to prevent saving a config file where "display index" properties equal -1...since -1 values will cause copyDiskToGrid() to throw error.
                //More info here: https://stackoverflow.com/a/2127935/722945
                if (col.DisplayIndex == -1)
                    return;

                //Safety for rare situation where no columns have any header text.  Mainly when developing and adding a brand new table.  
                if (col.Header != null)
                {
                    var rec = new DC_ConfigRecord()
                    {
                        columnOrder = col.DisplayIndex,
                        visible = col.Visibility == Visibility.Visible ? true : false,
                        width = col.Width,
                        headerTitle = col.Header.ToString()
                    };

                    data.Add(rec);
                }

            });

            //Re-sort.  Mainly for visual sake if looking at config files.  
            data = data.OrderBy(p=>p.columnOrder).ToList();

            //Save to hard drive.
            //Try/catch only needed in extreme situations.  I am adding it now on 2020-04-13 after a user tried opening six apps at the same time.  
            CTryCatchWrapper.wrap(() =>
            {
                disk.writeToDisk(data);
            });
            
            

        }


        /// <summary>
        /// Applies settings from hard drive onto grid.  
        /// </summary>
        /// Tested: 2019-10-27
        public void copyDiskToGrid()
        {
            List<DC_ConfigRecord> diskRead = disk.readFromDisk();

            if (diskRead.Count == 0)
                return;

            //Loop over disk records to get a 100% guaranteed restore of column order in the visual grid.  
            //I previously looped over visual grid columns, but it did not restore column order 100% of the time due to changing one column affected placement of another etc.  
            for (var i = 0; i < diskRead.Count; i++)
            {
                DC_ConfigRecord diskRec = diskRead[i];

                var gridCol = grid.GetColumnByHeaderTitle(diskRec.headerTitle);

                //Safety: Namely when new columns are added to WPF.  Also if grid loaded for first time with no profile.
                if (gridCol == null)
                    continue;

                //Safety: WPF requires the DisplayIndex be < column count.  So if a XAML column is removed in designer, it could try to assign a value above threshold and throw error. 
                //This potential error typically happens with last column anyhow, so almost all prior column settings will remain intact OK.  
                if (diskRec.columnOrder >= grid.Columns.Count)
                    continue;

                //Update grid's column settings.  
                gridCol.DisplayIndex = diskRec.columnOrder;
                gridCol.Visibility = diskRec.visible == true ? Visibility.Visible : Visibility.Hidden;
                gridCol.Width = diskRec.width;
                //p.dataFieldName not needed.  Remains the same.

            };

            //Do once at very end, in case app added/deleted columns. 
            //Try/catch only needed in extreme situations.  I am adding it now on 2020-04-13 after a user tried opening six apps at the same time.  
            CTryCatchWrapper.wrap(() =>
            {
                copyGridToDisk();
            });

            //Needed to update internal column ActualWidth property.  
            this.grid.UpdateLayout();

        }




    }



    /// <summary>
    /// Data contract.  
    /// </summary>
    public class DC_ConfigRecord
    {
        /// <summary>
        /// Display order for column.  Zero based index. 
        /// </summary>
        public int columnOrder { get; set; }

        public bool visible { get; set; }

        /// <summary>
        /// Column width.  Note: Cannot handle Int/Double.  Needs DataGridLength type.  
        /// </summary>
        public DataGridLength width { get; set; } 

        /// <summary>
        /// Header title is better than binding field name.  Esp since user will see this on the config screen.   
        /// Aka a unique identifier for a column.  
        /// </summary>
        public string headerTitle { get; set; }


        /// <summary>
        /// Placeholder for DataGridRR.  DataGridRR requires this value to know if OK for user to edit a record.   
        /// </summary>
        public bool IS_DATAGRID_ROW_EDITABLE_RR { get; set; } = true; 
    }




}
