﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace class_helper_rr
{

    /* Helper class for the Windows Forms Timer.  
       Main Purpose: Easy timer creation that does NOT repeat unexpectedly.  
            Fyi: Defining delegates via "timerObj.Tick += delegate..." actually adds another method for the timer to call (so if you define delegates 5 times on the same timer object, then the method will get called 5 times).  Our class here fixes it OK so it only calls once.   
       Fyi: stopTimer() resets the clock (doesn't mean "pause").  I tested OK.    
       Example:
            //Constructor options:
                CTimerForms t = new CTimerForms(2000);  //Runs every 2 seconds forever (since second parameter not defined).  
                CTimerForms t = new CTimerForms(2000,2); //Runs every 2 seconds and stops after 2 iterations.  
            //Callback options:
                t.callback = myCustomMethod; //Custom method that executes every XX seconds.  Define it like a normal method as usual.  
                t.callback = delegate {code here;}; //Another way.  Notice we can use "=" instead of "+=" (which is nice).  
                Fyi: Also see hasIntervalMsPassed() below for a handy way to execute code at a different interval than Timer is setup with.  
            //Start/stop:
                t.startTimer();
                t.stopTimer();//Not needed since we defined this to run twice then stop (in the arguments).   
       Tested: 2012-02-05 all passed.  
     */
    public class CTimerForms
    {
        /// <summary>
        /// Attach custom method to call every interval. 
        /// </summary>
        public Action callback;

        /// <summary>
        /// Timer will stop when this is reached. 
        /// </summary>
        public int timesToRepeat;

        /// <summary>
        /// Main timer object.  
        /// </summary>
        public System.Windows.Forms.Timer timerObj;


        /// <summary>
        /// Setup object.  
        /// </summary>
        /// <param name="intervalMs">Internal in milliseconds.  Ex: 2000 = 2 seconds.  </param>
        /// <param name="oTimesToRepeat">Times to iterate.  Then timer will stop automatically.</param>
        /// Tested: 2017-05-08 all passed.  
        public CTimerForms(int intervalMs, int oTimesToRepeat = 0)
        {
            this.timesToRepeat = oTimesToRepeat;

            this.timerObj = new System.Windows.Forms.Timer();
            this.timerObj.Interval = intervalMs;
            this.timerObj.Tick += delegate { this._eventHandler(); }; //Fyi: This is the main benefit of this class.  The "+= delegate" is only defined ONCE in the constructor (so no risk of adding the same method multiple times).    

        }


        /// <summary>
        /// Number of intervals run already.  
        /// </summary>
        public int countTimesRun = 0; //Keep public, this is a handy property.

        /// <summary>
        /// Handles the timer events & calls our "custom callback method".
        /// </summary>
        /// Tested: 2012-02-05 all passed. 
        private void _eventHandler()
        {
            //Run if under max times to repeat.  Also run if zero (which means unlimited).  
            if ((this.countTimesRun < this.timesToRepeat) || (this.timesToRepeat == 0))
            {
                this.callback();
                this.countTimesRun++;
            }
            else
            {
                this.stopTimer();
            }
        }

        //Tested: 2012-02-05 all passed.  
        public void startTimer()
        {
            this.stopTimer();//Reset the clock (in case start() is called many times quickly, just in case).  
            this.countTimesRun = 0;//Reset it.  
            this.timerObj.Start();
        }


        /// <summary>
        /// Stops (and resets timer clock if want to start again).  
        /// </summary>
        /// <remarks>
        /// About Stop resetting timer clock: http://stackoverflow.com/a/1042317/722945
        /// </remarks>
        /// Tested: 2012-02-05 all passed.  
        public void stopTimer()
        {
            this.timerObj.Stop();
        }

        //Tested: 2012-02-05 all passed.  
        public void stopAndDisposeTimer()
        {
            this.timerObj.Stop();
            this.timerObj.Dispose();
        }



        /// <summary>
        /// Handy way to do an action after so many milliseconds has passed.  
        /// Use this within the custom callBack method. 
        /// </summary>
        /// <remarks>
        /// If param is less than the timer interval (which is rare), then this just returns True at every timer interval (ex: param is 500ms but timer interval is 10000ms, then this returns true when the timer runs every 10000ms OK).  Very flexible. 
        /// Works even if param is not a perfect divisor to the timer interval (very flexible). 
        /// </remarks>
        /// <example>
        /// //How to execute code every 5 seconds within a timer that runs every 100ms.
        /// CTimerForms t = new CTimerForms(100);
        /// t.callback = delegate 
        /// {
        /// 	//Put regular code here to execute every 100ms interval.
        /// 
        /// 	//This code runs once every 5 seconds.  
        /// 	if (t.hasIntervalMsPassed(5000)) //Returns true at every 5 second mark.
        /// 	{
        /// 		CMsgBox.Show("Called every 5 seconds");
        /// 	}
        /// };
        /// t.startTimer();
        /// </example>
        /// <param name="intervalMilliseconds"></param>
        /// <returns></returns>
        /// Tested: 2012-02-18 on many different scenarios, all passed.
        public bool hasIntervalMsPassed(int intervalMilliseconds)
        {
            bool returnValue = false;
            //How many times does timer need to run to equal param value.  
            //Ex: Param = 500ms, Timer Interval = 100ms, then it takes 5 timer runs to equal the param.  
            int runTimesNeededToEqualParam = (int)(intervalMilliseconds / this.timerObj.Interval);
            runTimesNeededToEqualParam = runTimesNeededToEqualParam == 0 ? 1 : runTimesNeededToEqualParam; //Safety: Prevents "dividing by zero error" since (int) cast above will round to whole number.  

            if (this.countTimesRun % runTimesNeededToEqualParam == 0) //Modulus, returns zero if no division remainder.    
            {
                returnValue = true;
            }

            return returnValue;
        }







    }
}
