﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace rr_general.helper
{
    
    /// <summary>
    /// Catches all unhandled exceptions.  Logs stack trace before allowing exception to crash app.    
    /// Note: Local variable dump may not work.  I checked many Stackoverflow posts, local variable dumps are surprisingly tricky.  
    /// </summary>
    /// <example>
    /// //In MainWindow.xaml.cs->constructor, instantiate the class.  That's it.  This subscribes to the global "unhandled exception" event automatically.  
    /// var exceptionObj = new CExceptionLogger();
    /// 
    /// //To test an exception for debugging, use one of the Debug methods in this class.  
    /// CExceptionLogger.Debug_ThrowException_FromMainUIThread();
    /// </example>
    /// <remarks>
    /// There are multiple types of exceptions to handle.  However, see test results below.  Only need to subscribe to one event to catch all exceptions (in main UI thread, and background threads).
    /// 
    /// Test results: 
    /// --------------                                          ----------------------------                        --------------
    /// Exception type                                          Event subscribed to                                 Result
    /// --------------                                          ----------------------------                        --------------
    /// Debug_ThrowException_FromBackgroundRawThread            Application.Current.DispatcherUnhandledException    Did NOT catch exception.  
    /// Debug_ThrowException_FromBackgroundRawThread            AppDomain.CurrentDomain.UnhandledException          Catched exception OK.  
    /// 
    /// Debug_ThrowException_FromBackgroundTaskThread           Application.Current.DispatcherUnhandledException    Catched exception OK (which is a little surprising since Task runs in background thread).   
    /// Debug_ThrowException_FromBackgroundTaskThread           AppDomain.CurrentDomain.UnhandledException          Catched exception OK. 
    /// 
    /// Debug_ThrowException_FromMainUIThread                   Application.Current.DispatcherUnhandledException    Catched exception OK.   
    /// Debug_ThrowException_FromMainUIThread                   AppDomain.CurrentDomain.UnhandledException          Catched exception OK. 
    /// 
    /// Helpful links: 
    ///     Best: 
    ///     https://www.codeproject.com/Articles/90866/Unhandled-Exception-Handler-For-WPF-Applications  Excellent guide, with demo to create background exceptions. 
    ///     
    ///     Worth noting but see my testing above.   We do NOT need to subscribe to all four events. 
    ///     https://stackoverflow.com/a/1472562/722945  
    ///     https://stackoverflow.com/a/40295384/722945  Wrapper for Stackoverflow above.  
    /// </remarks>
    /// Tested: 2020-08-24
    public class CExceptionLogger
    {
        public CExceptionLogger()
        {
            //Subscribe to exceptions from both:  
            //- Main UI thread.
            //- Background threads.  
            AppDomain.CurrentDomain.UnhandledException += (sender, args) => CurrentDomainOnUnhandledException(args);  
        }


        /// <summary>
        /// Main exception handler.  Logs exception.  Displays message.  
        /// </summary>
        /// <remarks>
        /// From https://stackoverflow.com/a/40295384/722945  although we do not need to subscribe to all four exception events.  See testing notes up-top.  
        /// </remarks>
        /// Tested: 2020-08-24
        private void CurrentDomainOnUnhandledException(UnhandledExceptionEventArgs args)
        {
            var exception = args.ExceptionObject as Exception;

            if (exception != null)
            {
                //Showing a messagebox risks hanging application.  We'll try it for now since most examples show messagebox.  See comment under this answer: https://stackoverflow.com/a/5401483/722945
                //Note: Have not got much data from "variable value dump" but including for heck of it.  
                string msg = "Exception saved to log file for debugging.  \n\nException message:\n" + exception.Message + "\n\nStack trace:\n" + exception.StackTrace + "\n\nVariable value dump:\n" + exception.Data.ToJsonRR();
                CLogger.CreateLogEntry(msg, true, false);
            }

            //Fyi: If using "args.Handled = true" then need to ensure app is shutdown gracefully.    
            //  - https://stackoverflow.com/a/27355767/722945 mentions how to gracefully shut down app.  The project this post mentions uses "Handled = true".  
            //  - https://stackoverflow.com/a/5401483/722945  See comment under this answer.  


        }


        //-----------------------------------
        //DEBUG METHODS TO TEST IN MAIN UI THREAD/BACKGROUND THREAD.  
        //-----------------------------------

        /// <summary>
        /// Throws exception from Main UI thread.  
        /// </summary>
        /// Tested: 2020-08-24
        public static void Debug_ThrowException_FromMainUIThread()
        {
            //Throw divide by zero error.
            var a = 100/ "0".ToInt32();
        }



        /// <summary>
        /// Throws exception from background thread (using "Thread" class, not "Await/Task").  
        /// </summary>
        /// <remarks>
        /// See notes up-top about why I needed to create this.  The "UI thread exception catcher" was able to catch Await/Task exceptions.  It was not able to catch "Thread" exceptions like this method below creates.  So this provides a better background thread test.  
        /// </remarks>
        /// Tested: 2020-08-24
        public static void Debug_ThrowException_FromBackgroundRawThread()
        {
            //From demo project in https://www.codeproject.com/Articles/90866/Unhandled-Exception-Handler-For-WPF-Applications
            Thread newThread;
   
            newThread = new Thread(new ThreadStart(()=>
            {
                //Throw divide by zero error.
                var a = 100/ "0".ToInt32();
            }));

            newThread.Start();
        }


        /// <summary>
        /// Throws exception from "Await/Task".  
        /// </summary>
        /// Tested: 2020-08-24
        public static async void Debug_ThrowException_FromBackgroundTaskThread()
        {
            await Task.Run(() =>
            {
                //Throw divide by zero error.
                var a = 100/ "0".ToInt32();
            });
        }





    }
}
