﻿using class_helper_rr;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.helper
{
    public class CLogger
    {  
        /// <summary>
        /// Returns path to log directory.  
        /// Ex: "C:\Users\randy\AppData\Local\ThisAppName_app\2023\01"
        /// </summary>
        static string directoryPath = CDirectory.ConcatDirectory(CDirectory.GetDirLocal_StartupProject(), "Log", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"));


        /// <summary>
        ///     Writes log file to "C:\Users\randy\AppData\Local\ThisAppName_app\log\2023\01\31.txt".  //YYYY\MM\DD.txt
        ///     Has options to show message box and throw exception.  
        /// </summary>
        /// <example>
        ///     CLogger.CreateLogEntry("hello there");
        /// </example>
        /// <param name="logText">Message to write.</param>
        /// <param name="showMsgBox">True: Show popup. </param>
        /// <param name="throwException">True: Will throw an exception error and halt the program. </param>
        /// <remarks>
        ///     Modified from: https://docs.microsoft.com/en-us/dotnet/standard/io/how-to-open-and-append-to-a-log-file
        /// </remarks>
        ///     Tested: 2023-09-21
        public static void CreateLogEntry(string logText, bool showMsgBox = false, bool throwException = false)
        {
            //Creates directory if not already exist.  
            Directory.CreateDirectory(directoryPath);


            string fullPath = CDirectory.ConcatDirectoryAndFilePath(directoryPath, DateTime.Now.ToString("dd") + ".txt");

            //Extra safety, in case file is in use.  We're OK with a couple dropped logs, not the end of the world.  
            CTryCatchWrapper.wrap(delegate ()
            {
                using (StreamWriter w = File.AppendText(fullPath))
                {
                    string log_ = "\r\n------------------------\r\n"
                                    + DateTime.Now.ToString("yyyy-MM-dd") + "  " + DateTime.Now.ToLongTimeString()
                                    + "\r\n"
                                    + logText;

                    w.Write(log_);
                }
            }, false,false,false); //No need to draw attention if writing log file failed.  


            if (showMsgBox)
                CMsgBox.Show(logText);

            if (throwException)
                throw new Exception(logText);

        }



        /// <summary>
        /// Opens log file from "C:\Users\randy\AppData\Roaming\ThisAppName_app\log_r.txt". 
        /// </summary>
        /// Tested: 2018-09-09
        public static void OpenLog()
        {
            //https://stackoverflow.com/a/1283593/722945
            System.Diagnostics.Process.Start(directoryPath);
        }



    }
}
