﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.helper
{
    public static class CUrl
    {
        
        /// <summary>
        /// Gets string position from URL after the port number.
        /// </summary>
        /// <example>
        /// CUrl.GetPositionAfterPort(@"server_name:5580/subdir01/subdir02"); //Returns 16
        ///                                             ↑ returns this position.  
        /// </example>
        /// <param name="fullUrl"></param>
        /// <returns></returns>
        /// Tested: 2019-10-06 all url types.
        public static int GetPositionAfterPort(string fullUrl)
        {
            if (!fullUrl.HasValueRR())
                return CGlobal.ErrorNumber_Int;

            //Position of first slash after port.  
            int breakPosition = CGlobal.ErrorNumber_Int;

            //For AIF url.
            //Sample address..."net.tcp://HOSTNAME:8223/DynamicsAx/Services/SERVICE_GROUP_CREX_AIF_VendorRR"
            if (CString.StartsWith(fullUrl, @"net.tcp://") )
                breakPosition = CString.SubstringPositionAtOccurence(fullUrl, @"/", 3);
            
            //For regular http url. 
            //Example: http://server_name:5580/subdir01/subdir02
            else if (CString.StartsWith(fullUrl, @"http://") )
                breakPosition = CString.SubstringPositionAtOccurence(fullUrl, @"/", 3);

            //For URL without the "http://" prefix. 
            //Example: server_name:5580/subdir01/subdir02
            else 
                breakPosition = CString.SubstringPositionAtOccurence(fullUrl, @"/", 1);


            return breakPosition;
        }
        

        /// <summary>
        /// Returns substring after port.  
        /// </summary>
        /// <example>
        /// CUrl.GetSubstringAfterPort(@"http://server_name:5580/subdir01/subdir02"); //Returns "/subdir01/subdir02".
        /// </example>
        /// <param name="fullUrl"></param>
        /// <returns></returns>
        /// Tested: 2019-10-06 all url types.
        public static string GetSubstringAfterPort(string fullUrl)
        {
            if (!fullUrl.HasValueRR())
                return "";
            
            //Position of first slash after port.  
            int breakPosition = GetPositionAfterPort(fullUrl);
            
            return fullUrl.Substring(breakPosition);
        }


        /// <summary>
        /// Returns substring from first character thru port. 
        /// </summary>
        /// <example>
        /// CUrl.GetSubstringServerAndPort(@"http://server_name:5580/subdir01/subdir02"); //Returns "http://server_name:5580" no trailing slash.  
        /// </example>
        /// <param name="fullUrl"></param>
        /// <returns></returns>
        /// Tested: 2019-10-06 all url types.
        public static string GetSubstringServerAndPort(string fullUrl)
        {
            if (!fullUrl.HasValueRR())
                return "";

            //Position of first slash after port.  
            int breakPosition = GetPositionAfterPort(fullUrl);

            return fullUrl.Substring(0, breakPosition);

        }


        /// <summary>
        /// Concatenates two URL substrings.  Handles trailing/leading slashes. 
        /// </summary>
        /// <example>
        /// //All three examples below return the same string "http://server_name:5580/subdir01".
        /// CUrl.ConcatTwoSubstrings(@"http://server_name:5580/", "/subdir01");  
        /// CUrl.ConcatTwoSubstrings(@"http://server_name:5580", "/subdir01"); 
        /// CUrl.ConcatTwoSubstrings(@"http://server_name:5580", "subdir01"); 
        /// </example>
        /// <param name="leftString"></param>
        /// <param name="rightString"></param>
        /// <returns></returns>
        /// Tested: 2019-10-06 all url types.
        public static string ConcatTwoSubstrings(string leftString, string rightString)
        {
            if (!leftString.HasValueRR() || !rightString.HasValueRR())
                return "";
            
            //Remove any slashes
            string _leftString = CString.removeRight(leftString, new[] { @"/"});
            string _rightString = CString.removeLeft(rightString, new[] { @"/"});

            return _leftString + @"/" + _rightString;
        }





    }
}
