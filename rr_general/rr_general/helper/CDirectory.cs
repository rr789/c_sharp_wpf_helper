﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using class_helper_rr;
using System.Windows;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using rr_general.helper;
using System.Linq.Expressions;
using System.Reflection;

namespace class_helper_rr
{
    public static class CDirectory
    {
        /// <summary>
        /// Gets folder directory from full file path.
        /// Handles both forward and backward slashes.  
        /// </summary>
        /// <example>
        /// var a = @"C:\Users\randy\AppData\Roaming\my_app_name\myfile.txt";
        /// CDirectory.GetFilenameFromPath(a); //returns "C:\Users\randy\AppData\Roaming\my_app_name" (no trailing slash).
        /// </example>
        /// <param name="fullpath"></param>
        /// <returns></returns>
        /// Tested: 2020-06-15
        public static string GetDirectoryFromPath(string fullPath)
        {
            if (fullPath == null) return null;
            
            //So we can handle both slashes.  
            string _fullPath = fullPath.Replace(@"\", @"/");
            
            var delimiters = new List<string> { @"/" };
            var myList = CString.ParseWithDelimiters(_fullPath, delimiters);

            int lastSlashPosition = CString.SubstringPositionAtOccurence(_fullPath, @"/", (int)myList.CountRR()-1);

            //Safety:
            //Substring does not like -1.  
            if (lastSlashPosition == -1)
                return null;

            //Use orignal path passed-in.  This will retain the original forward/backward slashes.  
            return fullPath.Substring(0, lastSlashPosition);
        }







        //--------------------------
        //ROAMING DIRECTORY: 
        //--------------------------
        /// <summary>
        /// Path to current user AppData\Roaming directory
        /// Ex: "C:\Users\randy\AppData\Roaming". 
        /// </summary>
        /// <remarks>
        /// Diff between local and roaming: https://stackoverflow.com/a/9709394/722945 
        /// - Local: 
        ///     - Meant to store cached data/large files.   
        ///     - Does not sync between computers.
        /// - Roaming: 
        ///     - Meant to store user settings.  
        ///     - Files in roaming will sync between computers if network admin has enabled it.  
        ///     - Keep roaming files small since large files will increase Windows login time.  
        /// </remarks>
        /// Tested: 2020-06-21
        public static string GetDirRoam()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }


        /// <summary>
        /// Path to current user AppData\Roaming directory\App name (startup project name).
        /// "_app" is appended to end for uniqueness.  
        /// Ex: "C:\Users\randy\AppData\Roaming\ThisAppName_app".  
        /// </summary>
        /// Tested: 2021-07-16 
        public static string GetDirRoam_StartupProject()
        {
            string r = GetDirRoam() +@"\" +  CApp.StartUpProjectName() + "_app"; //"_app" helps avoid name conflict with other apps.
            
            Directory.CreateDirectory(r);
            
            return r;
        }

        //--------------------------
        //LOCAL DIRECTORY: 
        //--------------------------
        /// <summary>
        /// Path to current user AppData\Local directory.  
        /// Ex: "C:\Users\randy\AppData\Local". 
        /// </summary>
        /// <remarks>
        /// Diff between local and roaming: https://stackoverflow.com/a/9709394/722945 
        /// - Local: 
        ///     - Meant to store cached data/large files.   
        ///     - Does not sync between computers.
        /// - Roaming: 
        ///     - Meant to store user settings.  
        ///     - Files in roaming will sync between computers if network admin has enabled it.  
        ///     - Keep roaming files small since large files will increase Windows login time.  
        /// </remarks>
        /// Tested: 2021-07-16 
        public static string GetDirLocal()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        }

        /// <summary> 
        /// Path to current user AppData\Local\App name (startup project name).   
        /// "_app" is appended to end for uniqueness. 
        /// Ex: "C:\Users\randy\AppData\Local\ThisAppName_app".  
        /// </summary>
        /// Tested: 2021-07-16
        public static string GetDirLocal_StartupProject()
        {
            string r = GetDirLocal() + @"\" + CApp.StartUpProjectName() + "_app"; //"_app" helps avoid name conflict with other apps.  

            Directory.CreateDirectory(r);

            return r;
        }


        //--------------------------
        //TEMP DIRECTORY:
        //--------------------------
        /// <summary>
        /// Path to current user AppData\Local\App name (startup project name)\Temp. 
        /// Ex: "C:\Users\randy\AppData\Local\ThisAppName_app\Temp".  
        /// </summary>
        /// Tested: 2020-06-21
        public static string GetDirTemp_StartupProject()
        {
            string r = GetDirLocal_StartupProject() + @"\Temp"; 

            Directory.CreateDirectory(r);

            return r;
        }

        //--------------------------

        /// <summary>
        /// Safe way to concatenate portions of a directory.  Handles each portion with/without a starting/ending slash "\" or "/".  
        /// </summary>
        /// <example>
        /// CDirectory.ConcatDirectory(@"\dir01\", @"\dir02\", @"\dir03\", @"\dir04\", @"\dir05\"); //Returns "\dir01\dir02\dir03\dir04\dir05"
        /// CDirectory.ConcatDirectory(@"/dir01/", @"/dir02/", @"/dir03/", @"/dir04/", @"/dir05/"); //Returns "/dir01/dir02/dir03/dir04/dir05"
        /// </example>
        /// <returns></returns>
        /// Tested: 2023-09-21
        public static string ConcatDirectory(string directory01, string directory02 = "", string directory03 = "", string directory04 = "", string directory05 = "")
        {
            //Step 01: Remove the star/end slashes. 
            string directory01_Sanitized = CString.removeRight(directory01, new string[]{ @"\", @"/"});

            string directory02_Sanitized = CString.removeRight(directory02, new string[]{ @"\", @"/"});
                   directory02_Sanitized = CString.removeLeft(directory02_Sanitized, new string[]{ @"\", @"/"});

            string directory03_Sanitized = CString.removeRight(directory03, new string[]{ @"\", @"/"});
                   directory03_Sanitized = CString.removeLeft(directory03_Sanitized, new string[]{ @"\", @"/"});

            string directory04_Sanitized = CString.removeRight(directory04, new string[]{ @"\", @"/"});
                   directory04_Sanitized = CString.removeLeft(directory04_Sanitized, new string[]{ @"\", @"/"});

            string directory05_Sanitized = CString.removeRight(directory05, new string[]{ @"\", @"/"});
                   directory05_Sanitized = CString.removeLeft(directory05_Sanitized, new string[]{ @"\", @"/"});



            //Step 02: Add the slashes back in between directories.  
            string r = ""; 

            if (directory01_Sanitized.ContainsAnyRR(new string[] { @"\"}))
                r = directory01_Sanitized 
                    + (directory02_Sanitized.HasValueRR() ?  @"\" + directory02_Sanitized : "")
                    + (directory03_Sanitized.HasValueRR() ?  @"\" + directory03_Sanitized : "")
                    + (directory04_Sanitized.HasValueRR() ?  @"\" + directory04_Sanitized : "")
                    + (directory05_Sanitized.HasValueRR() ?  @"\" + directory05_Sanitized : "")
                    ;
            else 
                r = directory01_Sanitized 
                    + (directory02_Sanitized.HasValueRR() ?  @"/" + directory02_Sanitized : "")
                    + (directory03_Sanitized.HasValueRR() ?  @"/" + directory03_Sanitized : "")
                    + (directory04_Sanitized.HasValueRR() ?  @"/" + directory04_Sanitized : "")
                    + (directory05_Sanitized.HasValueRR() ?  @"/" + directory05_Sanitized : "")
                    ;
            return r;
        }



        /// <summary>
        /// Simple method to create full path.  Handles directory with/without trailing slash "\". 
        /// Also handles forward/back slashes.  So can pass Windows directory (AND) URLs.  
        /// </summary>
        /// <example>
        /// //Param without trailing slash.
        /// CDirectory.ConcatDirectoryAndFilePath(@"C:\Users", "file_name.txt"); //Returns "C:\Users\file_name.txt".
        /// 
        /// //Param with trailing and forward slash.
        /// CDirectory.ConcatDirectoryAndFilePath(@"C:\Users\", "\file_name.txt"); //Returns "C:\Users\file_name.txt".  Same.  
        /// </example>
        /// <param name="_dir">Directory.  Can handle with/without trailing slash. </param>
        /// <param name="_file">File name with extension. </param>
        /// <returns></returns>
        /// Tested: 2020-06-20
        public static string ConcatDirectoryAndFilePath(string _dir, string _file)
        {
            string dirSanitized = CString.removeRight(_dir, new string[]{ @"\", @"/"}) ;
            string fileSanitized = CString.removeLeft(_file, new string[]{ @"\", @"/"}) ;
            string r = ""; 

            if (_dir.ContainsAnyRR(new string[] { @"\"}))
                r = dirSanitized + @"\" + fileSanitized;
            else 
                r = dirSanitized + @"/" + fileSanitized;

            return r;
        }






        /// <summary>
        /// Returns directory where this app is installed.  Returns directory for general helper (RR_General) DLL.  
        /// Handles modes: 
        ///     Debug
        ///     Release
        ///     Installed
        /// For ClickOnce apps, the return value will look like: C:\Users\randy\AppData\Local\Apps\2.0\9OLMERWZ.6CT\MJBC3OYZ.1Y4\wpft..tion_36b918226c1706a7_0001.0000_dbff543222a54874
        /// </summary>
        /// Tested: 2020-06-20
        public static string GetAppInstallPath_GeneralHelperLibrary()
        {
            //Assembly.GetExecutingAssembly() returns location of DLL containing the method (aka this general helper library).  I tested.
            return System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }


        /// <summary>
        /// Returns directory for this app's "resources" directory.  Returns directory for general helper (RR_General) DLL.  
        /// 
        /// For ClickOnce apps, the return value will look like: C:\Users\randy\AppData\Local\Apps\2.0\9OLMERWZ.6CT\MJBC3OYZ.1Y4\wpft..tion_36b918226c1706a7_0001.0000_dbff543222a54874\Resources
        /// </summary>
        public static string GetAppInstallResourcePath_GeneralHelperLibrary()
        {
            return ConcatDirectoryAndFilePath(GetAppInstallPath_GeneralHelperLibrary(), @"Resources");
        }


        /// <summary>
        /// Deletes all files (not folders) in directory. 
        /// </summary>
        /// <example>
        /// CDirectory.DeleteAllFilesInDirectory(@"C:\Temp");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/1288747/722945
        /// </remarks>
        /// <param name="_dir">Directory path.  See example.</param>
        /// Tested: 2020-06-21
        public static void DeleteAllFilesInDirectory(string _dir)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(_dir);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete(); 
            }
        }


        /// <summary>
        /// Deletes all folders (not files) in directory. 
        /// </summary>
        /// <example>
        /// CDirectory.DeleteAllFoldersInDirectory(@"C:\Temp");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/1288747/722945
        /// </remarks>
        /// <param name="_dir">Directory path.  See example.</param>
        /// Tested: 2020-06-21
        public static void DeleteAllFoldersInDirectory(string _dir)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(_dir);

            foreach (DirectoryInfo oDir in di.GetDirectories())
            {
                oDir.Delete(true); 
            }
        }





    }
}
