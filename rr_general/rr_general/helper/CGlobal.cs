﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace class_helper_rr
{
    public class CGlobal
    {
        //Methods in CStringNumber.stringToNumber() & CTimeHelper.getSecondsFromStringHHMMSS() will return this value if they cannot convert the string to number (ie error out).  
        //Integer cannot be Null (reason for using this).  
        public const int            ErrorNumber_Int     = -9999999;
        public const long           ErrorNumber_Long    = -9999999;
        public const decimal        ErrorNumber_Decimal = -9999999.00M;

        //DateTime cannot be Null either.  
        public static DateTime ErrorDateTime = DateTime.Parse("1900-01-01");

        //Underscore helps with Excel SUMIFS.  If these values get uploaded to db.  
        public const string TrueStringRR    = "TRUE_";
        public const string FalseStringRR   = "FALSE_"; 

    }
}
