﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Reflection;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Purpose: 
    /// Adds event ContextMenuOpeningFromParentRR, since native "ContextMenuOpening" relates to opening a sub-menu inside a parent menu (kind of misleading).  https://stackoverflow.com/a/15003494/722945 
    /// Grabs parent control.  
    /// </summary>
    public class ContextMenuRR: ContextMenu
    {
        /// <summary>
        /// Parent control this ContextMenu is related to.  Ex: DataGrid, TreeView etc.  
        /// </summary>
        public FrameworkElement parentUI;

        /// <summary>
        /// Fires when menu opens on parent object (typical scenario).  Will get parent control reference automatically.  
        /// </summary>
        /// <remarks>
        /// Native ContextMenuOpening has nothing to do with menu showing on parent.  It relates to opening a sub-menu from a parent menu (kind of pointless).  https://stackoverflow.com/a/14376654/722945
        /// </remarks>
        public event ContextMenuEventHandler ContextMenuOpeningFromParentRR;

        /// <summary>
        /// Constructor.
        /// </summary>
        public ContextMenuRR()
        {
            CLoadedEvent.LoadedHandler(this, onLoad);
        }


        /// <summary>
        /// Note: Called when menu displays first time, not when control is "ready".  So it's different from most controls.  
        /// </summary>
        /// Tested: 2017-06-11 all passed.  
        private void onLoad(object sender, EventArgs e)
        {
            //Gets the parent (aka Datagrid) or other parent UI holding the context menu.  
            parentUI = (FrameworkElement)this.PlacementTarget;

            //Attach event.  
            //Important note.  https://stackoverflow.com/a/15003494/722945
            //parentUI.ContextMenuOpening WORKS OK (relates to menu opening from the parent).  
            //parentUI.ContextMenu.ContextMenuOpening will not work (relates to child menu inside the menu, aka sub-menus inside parent menus).  
            //this.ContextMenuOpening does not work (again, relates to child menu inside the menu, aka sub-menus inside parent menus).
            if (ContextMenuOpeningFromParentRR != null)
                parentUI.ContextMenuOpening += ContextMenuOpeningFromParentRR; //Easier to understand event.  

            //Prevents a tiny empty context menu box showing, if no menu items (OR) no visible menu items.  
            //From: https://stackoverflow.com/a/9381488/722945  I'd normally put this in xaml, but custom ContextMenu's are not allowed since ContextMenu's cannot have a parent.  
            if (HasItems == false || countVisibleItems() == 0)
                this.Visibility = Visibility.Hidden;
            

        }


        /// <summary>
        /// Imports (moves) a menu into this "main parent" context menu.  
        /// Adds it to bottom.  
        /// Handles types MenuItem and Separator.  
        /// </summary>
        /// <example>
        /// myContextMenu.ImportMenu(ContextMenuToImportIn);
        /// </example>
        /// <param name="import">ContextMenu to import in.  </param>
        /// Tested: 2017-07-09 all passed.
        public void ImportMenu(ContextMenuRR import)
        {
            if (import == null)
                return;

            //Use static count since for loop moves items.   
            var count = import.Items.Count;

            //Allows loop below to add items in original order.    
            import.Items.ReverseOrder();

            //Must loop backwards since disconnecting parent removes it from index.  
            for (var i=count-1; i>=0; i--)
            {
                var itemOrig = import.Items.GetItemAt(i);

                if (itemOrig is MenuItem)
                {
                    import.DisconnectChild((MenuItem)itemOrig);
                    this.Items.Add(itemOrig);
                }

                if (itemOrig is Separator || itemOrig is SeparatorRR)
                {
                    this.Items.Add(new SeparatorRR());
                }
            }

        }



        /// <summary>
        /// Count visible menu items.  Only counts MenuItem type (not Separator).  
        /// </summary>
        /// <returns></returns>
        /// Tested: 2017-07-16 all passed.  
        public int countVisibleItems()
        {
            var c = from a in this.Items.CastRR<MenuItem>()
                    where ((FrameworkElement)a).Visibility == Visibility.Visible
                    select a;

            return c.Count();

        }













    }




}
