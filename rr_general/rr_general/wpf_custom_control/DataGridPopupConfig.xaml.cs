﻿using class_helper_rr;
using rr_general.wpf_custom_control;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace rr_general.wpf_custom_control.popup
{
    public partial class DataGridPopupConfig : Window, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Popup datagrid's itemssource.  Stores config dataset.  User preferences for datagrid.  
        /// One record for each column in sourceGrid.  
        /// </summary>
        public ObservableCollection<DC_ConfigRecord> configRec { get; set; }

        /// <summary>
        /// Grid that owns/triggered this popup.  
        /// </summary>
        DataGridRR sourceGrid;

        //Constructor.  
        public DataGridPopupConfig(bool visible_, DataGridRR sourceGrid_)
        {
            InitializeComponent();
            sourceGrid = sourceGrid_;

            //Make sure to keep this above "this.Show()" or else xaml binding breaks.  
            configRec = new ObservableCollection<DC_ConfigRecord>();

            //Populate configRec data source.  Copies column values from source grid.  
            for (var i=0;i<sourceGrid.Columns.Count;i++)
            {
                var col = (DataGridColumn)sourceGrid.GetColumnByDisplayIndex(i);

                var temp = new DC_ConfigRecord()
                {
                    columnOrder = col.DisplayIndex,
                    visible = col.Visibility == Visibility.Visible ? true : false,
                    width = col.ActualWidth, //ActualWidth is not affected by visibility.  
                    headerTitle = col.Header.ToString()
                };

                configRec.Add(temp);
            };
            

            //Update "columnOrder" property when MoveUp/MoveDown occurs.  
            //Makes columnOrder = arrays index value.  
            configRec.CollectionChanged += (sender, e) =>
            {
                configRec.EachRR<DC_ConfigRecord>(rec =>
                {
                    //MoveUp/MoveDown() modify the ItemsSource.  So the array's index is automatically updated.  
                    rec.columnOrder = configRec.IndexOf(rec);
                });

                //Need Refresh() here so grid shows new columnOrder value.  Does not affect row selections.  UpdateLayout doesn't work.  
                //Bug happens when a row is moved up, then down.  But Refresh() fixes it.  
                gridObj.Items.Refresh();

            };

            //Need to set this before calling CenterWindowRR.  See extension Ext_Window.  
            this.Owner = sourceGrid.GetParent<Window>();
            this.CenterWindowRR();

            //-----------------------------------------------------
            //Keep this dead last, after all binding properties are set.  
            //Calling Show() "before" will break the bindings.  Weird bug....
            if (visible_) this.Show();


        }


        public void ButtonClick_MoveUp(object sender, EventArgs e)
        {
            gridObj.moveAllSelectedRowsUp<DC_ConfigRecord>();
        }

        public void ButtonClick_MoveDown(object sender, EventArgs e)
        {
            gridObj.moveAllSelectedRowsDown<DC_ConfigRecord>();
        }

        /// <summary>
        /// Updates source grid, and saves preferences to disk.  
        /// </summary>
        /// Tested: 2017-07-08.
        public void ButtonClick_Save(object sender, EventArgs e)
        {
            //---------------------------
            //Safety.
            //---------------------------
            //Count total rows where "Visible" is checked.  
            var a = from z in configRec
                    where z.visible == true
                    select z;

            if (a.Count() == 0)
            {
                CMsgBox.Show("Need to make at least one column visible.");
                return;
            }

            //---------------------------
            //Update sourceGrid.  
            //---------------------------
            configRec.EachRR(config =>
            {
                var sourceCol = sourceGrid.GetColumnByHeaderTitle(config.headerTitle);
                sourceCol.DisplayIndex = config.columnOrder;
                sourceCol.Visibility = config.visible ? Visibility.Visible : Visibility.Hidden;
            });

            sourceGrid.UpdateLayout();
            sourceGrid.configPrefs.copyGridToDisk();
            this.Close();
        }


        public void ButtonClick_Cancel(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
