﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace rr_general.wpf_custom_control
{

    public partial class DataGridTextColumnRR : DataGridTextColumn
    {
        /// <summary>
        /// Fired by DataGridRR when datagrid is loaded (and hence columns are loaded also).
        /// Relying on parent DataGridRR is the only way.  Columns have no loaded event.  Also, calling GetDataGridParent() in constructor returns null (since grid is not yet part of visual tree).  https://stackoverflow.com/questions/4518222/find-root-element-of-datagridcolumn
        /// </summary>
        public EventHandler loadedRR { get; set; }

        /// <summary>
        /// Fired by DataGridRR.  
        /// </summary>
        /// Tested: 2017-09-03.
        public void loadedRR_FireEvent(object sender, EventArgs e)
        {
            if (loadedRR != null)
                loadedRR(sender, e);
        }

        /// <summary>
        /// Sets width, minimum height and maximum height.  
        /// Applied once when control loads.  
        /// </summary>
        public double minMaxWidth { get; set; }

        //Constructor.
        public DataGridTextColumnRR()
        {
            InitializeComponent();

            //DataGrid fires this using CLoadedHelper.  So guaranteed to fire only once.  
            loadedRR += onLoad;
        }

        public void onLoad(object sender, EventArgs e)
        {
            if (minMaxWidth != 0)
            {
                Width = minMaxWidth;
                MinWidth = minMaxWidth;
                MaxWidth = minMaxWidth;
            }
        }












    }
}
