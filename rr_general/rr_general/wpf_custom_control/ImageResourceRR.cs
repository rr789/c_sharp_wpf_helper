﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace rr_general.wpf_custom_control
{

    /// <summary>
    /// Mainly for icons that get reused.
    /// </summary>
    /// <example>
    /// //In code-behind. 
    /// img = ImageResourceRR.folder_open_32x_grey_rr;  //img is BitmapImage type.  
    /// 
    /// //In xaml.  
    /// //For .bmp .jpg .png.   
    /// <Image Source="{Binding ElementName=root, Path=img}"/> 
    /// //For animated .gif.  Uses this library to show animation.  https://github.com/XamlAnimatedGif/WpfAnimatedGif
    /// <Image gif:ImageBehavior.AnimatedSource="{Binding ElementName=root, Path=img}" />
    /// 
    /// </example>
    /// Tested: 2017-07-30 all passed.
    public class ImageResourceRR : Image
    {
        //------------------------------
        //File name constants.  
        //------------------------------
        private const string ADD_GENERAL_32X_GREY_RR = "add_general_32x_grey_rr.png";
        private const string CALENDAR_32X_GREY_RR = "calendar_32x_grey_rr.png";
        private const string DATABASE_32X_GREY_RR = "database_32x_grey_rr.png";
        private const string DELETE_32X_GREY_RR = "delete_32x_grey_rr.png";
        private const string DELETE_32X_RED_RR = "delete_32x_red_rr.png";
        private const string DOCUMENT_32X_GREY_RR = "document_32x_grey_rr.png";
        private const string DOWNLOAD_32X_GREY_RR = "download_32x_grey_rr.png";
        private const string EDIT_32X_GREY_RR = "edit_32x_grey_rr.png";
        private const string FILE_ADD_32X_GREY_RR = "file_add_32x_grey_rr.png";
        private const string FILTER_32X_GREY_RR = "filter_32x_grey_rr.png";
        private const string FOLDER_CLOSED_32X_GREY_RR = "folder_closed_32x_grey_rr.png";
        private const string FOLDER_OPEN_32X_GREY_RR = "folder_open_32x_grey_rr.png";
        private const string REFRESH_32X_GREY_RR = "refresh_32x_grey_rr.png";
        private const string SAVE_32X_GREY_RR = "save_32x_grey_rr.png";
        private const string TABLE_32X_GREY_RR = "table_32x_grey_rr.png";
        private const string TABLE_ADD_32X_GREY_RR = "table_add_32x_grey_rr.png";
        private const string SPINNER_CIRCLE_20PX_GREY_RR = "spinner_circle_20px_grey_rr.gif";
        private const string SPINNER_CIRCLE_30PX_GREY_RR = "spinner_circle_30px_grey_rr.gif";

        //------------------------------
        //So xaml can access.    
        //------------------------------
        public static BitmapImage add_general_32x_grey_rr = bitmapImageLoad(createImagePath(ADD_GENERAL_32X_GREY_RR));
        public static BitmapImage calendar_32x_grey_rr = bitmapImageLoad(createImagePath(CALENDAR_32X_GREY_RR));
        public static BitmapImage database_32x_grey_rr = bitmapImageLoad(createImagePath(DATABASE_32X_GREY_RR));
        public static BitmapImage delete_32x_grey_rr = bitmapImageLoad(createImagePath(DELETE_32X_GREY_RR));
        public static BitmapImage delete_32x_red_rr = bitmapImageLoad(createImagePath(DELETE_32X_RED_RR));
        public static BitmapImage document_32x_grey_rr = bitmapImageLoad(createImagePath(DOCUMENT_32X_GREY_RR));
        public static BitmapImage download_32x_grey_rr = bitmapImageLoad(createImagePath(DOWNLOAD_32X_GREY_RR));
        public static BitmapImage edit_32x_grey_rr = bitmapImageLoad(createImagePath(EDIT_32X_GREY_RR));
        public static BitmapImage file_add_32x_grey_rr = bitmapImageLoad(createImagePath(FILE_ADD_32X_GREY_RR));
        public static BitmapImage filter_32x_grey_rr = bitmapImageLoad(createImagePath(FILTER_32X_GREY_RR));
        public static BitmapImage folder_closed_32x_grey_rr = bitmapImageLoad(createImagePath(FOLDER_CLOSED_32X_GREY_RR));
        public static BitmapImage folder_open_32x_grey_rr = bitmapImageLoad(createImagePath(FOLDER_OPEN_32X_GREY_RR));
        public static BitmapImage refresh_32x_grey_rr = bitmapImageLoad(createImagePath(REFRESH_32X_GREY_RR));
        public static BitmapImage save_32x_grey_rr = bitmapImageLoad(createImagePath(SAVE_32X_GREY_RR));
        public static BitmapImage table_32x_grey_rr = bitmapImageLoad(createImagePath(TABLE_32X_GREY_RR));
        public static BitmapImage table_add_32x_grey_rr = bitmapImageLoad(createImagePath(TABLE_ADD_32X_GREY_RR));
        public static BitmapImage spinner_circle_20px_grey_rr = bitmapImageLoad(createImagePath(SPINNER_CIRCLE_20PX_GREY_RR));
        public static BitmapImage spinner_circle_30px_grey_rr = bitmapImageLoad(createImagePath(SPINNER_CIRCLE_30PX_GREY_RR));

        //Static constructor.  Boilerplate code must reside inside static construction.  Or VS throws warning. 
        static ImageResourceRR() { DefaultStyleKeyProperty.OverrideMetadata(typeof(ImageResourceRR), new FrameworkPropertyMetadata(typeof(ImageResourceRR))); }

        //Normal constructor.
        public ImageResourceRR(){}
        


        /// <summary>
        /// Accesses images in the library's "image" folder.  
        /// </summary>
        /// <param name="imageName_">Use the constants in this class.</param>
        /// <returns></returns>
        /// Tested: 2017-04-15 all passed.  
        private static string createImagePath(string imageName_)
        {
            //The funky pack:// path allows loading image from library.  Otherwise have to copy images to every project that uses the library.  "component" has to get added to path also (even though no folder called "component" exists".  See: http://stackoverflow.com/a/5391514/722945
            return "pack://application:,,,/rr_general;component/image/" + imageName_;
        }



        /// <summary>
        /// Wrapper for loading bitmap images more concisely.  
        /// </summary>
        /// <param name="img">BitmapImage object.</param>
        /// <param name="filePath">Path to image file. </param>
        /// <returns></returns>
        /// Tested: 2017-04-15 all passed.  
        public static BitmapImage bitmapImageLoad(string filePath)
        {
            //From: https://msdn.microsoft.com/en-us/library/aa970269(v=vs.110).aspx
            BitmapImage img = new BitmapImage();

            //BitmapImage.UriSource must be in a BeginInit/EndInit block.
            img.BeginInit();
            img.UriSource = new Uri(filePath);
            img.EndInit();

            return img;
        }










    }
}
