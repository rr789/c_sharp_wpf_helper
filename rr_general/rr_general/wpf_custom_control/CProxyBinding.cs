﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace rr_general.wpf_custom_control
{


    /// <summary>
    /// Provides a proxy element for WPF objects outside of visual tree to grab onto. 
    /// Elements are referenced via the "Data" property.  
    ///  
    /// </summary>
    /// <example>
    ///     //In control header tag: 
    ///     <UserControl 
    ///         xmlns:rr="clr-namespace:rr_general.wpf_custom_control;assembly=rr_general"
    ///         x:Name="root"
    ///         ...
    /// 
    ///     //Add proxy resource.  
    ///     <Grid.Resources>
    ///     <!-- Proxy object.  Allows elements outside of visual tree to bind with properties inside this control. -->
    ///         <rr:CProxyBinding x:Key="Proxy" Data="{Binding ElementName=root}" />
    ///     </Grid.Resources>
    /// 
    ///     //Reference proxy.  
    ///     <rr:TextBoxRR Text="{Binding Data.InputFreightAmount, Source={StaticResource Proxy}}" />
    /// </example>
    /// <remarks>
    ///     From tom Levesque: https://thomaslevesque.com/2011/03/21/wpf-how-to-bind-to-data-when-the-datacontext-is-not-inherited
    /// </remarks>
    /// Tested: 2019-09-03
    public class CProxyBinding : Freezable
    {
        #region Overrides of Freezable

        protected override Freezable CreateInstanceCore()
        {
            return new CProxyBinding();
        }

        #endregion

        public object Data
        {
            get { return (object)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object), typeof(CProxyBinding), new UIPropertyMetadata(null));
    }
}
