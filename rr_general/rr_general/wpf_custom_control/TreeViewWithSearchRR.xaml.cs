﻿using class_helper_rr;
using rr_general.wpf_custom_control;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using rr_general.wpf_custom_control.treeview_item_rr;
using System.ComponentModel;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Very flexible treeview.  
    /// - Shows folder open/close items on groups.  
    /// - Mix items and groups on same level.  
    /// - Filter to show groups that contain a child with name containing search string.     
    /// </summary>
    /// <example>
    /// Add TreeViewWithSearchRR object to Xaml.  No special setup required.  
    /// 
    /// //Create items that hold other items.  
    /// Group group01 = new itemRR() { NameDisplay = "group 01",   TypeRR="GROUP"};
    /// Group group02 = new itemRR() { NameDisplay = "group 02",   TypeRR="GROUP"};
    /// Group group03 = new itemRR() { NameDisplay = "group 03",   TypeRR="GROUP"};
    /// 
    /// //Give them items.  .
    /// group01.AddNewItemRR(nameDisplay_: "item 01", typeRR_: "ITEM");
    /// group02.AddNewItemRR(nameDisplay_: "item 02", typeRR_: "ITEM");
    /// group03.AddNewItemRR(nameDisplay_: "item 03", typeRR_: "ITEM");
    /// 
    /// //Put Group02 & Group03 under Group01.
    /// group01.Items.Add(group02)
    /// group02.Items.Add(group03)
    /// 
    /// //Attach to tree. 
    /// myTreeObj.ItemsSourceRR = group01;
    /// 
    /// //Show children containing name "groupz".  
    /// myTreeObj.ItemsSourceRR.ApplyFilterRR("search text", CString.STARTS_WITH, false);
    /// 
    /// //Expand all nodes in the tree.  
    /// treeObj.expandAllNodes();
    /// 
    /// 
    /// </example>
    /// <remarks>
    /// Tested: 2017-05-27 all passed. 
    /// </remarks>
    public partial class TreeViewWithSearchRR : UserControl, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Treeview object.  To make the xaml object public to instantiated objects.     
        /// </summary>
        public TreeViewRR treeObj;

        public TextBoxRR searchTextBox;

        public BitmapImage folderOpen { get; set; }
        public BitmapImage folderClosed { get; set; }


        //Constructor
        public TreeViewWithSearchRR()
        {
            InitializeComponent();

            //Load images.
            this.folderOpen = ImageResourceRR.folder_open_32x_grey_rr;
            this.folderClosed = ImageResourceRR.folder_closed_32x_grey_rr;

            //_treeObj is defined in xaml.  
            this.treeObj = this._treeObj;
            this.searchTextBox = this._searchTextBox;

            //Activate "User Stops Typing" event.  
            this.searchTextBox.Activate_UserStopsTypingEvent();
            this.searchTextBox.DelayBeforeFiringUserStopsTypingEvent = 500;
            this.searchTextBox.UserStopsTypingEvent += FilterTreeUserStopsTyping;
            

        }


        /// <summary>
        /// Filters the searchbox when user stops typing.  
        /// </summary>
        private void FilterTreeUserStopsTyping()
        {
            var searchText = this.searchTextBox.Text;

            if (this.treeObj.ItemsSource != null)
            {
                if (searchText == "")
                {
                    //For level children.
                    this.treeObj.ItemsSourceRR.RemoveFiltersRR();

                    //For deeper children.  
                    foreach (itemRR itm in this.treeObj.Items)
                    {
                        itm.RemoveFiltersRR();
                    }
                }
                else
                {
                    //For level children.
                    this.treeObj.ItemsSourceRR.ApplyFilterRR(searchText, CString.STARTS_WITH, false);

                    //For deeper children. 
                    foreach (itemRR itm in this.treeObj.Items)
                    {
                        itm.ApplyFilterRR(searchText, CString.STARTS_WITH, false);
                    }

                    this.treeObj.expandAllNodes();

                }

            }

                
            
        }






    }


}