﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Gu.Wpf.Adorners;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace rr_general.wpf_custom_control
{

    /// <summary>
    /// Has "user stopped typing" event.  
    /// Has text hint adorner.  
    /// </summary>
    /// <example>
    /// //-------------------------
    /// //Example of regex validation.  Just define regex pattern in this XAML property.  Very simple.  
    /// //-------------------------
    /// //Ex: Require exactly five numbers.  Validation will clear textbox if regex fails.  
    /// <rr:TextBoxRR RegexPatternToMatch="[0-9]{5}" />
    /// 
    /// //-------------------------
    /// //Custom event ValueUpdatedEventRR.  
    /// //-------------------------
    /// //Use this to fire an event when the user is finished editing the textbox field.  
    /// //Triggers when user leaves textbox field (AND) new value != old value.  
    /// //Superior to native event TextChanged since that event fires on every keystroke.  
    /// 
    /// //-------------------------
    /// //Example of hooking up "user stops typing" event. 
    /// //-------------------------
    /// //Activate "User Stops Typing" event. 
    /// obj.Activate_UserStopsTypingEvent();
    /// 
    /// //Define what this method does elsewhere. 
    /// obj.UserStopsTypingEvent += MyEventWhenStopsTyping;  
    /// 
    /// //Set delay to fire after user stops typing.  
    /// obj.DelayBeforeFiringUserStopsTypingEvent = 5000; //5 seconds.
    /// 
    /// 
    /// 
    /// 
    /// </example>
    /// Tested: 2017-07-16
    public partial class TextBoxRR : TextBox, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        //Regular constructor.
        public TextBoxRR()
        {
            InitializeComponent();

            //For my custom event "ValueUpdatedEventRR".
            GotFocus += GotFocusHandler;
            LostFocus += LostFocusHandler;
            PreviewMouseLeftButtonDown += SelectivelyIgnoreMouseButton;
            
            //This respects setting property in xaml.  I teseted.      
            if (SelectAllOnFocusRR == null)
            {
                SelectAllOnFocusRR = true;
            }
            
            
        }


        /// <summary>
        /// Text to show when no value exists and no cursor focus.     
        /// This goes into an "Adorner" object.  "Text" property stays null (OK).   
        /// </summary>
        public string textHint { get; set; }

        /// <summary>
        /// Custom method to call when user stops typing.
        /// Must call Activate_UserStopsTypingEvent() to activate.  
        /// </summary>
        public Action UserStopsTypingEvent;

        /// <summary>
        /// Time to let user finish typing before firing event.  Default 2 seconds.  
        /// </summary>
        public int DelayBeforeFiringUserStopsTypingEvent = 2000; //2 seconds default.  

        /// <summary>
        /// Call this to activate event.  
        /// Saves CPU.  
        /// </summary>
        public void Activate_UserStopsTypingEvent()
        {
            this.KeyUp += KeyUp_Handler; 
        }

        /// <summary>
        /// Optional.  Define regex string for strings allowed.  Performs validation when text is entered.  Leave blank to disable regex validation.      
        /// </summary>
        public string RegexPatternToMatch { get; set; }

        /// <summary>
        /// True (default): Selects all text when this control gains focus.
        /// False: Regular textbox behavior.
        /// </summary>
        public bool? SelectAllOnFocusRR { get; set; }

        //100 for interval is placeholder, real interval value is set below.
        private CTimerForms KeyUpTimer = new CTimerForms(100, 0);

        /// <summary>
        /// Handles the "User Stops Typing" event.  
        /// See class notes for example.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// Tested: 2017-05-08 all passed. 
        private void KeyUp_Handler(object sender, KeyEventArgs e)
        {
            KeyUpTimer.timerObj.Interval = DelayBeforeFiringUserStopsTypingEvent;

            //Every keypress stops/resets the timer.  
            KeyUpTimer.stopTimer();
            KeyUpTimer.startTimer();

            //If reached, then user has stopped typing.  
            KeyUpTimer.callback = delegate
            {
                    //Need this to prevent the KeyUp event from firing (esp if msgbox is popped up).  Not an issue with the timer.  
                    KeyUpTimer.stopTimer();
                    this.UserStopsTypingEvent();
            };



        }




        /// <summary>
        ///     Validates regex pattern from property RegexPatternToMatch.  
        /// </summary>
        /// <example>
        ///     See class example above.  
        /// </example>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        ///     Modified from: https://stackoverflow.com/a/12721673/722945
        /// </remarks>
        /// Tested: 2019-07-20
        private void LostFocusHandlerRR(object sender, RoutedEventArgs e)
        {
            TextBoxRR txtbox = (TextBoxRR)sender;
            string originalText = txtbox.Text;

            //Safety: 
            //Need both checks for RegexPatternToMatch, I tested.  
            if (RegexPatternToMatch == "" || RegexPatternToMatch == null || originalText == "")
                return;

            if (!originalText.RegexHasMatchRR(RegexPatternToMatch))
                txtbox.Text = "";
            else
                txtbox.Text = originalText;
        }



        //-------------------------------
        //FOR CUSTOM EVENT ValueUpdatedEventRR
        //Tested all below: 2019-09-08.
        //-------------------------------
        /// <summary>
        /// Event is triggered when user leaves the textbox (AND) new value != old value.  
        /// </summary>
        public event EventHandler ValueUpdatedEventRR;

        private string oldValue;
        private void GotFocusHandler(object sender, EventArgs args)
        {
            oldValue = this.Text;

            //Selects all text when entering textbox.  
            //Fyi: Note the little bug fix at "SelectivelyIgnoreMouseButton()" in bloc below.  
            if ((bool)SelectAllOnFocusRR)
                this.SelectAll();
        }

        private void LostFocusHandler(object sender, EventArgs args)
        {
            if (this.Text != oldValue)
            {
                //Contains "binding" information.  
                //Ex: 
                //  bindingExpression.ResolvedSourcePropertyName; Returns name of bound dependency property.  Even blows thru proxy static resources to find name OK.  
                var bindingExpression = this.GetBindingExpression(TextBoxRR.TextProperty);

                //Safety: Prevents throwing error when text field is not bound to a dependency property.    
                if (bindingExpression != null)
                {
                    //Forces bound data source to receive an update.  
                    //I needed this when I bound a textbox to a dependency property, then accessed the dependency property in the "ValueUpdatedEventRR" handler.  It showed the old value.  This one liner fixed it OK.  
                    //https://stackoverflow.com/a/21655827/722945
                    bindingExpression.UpdateSource();
                }

                FireValueUpdatedEventRR();
                
            }
        }

        private void FireValueUpdatedEventRR()
        {
            ValueUpdatedEventRR?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Fixes a bug where textbox cannot select all text on first mouse click.  
        /// This compliements existing "GotFocus->this.SelectAll()" in bloc above.
        /// </summary>
        /// <remarks>
        /// Modified from: https://stackoverflow.com/a/2553297/722945
        /// </remarks>
        /// Tested: 2019-10-17
        private void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
        {
            //So we only deal with "first" mouse click.  
            if (!this.IsKeyboardFocusWithin)
            {
                //If the text box is not yet focussed, give it the focus and stop further processing of this click event.
                this.Focus();
                e.Handled = true;
            }

            //No need to call "this.SelectAll()".  The existing "GotFocus->this.SelectAll()" in bloc above handles selecting all text OK.  

        }



    }
}
