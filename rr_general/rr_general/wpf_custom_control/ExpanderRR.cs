﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Purpose: See bug fix below.  
    /// Note: Must extend with C# only (not with XAML).  Otherwise VS will throw error if setting a child control's "name" property inside the expander.  
    /// </summary>
    public partial class ExpanderRR : Expander
    {
        //Constructor.  
        public ExpanderRR()
        {
            CLoadedEvent.LoadedHandler(this, OnLoad);
        }

        private void OnLoad(object sender, EventArgs e)
        {
            //Fix to add child controls (inside expander) to the "visual tree".    
            //Fyi: 
            //Child controls are available to the "logical tree" anytime.  
            //However, child controls are not added to "visual tree" until expander is expanded.  
            //This means some actions (like combobox selecting default value in XAML) are not available until added to visual tree.  
            //To fix this "bug"...need to call these two lines.  I got them from a Stackoverflow site but lost the link.  
            this.IsExpanded = true;
            this.UpdateLayout();

        }


    }
}
