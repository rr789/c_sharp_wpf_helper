﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Only shows separator is visible menu items exist both above/below.    
    /// </summary>
    /// <remarks>
    /// From: https://stackoverflow.com/a/41170818/722945
    /// </remarks>
    /// Tested: 2017-07-16
    public class SeparatorRR : Separator
    {
        //Constructor.
        public SeparatorRR()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            //Starting collapsed so we don't see them disappearing.
            this.Visibility = Visibility.Collapsed;

            CLoadedEvent.LoadedHandler(this, OnLoaded);
        }


        private void OnLoaded(object sender, EventArgs e)
        {
            // We have to wait for all siblings to update their visibility before we update ours.
            // This is the best way I've found yet. I tried waiting for the context menu opening or visibility changed, on render and lots of other events
            Dispatcher.BeginInvoke(new Action(UpdateVisibility), DispatcherPriority.Render);
        }


        private void UpdateVisibility()
        {
            //Safety: Very rare, but when menu item was clicked to show DataGridPopup, VS threw error saying "cannot convert StackPanel to ItemsControl".  This check fixes it.  
            if (typeof(ItemsControl).IsAssignableFrom(Parent.GetType()) == false)
                return;

            //------------------------

            var showSeparator = false;

            // Go through each sibling of the parent context menu looking for a visible item before and after this separator
            var foundThis = false;
            var foundItemBeforeThis = false;
            foreach (var visibleItem in ((ItemsControl)Parent).Items.OfType<FrameworkElement>().Where(i => i.Visibility == Visibility.Visible || i == this))
            {
                if (visibleItem == this)
                {
                    // If there were no visible items prior to this separator then we hide it.
                    if (!foundItemBeforeThis)
                        break;

                    foundThis = true;
                }
                else if (visibleItem is SeparatorRR || visibleItem is Separator)
                {
                    // If we already found this separator and this next item is not a menu item so we hide this separator.
                    if (foundThis)
                        break;

                    foundItemBeforeThis = false; // The current item is a separator so we reset the search for an item.
                }
                else
                {
                    if (foundThis)
                    {
                        // We found a visible item AFTER finding this separator so we're done and should show this.
                        showSeparator = true;
                        break;
                    }

                    foundItemBeforeThis = true;
                }
            }

            Visibility = showSeparator ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
