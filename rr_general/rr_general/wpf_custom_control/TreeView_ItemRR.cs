﻿using PropertyChanged;
using class_helper_rr;
using rr_general.wpf_custom_control;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace rr_general.wpf_custom_control.treeview_item_rr
{


    /// <summary>
    /// Acts as both Group and Item.     
    /// </summary>
    /// Tested: 2017-05-21 all passed.  
    public class itemRR : ItemsControl, INotifyPropertyChanged //Will render as ItemsControl, but won't render as TreeViewItemRR.
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        //Used with typeRR.
        public const string GROUP = "GROUP";
        public const string ITEM = "ITEM";

        /// <summary>
        /// Changes treeview item style.  
        /// Set to constants GROUP or ITEM.  
        /// </summary>
        
        public string TypeRR { get; set; }

        /// <summary>
        /// Name rendered/shown to user in TreeView.  
        /// </summary>
        public string NameDisplay { get; set; }

        /// <summary>
        /// Related TreeViewItem associated with the data object ItemRR.  
        /// This is set automatically when SetItemsSourceRR is called (sets all items recursively).  
        /// </summary>
        public TreeViewItem TreeViewItemRR;


        //Constructor.    
        public itemRR(){}




        //-------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------
        // BELOW HANDLE GROUP FILTERING AND OPTIONAL HELPERS.  
        //-------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------



        /// <summary>
        /// Adds existing item.   
        /// </summary>
        /// <param name="item"></param>
        /// Tested: 2017-05-21 all passed.  
        public void AddItemRR(itemRR item)
        {
            this.Items.Add(item);
        }

        /// <summary>
        /// Fast way to create new item (AND) attach to node.      
        /// </summary>
        /// <param name="nameDisplay_"></param>
        /// <param name="typeRR_"></param>
        /// Tested: 2017-05-21 all passed.
        public void AddNewItemRR(string nameDisplay_, string typeRR_)
        {
            itemRR i = new itemRR() { NameDisplay = nameDisplay_, TypeRR = typeRR_ };
            this.Items.Add(i);
        }


        /// <summary>
        /// Counts number of items.  First level children (not recursive).  
        /// Namely to avoid null check.  
        /// </summary>
        /// <example>
        /// itemRR itm = new itemRR() { NameDisplay = "group 01",   TypeRR="GROUP"};
        /// itm.AddNewItemRR(nameDisplay_: "item 01", typeRR_: "ITEM");
        /// itm.AddNewItemRR(nameDisplay_: "item 02", typeRR_: "ITEM");
        /// CMsgBox.Show(itemRR.countChildren(itm)); //Returns 2
        /// </example>
        /// <param name="itm"></param>
        /// <returns></returns>
        /// Tested: 2017-05-21 all passed.
        public static int countChildren(itemRR itm)
        {
            //Halt if null.
            if (itm == null) { return 0; }
            
            return itm.Items.Count;
        }






        /// <summary>
        /// Walks the whole tree recursively.  Counts child items that have matching name (down to deepest level).  Case in-sensitive.   
        /// </summary>
        /// <example>
        /// itemRR itm = new itemRR() { NameDisplay = "group 01",   TypeRR="GROUP"};
        /// itm.AddNewItemRR(nameDisplay_: "item 01", typeRR_: "ITEM");
        /// itm.AddNewItemRR(nameDisplay_: "item 02", typeRR_: "ITEM");
        /// CMsgBox.Show(itm.CountChildrenWithCriteria("item", CString.STARTS_WITH, false)); //Returns 2
        /// </example>
        /// <param name="nameToFind">Needle string.</param>
        /// <param name="itm">Object to search.</param>
        /// <param name="optionConstant">Use CString.CONTAINS or CString.STARTS_WITH</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// <param name="count">Used for recursive walk.  Ignore when using. </param>
        /// <returns></returns>
        /// Tested: 2017-05-21 three levels, all passed.
        public int CountChildrenWithCriteria(string nameToFind, string optionConstant, bool caseSensitive = false)
        {
            int count = 0;
            
            //Current level.    
            if (CString.MatchesCriteria(this.NameDisplay, nameToFind, optionConstant, caseSensitive))
            {
                count++;
            }

            //Children.
            this.EachRR(itm =>
            {
                //Recursive.    
                count += itm.CountChildrenWithCriteria(nameToFind, optionConstant, caseSensitive);
            });

            return count;
        }

  

        /// <summary>
        /// Expands/collapses this node.  Does not expand children.    
        /// </summary>
        /// <example>
        /// obj.ExpandNodeLevel(true); //Expands this node.   
        /// obj.ExpandNodeLevel(false); //Collapses this node.    
        /// </example>
        /// <param name="expand"></param>
        /// Tested: 2017-05-21 all passed.  
        public void ExpandNodeLevel(bool expand)
        {
            this.TreeViewItemRR.IsExpanded = expand;
            this.TreeViewItemRR.UpdateLayout(); //Need to call this (after expanding) so ItemContainerGenerator can grab child objects.  I tested, ItemContainerGenerator returns null otherwise.  See here: http://stackoverflow.com/a/1927290/722945
        }


        /// <summary>
        /// Expands node, and all children.  
        /// From: http://stackoverflow.com/a/15346816/722945
        /// </summary>
        /// <param name="expand"></param>
        /// Tested: 2017-05-20 all passed.  
        public void ExpandNodeAllLevels(bool expand)
        {
            //Current level.
            this.ExpandNodeLevel(expand);

            //Children.  
            this.EachRR(itm =>
            {
                itm.ExpandNodeAllLevels(expand);
            });
        }


        /// <summary>
        /// Returns very top parent with type itemRR.    
        /// </summary>
        /// <example>
        /// CMsgBox.Show(item03.getTopParent().NameDisplay);
        /// </example>
        /// Tested: 2017-05-21 all passed.  
        public itemRR getTopParent()
        {
            itemRR _parent = null;

            //Go up the chain recursively.  
            if (this.Parent != null && this.Parent is itemRR)
            {
                var p = (itemRR)this.Parent;
                _parent = p.getTopParent();
            }
            else
            {
                _parent = this;
            }

            return _parent;
        }




        /// <summary>
        /// Filters to lowest child, returns items that include a child item with a matching string.    
        /// Matches partial strings.  Can define CONTAINS, STARTS_WITH options. 
        /// </summary>
        /// <example>
        /// item01.ApplyFilterRR("item name", CString.CONTAINS, false);
        /// tree_new.treeObj.ItemsSource = item01.Items;
        /// </example>
        /// <param name="needle">Needle string for property NameDisplay.</param>
        /// <param name="optionConstant">Use CString.CONTAINS or CString.STARTS_WITH</param>
        /// <param name="caseSensitive">True: match case.  False: Ignore case.</param>
        /// Tested: 2017-07-22 all passed.
        public void ApplyFilterRR(string needle, string optionConstant, bool caseSensitive = false)
        {

            //Apply filter on current level items.  
            this.Items.Filter = e =>
            {
                itemRR itm = e as itemRR;

                //Include if level name (OR) children contain criteria. 
                return CString.MatchesCriteria(itm.NameDisplay, needle, optionConstant, caseSensitive) || (itm.CountChildrenWithCriteria(needle, optionConstant, false) > 0);
            };
                      

            //Recursive, apply filter on child items.     
            this.EachRR(itm =>
            {
                itm.ApplyFilterRR(needle, optionConstant, caseSensitive);
            });

        }


        /// <summary>
        /// Removes filters on current level and all children of node.  
        /// </summary>
        /// <param name="needle"></param>
        /// <param name="optionConstant"></param>
        /// <param name="caseSensitive"></param>
        /// Tested: 2017-05-27 all passed.
        public void RemoveFiltersRR()
        {
            //Remove filter on current level items.  
            this.Items.Filter = null;

            //Recursive, remove filter on child items.
            this.EachRR(itm =>
            {
                itm.RemoveFiltersRR();
            });

        }




















    }



}
