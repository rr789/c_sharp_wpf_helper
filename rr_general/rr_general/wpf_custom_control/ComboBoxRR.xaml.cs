﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Gu.Wpf.Adorners;
using class_helper_rr;
using rr_general.helper;
using System.ComponentModel;
using System.Diagnostics;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Interaction logic for ComboBoxRR.xaml
    /// See xaml for example.
    /// </summary>
    public partial class ComboBoxRR : ComboBox, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Shows when no value is selected.  
        /// Ex: "Select item".  
        /// </summary>
        public string TextHint { get; set; }

        /// <summary>
        /// Enables searching values inside combobox itemsource.  
        /// Default is false.  
        /// </summary>
        public bool IsSearchableRR { get; set; }

        /// <summary>
        /// Set this to avoid the jerkiness when combobox items have dramatically more/less characters.  
        /// Prevents ComboBox from resizing as user scrolls through the list.  
        /// </summary>
        public int WidthItemListRR { get; set; }


        /// <summary>
        /// Main collection view for combobox.  Handles itemssource filters.   
        /// </summary>
        ICollectionView cv { get; set; }

        /// <summary>
        /// Textbox within this control.  User can enter search strings in here.  
        /// </summary>
        public TextBox TextBoxChildRR { get; set; }

        /// <summary>
        /// True: Allows the a blank ComboboxItemRR choice.  Where ComboboxItemRR->Value equals "".  
        /// </summary>
        public bool FilterAllowBlankChoice { get; set; }

        /// <summary>
        /// Superior to native OnSelectionChanged when enabling searchable combobox. 
        ///     Native OnSelectionChanged fires everytime the UP/DOWN arrows were pressed. 
        ///     OnSelectionChangedRR only fires when dropdown menu is closed.  Also, only fires if before/after value has changed.  
        /// </summary>
        public event SelectionChangedEventHandler SelectionChangedRR;

        /// <summary>
        /// Used with FireSelectionChanged. 
        /// </summary>
        private ComboBoxItemRR _priorSelectedItem { get; set; }

        /// <summary>
        /// Fires event SelectionChangedRR;
        /// </summary>
        /// Tested: 2019-11-09
        public void FireSelectionChangedRR()
        {
            //Prevents event from firing everytime a user clicks UP/DOWN thru the list. 
            if (!IsDropDownOpen)
            {
                if (_priorSelectedItem == this.getSelectedItem())
                {
                    //Debug: 
                    //Debug.WriteLine("PRIOR EQUAL TO CURRENT SELECTED ITEM.");
                    return;
                }

                //Have to call these three args since using "SelectionChangedEventHandler".  This way we can bind to event "SelectionChangedRR" in xaml just like the regular "SelectionChanged". 
                //https://stackoverflow.com/a/38339895
                SelectionChangedRR?.Invoke(this, new SelectionChangedEventArgs
                (
                    ComboBox.SelectionChangedEvent
                    ,new List<ComboBoxItemRR>() { _priorSelectedItem } 
                    ,new List<ComboBoxItemRR>() { this.getSelectedItem()}

                ));

                //Debug: 
                //if (_priorSelectedItem != null && getSelectedItem() != null)
                //    Debug.WriteLine("PRIOR: " + _priorSelectedItem.Content + "   NEW: " + getSelectedItem().Content);

                _priorSelectedItem = this.getSelectedItem();

                //Debug: 
                //Debug.WriteLine("CBOX TRIGGERED");
            }

        }
            











        //--------------------------------
        //OPTIONAL FILTER CRITERIA VALUES FOR "MAKE COMBOBOX SEARCHABLE".  
        //Used when want to add an extra filter that is not part of user's entered values.  
        //--------------------------------
        public string CBoxItemFilter_PropertyString01 { get; set; }
        public string CBoxItemFilter_PropertyString02 { get; set; }
        public string CBoxItemFilter_PropertyString03 { get; set; }
        public string CBoxItemFilter_PropertyString04 { get; set; }
        public string CBoxItemFilter_PropertyString05 { get; set; }
        public string CBoxItemFilter_PropertyString06 { get; set; }
        public string CBoxItemFilter_PropertyString07 { get; set; }
        public string CBoxItemFilter_PropertyString08 { get; set; }

        public ComboBoxRR()
        {
            InitializeComponent();

            //Keep this loop inside the constructor (not OnLoad method).  Otherwise the dummy placeholder won't delete in time and cause issues.  
            for (var i = 0; i < this.Items.Count; i++)
            {
                //Removes a little bug with comboboxes noticed 2018-07-20, where the first menu item is a thin "System.Windows.Controls.Grid" that is a bogus placeholder.  
                //If it cannot convert into ComboboxItemRR, then it's the dummy placeholder so remove it.  
                //Note: This also prevents error when databinding “Items collection must be empty before using ItemsSource.”
                var s = CType.ConvertType<ComboBoxItemRR>(this.Items[i]);

                if (s == null)
                {
                    this.Items.RemoveAt(i);
                    break;
                }
            }

            //Replacement for IsFocused.  I didn't have much luck with IsFocused.  HasFocusRR works great.  
            this.GotFocus += (obj, args) =>
            {
                HasFocusRR = true;


                //Helper...applies filter to comboboxes behind a tab control.  
                //Steps to recreate: 
                //1. In a tab control's (control) onload method...assign ItemsSource to a collection view.  Also set the combobox's CBoxItemFilter_PropertyString01 to a value.  
                //2. Start the app.  Do not click on the tab control tab.
                //3. Once app is fully loaded, then click on the tab.
                //Done.  Will notice the combobox is "NOT" filtered.  
                //Fyi: We only need to do this once (on very first focus event).  So below is a little overkill.  
                if (cv != null)  //<--Only need this "null check" safety if filtering comboboxes before they have finished rendering etc. 
                    if(CBoxItemFilter_PropertyString01.HasValueRR() || CBoxItemFilter_PropertyString02.HasValueRR() || CBoxItemFilter_PropertyString03.HasValueRR() || CBoxItemFilter_PropertyString04.HasValueRR() || CBoxItemFilter_PropertyString05.HasValueRR() || CBoxItemFilter_PropertyString06.HasValueRR() || CBoxItemFilter_PropertyString07.HasValueRR() || CBoxItemFilter_PropertyString08.HasValueRR() )
                        cv.Filter = CollectionView_FilterRR;
            };


            this.LostFocus += (obj, args) =>
            {
                HasFocusRR = false;
            };

            this.SelectionChanged +=(obj,args)=>
            {
                FireSelectionChangedRR();
            };


            //No CLoadedHelper.  Instead see OnApplyTemplate override.  It fires when control (and all children) are fully rendered.  
        }

        /// <summary>
        /// Superior alternative to IsFocused.  
        /// </summary>
        private bool HasFocusRR;


        /// <summary>
        /// OnApplyTemplate event is called after entire control (and all children) are fully rendered.  This is superior to Loaded event.   
        /// Needed since the child textbox is not available when combobox fires loaded event.  
        /// https://stackoverflow.com/questions/3450816/how-to-get-at-the-wpf-combobox-part-editabletextbox-because-combobox-not-getti/3451248
        /// I may replace the OnLoadRR helper with OnApplyTemplate later.  
        /// </summary>
        /// Tested: 2018-12-16
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            cv = CollectionViewSource.GetDefaultView(this.Items);

            //Get textbox child obj.  
            TextBoxChildRR = this.Template.FindName("PART_EditableTextBox", this) as TextBox;

            //Not needed.  
            if (TextBoxChildRR == null)
                CLogger.CreateLogEntry("Error: CComboBox->MakeComboBoxSearchable cannot find child textbox.", true, true);


            //Select the ComboBoxItemRR item with property IsSelected=True.  
            //Needed to make "IsSelected" property work since ComboBoxItemRR is a pure C# object (not a XAML ComboBoxItem object).
            if (this.SelectedValue == null)  //Safety when combobox is inside a TabControl->TabItem (and SelectedValue is already bound to a value).  In this case, databinding works OK, however OnApplyTemplate event is not called until the tab is clicked on.  This safety prevents a combobox with previously bound value from getting reset when TabItem is clicked.  I tested.    
            {
                for (var i = 0; i < this.Items.Count; i++)
                {
                    var s = CType.ConvertType<ComboBoxItemRR>(this.Items[i]);

                    if (s != null && s.IsSelected)
                    {
                        this.SelectedIndex = i;
                        break;
                    }   
                }
            }

            if (IsSearchableRR)
                MakeComboBoxSearchable();


            this.DropDownClosed+=(obj, args) =>
            {
                //This also allows a single ENTER/RETURN keypress to close the dropdown menu and fires FireSelectionChangedRR() OK.  I tested.   
                this.selectByContent(TextBoxChildRR.Text);  

                //Only purpose of this is to trigger selection change when MOUSE selects new value.  The above SelectByContent() was not doing it.  
                this.FireSelectionChangedRR();

            }; 

            
        }



        /// <summary>
        /// Adds ComboBoxItemRR to this combobox. 
        /// </summary>
        /// <param name="content_">Fyi: "content" is visible, "value" is hidden. </param>
        /// <param name="value_">Fyi: "content" is visible, "value" is hidden. </param>
        /// <example>
        /// cboxObj.addItem("visible text", "hidden value")
        /// </example>
        public void addItem(string content_, string value_)
        {
            var cboxItem = new ComboBoxItemRR();
            cboxItem.Content = content_;
            cboxItem.Value = value_;

            this.Items.Add(cboxItem);
        }


        /// <summary>
        /// Convenience method, returns selected item as a typed ComboBoxItemRR.  
        /// </summary>
        /// <remarks>
        /// Example: 
        ///     myComboBox.getSelectedItem().name //Get Name.
        ///     myComboBox.getSelectedItem().value //Get Value.
        ///     
        /// This method is typed to ComboBoxItemRR, whereas the native "selectedItem" property it typed to a generic object.     
        /// Tested: 2018-07-20
        /// </remarks>
        public ComboBoxItemRR getSelectedItem()
        {
            return this.SelectedItem as ComboBoxItemRR;
        }


        /// <summary>
        /// Gets the selected item's "content" property.  For convenience.
        /// Fyi: "content" is visible, "value" is hidden.  
        /// </summary>
        /// <remarks>
        /// Fyi: Name is always a String type.
        /// Tested: 2018-07-16
        /// </remarks>
        public string getSelectedContent()
        {
            string r = "";

            if (this.SelectedIndex > -1)
                r = this.getSelectedItem().Content.ToString();

            return r;
        }


        /// <summary>
        /// Gets the selected item's "value" property.  For convenience.
        /// Fyi: "content" is visible, "value" is hidden.  
        /// </summary>
        /// <remarks>
        /// Tested: 2018-07-16
        /// </remarks>
        public string getSelectedValue()
        {
            string r = "";

            if (this.SelectedIndex > -1)
                r = this.getSelectedItem().Value;

            return r;
        }


        /// <summary>
        /// Selects an item based on the "content" property.  Not case sensitive.   
        /// Fyi: "content" is visible, "value" is hidden.  
        /// </summary>
        /// <remarks>
        /// Got it from here: http://stackoverflow.com/a/5896836/722945
        /// Example: 
        ///     myComboBox.selectByContent("value_here");
        /// Tested: 2018-07-16
        /// </remarks>
        public void selectByContent(string contentString)
        {
            //Loop thru each item in ComboBox.  
            for (var i=0;i<this.Items.Count;i++)
            {
                ComboBoxItemRR c = this.Items[i] as ComboBoxItemRR;

                //Safety: Rare but prevents throwing exception if "content" property is not assigned a value (in ComboboxItemRR).  Normally all ComboBoxItemRR objects are assigned a content property.    
                if (c.Content == null)
                    continue;

                if (c.Content.ToString().ToLower() == contentString.ToLower())
                {
                    this.SelectedIndex = i;
                    break; 
                }
            }
        }



        /// <summary>
        /// Selects an item based on the "value" property.  Not case sensitive.   
        /// Fyi: "content" is visible, "value" is hidden.  
        /// </summary>
        /// <remarks>
        /// Got it from here: http://stackoverflow.com/a/5896836/722945
        /// Example: 
        ///     myComboBox.selectByValue("value_here");
        /// Tested: 2019-10-13
        /// </remarks>
        public void selectByValue(string _value)
        {
            //Safety: 
            if (_value == null)
            {
                this.SelectedIndex = -1;
                return;
            }

            //Loop thru each item in ComboBox.  
            for (var i=0; i<this.Items.Count; i++)
            {
                ComboBoxItemRR c = this.Items[i] as ComboBoxItemRR;

                if (c.Value.ToString().ToLower() == _value.ToLower())
                {
                    this.SelectedIndex = i;
                    break; 
                }
            }
        }



        /// <summary>
        /// Returns True if item is selected.  False if no item selected. 
        /// </summary>
        /// <remarks>
        /// Tested: 2018-07-20
        /// </remarks>
        public bool isItemSelected()
        {
            if (this.SelectedIndex > -1)
                return true;
            else
                return false;
        }


        /// <summary>
        /// Visual only.  Deselects the text and sets cursor to first (left most)position.  This keeps text aligned to the left.  
        /// Fyi: It's not a horizontalalignment issue, it's purely text selection working normally.   
        /// </summary>
        /// <remarks>
        /// From: https://social.msdn.microsoft.com/Forums/en-US/e86484fb-d7be-4e04-8e66-0d12326817c8/how-to-set-the-selected-text-of-combobox-align-left?forum=wpf  The author also mentioned doing a loop over all items but I tested and the loop has no effect.
        /// </remarks>
        /// Tested: 2019-10-13
        private void MoveCursorFarLeftRR()
        {
            this.TextBoxChildRR.SelectionStart = 0;
            this.TextBoxChildRR.SelectionLength = 0;
        }


        /// <summary>
        /// Determines if the search needle string exists as a menu choice.  
        /// Not case sensitive.  However, must match the entire string.  
        /// </summary>
        /// <param name="needle"></param>
        /// Tested: 2018-12-16
        public bool DoesContentValueExist(string needle)
        {
            bool r = false;

            if (needle == "")
                return false;

            this.Items.EachRR<ComboBoxItemRR>(obj =>
            {
                //Safety: Rare but prevents throwing exception if "content" property is not assigned a value (in ComboboxItemRR).  Normally all ComboBoxItemRR objects are assigned a content property.    
                if (obj.Content != null)
                    if (obj.Content.ToString().ToUpper() == needle.ToUpper())
                        r = true;

            });

            return r;
        }






        private bool PauseFilter = false;

        /// <summary>
        /// Call this once to make combobox permanently searchable.  
        /// ★★★ Note: To apply additional filters use one of the CBoxItemFilter_PropertyStringXXX properties.    
        /// </summary>
        /// <remarks>
        /// Modified from: https://gist.github.com/mariodivece/0bbade976aea8d416d52
        /// </remarks>
        /// Tested: 2019-09-12
        private void MakeComboBoxSearchable()
        {
            this.IsEditable = true;
            this.IsReadOnly = false;
            this.IsTextSearchEnabled = false; //True interferes with the custom text search below (I tested).  Best to set to false.  

            TextBoxChildRR.TextChanged += (o, args) =>
            {
                //PauseFilter is only set when selection changed event fires (aka when user is browsing menu items with down/up keys).  
                //During this time, the filter is paused since user is deciding which item to pick.        
                if (!PauseFilter)
                {
                    ////Filter collection.  
                    cv.Filter = CollectionView_FilterRR;
                }

                //Fixes bug where DOWN/UP arrow keys cannot select a dropdown menu item.    
                //This comment mentions same bug (I borrowed a little code to fix).  https://gist.github.com/mariodivece/0bbade976aea8d416d52#gistcomment-1867349
                //To re-create bug: 
                //1. Put cursor inside textbox.
                //2. Enter one/more text characters (ex: "h"). 
                //3. Press DOWN one or more times.  At this point, selecting via down/up keys will work.   
                //4. Delete text in textbox.
                //5. Try pressing DOWN again, selecting items will not work.  
                if (TextBoxChildRR.Text == "" && this.HasFocusRR) //Only want to call when textbox has no value since refocusing causes dropdown menu to disappear/reappear.  So want to limit the number of times we refocus.  
                {
                    Keyboard.ClearFocus();
                    Keyboard.Focus(TextBoxChildRR);
                }

                //Show the dropdown.  
                CTryCatchWrapper.wrap(delegate //Need try/catch for very rare circumstance where a combobox has another event handler on it (watching for value to change).  Throws error "combobox XXX is not set to an instance of an object".  I tried using an "othis" for combobox but didn't fix it.  Try/catch works fine.  
                {
                    //This focus check prevents cbox from dropping the list down when setting default value.  I had it happen a couple times.  
                    //HasFocusRR works better than IsFocused.  Did not have much luck with IsFocused.  
                    if (this.HasFocusRR)
                    { 
                        this.IsDropDownOpen = true;  
                    }
                        
                },false);

                //Prevents bug where clicking first keyboard character to search selects the same character.  So a user had to enter the first character twice.  
                //Code below basically just unselects the text and moves cursor to far right.  
                this.TextBoxChildRR.SelectionStart = TextBoxChildRR.Text.Length;
                this.TextBoxChildRR.SelectionLength = 0;
            };


            //(Mentioned above also). 
            //PauseFilter is only set when selection changed event fires (aka when user is browsing menu items with down/up keys).  
            //During this time, the filter is paused since user is deciding which item to pick.    
            //SelectionChanged fires "before" TextChanged.  I tested.  
            this.SelectionChanged += (obj, args) =>
            {
                if (this.SelectedItem != null)
                    PauseFilter = true;
                else
                    PauseFilter = false;

                return;

            };

            //Visual only. 
            //To recreate need: 
            //1. Put cursor in combobox.
            //2. Press DOWN a couple times.  Select an item with long description. 
            //3. Press enter.  Notice the cursor defaults to far right (which basically aligns text to far right).  We want cursor to show at far left instead.
            this.DropDownClosed += (obj,args) =>
            {
                MoveCursorFarLeftRR();
            };



 
            //Use PreviewKeyDown (not KeyDown) since KeyDown cannot detect Backspace nor Delete keys.  https://stackoverflow.com/a/50769901/722945
            this.PreviewKeyDown+= (object obj, KeyEventArgs args) =>
            {
                //This check is not needed, since native combobox hijacks keyboard arrow keys "Down" and "Up" (since combobox already watches those keys to change selected item).  
                //Leaving the check in for fyi sake.  
                if (args.Key.ToString() != "Up" && args.Key.ToString() != "Down")
                {
                    PauseFilter = false;
                }

                //Safety: Ensures no item is selected when text is empty.  This snippet resolves the issue.  Only happened when text search was enabled.  
                //If want to test: 
                //1. Comment out this snippet.  Ensure property IsSearchableRR=true.
                //2. Add this snippet somewhere in this control.  
                //      this.SelectionChanged+=(object obj, SelectionChangedEventArgs args)=>
                //      {
                //          CMsgBox.Show("changed: value: " + this.getSelectedValue());
                //      };
                //2. Run app.  Select value.  
                //3. Click inside combobox.  Press backspace enough times to clear value.
                //4. Press Enter key.  Will see old previously selected value.  
                if (this.Text == "")
                    this.SelectedIndex = -1; 



                //Resolves a bug where selected item was stuck and could not change when user clicked backspace/delete.
                //To recreate: 
                //1. Click inside combobox.
                //2. Press DOWN key a couple times to select an item.
                //3. Press ENTER key (important, this triggers the bug). 
                //4. Press TAB or mouse click out of the combobox. 
                //5. Press SHIFT+TAB or mouse click into combobox. 
                //Done.  At this point, pressing delete/backspace has no effect on selected value.  The selected value is stuck.  
                if (args.Key == Key.Delete || args.Key == Key.Back ) //"Back" for backspace (I tested).  
                {
                    //Clear all text and choices.    
                    this.SelectedIndex = -1; //This also changes the combobox to empty.  I tested.  
                }
                //Ensures a new search retains first character pressed.  Had a bug where person searching had to type first character twice.   
                else if (TextBoxChildRR.IsAllTextSelectedRR() && args.Key != Key.Up && args.Key != Key.Down && args.Key != Key.Left && args.Key != Key.Right && args.Key != Key.Return && args.Key != Key.Enter && args.Key != Key.Tab && args.Key != Key.LeftShift && args.Key != Key.RightShift && args.Key != Key.CapsLock )
                {
                    //Clear all text and choices.    
                    this.SelectedIndex = -1; //This also changes the combobox to empty.  I tested. 
                }

            };

         
            //Erase textbox value if matching item is not selected.  
            //Also select item if textbox matches an item content.  
            //Fyi: Ensures only listed items are allowed.  
            this.LostFocus += (obj, args) =>
            {
                if (TextBoxChildRR.Text != "")
                {
                    if (DoesContentValueExist(TextBoxChildRR.Text))
                    {
                        selectByContent(TextBoxChildRR.Text);

                        //TODO: This method executes correctly.  However we are not calling it in the correct bloc of code.  
                        //To recreate issue:
                        //1. Put cursor in combobox.
                        //2. Press DOWN a couple times to find item with long description.
                        //3. Press TAB to exit the control.  Notice we exited the control without the "drop down" event happening. 
                        //Done..Will notice cursor has not moved to far left.  
                        //PS. I tried calling this in the textbox->"on preview lost focus" but it had no effect.  
                        MoveCursorFarLeftRR(); //This does not work that great.  If one tabs 
                    }
                    else
                    {
                        TextBoxChildRR.Text = "";
                        this.SelectedIndex = -1; //For heck of it.  
                    }
                }
            };


            this.GotFocus += (obj, args) =>
            {
                //Little help for skilled typists.  Select all the text on first focus event.  
                this.TextBoxChildRR.SelectAll();
            };












        }


        /// <summary>
        /// Filter used for textbox search.  
        /// Applied to CollectionView.Filter = this method.  
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// Tested: 2019-09-12
        private bool CollectionView_FilterRR(object obj)
        {
            ComboBoxItemRR itm = obj as ComboBoxItemRR;

            var searchText = TextBoxChildRR.Text;

            //Optional: Allow first blank choice.
            if (FilterAllowBlankChoice && itm.Value == "")
                return true;

            //----------------------
            //First filter: 
            //Check user's entered search string.  
            //----------------------
            if (!itm.Content.ContainsRR(searchText) && searchText != "")
                return false;

            //----------------------
            //Secondary filters: 
            //Check if optional "property string XXX" value matches.  
            //These are only used if we want "additional" filters placed on the combobox.  
            //----------------------
            if (CBoxItemFilter_PropertyString01.HasValueRR())
                if (itm.PropertyString01.ToUpper() != CBoxItemFilter_PropertyString01.ToUpper())
                    return false;

            if (CBoxItemFilter_PropertyString02.HasValueRR())
                if (itm.PropertyString02.ToUpper() != CBoxItemFilter_PropertyString02.ToUpper())
                    return false;

            if (CBoxItemFilter_PropertyString03.HasValueRR())
                if (itm.PropertyString03.ToUpper() != CBoxItemFilter_PropertyString03.ToUpper())
                    return false;

            if (CBoxItemFilter_PropertyString04.HasValueRR())
                if (itm.PropertyString04.ToUpper() != CBoxItemFilter_PropertyString04.ToUpper())
                    return false;

            if (CBoxItemFilter_PropertyString05.HasValueRR())
                if (itm.PropertyString05.ToUpper() != CBoxItemFilter_PropertyString05.ToUpper())
                    return false;

            if (CBoxItemFilter_PropertyString06.HasValueRR())
                if (itm.PropertyString06.ToUpper() != CBoxItemFilter_PropertyString06.ToUpper())
                    return false;

            if (CBoxItemFilter_PropertyString07.HasValueRR())
                if (itm.PropertyString07.ToUpper() != CBoxItemFilter_PropertyString07.ToUpper())
                    return false;

            if (CBoxItemFilter_PropertyString08.HasValueRR())
                if (itm.PropertyString08.ToUpper() != CBoxItemFilter_PropertyString08.ToUpper())
                    return false;

            return true;
        }


        /// <summary>
        /// Special Fody->PropertyChanged override method.  
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="before"></param>
        /// <param name="after"></param>
        /// Tested: 2019-09-12
        public void OnPropertyChanged(string propertyName, object before, object after)
        {
            //Reapply filter for visual sake.  
            //Without this, the user will see all choices.  I tested.  
            if (propertyName.EqualsAnyRR(nameof(CBoxItemFilter_PropertyString01), nameof(CBoxItemFilter_PropertyString02), nameof(CBoxItemFilter_PropertyString03), nameof(CBoxItemFilter_PropertyString04), nameof(CBoxItemFilter_PropertyString05), nameof(CBoxItemFilter_PropertyString06), nameof(CBoxItemFilter_PropertyString07), nameof(CBoxItemFilter_PropertyString08) ) )
            {
                //Only need this safety if filtering comboboxes before they have finished rendering etc.  
                if (cv != null)
                {
                    //This works well to "un-stuck" the choice.  Mainly a visual thing.  It also does not interfere with cbox data binding (the binding link remains).    
                    this.SelectedIndex = -1; 

                    cv.Filter = CollectionView_FilterRR;
                }
                    
            }

            //Required to trigger original event since OnPropertyChanged intercepts the original event.  See OneNote.  
            //PropertyChanged is null until it gets subscribed to.  
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
























    }




}
