﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using MS.Internal;

namespace rr_general.wpf_custom_control.microsoft_source_code_override
{
    /// <summary>
    /// Used by DataGridComboBoxColumnRR override to swap out the native ComboBox with my ComboBoxRR.  
    /// The native ComboBox does not virtualize and chokes on large datasets.  My ComboBoxRR automatically virtualizes all datasets.  
    /// 
    /// Code below is from Microsoft's .Net 4.8 library here: https://referencesource.microsoft.com/#PresentationFramework/src/Framework/System/Windows/Controls/DataGridHelper.cs,4d69af066a7c1080
    /// Custom overrides are noted with ★★★.  
    /// </summary>
    /// Created: 2020-07-11
    public static  class DataGridHelperRR
    { 
        public static bool IsDefaultValue(DependencyObject d, DependencyProperty dp)
        {
            return DependencyPropertyHelper.GetValueSource(d, dp).BaseValueSource == BaseValueSource.Default;
        }

        //★★★ changed from internal to public.  
        public static void SyncColumnProperty(DependencyObject column, DependencyObject content, DependencyProperty contentProperty, DependencyProperty columnProperty)
        {
            if (IsDefaultValue(column, columnProperty))
            {
                content.ClearValue(contentProperty);
            }
            else
            {
                content.SetValue(contentProperty, column.GetValue(columnProperty));
            }
        }


    }
}
