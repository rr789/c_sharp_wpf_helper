﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bindables;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// See xaml file for documentation about this control.  
    /// </summary>
    public partial class ButtonRR : Button, INotifyPropertyChanged
    {
        //Boilerplate 
        public event PropertyChangedEventHandler PropertyChanged;
        
        [DependencyProperty]
        public string ContentRR { get; set; }

        //Constructor
        public ButtonRR()
        {
            InitializeComponent();
        }

        
    }
}
