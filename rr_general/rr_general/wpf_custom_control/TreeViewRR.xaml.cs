﻿using class_helper_rr;
using rr_general.wpf_custom_control.treeview_item_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace rr_general.wpf_custom_control
{

    /// <summary>
    /// See notes in TreeViewWithSearchRR for complete example.  
    /// </summary>
    public partial class TreeViewRR : TreeView
    {
        /// <summary>
        /// Stores reference to node last selected.  
        /// To get underlying data object just access "selectedNode.DataContext" property.   
        /// </summary>
        public TreeViewItem selectedNode;


        /// <summary>
        /// Attach events for when ItemsSource changes.  Aka when SetItemsSourceRR is called.  
        /// </summary>        
        /// <example>
        /// treeObj.Event_ItemsSourceAfterChange += delegate { CMsgBox.Show("hi"); };
        /// </example>
        public event EventHandler Event_ItemsSourceAfterChange;


        /// <summary>
        /// Stores value for ItemsSourceRR.  
        /// </summary>
        private itemRR _ItemsSourceRR;

        /// <summary>
        /// Use this to set ItemsSource.  
        /// - Calls event handler. 
        /// - Applies TreeViewRR property to each item.  
        /// 
        /// Native ItemsSource property has no "on data changed" events.  I checked many articles.   
        /// </summary>
        /// <example>
        /// //Add collection to TreeView.
        /// treeObj.ItemsSourceRR = item01; 
        /// 
        /// //Attach event when ItemsSourceRR changes.
        /// treeObj.Event_ItemsSourceAfterChange += this.myhandler;
        /// 
        /// private void myhandler(object sender, EventArgs e)
        /// {
        ///     CMsgBox.Show(this.Name); //Shows TreeView name.  
        /// }
        /// </example>
        /// Tested: 2017-05-27 all passed.
        public itemRR ItemsSourceRR
        {
            get { return _ItemsSourceRR; }
            set
            {
                //Safety: Need TreeView loaded in UI for ItemContainerGenerator.    
                if (this.IsLoaded == false)
                {
                    CMsgBox.Show("Error in TreeViewRR: Cannot set TreeView->ItemsSource before TreeView is loaded.");
                }
                else
                {
                    if (_ItemsSourceRR != value)
                    {
                        //Set value.
                        _ItemsSourceRR = value;

                        //Set native ItemsSource.
                        this.ItemsSource = _ItemsSourceRR.Items;

                        //Fire any custom events. 
                        if (this.Event_ItemsSourceAfterChange != null) { this.Event_ItemsSourceAfterChange(this, new EventArgs()); }

                        //Turn apply TreeItemRR property to every itemRR.  
                        Apply_TreeViewItemRR_ToItemsSource();

                    }
                }
            }
        }


        //Constructor.
        public TreeViewRR()
        {
            //Register event handlers.  
            AddHandler(TreeViewItem.SelectedEvent, new RoutedEventHandler(OnItemSelectedHandler)); //TreeViewItem is selected.

        }




        /// <summary>
        /// Fires when tree item is selected.  Keeps selectedNode property updated.  
        /// http://stackoverflow.com/a/15976957/722945
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// Tested: 2017-05-15 all passed.  
        private void OnItemSelectedHandler(object sender, RoutedEventArgs e)
        {
            selectedNode = e.OriginalSource as TreeViewItem;
        }


        /// <summary>
        /// Expands all notes in tree, all levels deep.  
        /// Mainly used to ensure all nodes are visible, so ItemContainerGenerator can grab them.  
        /// </summary>
        /// Tested: 2017-05-21 all passed.
        public void expandAllNodes()
        {
            if (this.ItemsSource != null)
            {
                //Use native ItemsSource for this section since this is called when control is first loaded (via Apply_TreeViewItemRR_ToItemsSource()).    
                foreach (itemRR i in this.ItemsSource)
                {
                    var t = (TreeViewItem)this.ItemContainerGenerator.ContainerFromItem(i);
                    t.ExpandSubtree(); //ExpandSubtree expands current node and all child nodes.  
                }
            }
        }


        /// <summary>
        /// Collapses all notes in tree, all levels deep.  
        /// Make sure to use after Apply_TreeViewItemRR_ToItemsSource() is run since this uses property TreeViewItemRR.   
        /// </summary>
        /// Tested: 2017-05-21 all passed.
        public void collapseAllNodes()
        {
            ItemsSourceRR.EachRR(itm =>
            {
                itm.ExpandNodeAllLevels(false);
            });

        }


        /// <summary>
        /// Wrapper to apply property TreeViewItemRR to ALL items in ItemsSource.   
        /// </summary>
        public void Apply_TreeViewItemRR_ToItemsSource()
        {
            if (this.ItemsSource != null)
            {
                //Always call first.  Must expand node (once) so a container gets rendered in UI.  Then ItemContainerGenerator can grab container.  Can collapse anytime, nodes just need to get expanded once.   
                this.expandAllNodes();

                foreach (itemRR item in this.ItemsSource)
                {
                    //This method recursively drills down to all children.  
                    Apply_TreeViewItemRR_ToNode(item);
                }

                //Call afterword.  
                this.collapseAllNodes();
            }
        }



        /// <summary>
        /// Applies property TreeViewItemRR to node and child nodes.  
        /// </summary>
        /// <param name="item"></param>
        /// <param name="countDepth"></param>
        /// Tested: 2017-05-21 all passed.  
        public void Apply_TreeViewItemRR_ToNode(itemRR item, int countDepth = 0)
        {
            if (item != null)
            {
                //------------------------------------
                //Apply TreeViewItem to level object.  
                //------------------------------------

                //First level uses TreeView itself to find container.  
                //ItemContainerGenerator can only find containers for first level children.  
                if (countDepth == 0)
                {
                    item.TreeViewItemRR = (TreeViewItem)this.ItemContainerGenerator.ContainerFromItem(item);
                }
                //Deeper children: Call ItemContainerGenerator from parent node.     
                else
                {
                    var parent_ = (itemRR)item.Parent;
                    item.TreeViewItemRR = (TreeViewItem)parent_.TreeViewItemRR.ItemContainerGenerator.ContainerFromItem(item);
                }


                //------------------------------------
                //Apply TreeViewItem to children (recursive).  
                //------------------------------------
                foreach (itemRR i in item.Items)
                {
                    Apply_TreeViewItemRR_ToNode(i, countDepth + 1);
                }
            }

        }







    }
}
