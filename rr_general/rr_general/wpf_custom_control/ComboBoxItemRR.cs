﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Purpose: 
    ///     Superior to native ComboBoxItem since ComboBoxItemRR can get created in background threads.    
    ///     Background threads cannot create XAML elements like ComboBoxItem.  
    /// </summary>
    /// 
    /// <example>
    /// //------------------
    /// //In C#
    /// //------------------
    /// public List<ComboBoxDataSourceRR> VendorList { get; set; }
    /// 
    /// //Inside method.
    /// VendorList = new List<ComboBoxItemRR>();
    /// VendorList.Add(new ComboBoxItemRR() { Content = "visible name", Value = "hidden value" });
    /// 
    /// //------------------
    /// //In XAML
    /// //------------------
    /// <wpf_custom_control:ComboBoxRR  Name="cbox_vendor_list" Width="200" HorizontalAlignment="Left"  Margin="8,0"
    /// 								DisplayMemberPath="Content"
    /// 								ItemsSource="{Binding ElementName=root, Path=vendorList}"/>
    /// </example>
    /// Tested: 2018-09-08
    public class ComboBoxItemRR
    {
        /// <summary>
        /// Use "content" property for visible text. 
        /// </summary>
        public string Content { get; set; } 

        /// <summary>
        /// Use "value" property for behind the scenes value.  
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// ComboBoxRR has an OnLoad method to loop thru and selected the item where this property = true.  
        /// </summary>
        public bool IsSelected { get; set; }

        //----------------------------------
        //OPTIONAL PROPERTIES.  
        //----------------------------------

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString01 { get; set; }

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString02 { get; set; }

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString03 { get; set; }

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString04 { get; set; }

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString05 { get; set; }

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString06 { get; set; }

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString07 { get; set; }

        /// <summary>
        /// Optional field to store extra values. 
        /// </summary>
        public string PropertyString08 { get; set; }




    }
}
