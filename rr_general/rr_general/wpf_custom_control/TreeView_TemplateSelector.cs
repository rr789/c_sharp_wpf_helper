﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using rr_general.wpf_custom_control;
using rr_general.wpf_custom_control.treeview_item_rr;

namespace rr_general.wpf_custom_control
{

    /// <summary>
    /// Selects datatemplate based on ItemRR->typeRR property. 
    /// 
    /// </summary>
    /// <example>
    /// Just the xaml portion.  
    /// 
    /// <!-- Apply template to items with GROUP property.  --> 
    /// <HierarchicalDataTemplate x:Key="GroupTemplate" DataType="{x:Type treeviewitem_rr:itemRR }" ItemsSource="{Binding Items}"  >
    /// 	<custom:StackPanelRR Orientation="Horizontal" >
    /// 		<Image Style="{StaticResource FolderOpenCloseStyle}" /> 
    /// 		<custom:TextBlockRR Text="{Binding Path=NameDisplay}" />
    /// 	</custom:StackPanelRR>
    /// </HierarchicalDataTemplate>
    /// 
    /// <!-- Apply template to items with ITEM property.  --> 
    /// <DataTemplate x:Key="ItemTemplate" DataType="{x:Type treeviewitem_rr:itemRR }">
    /// 	<StackPanel>
    /// 		<custom:TextBlockRR Text="{Binding Path=NameDisplay}" />
    /// 	</StackPanel>
    /// </DataTemplate>
    /// 
    /// <!-- Define template selector here.  --> 
    /// <custom:TreeView_TemplateSelector x:Key="MyTemplateSelector"
    /// 	oGroupTemplate="{StaticResource GroupTemplate}"
    /// 	oItemTemplate="{StaticResource ItemTemplate}"
    ///  />
    /// </example>
    /// <remarks>
    /// From http://stackoverflow.com/a/10769273/722945
    /// Tested: 2017-05-15 all passed.
    /// </remarks>
    class TreeView_TemplateSelector : DataTemplateSelector
    {
        public HierarchicalDataTemplate oGroupTemplate { get; set; }
        public DataTemplate oItemTemplate { get; set; }
        public DataTemplate oDefaultTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            itemRR e = (itemRR)item;
            DataTemplate r = null; //Returned.

            switch (e.TypeRR)
            {
                case itemRR.GROUP:
                    r = oGroupTemplate;
                    break;

                case itemRR.ITEM: //Optional (future sake).   Default applies ITEM template also.    
                    r = oItemTemplate;
                    break;

                default:
                    r = oItemTemplate; //Can change this later if needed.  For now, default is the ItemTemplate.  For heck of it.  
                    break;
            }

            return r;

        }



    }
}
