﻿using class_helper_rr;
using rr_general.helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// A button with spinner, with event to hook into when clicked.  
    /// </summary>
    /// Tested: 2017-07-30.
    public partial class DataGridPopupCancelQuery : UserControl, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        public BitmapImage spinner { get; set; }

        /// <summary>
        /// Event for external classes to hook into.  
        /// </summary>
        public EventHandler cancelButtonClicked { get; set; }


        public DataGridPopupCancelQuery()
        {
            InitializeComponent();
            CLoadedEvent.LoadedHandler(this, onLoad);
        }

        public void onLoad(object sender, EventArgs e)
        {
            spinner = ImageResourceRR.spinner_circle_20px_grey_rr;
        }

        /// <summary>
        /// Fires external button click event when internal button is clicked.    
        /// Also disposes itself.  
        /// </summary>
        /// Tested: 2017-07-30.
        private void buttonClick(object sender, EventArgs e)
        {
            if (cancelButtonClicked != null)
                cancelButtonClicked(sender, e);

            this.DisposeSelfRR<DataGridPopupCancelQuery>();
        }

        /// <summary>
        /// Adds element to visual tree.  
        /// </summary>
        /// Tested: 2017-08-12.
        public void displayInVisualTree()
        {
            //Get current window.  
            var win = Application.Current.MainWindow;

            win.AddChildRR(this);
        }

    }
}
