﻿using rr_general.wpf_custom_control.popup;
using class_helper_rr;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using rr_general.config;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Interaction logic for DataGridRR.xaml
    /// Note: To enable saving column preferences...need to set profileGridIdentifierID (AND) x:Name.  Then it will save preferences automatically.
    /// </summary>
    public partial class DataGridRR : DataGrid, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Unique ID set manually.  CDatagridConfig uses this value as filename.  For user profile settings. 
        /// Note: As mentioned above, need to set this (AND) x:Name to save preferences automatically.  
        /// </summary>
        public string profileGridIdentifierID { get; set; }

        /// <summary>
        /// Popup window to re-order columns etc.  
        /// </summary>
        public DataGridPopupConfig configPopup { get; set; }

        /// <summary>
        /// Stores user preferences (column order, visible columns etc).  
        /// </summary>
        public CDatagridConfig configPrefs { get; set; }

        //----------------------------------------------------------
        /// <summary>
        /// Default config menu items allowed to show in grid header (aka column headers).  Defaults to visible.   
        /// Used in xaml.  
        /// </summary>
        public Visibility Menu_GridHeader_DefaultOption_Visibility { get; set; } = Visibility.Visible;

        /// <summary>
        /// Default config menu items allowed to show in grid body.  Defaults to visible.   
        /// Used in xaml.  
        /// </summary>
        public Visibility Menu_GridBody_DefaultOption_Visibility { get; set; } = Visibility.Visible;

        //----------------------------------------------------------

        /// <summary>
        /// Provides concise way to disable sort on all columns.  So no need to define CanUserSort="False" on each column.  
        /// True: User cannot sort any data grid column.  
        /// False: Default.  User can sort.  
        /// </summary>
        public bool DisableSortAbilityRR { get; set; } 


        /// <summary>
        /// Fires when any column width changes.  Applies to all columns in grid.  
        /// Mouse activated.  See example.  
        /// </summary>
        /// <example>
        /// grid.ColumnWidthChangedRR += (sender, e) =>
        /// {
        ///     var col = (DataGridTextColumn)sender;
        ///     CMsgBox.Show(col); 
        /// };
        /// </example>
        public event PropertyChangedEventHandler ColumnWidthChangedRR;



        /// <summary>
        /// Fires "after" changes are committed to data source.  
        /// ★★★ Note: Does not work on columns with "UpdateSourceTrigger=PropertyChanged".  That setting causes sanity check props beforeRowEditJson/afterRowEditJson to have same values, so the event never fires.  2020-10-22
        /// Alternative to event RowEditEnding which fires "before" changes are committed to data source. 
        /// </summary>
        /// <example>
        /// User updated a dg cell, then pressed enter.Once the data source is updated, then this event fires.
        /// </example>
        /// Tested: 2018-10-07
        public event EventHandler RowEditEndedRR;

        /// <summary>
        ///     Fires when row is finished editing.  Will only fire if at least one cell on the row has changed value.  
        ///     ★★★ Note: Does not work on columns with "UpdateSourceTrigger=PropertyChanged".  That setting causes sanity check props beforeRowEditJson/afterRowEditJson to have same values, so the event never fires.  2020-10-22
        /// </summary>
        /// <example>
        ///     To get previoulsy edited row: 
        ///         - Access "sender" that gets passed. 
        ///         (OR)
        ///         - Call GetSelectedRows() inside this RowEditEndedRR handler.  It will return the previously edited row (even if user enters new value then clicks on another row, I tested).  
        /// </example>
        /// <remarks>
        ///     As a bonus, this even updates the grid's data source before firing the event RowEditEndedRR.
        /// </remarks>
        /// Tested: 2021-04-22
        private void FireEvent_RowEditEndedRR(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (RowEditEndedRR == null)
                return;
            
            if (this.SelectedItem == null)
                return;

            //Snapshot of all cell values on row "before" user updated values.  I tested before/after, this works.    
            string beforeRowEditJson = JsonConvert.SerializeObject(this.SelectedItem);

            //Extra action...commit changes to data source.  
            //We need to commit these changes since event RowEditEnding fires "before" changes are committed to data source.  https://stackoverflow.com/a/20595403/722945
            (sender as DataGridRR).RowEditEnding -= FireEvent_RowEditEndedRR;
            (sender as DataGridRR).CommitEdit();
            
            //2019-07-21 I commented this out, had a big performance hit after editing a single row.  It didn't do anything... 
            ////(sender as DataGridRR).Items.Refresh();
            
            (sender as DataGridRR).RowEditEnding += FireEvent_RowEditEndedRR;

            //Snapshot of all cell values on row "after" user updated values.  I tested before/after, this works.  
            string afterRowEditJson = JsonConvert.SerializeObject(this.SelectedItem);

            if (beforeRowEditJson != afterRowEditJson)
                RowEditEndedRR?.Invoke(this.SelectedItem, EventArgs.Empty);   

            //Debug: 
            //Clipboard.SetText("BEFORE: \n" + beforeRowEditJson + "\n\nAFTER:\n" + afterRowEditJson);

        }




        //Constructor.
        public DataGridRR()
        {
            InitializeComponent();
            CLoadedEvent.LoadedHandler(this, onLoad);
        }

        public void onLoad(object sender, EventArgs e)
        {
            //Called in onLoad, since "this datagrid" isn't fully created in constructor.  
            configPrefs = new CDatagridConfig(this);

            //Setup menus.
            Setup_MenuGridBody();

            applyListener_ColumnWidthChangedRR();
            applyListener_ColumnHeaderRightClick();

            callLoadedEventOnAllColumns();

            //Disable sort ability on all columns (if param is set).  
            if (DisableSortAbilityRR)
            {
                for (var i = 0; i < this.Columns.Count; i++)
                {
                    this.Columns[i].CanUserSort = false;
                }
            }

            //Hook into event.  
            this.RowEditEnding += FireEvent_RowEditEndedRR;


        }

        /// <summary>
        /// Calls loadedRR() event on all custom column types.  
        /// </summary>
        /// <remarks>
        /// Relying on parent DataGridRR is the only way.  Columns have no loaded event.  Also, calling GetDataGridParent() in constructor returns null (since grid is not yet part of visual tree).  https://stackoverflow.com/questions/4518222/find-root-element-of-datagridcolumn
        /// </remarks>
        /// Tested: 2017-09-05.
        private void callLoadedEventOnAllColumns()
        {
            //Gets string name of method "loadedRR".  In case the name is refractored in future...
            var dummyColumn = new DataGridTextColumnRR(); //Only used to get loadedRR method name, then disposed of.  
            string methodName = nameof(dummyColumn.loadedRR_FireEvent); //Ex: "loadedRR". 

            //Call method on each column.
            this.Columns.EachRR(col =>
            {
                //If it has loadedRR() method.  
                if (col.hasMethodRR(methodName))
                {
                    //Ex: loadedRR(sender, eventArgs);  
                    col.InvokeMethodRR(methodName, new object[] { col, new EventArgs() });
                }
            });            

        }

        /// <summary>
        /// Sets grid "CurrentColumn" (aka selected column) when column header is right clicked.  
        /// Resolves bug where first click a grid receives is on the column header, CurrentColumn is not set.  
        /// </summary>
        /// Tested: 2017-07-08.
        private void applyListener_ColumnHeaderRightClick()
        {
            this.Columns.EachRR(col =>
            {
                DataGridColumnHeader h = col.GetHeader();

                //Safety: Header obj only available when column is visible.  See https://stackoverflow.com/a/4031253/722945 
                if (h != null)
                {
                    var grid = h.Column.GetDataGridParent();

                    h.PreviewMouseRightButtonDown += delegate
                    {
                        grid.CurrentColumn = h.Column;
                    };
                }
            });
        }


        /// <summary>
        ///     Convenient way to get selected row(s).  Converts to data contract type.  
        /// </summary>
        /// <example>
        ///     //Grab rows.
        ///     var selectedRows = GridMain.GetSelectedRows<DC_DOCMAN_INVOICE_HEAD_SCRAPED_V>();
        ///     
        ///     //Get first selected row's value in column "SAMPLE_COLUMN".  Assumes there is a data contract with property SAMPLE_COLUMN.  
        ///     selectedRows[0].SAMPLE_COLUMN;  
        /// </example>
        /// Tested: 2019-07-21
        public List<T> GetSelectedRows<T>()
        {
            return this.SelectedItems.Cast<T>().ToList(); //Using the regular "cast" syntax does not work.  Instead had to cast like this.  Cast trick from here: https://stackoverflow.com/a/24964527/722945
        }



        /// <summary>
        /// Builds combined collection for grid body menu.  Imports default config menu items + custom menu items defined in an external app.  
        /// Then replaces existing ContextMenu with a new (combined) collection.      
        /// </summary>
        /// Tested: 2024-08-15
        private void Setup_MenuGridBody()
        {
            //Grab menu defined in XAML.  For the default options like "export to Excel" etc.  
            var menuDefaultOptions = (ContextMenuRR)this.Resources["Menu_GridBody_DefaultOption"];

            //Load both "default menu" and "custom menu from external app" into one combined menu.  
            var menuCombinedFinal = new ContextMenuRR();

            //Part 01: Import default menu items.  
            menuCombinedFinal.ImportMenu(menuDefaultOptions);
            //Part 02: Import custom menu items defined in external app.  
            menuCombinedFinal.ImportMenu((ContextMenuRR)this.ContextMenu); 

            //Assign main ContextMenu to final collection.  
            //TODO 2024-08-15 I'd like to replace this C# hack with binding in the XAML side.  
            this.ContextMenu = menuCombinedFinal;

        }


        /// <summary>
        /// Applies "did width change?" listener to every column.  
        /// Each column listener fires the grid's umbrella event ColumnWidthChangedRR();  
        /// </summary>
        /// Tested: 2017-07-03 all passed.  
        private void applyListener_ColumnWidthChangedRR()
        {
            this.Columns.EachRR(col =>
            {
                //Header is only column element with decent events.  
                DataGridColumnHeader h = col.GetHeader();

                //Safety: Header obj only available when column is visible.  See https://stackoverflow.com/a/4031253/722945  So, will need to call this method again if hidden columns become visible.  
                if (h != null)
                {
                    var oldWidth = h.ActualWidth;

                    //MouseLeave works best.  SizeChanged fires at every pixel size change.  https://stackoverflow.com/a/8450445/722945
                    h.MouseLeave += (sender, e) =>
                    {
                        //Fire event.  
                        if (ColumnWidthChangedRR != null)
                            if (oldWidth != h.ActualWidth)
                                ColumnWidthChangedRR(h.Column, new PropertyChangedEventArgs(null));

                        oldWidth = h.ActualWidth;
                    };
                }
            });

        }



        /// <summary>
        /// Handles creating popup window that displays column order, hide/show etc.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// Tested 2017-06-20.
        private void menuClick_ShowPopup(object sender, RoutedEventArgs e)
        {
            //Re-create window, since when window closes the visual elements are gone.  
            configPopup = new DataGridPopupConfig(true, this);

        }


        /// <summary>
        /// Hides current column with right-mouse button option.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// Tested: 2017-07-08.
        private void menuClick_HideColumn(object sender, RoutedEventArgs e)
        {
            //Safety: Related to clicking on empty space to the right.  
            if (this.CurrentColumn == null)
                return;
            
            if (this.countVisibleColumns() > 1)
            {
                //CurrentColumn is the last clicked/selected column.  
                this.CurrentColumn.Visibility = Visibility.Hidden;

                configPrefs.copyGridToDisk();
            }
            else
                CMsgBox.Show("Cannot hide last visible column.  ");

        }

        /// <summary>
        /// Provides a menu item to save datagrid column order/width preferences manually. 
        /// </summary>
        /// <remarks>
        /// 2024-05-23 Encountered a situation where the datagrid event for method applyListener_ColumnWidthChangedRR() did not 
        /// trigger when a datagrid was in a tab control.  Datagrid in first tab triggered OK.  However, datagrids in second/third tabs
        /// did not fire the MouseLeave event.  
        /// </remarks>
        private void menuClick_SavePreference(object sender, RoutedEventArgs e)
        {
            configPrefs.copyGridToDisk();
        }



        /// <summary>
        /// Returns count of visible columns.    
        /// </summary>
        /// Tested: 2017-07-08.
        public int countVisibleColumns()
        {
            return  (
                        from col in this.Columns
                        where col.Visibility == Visibility.Visible
                        select col
                    ).Count();

        }



        /// <summary>
        /// Adds record to DataGrid.  No column type.    
        /// </summary>
        /// <example>
        /// IOrderedDictionary record = new OrderedDictionary();
        /// record["col1"] = "val1.1";
        /// record["col2"] = "val2.1";
        /// myDataGrid.addRecordNoType(record);
        /// </example>
        /// <remarks>
        /// Tested: 2017-04-01 all passed.  
        /// </remarks>
        /// <param name="arr"></param>
        public void addRecordNoType(IOrderedDictionary arr)
        {
            //Using Dynamic type so VS is not picking about setting properties with brackets (ex: record[columnName]). Example here: http://stackoverflow.com/a/35921445/722945
            ExpandoObject record = new ExpandoObject();

            IDictionaryEnumerator myEnumerator = arr.GetEnumerator();
            while (myEnumerator.MoveNext())
            {
                string columnName = myEnumerator.Key as string;
                string columnValue = myEnumerator.Value as string;

                //ExpandoObject will accept IDictionary cast. 
                //IDictionary needed for indexing.  Otherwise VS throws error "Cannot apply indexing with [] to an expression of type 'System.Dynamic.ExpandoObject'".  See http://stackoverflow.com/a/7829099/722945  
                ((IDictionary<string, object>)record)[columnName] = columnValue;
            }

            this.Items.Add(record);
        }



        /// <summary>
        /// Moves all selected rows up by one row.  
        /// </summary>
        /// Tested: 2017-07-02 all passed.  
        public void moveAllSelectedRowsUp<rowDataType>()
        {
            for (var i = 0; i < this.Items.Count; i++)
            {
                DataGridRow r = (DataGridRow)(this.ItemContainerGenerator.ContainerFromIndex(i));
                if (r.IsSelected)
                    r.moveUp<rowDataType>();
            }
        }


        /// <summary>
        /// Moves all selected rows down by one row.  
        /// </summary>
        /// Tested: 2017-07-02 all passed.  
        public void moveAllSelectedRowsDown<rowDataType>()
        {
            //Have to loop backwards when moving "down".  
            for (var i=this.Items.Count-1; i>=0; i--)
            {
                DataGridRow r = (DataGridRow)(this.ItemContainerGenerator.ContainerFromIndex(i));
                if (r.IsSelected)
                    r.moveDown<rowDataType>();
            }
        }


        /// <summary>
        /// Returns column object whose header title matches param.  
        /// </summary>
        /// Tested: 2017-09-02
        public DataGridColumn GetColumnByHeaderTitle(string name)
        {
            DataGridColumn r = null;

            this.Columns.EachRR(col =>
            {
                if (col.Header.ToString() == name)
                    r = (DataGridColumn)col;
            });

            return r;
        }


        /// <summary>
        /// Gets column object that matches display index.  
        /// </summary>
        /// <remarks>
        /// Using grid.columns[1] does not grab by DisplayIndex.  Rather it references the original order in which columns were defined.  Moving columns around does not affect the "columns" collection.  
        /// </remarks>
        /// <param name="displayIndex_"></param>
        /// <returns></returns>
        public DataGridColumn GetColumnByDisplayIndex(int displayIndex_)
        {
            DataGridColumn r = null;

            this.Columns.EachRR(col =>
            {
                if (col.DisplayIndex == displayIndex_)
                    r = col;
            });

            return r;
        }

      

        /// <summary>
        /// Enable single-click editing on a cell.  
        /// Activates on DataGridComboBox and DataGridCheckBox columns (the RR columns also).  
        /// </summary>
        /// <remarks>
        /// I tried a couple xaml-only solutions, this works the best.  
        /// Modified from: http://wpf.codeplex.com/wikipage?title=Single-Click%20Editing&ProjectName=wpf
        /// </remarks>
        /// Tested: 2019-07-16.
        private void DataGridCell_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;

            //Only activate on ComboBox or CheckBox columns.  
            if (typeof(DataGridComboBoxColumn).IsAssignableFrom(cell.Column.GetType()) || typeof(DataGridCheckBoxColumn).IsAssignableFrom(cell.Column.GetType()))
            {
                if (cell != null && !cell.IsEditing && !cell.IsReadOnly)
                {
                    if (!cell.IsFocused)
                    {
                        cell.Focus();
                    }

                    var dataGrid = cell.GetParent<DataGridRR>();

                    //Safety: Prevents throwing an error "dataGrid was null".  
                    //The error happens when clicking from one row (with datagridcombobox) to another row (also with a datagridcombobox).    
                    if (dataGrid == null)
                        return;


                    if (dataGrid.SelectionUnit != DataGridSelectionUnit.FullRow)
                    {
                        if (!cell.IsSelected)
                            cell.IsSelected = true;
                    }
                    else
                    {
                        var row = cell.GetParent<DataGridRow>();

                        //This is mainly for the ComboBox column.  
                        if (row != null && !row.IsSelected)
                            row.IsSelected = true;

                    }
                }
            }
        }



        /// <summary>
        /// ★★★ NOTE: SHOULD NOT NEED TO CALL THIS IF DATACONTRACT CLASS IMPLEMENTS INOTIFYPROPERTYCHANGED.  SEE ONENOTE.  
        /// Ensures refreshing datagrid items will not throw error "System.InvalidOperationException: ''Refresh' is not allowed during an AddNew or EditItem transaction.'"
        /// </summary>
        /// <remarks>
        /// From: https://stackoverflow.com/a/28526533/722945
        /// </remarks>
        /// Tested: 2019-09-17
        public void RefreshItemsSafelyRR()
        {
            //Have to call this twice.  See Stackoverflow link above.  
            this.CommitEdit();
            this.CommitEdit();

            this.Items.Refresh();
        }





        /// <summary>
        /// Fast method to make all columns read-only.  
        /// </summary>
        /// Tested: 2019-11-07
        public void ReadOnlyAllColumns()
        {
            for (var i=0; i<this.Columns.CountRR(); i++)
            {
                var col = (DataGridColumn)this.Columns[i];

                col.IsReadOnly = true;
            }
        }



        /// <summary>
        /// Exports entire datagrid to Excel.  Only exports visible columns.  
        /// Data is put into a table/list object.
        /// </summary>
        /// <remarks>
        /// Modified from here: https://stackoverflow.com/a/35815211/722945
        /// </remarks>
        /// Tested: 2020-09-29
        public void ExportDataGridToExcel()
        {
            //----------------------
            //MISC PROPERTIES
            //----------------------
            long columnCount        = this.GetVisibleColumns().CountRR();
            long rowCount           = this.ItemsSource.CountRR();
            //Array of values that will go into Excel.  This allows us to dump all values in one shot.  Very fast.  
            var  arr                = new object[rowCount,columnCount];

            //----------------------
            //CREATE EXCEL APP.
            //----------------------
            var excel               = new Excel.Application { Visible = true };
            excel.ScreenUpdating    = false; //Some speed-up
            var book                = excel.Workbooks.Add();
            var sheet               = (Excel.Worksheet)book.Sheets[1];


            //----------------------
            //RANGES
            //----------------------
            //Table header.  One row.    
            var rangeHeader = sheet.Range["A1"].Resize[1, columnCount];

            //Table data.    
            var rangeData = sheet.Range["A2"].Resize[rowCount, columnCount];

            //Table header + data.      
            var rangeAll = sheet.Range["A1"].Resize[rowCount + 1, columnCount]; //+1 since includes header row.  


            //----------------------
            //POPULATE HEADER ROW.  
            //----------------------
            for (var i=0; i<this.GetVisibleColumns().CountRR(); i++)
            {
                string header = (string)this.GetVisibleColumns()[i].Header;

                rangeHeader.Cells[1,i+1].Value = header; //+1 since params start at 1 (not zero).  
            }


            //----------------------
            //POPULATE BODY/DATA ROWS.    
            //----------------------
            int currentRowNumber = 0;

            for (var rowNum=0; rowNum<this.ItemsSource.CountRR(); rowNum++)
            {
                var rec = this.Items.GetItemAt(rowNum);

                for (var colNum=0; colNum<this.GetVisibleColumns().CountRR(); colNum++)
                {
                    //Binding field name.  
                    string fieldName = this.GetVisibleColumns()[colNum].getBindingFieldNameRR();
                    
                    var value = rec.GetPropertyValueRR(fieldName);

                    arr.SetValue(value, currentRowNumber, colNum);
                }
                
                currentRowNumber++;

            };

            //Sets all the data rows in one shot.  Very fast.  
            rangeData.Value = arr;

            //----------------------
            //CONVERT TO TABLE/LISTOBJECT.   
            //----------------------
            // Add ListObject to sheet   https://stackoverflow.com/a/25532781/722945
            // ListObjects.AddEx Method  http://msdn.microsoft.com/en-us/library/microsoft.office.interop.excel.listobjects.addex%28v=office.14%29.aspx
            Excel.ListObject tbl = (Excel.ListObject)sheet.ListObjects.AddEx(
                SourceType: Excel.XlListObjectSourceType.xlSrcRange,
                Source: rangeAll,
                XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);

            //Dark blue header.  No row coloring.  
            tbl.TableStyle = "TableStyleLight13";

            excel.ScreenUpdating        = true;


        }


        /// <summary>
        /// Handler for the "Export to Excel" right click menu in data grid body.  
        /// </summary>
        /// Tested: 2020-09-29
        private void menuClick_ExportToExcel(object s, RoutedEventArgs e)
        {
            ExportDataGridToExcel();
        }




        /// <summary>
        /// Returns collection of visible columns.  
        /// </summary>
        /// Tested: 2020-09-29 all passed.  
        public ObservableCollection<DataGridColumn> GetVisibleColumns()
        {
            var r = new ObservableCollection<DataGridColumn>();

            for (var i=0; i<this.Columns.CountRR(); i++)
            {
                var columnObj = this.Columns[i];

                if (columnObj.Visibility == Visibility.Visible)
                    r.Add(columnObj);
            }

            return r;

        }











    }
}
