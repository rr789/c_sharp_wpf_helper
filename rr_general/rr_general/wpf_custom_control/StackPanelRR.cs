﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// This control is "code only".  Just adding a "Visibility" property, so no need for xaml.    
    /// </summary>
    public class StackPanelRR : StackPanel
    {
        //Boilerplate for DependencyProperty setup.  
        public static readonly DependencyProperty VisibleProperty = DependencyProperty.Register("Visible", typeof(bool), typeof(StackPanelRR));

        /// <summary>
        /// Boolean setter to show/hide element.  
        /// True sets Visibility=Visible.  False sets Visibility=Hidden.  Ignores "Collapsed" property.  
        /// The native property "Visibility" has string values "Visible", "Hidden", "Collapsed".  
        /// </summary>
        /// Tested: 2017-04-24 all passed. 
        public bool Visible
        {
            get { return (bool)GetValue(VisibleProperty); }
            set
            {
                //Update property.  Dependency properties use SetValue method.  
                SetValue(VisibleProperty, value);

                //Then update appearance on canvas.  
                if (value == true)
                {
                    Visibility = Visibility.Visible;
                }
                else
                {
                    Visibility = Visibility.Hidden;
                }
            }
        }

    }
}
