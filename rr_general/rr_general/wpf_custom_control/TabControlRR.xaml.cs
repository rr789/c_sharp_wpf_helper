﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using class_helper_rr;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Interaction logic for TabControlRR.xaml
    /// </summary>
    public partial class TabControlRR : TabControl
    {
        public TabControlRR()
        {
            InitializeComponent();
            CLoadedEvent.LoadedHandler(this, OnLoad);
                 
        }

        //Tested: 2019-11-07
        private void OnLoad(object sender, EventArgs args)
        {
            //Select first "enabled" tab.  Mainly for security since TabControl will show contents of tab if it was selected before tab gets disabled.   
            for (var i=0;i<this.Items.CountRR();i++)
            {
                var tab = (TabItem)this.Items[i];

                if (tab.IsEnabled)
                {
                    tab.IsSelected = true;
                    break;
                }
            }
        }

        /// <summary>
        ///     Select tab item via header name.  
        /// </summary>
        /// <example>
        ///     Obj.SelectTabByHeaderName("tab item name");
        /// </example>
        /// <param name="tabHeaderName"></param>
        /// Tested: 2019-07-21
        public void SelectTabByHeaderName(string tabHeaderName)
        {
            this.Items.EachRR<TabItem>(p =>
            {
                if (p.Header.ToString().ToUpper() == tabHeaderName.ToUpper())
                    p.IsSelected = true;
            });
        }



        /// <summary>
        /// Returns TabItem object.  
        /// </summary>
        /// <example>
        /// var t = TabControlObject.GetTabByHeaderName("Setting");
        /// CMsgBox.Show(t.Header); //Show tab header text.  
        /// </example>
        /// <param name="tabHeaderName"></param>
        /// <returns></returns>
        /// Tested: 2019-09-06
        public TabItem GetTabByHeaderName(string tabHeaderName)
        {
            TabItem r = null;

            this.Items.EachRR<TabItem>(p =>
            {
                if (p.Header.ToString().ToUpper() == tabHeaderName.ToUpper())
                    r = p;
            });

            return r;
        }





        /// <summary>
        /// Gets tab content (which is typically a custom usercontrol) related to the individual tab.  
        /// </summary>
        /// <example>
        /// var tab = MyTabControlObj.GetTabContent_ByHeaderName<My_Usercontrol_Class>("TabName");
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="tabHeaderName"></param>
        /// <returns></returns>
        /// <remarks>
        /// From: 
        ///     https://stackoverflow.com/q/10667064/722945   
        ///     https://stackoverflow.com/a/20140193/722945
        /// </remarks>
        /// Tested: 2019-09-06
        public T GetTabContent_ByHeaderName<T>(string tabHeaderName)
        {
            T r;
            TabItem t = GetTabByHeaderName(tabHeaderName);
            
            r = (T)t.Content;

            return r;
        }







    }
}
