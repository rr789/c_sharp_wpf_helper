﻿using class_helper_rr;
using rr_general.wpf_custom_control.microsoft_source_code_override;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Interaction logic for DataGridComboBoxColumnRR.xaml
    /// </summary>

    public partial class DataGridComboBoxColumnRR : DataGridComboBoxColumn, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Fired by DataGridRR when datagrid is loaded (and hence the columns are loaded also).
        /// Relying on parent DataGridRR is the only way.  Columns have no loaded event.  Also, calling GetDataGridParent() in constructor returns null (since grid is not yet part of visual tree).  https://stackoverflow.com/questions/4518222/find-root-element-of-datagridcolumn
        /// </summary>
        public EventHandler loadedRR { get; set; }

        /// <summary>
        /// Fired by DataGridRR.  
        /// </summary>
        /// Tested: 2017-09-03.
        public void loadedRR_FireEvent(object sender, EventArgs e)
        {
            if (loadedRR != null)
                loadedRR(sender, e);
        }

        /// <summary>
        /// Sets width, minimum height and maximum height.  
        /// Applied once when control loads.  
        /// </summary>
        public double minMaxWidth { get; set; }

        //Constructor.
        public DataGridComboBoxColumnRR()
        {
            InitializeComponent();
            
            //DataGrid fires this using CLoadedHelper.  So guaranteed to fire only once.  
            loadedRR += onLoad;
        }
        
        public void onLoad(object sender, EventArgs e)
        {
            if (minMaxWidth != 0)
            {
                Width = minMaxWidth;
                MinWidth = minMaxWidth;
                MaxWidth = minMaxWidth;
            }

        }

        //------------------------------------------
        //MICROSOFT SOURCE CODE OVERRIDES: 
        //Used to swap the native ComboBox with my ComboBoxRR.  
        //The native ComboBox does not virtualize and chokes on large datasets.  My ComboBoxRR automatically virtualizes all datasets.  
        //Copied methods from Microsoft .Net 4.8 source code here: https://referencesource.microsoft.com/#PresentationFramework/src/Framework/System/Windows/Controls/DataGridComboBoxColumn.cs
        //Custom overrides are noted with ★★★.  
        //------------------------------------------


        /// <summary>
        ///     Called when a cell has just switched to edit mode.
        /// </summary>
        /// <param name="editingElement">A reference to element returned by GenerateEditingElement.</param>
        /// <param name="editingEventArgs">The event args of the input event that caused the cell to go into edit mode. May be null.</param>
        /// <returns>The unedited value of the cell.</returns>
        protected override object PrepareCellForEdit(FrameworkElement editingElement, RoutedEventArgs editingEventArgs)
        {
            //★★★ Changed from ComboBox to ComboBoxRR.  
            ComboBox comboBox = editingElement as ComboBoxRR;
            //★★★ Added this line.  
            comboBox.IsTextSearchEnabled = true;

            if (comboBox != null)
            {
                comboBox.Focus();
                object originalValue = GetComboBoxSelectionValue(comboBox);
 
                if (IsComboBoxOpeningInputEvent(editingEventArgs))
                {
                    comboBox.IsDropDownOpen = true;
                }
 
                return originalValue;
            }
 
            return null;
        }


        /// <summary>
        ///     Creates the visual tree for text based cells.
        /// </summary>
        protected override FrameworkElement GenerateEditingElement(DataGridCell cell, object dataItem)
        {
            //★★★ Changed from ComboBox to ComboBoxRR.
            var comboBox = new ComboBoxRR();
            //★★★ Added this line.  
            comboBox.IsSearchableRR = true;

            //★★★ Clears default width.  My ComboBoxRR has a default width.  
            //With default width reset, ComboBoxRR will stretch to fill the datagrid column width.  
            comboBox.ClearValue(Control.WidthProperty);

            //★★★ Don't use this method.  It works when clicking into the cell.  However, clicking in the cell, then resizing the column causes cbox to keep same width until clicking out/back into the cell.  
            //comboBox.Width = ActualWidth - 1;  

            //★★★No need to clear default height.  I tested, has no effect.  Also, native cbox does not auto-expand to match row height.  

            ApplyStyle(/* isEditing = */ true, /* defaultToElementStyle = */ false, comboBox);
            ApplyColumnProperties(comboBox);
 
            //★★★ Commented out.  I tested, removing RestoreFlowDirection() has no effect on sorting or setting value.  
            //Activating this method requires also adding a DependencyProperty (called FlowDirectionCacheProperty) in DataGridHelper class.  Not worth the trouble...
            //DataGridHelperRR.RestoreFlowDirection(comboBox, cell);
 
            return comboBox;
        }

        private void ApplyColumnProperties(ComboBox comboBox)
        {
            ApplyBinding(SelectedItemBinding, comboBox, ComboBox.SelectedItemProperty);
            ApplyBinding(SelectedValueBinding, comboBox, ComboBox.SelectedValueProperty);
            ApplyBinding(TextBinding, comboBox, ComboBox.TextProperty);
 
            //★★★ Changed reference from DataGridHelper to DataGridHelperRR.  
            //Native DataGridHelper is a protected class.  So needed to make my own.    
            DataGridHelperRR.SyncColumnProperty(this, comboBox, ComboBox.SelectedValuePathProperty, SelectedValuePathProperty);
            DataGridHelperRR.SyncColumnProperty(this, comboBox, ComboBox.DisplayMemberPathProperty, DisplayMemberPathProperty);
            DataGridHelperRR.SyncColumnProperty(this, comboBox, ComboBox.ItemsSourceProperty, ItemsSourceProperty);
        }

        /// <summary>
        ///     Assigns the Binding to the desired property on the target object.
        /// </summary>
        private static void ApplyBinding(BindingBase binding, DependencyObject target, DependencyProperty property)
        {
            if (binding != null)
            {
                BindingOperations.SetBinding(target, property, binding);
            }
            else
            {
                BindingOperations.ClearBinding(target, property);
            }
        }
 
        /// <summary>
        ///     Assigns the ElementStyle to the desired property on the given element.
        /// </summary>
        internal void ApplyStyle(bool isEditing, bool defaultToElementStyle, ComboBoxRR element)
        {
            Style style = PickStyle(isEditing, defaultToElementStyle);
            if (style != null)
            {
                element.Style = style;
            }
        }

        private Style PickStyle(bool isEditing, bool defaultToElementStyle)
        {
            Style style = isEditing ? EditingElementStyle : ElementStyle;
            if (isEditing && defaultToElementStyle && (style == null))
            {
                style = ElementStyle;
            }
 
            return style;
        }

        /// <summary>
        /// Helper method which returns selection value from
        /// combobox based on which Binding's were set.
        /// </summary>
        /// <param name="comboBox"></param>
        /// <returns></returns>
        private object GetComboBoxSelectionValue(ComboBox comboBox)
        {
            if (SelectedItemBinding != null)
            {
                return comboBox.SelectedItem;
            }
            else if (SelectedValueBinding != null)
            {
                return comboBox.SelectedValue;
            }
            else
            {
                return comboBox.Text;
            }
        }
 
        private static bool IsComboBoxOpeningInputEvent(RoutedEventArgs e)
        {
            KeyEventArgs keyArgs = e as KeyEventArgs;
            if ((keyArgs != null) && keyArgs.RoutedEvent == Keyboard.KeyDownEvent && ((keyArgs.KeyStates & KeyStates.Down) == KeyStates.Down))
            {
                bool isAltDown = (keyArgs.KeyboardDevice.Modifiers & ModifierKeys.Alt) == ModifierKeys.Alt;
 
                // We want to handle the ALT key. Get the real key if it is Key.System.
                Key key = keyArgs.Key;
                if (key == Key.System)
                {
                    key = keyArgs.SystemKey;
                }
 
                // F4 alone or ALT+Up or ALT+Down will open the drop-down
                return ((key == Key.F4) && !isAltDown) ||
                       (((key == Key.Up) || (key == Key.Down)) && isAltDown);
            }
 
            return false;
        }








    }
}
