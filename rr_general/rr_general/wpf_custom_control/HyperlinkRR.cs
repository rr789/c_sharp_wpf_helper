﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Navigation;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Easy way to add a hyperlink in XAML without defining an event handler in C#.  
    /// Opens <see cref="Hyperlink.NavigateUri"/> in a default system browser
    /// </summary>
    /// 
    /// <example>
    /// <wpf_custom_control:HyperlinkRR NavigateUri="http://google.com">
    ///     Link display text
    /// </wpf_custom_control:HyperlinkRR>
    /// </example>
    /// 
    /// <remarks>
    /// From https://stackoverflow.com/a/27609749/722945
    /// </remarks>
    /// Tested: 2018-10-10
    public class HyperlinkRR : Hyperlink
    {
        public HyperlinkRR()
        {
            RequestNavigate += OnRequestNavigate;
        }

        private void OnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
