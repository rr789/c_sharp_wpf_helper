﻿using class_helper_rr;
using rr_general.contract;
using rr_general.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Core;
using ToastNotifications.Lifetime;
using ToastNotifications.Lifetime.Clear;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    ///     Show a pop-up message in upper right corner.  
    /// </summary>
    /// <example>
    /// //1. Add NuGet package "ToastNotifications.Messages".
    /// 
    /// 
    /// //2. Add this to App.xaml file.
    /// <Application.Resources>
    ///     <ResourceDictionary>
    ///         <ResourceDictionary.MergedDictionaries>
    ///             <ResourceDictionary Source="pack://application:,,,/ToastNotifications.Messages;component/Themes/Default.xaml" />
    ///         </ResourceDictionary.MergedDictionaries>
    ///     </ResourceDictionary>
    /// </Application.Resources>
    /// 
    /// //3. Call via examples below.  
    /// </example>
    /// <remarks>
    ///     From: https://github.com/rafallopatka/ToastNotifications
    /// </remarks>
    public class ToastNotificationRR
    {
        public ToastNotifications.Notifier notifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.TopRight,
                offsetX: 1,
                offsetY: 1
                );

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                //999 for unlimited.  Instead, I have my own dispose method.  The "configuration" property (where this lifetime duration is available) is protected.  
                 notificationLifetime: TimeSpan.FromSeconds(999)
                , maximumNotificationCount: MaximumNotificationCount.FromCount(10)
                );

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        //Constructor
        public ToastNotificationRR()
        {}


        //---------------------------------
        //HELPERS:
        //---------------------------------
        /// <summary>
        /// Clears messages from screen after XXX seconds.  
        /// </summary>
        /// <param name="durationSeconds"></param>
        /// Tested: 2019-08-22
        public void ClearMessagesAfterTimeout(int durationSeconds)
        {
            CTimerForms t = new CTimerForms(durationSeconds * 1000, 1);
            t.callback = delegate 
            {
                notifier.ClearMessages(new ClearAll());
            };
            t.startTimer();
        }


        //---------------------------------
        //SHOW MESSAGES:
        //---------------------------------
        /// <summary>
        ///     Shows notification box.  
        /// </summary>
        /// <example>
        ///     var toast = new ToastNotificationRR();
        ///     toast.ShowMsg_InfoBlue("hi", 5);
        /// </example>
        /// <param name="text"></param>
        /// <param name="durationSeconds"></param>
        /// <param name="clearOtherMessages">True: Clears any existing messages on screen.</param>
        /// Tested: 2019-08-22
        public void ShowMsg_InfoBlue(string text, int durationSeconds = 3, bool clearOtherMessages = true)
        {
            if (clearOtherMessages)
                notifier.ClearMessages(new ClearAll());

            notifier.ShowInformation(text);

            ClearMessagesAfterTimeout(durationSeconds);
        }


        /// <summary>
        ///     Shows notification box.  
        /// </summary>
        /// <example>
        ///     var toast = new ToastNotificationRR();
        ///     toast.ShowMsg_SuccessGreen("hi", 5);
        /// </example>
        /// <param name="text"></param>
        /// <param name="durationSeconds"></param>
        /// <param name="clearOtherMessages">True: Clears any existing messages on screen.</param>
        /// Tested: 2019-08-22
        public void ShowMsg_SuccessGreen(string text, int durationSeconds = 3, bool clearOtherMessages = true)
        {
            if (clearOtherMessages)
                notifier.ClearMessages(new ClearAll());

            notifier.ShowSuccess(text);

            ClearMessagesAfterTimeout(durationSeconds);
        }


        /// <summary>
        ///     Shows notification box.  
        /// </summary>
        /// <example>
        ///     var toast = new ToastNotificationRR();
        ///     toast.ShowMsg_WarningOrange("hi", 5);
        /// </example>
        /// <param name="text"></param>
        /// <param name="durationSeconds"></param>
        /// <param name="clearOtherMessages">True: Clears any existing messages on screen.</param>
        /// Tested: 2019-08-22
        public void ShowMsg_WarningOrange(string text, int durationSeconds = 3, bool clearOtherMessages = true)
        {
            if (clearOtherMessages)
                notifier.ClearMessages(new ClearAll());

            notifier.ShowWarning(text);

            ClearMessagesAfterTimeout(durationSeconds);
        }


        /// <summary>
        ///     Shows notification box.  
        /// </summary>
        /// <example>
        ///     var toast = new ToastNotificationRR();
        ///     toast.ShowMsg_ErrorRed("hi", 5);
        /// </example>
        /// <param name="text"></param>
        /// <param name="durationSeconds"></param>
        /// <param name="clearOtherMessages">True: Clears any existing messages on screen.</param>
        /// Tested: 2019-10-02
        public void ShowMsg_ErrorRed(string text, int durationSeconds = 3, bool clearOtherMessages = true, bool logMessage = true)
        {
            if (clearOtherMessages)
                notifier.ClearMessages(new ClearAll());

            notifier.ShowError(text);

            if (true)
                CLogger.CreateLogEntry("Red toast notification: " + text, false, false); 

            ClearMessagesAfterTimeout(durationSeconds);
        }


        //---------------------------------
        //CBOOLEAN MESSAGE HELPER:
        //This is a special object containing: 
        //  1. Boolean value (of course).
        //  2. Optional description. 
        //  3. Optional severity color.  
        //Helpful for multi-threading.  To transmit boolean value (and) a message to display after the thread completes.  
        //---------------------------------
        
            
            
        public void ShowToastForCBoolean(CBoolMessage obj)
        {
            switch (obj.SeverityColorRR)
            {
                case CBoolMessage.SEVERITY_COLOR_GREEN:
                {
                    ShowMsg_SuccessGreen(obj.MessageRR, 5, obj.OverwriteExistingMessages);
                    return;
                }

                case CBoolMessage.SEVERITY_COLOR_BLUE:
                {
                    ShowMsg_InfoBlue(obj.MessageRR, 5, obj.OverwriteExistingMessages);
                    return;
                }

                case CBoolMessage.SEVERITY_COLOR_ORANGE:
                {
                    ShowMsg_WarningOrange(obj.MessageRR, 7, obj.OverwriteExistingMessages); 
                    return;
                }

                case CBoolMessage.SEVERITY_COLOR_RED:
                {
                    ShowMsg_ErrorRed(obj.MessageRR, 10, obj.OverwriteExistingMessages);
                    return;
                }


            };




        }


















    }
}