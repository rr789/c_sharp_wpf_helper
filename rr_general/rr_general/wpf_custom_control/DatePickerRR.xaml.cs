﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using rr_general.config;
using class_helper_rr;
using System.Windows.Controls.Primitives;
using System.ComponentModel;

namespace rr_general.wpf_custom_control
{
    /// <summary>
    /// Interaction logic for DatePickerRR.xaml
    /// TODO: 2019-09-07 Have popup calender default to today's date when opened...especially if "SelectedDate" was previously set to say 1900-01-01.  Otherwise all the code below is clean and tested.
    /// </summary>
    public partial class DatePickerRR : DatePicker, INotifyPropertyChanged
    {
        //Boilerplate for INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// True: Enable past dates.   
        /// False: Disable past dates.  Past dates will not show on the pop-up calendar.  
        /// </summary>
        public bool EnablePastDate { get; set; }

        /// <summary>
        /// Textbox inside the DatePicker.  If ever need this.  
        /// </summary>
        public DatePickerTextBox TextBoxObjRR { get; set; }

        /// <summary>
        /// True: Automatically sets date to today.  
        /// </summary>
        public bool DefaultTodayDate { get; set; }


        //Constructor.  
        public DatePickerRR()
        {
            InitializeComponent();

            //Don't use OnLoad for DatePicker.  OnLoad gets triggered before children are loaded.   
            //We override OnApplyTemplate() instead.  
        }


        //Superior to Onload event.  This is called after the children are loaded.  
        //https://stackoverflow.com/a/4751330/722945
        //Tested: 2019-09-07
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //Find child textbox.  
            //From: https://social.msdn.microsoft.com/Forums/sqlserver/en-US/3b8da995-889c-43b6-adb9-8e0ea3506f24/datepickertext-hint-text?forum=wpf
            TextBoxObjRR = this.FindVisualChildren<DatePickerTextBox>()[0];

            if (!EnablePastDate)
                this.DisplayDateStart = DateTime.Today;

            UpdateDatePickerValueRR();

            if (DefaultTodayDate)
                this.SelectedDate = DateTime.Today;


            //-------------------------
            //Below ensures a mouse click will select all text inside the child textbox. 
            //-------------------------
            TextBoxObjRR.PreviewMouseLeftButtonDown += SelectivelyIgnoreMouseButton;

            TextBoxObjRR.GotFocus += (obj, args) =>
            {
                TextBoxObjRR.SelectAll();
            };
            //-------------------------


        }


        /// <summary>
        /// Removes value if prior date entered (AND) EnablePastDate = false.  
        /// </summary>
        /// Tested: 2018-09-14
        private void LostFocusEventHandler(object sender_, RoutedEventArgs e_)
        {
            //Little trick to only run if focus is truly lost.  Ex: Clicking the little date picker trigggers a LostFocus event.  
            if (!this.IsKeyboardFocusWithin)
            {
                if (!EnablePastDate && this.DisplayDate < DateTime.Today)
                    this.SelectedDate = null; 
            }
        }



        /// <summary>
        /// Superior alternative to native SelectedDate.  
        /// Returns a not-null DateTime value.  NULL date is returned as "1900-01-01".     
        /// 
        /// Note: 
        ///     No value returns "1900-01-01 12:00:00 AM".  
        /// </summary>
        /// Tested: 2019-09-07
        public DateTime SelectedDateRR()
        {
            if (this.SelectedDate == null)
                return CGlobal.ErrorDateTime;
            else if (this.SelectedDate <= CGlobal.ErrorDateTime) //Less than to also include "0001-01-01".
                return CGlobal.ErrorDateTime;

            return Convert.ToDateTime(this.SelectedDate);
        }



        /// <summary>
        /// Just a wrapper. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// Tested: 2019-09-07
        public void SelectedDateChanged_Handler(object sender, EventArgs args)
        {
            UpdateDatePickerValueRR();
        }



        /// <summary>
        /// Visual aid.  Sets date value to NULL if date is < "1900-01-01". 
        /// Allows the little watermark "Select a date" to show-up when a 1900-01-01 date assigned for whatever reason (Not-NULL database columns will default to 1900-01-01).  
        /// </summary>
        /// <remarks>
        /// Separate method since this class calls this method in two locations. 
        /// </remarks>
        /// Tested: 2019-09-07
        private void UpdateDatePickerValueRR()
        {
            if (SelectedDateRR() <= CGlobal.ErrorDateTime) //Less than to also include "0001-01-01".  CGlobal.ErrorDateTime has value of "1900-01-01".  
            {
                //TODO ★★★: NEED TO GRAB EVENT "AFTER" SETTING SELECTED DATE.  CURRENTLY THIS IS GRABBING THE "BEFORE SELECTED DATE CHANGED" SO 
                //IT IS NULLING OUT ANY VALUES SET PROGRAMATICALLY.  IT DOESN'T AFFECT VALUES THE USER SETS IN UI.  SO KIND OF DECEIVING. 
                //A BETTER WAY IS TO JUST HIDE THE TEXT (OR TURN FONT WHITE) IF DATE IS 1900-01-01 ETC.
                //this.SelectedDate = null;
            }
        }


        /// <summary>
        /// Fixes a bug where textbox cannot select all text on first mouse click.  
        /// This compliements existing "GotFocus->this.SelectAll()" in bloc above.
        /// </summary>
        /// <remarks>
        /// Modified from: https://stackoverflow.com/a/2553297/722945
        /// </remarks>
        /// Tested: 2019-10-17
        private void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
        {
            //So we only deal with "first" mouse click.  
            if (!this.IsKeyboardFocusWithin)
            {
                //If the text box is not yet focussed, give it the focus and stop further processing of this click event.
                this.Focus();
                e.Handled = true;
            }

            //No need to call "this.SelectAll()".  The existing "GotFocus->this.SelectAll()" in bloc above handles selecting all text OK.  

        }




    }
}
