﻿<DataGrid   x:Class="rr_general.wpf_custom_control.DataGridRR"
            x:Name="root"
            xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
            xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
            xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
            xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
            xmlns:local="clr-namespace:rr_general.wpf_custom_control"
            xmlns:converter="clr-namespace:rr_general.wpf_converter"
            mc:Ignorable="d" 
            d:DesignHeight="300" d:DesignWidth="300" 
            CanUserAddRows="False"      
            AutoGenerateColumns="False"
            RowHeight="25"
            >
    <!--
        ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
        DATAGRID GENERAL PROPERTIES: 
        ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
        RowHeight:
            - Setting to any value (even a value smaller than the ComboBox inside) will prevent the row from enlarging when clicking into a ComboBox column.    
            - ComboBoxRR default height is 24, so setting DataGrid row height to 25 is perfect.  I tested many times.  Setting to 24 cuts off the bottom ComboBox border (although 24 still looks OK).  
    
        CanUserAddRows: 
            - False (default): prevents a blank row from showing at bottom.  
            - User can override this to "True" if needed (rare).  
    
        Virtualization: 
            Properties with virtualization enabled by default:      
                - EnableRowVirtualization:
                    - Default is true.  See: https://docs.microsoft.com/en-us/dotnet/api/system.windows.controls.datagrid.enablerowvirtualization?view=netcore-3.1  
                - ScrollViewer.CanContentScroll="True"
                    - Default is true.  True makes minimum scroll unit = whole row.  False makes minimun scroll unit = pixel.    
                - VirtualizingPanel.VirtualizationMode: 
                    - Default is "recycling".  
                        - See .Net source code here.  It sets VirtualizationMode = "Recycling".  
                        - This post says "recycling" is default.  https://stackoverflow.com/a/60151118/722945 
             
            Properties with virtualization NOT enabled by default:    
                - EnableColumnVirtualization: 
                    - Default is false.  See: https://stackoverflow.com/a/11379820/722945
                    - Setting to true does help with vertical row scolling (I tested).  However, the horizontal side-to-side column scrolling becomes janky.  I generally prefer to leave this as false.  
        
        ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
        DATA RECORD REQUIREMENTS.
        ★★★The record's data contract must contain the field(s) below.   
        ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
        IS_DATAGRID_ROW_EDITABLE_RR: 
            - True (default): Allows user to edit row.  
            - False: Makes row read-only.  
        -Fyi: 
            - If not added, C# will not throw an error.  However, Visual Studio debugger will show a binding error (red icon) when running the app.  
                - No native event exists for "dataset is now bound".  This link has a couple attempts but none are satisfactory: https://stackoverflow.com/questions/3836104  
    
    
        ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
        CONTEXT MENU (WITH RIGHT MOUSE CLICK): 
        ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
        Grid "header" (aka column headers): 
            - Adding custom menu items from an external app is not supported currently.  
    
        Grid "body": 
            - Below shows how to add custom menu items from an external app.  It is quite easy.  Just define a ContextMenu inside
              the DataGrid like normal.  
    
                <rr:DataGridRR>
                    <rr:DataGridRR.ContextMenu>
                        <rr:ContextMenuRR>
                            <MenuItem    Header="Custom menu 01" Click="ClickHandlerMethod01"/>
                            <MenuItem    Header="Custom menu 02" Click="ClickHandlerMethod02"/>
                        </rr:ContextMenuRR>
                    </rr:DataGridRR.ContextMenu>
    
                    //And so on...
                </rr:DataGridRR>
    --> 

  
    <DataGrid.Resources>
        <converter:HasValueConverter x:Key="hasValueConverter"/>
    
        <!-- 
        ================================
        CONTEXT MENU->GRID HEADER->DEFINE STYLE.
        This style is applied explicitly to each default menu item.   
        The default visibility setting is defined in the C# backend file.  
        ================================
        -->
        <Style x:Key="Menu_ColumnHeader_DefaultConfig_Style" TargetType="MenuItem" BasedOn="{StaticResource {x:Type MenuItem}}" >
            <Setter Property="Visibility" Value="{Binding Menu_GridHeader_DefaultOption_Visibility, Source={x:Reference Name=root}}" />
        </Style>


        <!-- 
        ================================
        CONTEXT MENU->GRID HEADER->DEFINE DEFAULT MENU ITEMS. 
        ================================
        -->
        <!--
            Setup the default menu options.  
            2024-08-15 Note: 
                - Grid "header" (aka column headers) do NOT currently support custom menu items defined in an external app.  I may add this ability in the future
                  but it would require setting-up a new DependencyProperty to store custom menu items from an external app.  Then, the default options below would get merged with the custom menu items from external app into one combined "final" menu.    
        
                - Grid "body" however, does support custom menu items.  It has code to merge default options with custom menu items from an external app.  
        -->
        <local:ContextMenuRR x:Key="Menu_GridHeader_DefaultOption"  >
            <MenuItem    Header="Configure columns" Click="menuClick_ShowPopup" Style="{StaticResource Menu_ColumnHeader_DefaultConfig_Style}" />
            <MenuItem    Header="Hide column" Click="menuClick_HideColumn" Style="{StaticResource Menu_ColumnHeader_DefaultConfig_Style}" />
            <MenuItem    Header="Save grid preferences (column order and column width)" Click="menuClick_SavePreference" Style="{StaticResource Menu_ColumnHeader_DefaultConfig_Style}" />
            <local:SeparatorRR   />
        </local:ContextMenuRR>


        <!-- 
        ================================
        CONTEXT MENU->GRID HEADER
        1. ★★★Bind menu to final combined collection. 
        2. Word wrap column header text.  
        3. Center header text.
        ================================
        -->
        <Style TargetType="DataGridColumnHeader" BasedOn="{StaticResource {x:Type DataGridColumnHeader}}" >
            <!--
                Bind to menu.  2024-08-15 Again, grid "header" does not (currently) support custom menu items from an external app. 
                We only show the "default" menu items (like "save grid preferences" etc).  
            -->
            <Setter Property="ContextMenu" Value="{StaticResource Menu_GridHeader_DefaultOption}" />

            <!-- Center header text.  https://stackoverflow.com/a/19131288/722945 -->
            <Setter Property="HorizontalContentAlignment" Value="Center" />
            
            <!-- Word wrap column header text.  https://stackoverflow.com/a/7194054/722945-->
            <Setter Property="ContentTemplate">
                <Setter.Value>
                    <DataTemplate>
                        <TextBlock TextWrapping="Wrap" Text="{Binding}" ></TextBlock>
                    </DataTemplate>
                </Setter.Value>
            </Setter>
        </Style>





        <!-- 
        ================================
        CONTEXT MENU->GRID BODY->DEFINE STYLE.
        This style is applied explicitly to each default menu item. 
        The default visibility setting is defined in the C# backend file.  
        ================================
        -->
        <Style x:Key="Menu_GridBody_DefaultConfig_Style" TargetType="MenuItem" BasedOn="{StaticResource {x:Type MenuItem}}" >
            <Setter Property="Visibility" Value="{Binding Menu_GridBody_DefaultOption_Visibility, Source={x:Reference Name=root}}" />
        </Style>


        <!-- 
        ================================
        CONTEXT MENU->GRID BODY->DEFINE DEFAULT MENU ITEMS. 
        ================================
        -->
        <!--
            Setup the default menu options.  
            2024-08-15 Note: 
                - Grid "body" does support custom menu items defined in an external app.  The C# side will merge the default 
                  menu items below together with custom menu items defined in an external app.  
        -->
        <local:ContextMenuRR x:Key="Menu_GridBody_DefaultOption">
            <MenuItem    Header="Export to Excel (entire table)" Click="menuClick_ExportToExcel" Style="{StaticResource Menu_GridBody_DefaultConfig_Style}"  />
        </local:ContextMenuRR>


        <!-- 
        ================================
        CONTEXT MENU->GRID BODY
        1. ★★★Bind menu to final combined collection. 
        ================================
        -->
        <!--The binding is actually done in the C# side.   See method Setup_MenuGridBody().-->


        <!-- ================================ -->
        <Style TargetType="DataGridRow">
            <!-- 
            ================================
            MAKE ROW READ-ONLY.  
            From: https://stackoverflow.com/a/17216605/722945
            Tested: 2023-11-03
            ================================
            -->
            <Style.Triggers>
                <DataTrigger Binding="{Binding IS_DATAGRID_ROW_EDITABLE_RR}" Value="False" >
                    <Setter Property="IsEnabled" Value="False" />
                    <!-- From Stackoverflow author...You can also set "IsHitTestVisble" = False but please note that this won't prevent the user from changing the values using the keyboard arrows -->
                </DataTrigger>

            </Style.Triggers>

        </Style>



        <!-- 
        ================================
        STYLE->DATA GRID CELL: 
        ALL CELLS: 
            - REMOVE DARK BLACK BORDER FROM RIGHT SIDE & BOTTOM.  REPLACE WITH LIGHT GRAY BORDER.  
            - VERTICALLY CENTER CONTENT
            - MAINTAINS LIGHT BLUE COLOR ON SELECTED ROWS EVEN WHEN GRID LOSES FOCUS.   
        BEHAVIOR: 
            - SINGLE CLICK EDIT OF COMBOBOX & CHECKBOX COLUMNS.  
        ================================
        -->
        <!--Remove the black "datagrid table" border completely.  We replace with gray "cell" borders on left/bottom further below.  -->
        <Style TargetType="local:DataGridRR">
            <Setter Property="GridLinesVisibility" Value="None" />
        </Style>

        <!-- 
            Vertical center from: https://stackoverflow.com/a/3995839/722945 
            Border from: https://stackoverflow.com/a/38792590/722945
            The ControlTemplate overrides all styles.  So add future style properties inside the ControlTemplate.  
        -->
        <Style TargetType="DataGridCell">
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="DataGridCell">
                        <!--TODO: 2020-07-08 After this update, the far right column's right side border is blank.  I can update the BorderThickness to "0,0,1,1" (to put border on right side) however it slightly mis-aligns with the column header borders. -->
                        <Border BorderThickness="1,0,0,1">
                            <Border.BorderBrush >
                                <SolidColorBrush Color="LightGray" />
                            </Border.BorderBrush>

                            <Grid Background="{TemplateBinding Background}">
                                <ContentPresenter VerticalAlignment="Center" />
                            </Grid>
                        </Border>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>

            <!--
            Allow single click combobox or checkbox updates to datagrid cell.   
            Note: Keep this EventSetter outside of Style->DataTrigger.  Prevents throwing an error.  Style->DataTrigger can only handle "Routed Events" but creating all that boilerplate is a pain.  See: https://social.msdn.microsoft.com/Forums/vstudio/en-US/e376cd80-c675-48ce-b01f-f94a82cb51f4/eventsetter-bug?forum=wpf  
            C# inside code only activates this if ComboBox or CheckBox column.  
            
            2024-08-16 Keep this above the "Style.Trigger" node.  Otherwise will get weird error "Property elements cannot be in the middle of an element's content. They must be before or after the content."
            -->
            <EventSetter Event="PreviewMouseLeftButtonDown" Handler="DataGridCell_PreviewMouseLeftButtonDown" />

            <Style.Triggers>
                <!--
                    Keep light blue background on selected rows even when datagrid loses focus.  
                    Modified from: https://stackoverflow.com/a/26043015/722945  
                    Below is simpler than the Stackoverflow answer.  No need to use "multi datatrigger" if we 
                    want same color for selected rows whether grid has focus or not.  
                -->
                <DataTrigger Binding="{Binding RelativeSource={RelativeSource Self}, Path=IsSelected}" Value="True">
                    <Setter Property="Background" Value="#D8DFFF" />
                    <Setter Property="Foreground" Value="Black" />
                    <Setter Property="BorderBrush" Value="LightGray" />
                </DataTrigger>
            </Style.Triggers>

        </Style>




    </DataGrid.Resources>




</DataGrid>
