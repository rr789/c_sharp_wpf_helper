﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace rr_general.config
{
    public class ConfigUserProfile
    {
        /// <summary>
        /// Returns path to current active profile. 
        /// Ex: "C:\Users\randy\AppData\Roaming\my_app_name\profiles\profile_id".
        /// Used with CDataStore.  
        /// </summary>
        /// Tested: 2017-06-20.
        public static string getUserProfileDirectory()
        {
            return CDirectory.GetDirRoam_StartupProject() + @"\profile\" + Environment.UserName;
        }

        /// <summary>
        /// Location where DataGrid settings are saved. 
        /// Ex: "C:\Users\randy\AppData\Roaming\my_app_name\profiles\profile_id\datagrid".
        /// </summary>
        /// Tested: 2017-06-20.
        public static string getUserProfileDataGridDirectory()
        {
            return getUserProfileDirectory() + @"\datagrid";
        }




    }
}
