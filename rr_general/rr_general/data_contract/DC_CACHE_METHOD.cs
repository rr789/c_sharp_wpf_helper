﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rr_general.data_contract
{
    
    /// <summary>
    /// Data contract used in class CCacheMethod.  
    /// Firebird db table MAIN_T 
    /// </summary>
    public class DC_CACHE_METHOD
    {
        //-------------------------------------
        //DATA CONTRACT.  
        //-------------------------------------
        public long     REC_ID                              { get; set; }
        /// <summary>
        /// Required.  Primary lookup key.  Passed into every CallMethodRR().   
        /// </summary>
        public string   CACHE_KEY_01                        { get; set; }


        /// <summary>
        /// Optional:  Provides a secondary lookup key.  Set at class level.  
        /// CallMethodRR() does not have a param for this.  However, it does reference this property.  
        /// </summary>
        /// <example>
        /// Set to dev/prod server address.  This will keep cached records for dev/prod separate.  
        /// </example>
        public string   CACHE_KEY_02                        { get; set; }
        public string   JSON_DATA_FILE_PATH                 { get; set; } //Path to the file containing serialized json.    
        public DateTime DATE_TIME_CREATED                   { get; set; }


        //-------------------------------------
        //COLUMN NAME CONSTANT "C_".  FOR MISC USE.  
        //-------------------------------------
        public const string C_REC_ID                = "REC_ID";   
        public const string C_CACHE_KEY_01          = "CACHE_KEY_01"; 
        public const string C_CACHE_KEY_02          = "CACHE_KEY_02";
        public const string C_JSON_DATA_FILE_PATH   = "JSON_DATA_FILE_PATH";     
        public const string C_DATE_TIME_CREATED     = "DATE_TIME_CREATED";   
    }
}
