﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using class_helper_rr;

namespace System //String uses system namespace.  
{
    public static class Ext_String
    {
        /// <summary>
        /// Removes spaces, line feeds from start/end of string.  
        /// </summary>
        /// <param name="str"></param>
        /// Tested: 2018-09-11
        public static string TrimRR(this string str)
        {
            return str.Trim(new char[] { ' ', '\n','\r' });
        }


        /// <summary>
        /// Converts string to Int64.  
        /// </summary>
        /// <example>
        /// " 45678 ".ToInt64(); //Returns Int64 value. Removes empty spaces before converting OK.
        /// "45678A".ToInt64(0); //Returns zero value.  Could not convert due to "A" character in the string.   
        /// </example>
        /// <param name="str">String to convert.</param>
        /// <param name="errorNumber">Optional, can override the default CGlobal->error number -9999999</param>
        /// Tested: 2018-09-16
        public static Int64 ToInt64(this string str, Int64 errorNumber = CGlobal.ErrorNumber_Int)
        {
            Int64 r = errorNumber;

            if (str != null && str != "")
            {
                string str_ = str.TrimRR();

                if (Int64.TryParse(str_, out var ignoreme))
                    r = Int64.Parse(str_);
            }

            return r;
        }


        /// <summary>
        /// Converts string to Int32.  
        /// </summary>
        /// <example>
        /// " 45678 ".ToInt32(); //Returns Int32 value. Removes empty spaces before converting OK.
        /// "45678A".ToInt32(0); //Returns zero value.  Could not convert due to "A" character in the string.   
        /// </example>
        /// <param name="str">String to convert.</param>
        /// <param name="errorNumber">Optional, can override the default CGlobal->error number -9999999</param>
        /// Tested: 2018-09-16
        public static Int32 ToInt32(this string str, Int32 errorNumber = CGlobal.ErrorNumber_Int)
        {
            Int32 r = errorNumber;

            if (str != null && str != "")
            {
                string str_ = str.TrimRR();

                if (Int32.TryParse(str_, out var ignoreme))
                    r = Int32.Parse(str_);
            }

            return r;
        }


        /// <summary>
        /// Converts string to double number.  
        /// </summary>
        /// <example>
        /// "1,234.56".ToDouble(); //Returns double number 1234.56. 
        /// </example>
        /// <param name="str"></param>
        /// <param name="errorNumber"></param>
        /// <param name="convertNullToZero">
        /// True: Null gets converted to zero.
        /// False: Null returns global error value (ex: -9,999,999);
        /// </param>
        /// <returns></returns>
        /// Tested: 2019-12-29
        public static double ToDouble(this string str, double errorNumber = CGlobal.ErrorNumber_Int, bool convertNullToZero = true)
        {
            if (!str.HasValueRR() && convertNullToZero )
                return 0;

            if (str.HasValueRR())
            {
                return CStringNumber.stringToDouble(str);
            }

            return errorNumber;
        }




        /// <summary>
        /// Converts string to decimal number.  
        /// </summary>
        /// <example>
        /// "1,234.56".ToDecimal(); //Returns decimal number 1234.56. 
        /// </example>
        /// <param name="str"></param>
        /// <param name="errorNumber"></param>
        /// <param name="convertNullToZero">
        /// True: Null gets converted to zero.
        /// False: Null returns global error value (ex: -9,999,999);
        /// </param>
        /// <returns></returns>
        /// Tested: 2021-09-22
        public static decimal ToDecimal(this string str, decimal errorNumber = CGlobal.ErrorNumber_Int, bool convertNullToZero = true)
        {
            if (!str.HasValueRR() && convertNullToZero)
                return 0;

            if (str.HasValueRR())
                return CStringNumber.stringToDecimal(str);

            return errorNumber;
        }


        /// <summary>
        /// Does string contain sub-string?   
        /// Improvement over native stringObject.contains() which does not support CompareOptions for strings with > 1 character. 
        /// Very fast performance, can call thousands of times.  
        /// </summary>
        /// <example>
        /// CMsgBox.Show("hi there".ContainsRR("THE")); //Returns true.  Default is case insensitive.  
        /// CMsgBox.Show("hi there".ContainsRR("THE", CompareOptions.None)); //Returns false.  "None" makes it case sensitive.  
        /// CMsgBox.Show("hi there".ContainsRR("the", CompareOptions.None)); //Returns true.  "None" makes it case sensitive. 
        /// </example>
        /// <param name="str">Current string object.</param>
        /// <param name="needle">String to search for.</param>
        /// <param name="options">Default ignores case. </param>
        /// Tested: 2018-12-16.
        public static bool ContainsRR(this string str, string needle, CompareOptions options = CompareOptions.IgnoreCase)
        {
            bool r = false;
            
            //Safety.
            if (!str.HasValueRR() || !needle.HasValueRR())
                return false;

            //Returns a cached culture object, so no overhead.  https://stackoverflow.com/a/18904227/722945
            //Can handle any language.  
            var cult = CultureInfo.GetCultureInfo(CultureInfo.CurrentCulture.Name);

            //Most efficient method.  Does not create a new string in-memory when ignoring case. https://stackoverflow.com/a/15464440/722945
            if (cult.CompareInfo.IndexOf(str, needle, options) >= 0) //Returns -1 if not found.
                r = true;

            return r;

        }



        /// <summary>
        /// Does string start with any in list of needles.  
        /// </summary>
        /// <example>
        /// "test01 test02".StartsWithRR(new List<string>() { "test01", "test02"});     //Returns true.
        /// "test01 test02".StartsWithRR(new List<string>() { "test02", "test01"});     //Returns true.
        /// 
        /// "test01 test02".StartsWithRR(new List<string>() { "TEST01"});               //Returns true.  Defaults to ignore case.  
        /// "test01 test02".StartsWithRR(new List<string>() { "TEST01"}, true);         //Returns false.  Overrode the ignore case.
        /// 
        /// "test01 test02".StartsWithRR(new List<string>() { "test01A"});              //Returns false.
        /// </example>
        /// <param name="str"></param>
        /// <param name="needleList"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        /// Tested: 2019-10-22
        public static bool StartsWithRR(this string str, List<string> needleList, bool caseSensitive = false)
        {
            //Safety.
            if (!str.HasValueRR() || needleList.CountRR() == 0)
                return false;

            for (var i=0;i<needleList.Count;i++)
            {
                string needle = needleList[i];

                if (CString.StartsWith(str, needle, caseSensitive))
                    return true;

            }

            return false;

        }


        /// <summary>
        /// Does string end with any in list of needles.  
        /// </summary>
        /// <example>
        /// "test01 test02".EndsWithRR(new List<string>() { "test01", "test02"});     //Returns true.
        /// "test01 test02".EndsWithRR(new List<string>() { "test02", "test01"});     //Returns true.
        /// 
        /// "test01 test02".EndsWithRR(new List<string>() { "TEST02"});               //Returns true.  Defaults to ignore case.  
        /// "test01 test02".EndsWithRR(new List<string>() { "TEST02"}, true);         //Returns false.  Overrode the ignore case.
        /// 
        /// "test01 test02".EndsWithRR(new List<string>() { "test02A"});              //Returns false.
        /// </example>
        /// <param name="str"></param>
        /// <param name="needleList"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        /// Tested: 2019-11-18
        public static bool EndsWithRR(this string str, List<string> needleList, bool caseSensitive = false)
        {
            //Safety.
            if (!str.HasValueRR() || needleList.CountRR() == 0)
                return false;

            for (var i=0;i<needleList.Count;i++)
            {
                string needle = needleList[i];

                if (CString.EndsWith(str, needle, caseSensitive))
                    return true;

            }

            return false;

        }













        
        /// <summary>
        ///     Checks if string matches regex pattern one or more times.  
        /// </summary>
        /// <example>
        ///     "123".RegexHasMatchRR(@"[0-9]{3}"); //Returns true.
        ///     "123".RegexHasMatchRR(@"[0-9]{4}"); //Returns false.
        /// </example>
        /// <param name="str"></param>
        /// <param name="regexPattern"></param>
        /// <returns></returns>
        /// Tested: 2019-07-23
        public static bool RegexHasMatchRR(this string str, string regexPattern)
        {
            //Safety.
            if (str == "" || regexPattern == "")
                return false;

            var regex = new Regex(regexPattern);

            if (regex.Matches(str).Count > 0)
                return true;

            return false;
        }



        /// <summary>
        ///     Replaces string characters that match the regex expression.  
        ///     Returns a "copy" of the original string.  Does not modify the original string.  I tested.
        /// </summary>
        /// <example>
        ///     "01hello23".RegexReplaceRR("[0-9]{1,}", ""); //Returns "hello".  All numbers removed.   
        ///     "$-1,234.56".RegexReplaceRR("[^[0-9-.]", ""); //Returns "-1234.56".  Ready to convert into double.  
        /// </example>
        /// <param name="str"></param>
        /// <param name="regexPattern"></param>
        /// <param name="replacementString"></param>
        /// <returns></returns>
        /// Tested: 2019-07-23
        public static string RegexReplaceRR(this string str, string regexPattern, string replacementString)
        {
            //Safety.
            if (str == null || str == "" || regexPattern == "")
                return "";

            var regex = new Regex(regexPattern);
            return regex.Replace(str, replacementString);
        }


        /// <summary>
        /// Extracts substring(s) that match the regex pattern. 
        /// </summary>
        /// <example>
        /// string a = @"ABC 123 DEF 456";
        /// a = a.RegexExtractRR(@"[0-9]{1,}"); //Returns 123456.  
        /// </example>
        /// <param name="str"></param>
        /// <param name="regexPattern"></param>
        /// <returns></returns>
        /// Tested: 2019-10-09
        public static string RegexExtractRR(this string str, string regexPattern)
        {
            //Safety.
            if (str == null || str == "" || regexPattern == "")
                return "";

            string r = "";

            var regex = new Regex(regexPattern);

            //Note the plural "Matches" property.  This enables extracting multiple parts of the string that match.    
            for (var i=0; i<regex.Matches(str).Count; i++)
            {
                r += regex.Matches(str)[i];
            }

            return r; //https://stackoverflow.com/a/5651002/722945
        }



        /// <summary>
        /// Basic check to ensure string is not null (AND) not empty string "".
        /// </summary>
        /// <example>
        /// string a = "";
        /// aa.HasValueRR(); //Returns false.
        /// 
        /// string a = null;
        /// aa.HasValueRR(); //Returns false.
        /// 
        /// string a = "value";
        /// aa.HasValueRR(); //Returns true.
        /// </example>
        /// <param name="str"></param>
        /// <returns>
        /// True: String has value.  
        /// False: String is either null or has empty string.
        /// </returns>
        /// Tested: 2019-08-24
        public static bool HasValueRR(this string str)
        {
            if (str == null || str == "")
                return false;

            return true;
        }


        /// <summary>
        /// Determines if at least one needle in list equals haystack. Case in-sensitive.
        /// </summary>
        /// <example>
        ///     "TEST_A".EqualsAnyRR("Test_A", "Test_B", "Test_C") //Returns true. 
        ///     "TEST_A".EqualsAnyRR("Z", "ZZ", "ZZZ") //Returns false. 
        /// </example>
        /// <param name="haystack">String to match.</param>
        /// <param name="needles">List of candidate strings to try and match.</param>
        /// <returns></returns>
        /// Tested: 2019-09-13
        public static bool EqualsAnyRR(this string str, params string[] needles)
        {
            return CString.EqualsAny(str, needles);
        }


        /// <summary>
        /// Determines if string contains at least one substring param. 
        /// Case in-sensitive.
        /// </summary>
        /// <example>
        /// "test01 test02 test03".ContainsAnyRR("XXX", "test02", "XXX");   //Returns true. 
        /// "test01 test02 test03".ContainsAnyRR("XXX", "TEST02", "XXX");   //Returns true.
        /// "test01 test02 test03".ContainsAnyRR("XXX", "XXX");             //Returns false.
        /// </example>
        /// <param name="str"></param>
        /// <param name="needles"></param>
        /// <returns></returns>
        /// Tested: 2019-10-26
        public static bool ContainsAnyRR(this string str, params string[] needles)
        {
            return CString.ContainsAny(str, needles.ToList<string>(), false);
        }






        /// <summary>
        /// Returns string as UPPERCASE.  
        /// Main purpose of extension: Has safety to return empty string "" if object is null.  
        /// </summary>
        /// <example>
        /// //Test null.
        /// string a = null;
        /// CMsgBox.Show(a.ToUpperRR()); //Shows empty string. 
        /// 
        /// //Test string. 
        /// string a = "hi";
        /// CMsgBox.Show(a.ToUpperRR()); //Shows "HI".  
        /// </example>
        /// <param name="str"></param>
        /// <returns></returns>
        /// Tested: 2020-07-08
        public static string ToUpperRR(this string str)
        {
            //Safety.
            if (!str.HasValueRR())
                return "";

            return str.ToUpper();
        }
















    }
}
