﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System //System is same namespace as Object.
{
    public static class Ext_Type
    {
        /// <summary>
        /// Check if object has method.  
        /// Only works on public methods.  
        /// </summary>
        /// <example>
        /// myObject.hasMethod("MethodName");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/5114514/722945
        /// </remarks>
        /// Tested: 2017-09-03.
        public static bool hasMethodRR(this object objectToCheck, string methodName)
        {
            var type = objectToCheck.GetType();
            return type.GetMethod(methodName) != null;
        }

        /// <summary>
        /// Check if object has property.  
        /// ☆ NOTE: Must set property to Public and have { get; set; }.  See example.  
        /// </summary>
        /// <example>
        /// public string PropertyName { get; set; } //Must define property like this.  
        /// myObject.hasProperty("PropertyName");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/5114514/722945
        /// </remarks>
        /// Tested: 2017-09-03.
        public static bool hasPropertyRR(this object objectToCheck, string propertyName)
        {
            var type = objectToCheck.GetType();   
            return type.GetProperty(propertyName) != null;
        }

        /// <summary>
        /// Check if object has event is available to hook into.  
        /// ☆ NOTE: Must set event to Public and have { get; set; }.  See example.  
        /// </summary>
        /// <example>
        /// public EventHandler EventName { get; set; }
        /// myObject.hasEvent("EventName");
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/5114514/722945
        /// </remarks>
        /// Tested: 2017-09-03.
        public static bool hasEventRR(this object objectToCheck, string eventName)
        {
            var type = objectToCheck.GetType();
            return type.GetProperty(eventName) != null;
        }



    }
}
