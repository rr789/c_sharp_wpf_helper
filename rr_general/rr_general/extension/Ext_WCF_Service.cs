﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ServiceModel.Channels;
using System.Xml;
using System.ServiceModel;
using System.Net;
using class_helper_rr;
using rr_general.helper;

namespace System.ServiceModel.Description //Same as what EndpointAddress uses.  
{
    public static class Ext_WCF_Service
    {
        /// <summary>
        /// Changes the server address.  
        /// </summary>
        /// <example>
        /// myServiceObj.aifService.Endpoint.UpdateRR(@"net.tcp://rm-dc1-DEV2:8201");
        ///     Changes from old address "net.tcp://HOSTNAME:8223/DynamicsAx/Services/SERVICE_GROUP_CREX_AIF_VendorRR".
        ///     To new address "net.tcp://rm-dc1-DEV2:8201/DynamicsAx/Services/SERVICE_GROUP_CREX_AIF_VendorRR"
        /// </example>
        /// <param name="obj">Object being extended.</param>
        /// <param name="serverAddressWithPortNew">New server address.  Do not add ending slash.</param>
        /// Tested: 2019-10-06
        public static void UpdateRR(this ServiceEndpoint obj, string serverAddressWithPortNew)
        {
            //2019-08-27 Safety.  I ran into this once. Fat fingers issue.    
            if (!serverAddressWithPortNew.HasValueRR())
                CMsgBox.Show("Error with Ext_WCF_Service.  The param serverAddress was empty.  This often happens when an AIF wrapper class tries to make lists/arrays of the AIF wrapper instead of the 'data contract' class.  Just check the syntax of the wrapper and try again. ");

            //2018-10-28 Wrapping in a try/catch helps prevent crashes when running in the RDP server.  Wierd bug but this wrap fixed it.  
            CTryCatchWrapper.wrap(delegate
            {
                //Sample address..."net.tcp://HOSTNAME:8223/DynamicsAx/Services/SERVICE_GROUP_CREX_AIF_VendorRR"
                string fullAddressOld = obj.Address.Uri.ToString();

                string serverAddressWithPortOld = CUrl.GetSubstringServerAndPort(fullAddressOld);
                string afterPortSubstringOld = CUrl.GetSubstringAfterPort(fullAddressOld);

                //Maybe save a little cpu.  
                if (serverAddressWithPortOld != serverAddressWithPortNew)
                {
                    string fullAddressNew = CUrl.ConcatTwoSubstrings(serverAddressWithPortNew, afterPortSubstringOld);
                    
                    //Debug: 
                    //CMsgBox.Show("switched.  old: " + serverAddressWithPortOld + "\nnew: " + serverAddressWithPortNew );
                    obj.Address = new EndpointAddress(new Uri(fullAddressNew), EndpointIdentity.CreateSpnIdentity(String.Empty)); //The second EndpointIdentity param is required to refresh server credentials.  See https://social.msdn.microsoft.com/Forums/en-US/8389c3f8-3ef6-4a5d-abfa-880dab1f6cea/error-quotcall-to-sspi-failedquot-quotthe-target-principal-name-is-incorrectquot-with?forum=wcf  
                }
                
            }, true, true, true);
        }





    }
}
