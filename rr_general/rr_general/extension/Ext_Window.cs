﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace System.Windows //Same as Window. 
{
    public static class Ext_Window
    {
        /// <summary>
        /// Centers window inside window's owner.  Works with multiple monitors.  
        /// ☆Note:
        ///     Must set window's Owner property.  
        ///     Must call this before window is shown.
        /// </summary>
        /// <example>
        /// //Set window's owner.  
        /// this.Owner = sourceGrid.GetParent<Window>();
        /// //Call this to center.  
        /// this.CenterWindowRR();
        /// </example>
        /// <remarks>
        /// From https://stackoverflow.com/a/28597480/722945
        /// </remarks>
        /// Tested: 2017-07-11.
        public static void CenterWindowRR(this Window win)
        {
            win.WindowStartupLocation = WindowStartupLocation.CenterOwner;
        }




    }
}
