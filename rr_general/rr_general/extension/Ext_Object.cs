﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System //System is same namespace as Object.  
{
    public static class Ext_Object
    {
        /// <summary>
        /// Dynamically invokes a method using reflection.  
        /// </summary>
        /// <remarks>
        /// Same as below, allows calling without defining T type.   
        /// </remarks>
        /// Tested: 2017-09-04
        public static object InvokeMethodRR(this object obj, string methodName, params object[] parameters)
        {
            return InvokeMethodRR<object>(obj, methodName, parameters);
        }

        /// <summary>
        /// Dynamically invokes a method and returns its value in a typed manner.
        /// </summary>
        /// <remarks>
        /// Modified from https://www.nuget.org/packages/dnpextensions (source code page: http://dnpextensions.codeplex.com/SourceControl/latest#PGK.Extensions/ObjectExtensions.cs)
        /// </remarks>
        /// <example>
        /// myObj.InvokeMethodRR("myMethodName", new object[] { param1, param2 }); //Can pass in null if no params.  
        /// </example>
        /// <typeparam name = "T">The expected return data type.</typeparam>
        /// <param name = "obj">Object to perform on.</param>
        /// <param name = "methodName">Name of the method.</param>
        /// <param name = "parameters">Parameters passed to the method.</param>
        /// <returns>The method's return value.</returns>
        public static T InvokeMethodRR<T>(this object obj, string methodName, params object[] parameters)
        {
            var method = obj.GetType().GetMethod(methodName); //https://stackoverflow.com/a/6469143/722945

            if (method == null)
                throw new ArgumentException("Method " + methodName + " not found.");

            var value = method.Invoke(obj, parameters);
            return (value is T ? (T)value : default(T));
        }


        /// <summary>
        /// Generates a copy of original object.
        /// Copy is a brand new object with no references to prior object.  
        /// Mainly used when multi-threading, to make a copy the internal thread can access.  
        /// Note: Only rely on this to grab properties.  I have not tested methods yet.   
        /// </summary>
        /// <example>
        ///     //Grab one record from db.
        ///     var originalRecord = GetSingleRecord<DATA_CONTRACT_CLASS>(999);
        ///     
        ///     //Make a copy of the record. 
        ///     var copy = originalRecord.CopyObjectRR<DATA_CONTRACT_CLASS>();
        ///     
        ///     //Access property from copied object.  
        ///     CMsgBox.Show(copy.MY_PROPERTY);
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// Tested: 2019-09-03
        public static T CopyObjectRR<T>(this object obj)
        {
            if (obj == null)
                return default(T); //Default(T) returns empty value depending on type.  Ex: Null for controls/strings, 0 for integers.  I tested.  

            string json = JsonConvert.SerializeObject(obj);

            return JsonConvert.DeserializeObject<T>(json);
        }


        /// <summary>
        /// Convert any object to json string. 
        /// </summary>
        /// <example>
        ///     var d = new Dictionary<string, string>()
        ///     {
        ///         { "KEY_01","VAL_01" }
        ///         ,{"KEY_02", "VAL_02" }
        ///     };
        ///     
        ///     d.ToJsonRR(); //Returns json string. 
        /// </example>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// Tested: 2019-09-16
        public static string ToJsonRR(this object obj)
        {
            //Safety: For the heck of it. 
            if (obj == null)
                return "";

            return JsonConvert.SerializeObject(obj);
        }



        /// <summary>
        /// Handy method to get non-public properties via reflection.
        /// </summary>
        /// <example>
        /// obj.GetPropertyValueRR("my_property_name");
        /// </example>
        /// <param name="srcObj"></param>
        /// <param name="propertyName"></param>
        /// <remarks>
        /// From: https://stackoverflow.com/a/28733237/722945   
        /// </remarks>
        /// <returns></returns>
        /// Tested: 2019-09-17
        public static object GetPropertyValueRR(this object srcObj, string propertyName)
        {
            //Safety:
            if (srcObj == null) 
                return null; 
            
            PropertyInfo pi = srcObj.GetType().GetProperty(propertyName.Replace("[]", ""));
            if (pi == null)
                return null;

            return pi.GetValue(srcObj);
        }
















    }

}

