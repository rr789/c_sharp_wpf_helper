﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace System.Windows //Same as FrameworkElement.
{
    public static class Ext_ChildHelper 
    {

        /// <summary>
        /// Finds visual (not logical) children by type.  Recursive search.    
        /// </summary>
        /// <example>
        /// //Find all column headers.  
        /// var arr = myGrid.FindVisualChildren<DataGridColumnHeader>();
        /// arr.EachRR(head =>
        /// {
        ///     CMsgBox.Show(head);
        /// });
        /// 
        /// //Find all treeview items.  
        /// var arr = myTreeview.FindVisualChildren<TreeViewItem>();
        /// arr.EachRR(item =>
        /// {
        ///     CMsgBox.Show(item);
        /// });
        /// 
        /// </example>
        /// <remarks>
        /// Modified from http://stackoverflow.com/a/978352/722945
        /// </remarks>
        /// <typeparam name="T">Type to look for.</typeparam>
        /// <param name="depObj">Element to look under for children.  </param>
        /// <param name="recursiveSearch">True=Search unlimited levels deep.  False=Search one level.  </param>
        /// Tested: 2017-07-09.
        public static ObservableCollection<T> FindVisualChildren<T>(this FrameworkElement depObj, bool recursiveSearch = true) where T : FrameworkElement
        {
            var r = new ObservableCollection<T>(); 
            DependencyObject temp = (DependencyObject)depObj;

            if (temp == null)
                return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(temp, i);

                if (child == null)
                    break;

                if (child is T)
                    r.Add((T)child);

                //Recursive. 
                //It's a little tricky, but the EachRR loop called at each level pushes each individual element to the top ObservableCollection.  So there are no buried collections.   I tested on TreeView.  
                if (recursiveSearch)
                {
                    if (child is FrameworkElement)
                    {
                        ((FrameworkElement)child).FindVisualChildren<T>().EachRR(subChild =>
                        {
                            if (subChild is T)
                                r.Add(subChild);
                        });
                    }
                }


            }
            
            if (r.Count() == 0)
                r = null;

            return r;
        }

        //--------------------------------------------------

        /// <summary>
        /// Detaches child from parent.  Does not delete child, so can move child under another element OK.  
        /// </summary>
        /// <example>
        /// //As extension.
        /// var p = myObj.GetParent<FrameworkElement>();
        /// p.DisconnectChild(myObj);
        /// 
        /// //As static method.
        /// var p = myObj.GetParent<FrameworkElement>();
        /// Ext_ChildHelper.DisconnectChild(p, myObj); //Disconnects myObj.  
        /// </example>
        /// <remarks>
        /// From: https://stackoverflow.com/a/19318405/722945
        /// </remarks>
        /// Tested: 2017-07-10 all passed.  
        public static void DisconnectChild(this FrameworkElement parent, FrameworkElement child)
        {
            //Grid, StackPanel, Canvas inherit from Panel.  So this is most common.  I tested all three types and this section was hit on all.  
            var panel = parent as Panel;
            if (panel != null)
            {
                panel.Children.Remove(child); //Remove disconnects, does not delete so OK.  
                return;
            }

            var decorator = parent as Decorator;
            if (decorator != null)
            {
                if (decorator.Child == child)
                {
                    decorator.Child = null;
                }
                return;
            }

            var contentPresenter = parent as ContentPresenter;
            if (contentPresenter != null)
            {
                if (contentPresenter.Content == child)
                {
                    contentPresenter.Content = null;
                }
                return;
            }

            //Window inherits from ContentControl.  
            var contentControl = parent as ContentControl;
            if (contentControl != null)
            {
                if (contentControl.Content == child)
                {
                    contentControl.Content = null;
                }
                return;
            }

            var contextMenu = parent as ContextMenu;
            if (contextMenu != null)
            {
                contextMenu.Items.Remove(child); //Remove just disconnects child reference.  The child object still exists OK.  

                return;
            }

            // maybe more
        }

        //--------------------------------------------------

        /// <summary>
        /// Attaches child to parent. 
        /// </summary>
        /// <example>
        /// //As extension.
        /// DependencyObject p = VisualTreeHelper.GetParent(myObj);
        /// Canvas childNew = new Canvas();
        /// p.AddChildRR(childNew); 
        /// 
        /// //As static method.
        /// DependencyObject p = VisualTreeHelper.GetParent(myObj);
        /// Canvas childNew = new Canvas();
        /// CChildHelper.AddChildRR(p, childNew); 
        /// </example>
        /// <remarks>
        /// Similar to DisconnectChild above. 
        /// </remarks>
        /// Tested: 2017-08-06 all passed.  
        public static void AddChildRR(this FrameworkElement parent, UIElement child)
        {
            //Grid, StackPanel, Canvas inherit from Panel.  So this is most common.  I tested all three types and this section was hit on all.  
            var panel = parent as Panel;
            if (panel != null)
            {
                panel.Children.Add(child);
                return;
            }

            //Decorator has a single child.  http://www.blackwasp.co.uk/WPFDecorator.aspx
            var decorator = parent as Decorator;
            if (decorator != null)
            {
                if (decorator.Child == null)
                    decorator.Child = child;
                else
                    Debug.WriteLine("ERROR WITH EXT_CHILDHELPER.  DECORATOR ALREADY HAS A CHILD. ");
                
                return;
            }

            //Window.  
            //https://stackoverflow.com/a/30055598/722945
            var win = parent as Window;
            if (win != null)
            {
                //Fyi: Window structure: 
                //Border
                //  AdornerDecorator
                //      ContentPresenter
                //          Grid (this is what we want).  


                //Find immediate grid.   
                //Runs recursively, but First() grabs the most immediate child.  I tested.
                var gridChild = win.FindVisualChildren<Grid>().First();    

                if (gridChild != null)
                {
                    gridChild.Children.Add(child);
                }

                return;
            }

            // maybe more
        }




        





    }
}
