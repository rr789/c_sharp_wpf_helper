﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Windows.Controls //Same as native TextBox.  
{
    public static class Ext_TextBox
    {

        /// <summary>
        /// Handy method to check if user selected all text inside the textbox.  
        /// </summary>
        /// Tested: 2019-11-09.
        public static bool IsAllTextSelectedRR(this TextBox obj)
        {
            if (obj == null)
                return false;

            var tBox = (TextBox)obj;

            if (tBox.Text.CountRR() == 0)
                return false;

            int textLength = tBox.Text.Length;

            int textSelectedLength = tBox.SelectionLength;

            if (textLength == textSelectedLength)
                return true;
            else 
                return false;
        }


    }
}
