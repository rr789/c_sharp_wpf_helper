﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace System.Windows //Same as FrameworkElement.
{
    public static class Ext_DisposeSelf
    {

        /// <summary>
        /// Disposes self.  Removes from visual tree and turns element null.  
        /// </summary>
        /// <example>
        /// myElement.DisposeSelfRR<MyElementType>();
        /// </example>
        /// <typeparam name="T">Type of self.  Redundant but anyhow.</typeparam>
        /// <param name="elem"></param>
        /// Tested: 2017-07-30.
        public static void DisposeSelfRR<T>(this FrameworkElement elem) where T: FrameworkElement
        {
            var parent = (FrameworkElement)elem.Parent;

            parent.DisconnectChild(elem);
            elem = null;
        }


    }
}

