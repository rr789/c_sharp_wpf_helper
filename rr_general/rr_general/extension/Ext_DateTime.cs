﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using class_helper_rr;
using rr_general.config;

namespace System //DateTime uses system namespace. 
{
    public static class Ext_DateTime
    {
        /// <summary>
        /// Basic validation to ensure a valid date is set.  Aka not null, and not 1900-01-01.
        /// </summary>
        /// <example>
        /// Convert.ToDateTime("1900-01-01").IsValidDateRR(); //False
        /// Convert.ToDateTime("2019-01-01").IsValidDateRR(); //True
        /// </example>
        /// <param name="obj"></param>
        /// Tested: 2019-12-22
        public static bool IsValidDateRR(this DateTime obj)
        {
            if (obj == null)
                return false;

            var d = (DateTime)obj;

            if (d <= CGlobal.ErrorDateTime)
                return false;

            return true;
        }


        /// <summary>
        /// Determines the date of the first day of the week for given date.
        /// </summary>
        /// <example>
        ///     //Determines date of first day of current week if using a Mon-Sun week:
        ///     DateTime weekFirstDate = DateTime.Today.FirstDateInWeek(DayOfWeek.Monday);
        /// </example>
        /// <param name="dt"></param>
        /// <param name="weekStartDay"> Which day of the week is considered the first day. Usually Sunday or Monday. </param>
        /// <returns></returns>
        /// Tested: 2022-03-28
        public static DateTime FirstDateInWeek(this DateTime dt, DayOfWeek weekStartDay)
        {
            while (dt.DayOfWeek != weekStartDay)
                dt = dt.AddDays(-1);
            return dt;
        }


        /// <summary>
        /// Determines the date of the last day of the week for given date.
        /// </summary>
        /// <example>
        ///     //Determines date of last day of current week if using a Sun-Sat week:
        ///     DateTime weekLastDate = DateTime.Today.LastDateInWeek(DayOfWeek.Saturday);
        /// </example>
        /// <param name="dt"></param>
        /// <param name="weekEndDay"> Which day of the week is considered the last day. Usually Saturday or Sunday. </param>
        /// <returns></returns>
        /// Tested: 2022-03-28
        public static DateTime LastDateInWeek(this DateTime dt, DayOfWeek weekEndDay)
        {
            while (dt.DayOfWeek != weekEndDay)
                dt = dt.AddDays(1);
            return dt;
        }






    }
}
