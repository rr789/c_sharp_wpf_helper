﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Windows.Data //Same as CompositeCollection.
{
    public static class Ext_Cast
    {
        /// <summary>
        /// Similar to native Cast() except this filters out incompatible elements. The native Cast() tries to cast everything.  
        /// Returns List T.  
        /// </summary>
        /// Tested: 2017-07-03.
        public static List<T> CastRR<T>(this CompositeCollection obj)
        {
            if (obj == null)
                return null;

            var r = new List<T>();

            foreach (var item in obj)
            {
                if (item is T)
                    r.Add((T)item);
            }

            if (r.Count == 0)
                r = null;

            return r;
        }


    }
}







//---------------------------------------------------------------------
namespace System.Windows.Controls //Same as ItemCollection/ItemsControl.
{
    public static class Ext_Cast
    {
        /// <summary>
        /// Similar to native Cast() except this filters out incompatible elements. The native Cast() tries to cast everything.  
        /// Returns List T.  
        /// </summary>
        /// Tested: 2017-07-03.
        public static List<T> CastRR<T>(this ItemCollection obj) //Define ItemCollection here, so ItemsControl is extended.  Defining ItemsControl here does not extend.    
        {
            if (obj == null)
                return null;

            var r = new List<T>();

            foreach (var item in obj)
            {
                if (item is T)
                    r.Add((T)item);
            }

            if (r.Count == 0)
                r = null;

            return r;
        }


    }
}