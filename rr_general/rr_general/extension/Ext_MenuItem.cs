﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace System.Windows.Controls //Same as MenuItem.
{
    public static class Ext_MenuItem
    {

        /// <summary>
        /// Returns the control that owns this menu item.  
        /// Makes it easy.  Trying to get it natively is cumbersome, see https://stackoverflow.com/a/44610539/722945
        /// </summary>
        /// <example>
        /// Use this in "Menu Button Click" event.  Only works while menu is visible (since it relies on PlacementTarget) property.    
        /// var a = ((MenuItem)sender).getOwnerControl<DataGridRR>();
        /// CMsgBox.Show(a);
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// Tested: 2017-07-02 all passed.  
        public static T getOwnerControl<T>(this MenuItem menu) where T : FrameworkElement
        {
            FrameworkElement r = null;

            //----------------------------
            //Step 1: Get Popup parent.  
            //----------------------------

            //Fyi: Structure of context menu.  
            //Popup
            //  ContextMenu
            //    MenuItem(s).  
            //      MenuItem(s).  Optional, if parent/child menus.  

            //Goes up the tree until it finds a Popup type.  
            Popup popup_ = menu.GetParent<Popup>();
            
            if (popup_ != null)
            {
                //MenuItems exist outside in the visual tree.  So have to use PlacementTarget.  
                //https://stackoverflow.com/a/44610539/722945
                r = (FrameworkElement)popup_.PlacementTarget; 
            }

            //----------------------------
            //Step 2: Get parent (above Popup) that we are really looking for.    
            //----------------------------
            return r.GetParent<T>();
        }





    }
}
