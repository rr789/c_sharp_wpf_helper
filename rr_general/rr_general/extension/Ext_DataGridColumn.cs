﻿using class_helper_rr;
using rr_general.wpf_custom_control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;

namespace System.Windows.Controls //Same namespace as DataGridColumn & DataGridBoundColumn.   
{
    public static class Ext_DataGridBoundColumn
    {
        /// <summary>
        /// Returns data field name of column.  
        /// Handles DataGridComboBox columns OK.  They bind differently but this still gets the db fieldname.  
        /// </summary>
        /// <example>
        /// //In xaml <DataGridTextColumn Binding="{Binding column1}"/>
        /// foreach (DataGridBoundColumn c in grid.Columns)
        /// {
        ///     c.getBindingFieldNameRR(); //Returns "column1" etc....
        /// }
        /// </example>
        /// <param name="col">Column object.</param>
        /// <returns></returns>
        /// Tested: 2017-09-26 all passed.
        public static string getBindingFieldNameRR(this DataGridBoundColumn col)
        {
            Binding b = (Binding)col.Binding; //https://stackoverflow.com/a/44610813/722945

            //Aka if not combobox column.  
            if (b != null)
            {
                return b.Path.Path;
            }

            //Handle combobox columns.  
            if (typeof(DataGridComboBoxColumn).IsAssignableFrom(col.GetType()))
            {
                var tempCol2 = CType.ConvertType<DataGridComboBoxColumnRR>(col);

                //Used when combobox binds to entire db record (multiple columns).    
                if (tempCol2.SelectedItemBinding != null)
                    return tempCol2.DisplayMemberPath;

                //Used when combobox binds to one db column.  
                if (tempCol2.SelectedValueBinding != null)
                {
                    b = (Binding)tempCol2.SelectedValueBinding; //Gotta use this (Binding) trick to get Path.Path.  https://stackoverflow.com/a/44610813/722945
                    return b.Path.Path;
                }
            }

            CMsgBox.Show("Error getBindingFieldNameRR: Cannot get binding name for column " + col.Header.ToString());

            return null;

        }


        /// <summary>
        /// Gets DataGrid object that owns this column.  
        /// </summary>
        /// <param name="column"></param>
        /// <returns>DataGrid</returns>
        /// Tested: 2017-07-01 all passed.  
        public static DataGrid GetDataGridParent(this DataGridBoundColumn column)
        {
            /*
             * WPF has the property DataGridOwner, but it is private.  So have to dig it out this way.  
             * https://stackoverflow.com/a/4519632/722945.  
             */
            PropertyInfo propertyInfo = column.GetType().GetProperty("DataGridOwner", BindingFlags.Instance | BindingFlags.NonPublic);
            return propertyInfo.GetValue(column, null) as DataGrid;
        }


        /// <summary>
        /// Gets data grid header object.  
        /// </summary>
        /// <remarks>
        /// WPF has no built-in "header obj" property.  Can get the header "text" but not the obj.  
        /// Modified from: https://stackoverflow.com/a/24104351/722945
        /// </remarks>
        /// <param name="col">DataGridColumn</param>
        /// Tested: 2017-07-01.
        public static DataGridColumnHeader GetHeader(this DataGridBoundColumn col)
        {
            DataGridColumnHeader r = null; //Returned value.  

            DataGrid grid = col.GetDataGridParent();

            //Digs thru grid recursively to find children of type DataGridColumnHeader.  
            var headers = grid.FindVisualChildren<DataGridColumnHeader>();

            headers.EachRR(c =>
            {
                if (c.DisplayIndex == col.DisplayIndex)
                    r = c;
            });

            return r;
        }


    }

    //-------------------------------------

    public static class Ext_DataGridColumn
    {
        /// <summary>
        /// Returns data field name of column.  
        /// Handles DataGridComboBox columns OK.  They bind differently but this still gets the db fieldname.  
        /// </summary>
        /// <example>
        /// //In xaml <DataGridTextColumn Binding="{Binding column1}"/>
        /// foreach (DataGridBoundColumn c in grid.Columns)
        /// {
        ///     c.getBindingFieldNameRR(); //Returns "column1" etc....
        /// }
        /// </example>
        /// <param name="col">Column object.</param>
        /// <returns></returns>
        /// Tested: 2017-09-26 all passed.
        public static string getBindingFieldNameRR(this DataGridColumn col)
        {
            Binding b = null;

            //Try to get bindable property.  Mainly a safety for ComboBox columns, since they have no Binding property.  
            if (typeof(DataGridBoundColumn).IsAssignableFrom(col.GetType()) )
            {
                DataGridBoundColumn tempCol1 = (DataGridBoundColumn)col;
                b = (Binding)tempCol1.Binding; //Gotta use this (Binding) trick to get Path.Path.  https://stackoverflow.com/a/44610813/722945
            }

            //Return binding from above (aka if not combobox column).  
            if (b != null)
                return b.Path.Path;

            //Handle combobox columns.  
            if (typeof(DataGridComboBoxColumn).IsAssignableFrom(col.GetType()))
            {
                var tempCol2 = CType.ConvertType<DataGridComboBoxColumnRR>(col);

                //Used when combobox binds to entire db record (multiple columns).    
                if (tempCol2.SelectedItemBinding != null)
                    return tempCol2.DisplayMemberPath;

                //Used when combobox binds to one db column.  
                if (tempCol2.SelectedValueBinding != null)
                {
                    b = (Binding)tempCol2.SelectedValueBinding; //Gotta use this (Binding) trick to get Path.Path.  https://stackoverflow.com/a/44610813/722945
                    return b.Path.Path;
                }
            }

            CMsgBox.Show("Error getBindingFieldNameRR: Cannot get binding name for column " + col.Header.ToString());

            return null;


        }



        /// <summary>
        /// Gets DataGrid object that owns this column.   
        /// </summary>
        /// <param name="column"></param>
        /// <returns>DataGrid</returns>
        /// Tested: 2017-07-01 all passed.  
        public static DataGrid GetDataGridParent(this DataGridColumn column)
        {
            /*
             * WPF has the property DataGridOwner, but it is private.  So have to dig it out this way.  
             * https://stackoverflow.com/a/4519632/722945.  
             */
            PropertyInfo propertyInfo = column.GetType().GetProperty("DataGridOwner", BindingFlags.Instance | BindingFlags.NonPublic);
            return propertyInfo.GetValue(column, null) as DataGrid;
        }


        /// <summary>
        /// Gets data grid header object.  
        /// </summary>
        /// <remarks>
        /// WPF has no built-in "header obj" property.  Can get the header "text" but not the obj.  
        /// Modified from: https://stackoverflow.com/a/24104351/722945
        /// </remarks>
        /// <param name="col">DataGridColumn</param>
        /// Tested: 2017-07-01.
        public static DataGridColumnHeader GetHeader(this DataGridColumn col)
        {
            DataGridColumnHeader r = null; //Returned value.  

            DataGrid grid = col.GetDataGridParent();

            //Digs thru grid recursively to find children of type DataGridColumnHeader.  
            var headers = grid.FindVisualChildren<DataGridColumnHeader>();

            headers.EachRR(c =>
            {
                if (c.DisplayIndex == col.DisplayIndex)
                    r = c;
            });

            return r;
        }



    }



}
