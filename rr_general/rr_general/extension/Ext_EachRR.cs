﻿using class_helper_rr;
using rr_general.wpf_custom_control.treeview_item_rr;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

//---------------------------------------------------------------------
// Adds .EachRR to object types.  To avoid null check before foreach loops.  
//---------------------------------------------------------------------


namespace rr_general.wpf_custom_control.treeview_item_rr //Same as itemRR.  
{
    public static class Ext_EachRR_ItemRR
    {
        /// <summary>
        /// Easy ForEach loop with automatic null check.  
        /// Only works with itemRR
        /// </summary>
        /// <example>
        /// //One line.
        /// myItemRR.EachRR(p => CMsgBox.Show(p.NameDisplay));
        /// 
        /// //Multi-line.   
        /// myItemRR.EachRR(p =>
        /// {
        ///     CMsgBox.Show(p.NameDisplay);
        ///     //etc....
        /// });
        /// </example>
        /// <remarks>
        /// From: http://surmount.github.io/2009/02/11/ienumerableteach-as-c-extension-method/
        /// </remarks>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        /// Tested: 2017-05-28 all passed.
        public static void EachRR(this itemRR obj, Action<itemRR> action) //"this" in the params makes it an extension.  https://www.dotnetperls.com/extension   No <T> here since only extending a single type (itemRR).   
        {
            if (obj != null)
            {
                foreach (itemRR itm in obj.Items)
                {
                    action(itm);
                }
            }
        }

    }
}


//---------------------------------------------------------------------
namespace System.Collections.Generic //Same as IEnumerable.
{
    public static class Ext_EachRR_IEnumerable
    {
        /// <summary>
        /// Easy ForEach loop with automatic null check.  
        /// Only works with IEnumerable objects.  
        /// </summary>
        /// <example>
        /// IEnumerable<string> x = new[] { "hello", "world" };
        /// x.EachRR(p =>
        /// { 
        ///     CMsgBox.Show(p);
        /// });
        /// </example>
        /// <remarks>
        /// From: http://surmount.github.io/2009/02/11/ienumerableteach-as-c-extension-method/
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        /// Tested: 2017-05-28 all passed.
        public static void EachRR<T>(this IEnumerable<T> obj, Action<T> action)
        {
            if (obj != null)
            {
                foreach (var item in obj)
                {
                    action(item);
                }
            }
        }

    }

    //---------------------------------------------------------------------

}




//---------------------------------------------------------------------
namespace System.Windows.Controls //Same as ItemCollection/ItemsControl.
{
    public static class Ext_EachRR_IEnumerable
    {
        /// <summary>
        /// Easy ForEach loop with automatic null check.  
        /// Extends ItemsControl objects.  
        /// </summary>
        /// <example>
        /// Requires defining the type since ItemsControl can hold any type of object.  
        /// 
        /// //Datagrid with ExpandoObject rows.  
        /// grid.Items.EachRR<ExpandoObject>(rowObj =>
        /// {
        ///     //IDictionary cast only needed for Expandoobject rows, so VS allows using p["column1"].  
        ///     CMsgBox.Show(((IDictionary<String, Object>) rowObj)["column1"]);
        /// });
        /// </example>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        /// Tested: 2017-07-02.
        public static void EachRR<T>(this ItemCollection obj, Action<T> action) //Define ItemCollection here, so ItemsControl is extended.  Defining ItemsControl here does not extend.    
        {
            if (obj != null)
            {
                foreach (var item in obj)
                {
                    action((T)item);
                }
            }
        }

        

    }

}




