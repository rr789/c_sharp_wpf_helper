﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Collections.Generic //Matches IEnumerable namespace. 
{
    public static class Ext_IEnumerable
    {

        /// <summary>
        /// Returns count of elements (in list, or any IEnumerable object).  Returns zero if null.     
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// Tested: 2019-09-18
        public static long CountRR<T>(this T obj)
        {
            if (obj == null)
                return 0;

            IEnumerable r = (IEnumerable)obj;

            long count = 0;

            var _enum = r.GetEnumerator();
            while (_enum.MoveNext())
            {
                count++;
            }

            return count;
        }



    }
}
