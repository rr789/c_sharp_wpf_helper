﻿using class_helper_rr;
using rr_general.wpf_custom_control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace rr_general.wpf_custom_control //Same as SeparatorRR.
{
    public static class Ext_SeparatorRR
    {

        /// <summary>
        /// Returns the control that owns this separator item.  Intended when separator is in ContextMenu.  
        /// Makes it easy.  Trying to get it natively is cumbersome, see https://stackoverflow.com/a/44610539/722945
        /// </summary>
        /// <example>
        /// Use this in "Menu Button Click" event.  Only works while menu is visible (since it relies on PlacementTarget) property.    
        /// var a = ((SeparatorRR)sender).getOwnerControl<DataGridRR>();
        /// CMsgBox.Show(a);
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// Tested: 2017-07-17 all passed.  
        public static T getOwnerControl<T>(this SeparatorRR separator_) where T : FrameworkElement
        {
            FrameworkElement r = null;

            //----------------------------
            //Step 1: Get Popup parent.  
            //----------------------------

            //Fyi: Structure of context menu.    
            //Popup
            //  ContextMenu
            //    MenuItem(s) and separators.  
            //      MenuItem(s) and separators.  Optional, if parent/child menus.  

            //Goes up the tree until it finds a Popup type.  
            Popup popup_ = separator_.GetParent<Popup>();

            if (popup_ != null)
            {
                //MenuItems exist outside in the visual tree.  So have to use PlacementTarget.  
                //https://stackoverflow.com/a/44610539/722945
                r = (FrameworkElement)popup_.PlacementTarget;
            }

            //----------------------------
            //Step 2: Get parent (above Popup) that we are really looking for.    
            //----------------------------
            return r.GetParent<T>();
        }





    }
}
