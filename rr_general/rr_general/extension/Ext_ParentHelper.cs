﻿using class_helper_rr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace System.Windows //Same as FrameworkElement.
{
    public static class Ext_ParentHelper
    {
        /// <summary>
        /// Goes up the tree (recursive) until it finds parent matching type. 
        /// Tries two different ways to get parent.  Very powerful.  
        /// </summary>
        /// <example>
        /// //Get popup parent from child menu.    
        /// menuObj.GetParent<Popup>(); //Finds parent with type Popup. 
        /// 
        /// //Get DataGridRR parent from child DataGridRow.  
        /// rowObj.GetParent<DataGridRR>(); //For this example, it works if rowObj is DataGridRow type (since DataGridRow has hooks into visual tree).  
        /// </example>
        /// <typeparam name="T">Type to find.</typeparam>
        /// <param name="child">Extended object.</param>
        /// Tested: 2017-07-02 all passed.  
        public static T GetParent<T>(this FrameworkElement child) where T : FrameworkElement
        {
            T r = null;

            //---------------------------------------------
            //First attempt.
            //---------------------------------------------
            var try1 = GetParent_UsingParentProperty<T>(child);

            if (try1 != null)
                r = try1;

            //---------------------------------------------
            //Second attempt.
            //---------------------------------------------
            var try2 = GetParent_VisualTreeHelper<T>(child);

            if (try2 != null)
                r = try2;


            return r;
        }


        ///---------------------------------------------
        /// PRIVATE FUNCTION ATTEMPT 1.  SUPPORTS GetParent() ABOVE.    
        /// Get parent using Parent property.  
        /// This works for MenuItem to find Popup parent.  
        ///---------------------------------------------
        /// <summary>
        /// Goes up the tree (recursive) until it finds parent matching type.  
        /// </summary>
        /// <typeparam name="T">Type to find.</typeparam>
        /// <param name="child">Child object.</param>
        /// <returns></returns>
        /// Tested: 2017-07-02 all passed.  
        private static T GetParent_UsingParentProperty<T>(FrameworkElement child) where T : FrameworkElement
        {
            T r = default(T); //Returned. 

            if (child == null)
                return r;

            FrameworkElement parent = (FrameworkElement)child.Parent;

            if (parent != null)
            {
                if (parent is T && parent != child) 
                {
                    r = CType.ConvertType<T>(parent); //I forget why ConvertType was needed.  Regular casting wasn't working on some type.  
                }
                else
                {
                    //Recursive.
                    r = GetParent_UsingParentProperty<T>(parent);
                }
            }

            return r;
        }




        ///---------------------------------------------
        /// PRIVATE FUNCTION ATTEMPT 2.  SUPPORTS GetParent() ABOVE. 
        /// Get parent using VisualTreeHelper.   
        ///---------------------------------------------
        /// <summary>
        /// Gets parent object with matching type.  Searches visual tree.  Recursive search upward.  
        /// </summary>
        /// <typeparam name="T">Type to find.</typeparam>
        /// <param name="child">Child object.</param>
        /// Tested: 2017-07-09 all passed.  
        private static T GetParent_VisualTreeHelper<T>(FrameworkElement child) where T : FrameworkElement
        {
            DependencyObject r = (DependencyObject)child;

            //Recursive search up the tree.  From: https://stackoverflow.com/a/42167892/722945
            while (r != null && !(r is T) || r == child) //Need the "r == child".  I tested.  
            {
                r = VisualTreeHelper.GetParent(r);
            }

            //Safety: Return null if no parent found.  
            if (!(r is T))
                r = null;

            return CType.ConvertType<T>(r); //ConvertType not needed here, but since Attempt 1 uses it, keeping consistent....
        }









    }
}
