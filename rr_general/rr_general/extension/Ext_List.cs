﻿using rr_general.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Collections.Generic
{
    public static class Ext_List
    {
        /// <summary>
        /// Replaces item in list object.  Modifies original object (does not create a copy).  
        /// </summary>
        /// <example>
        /// var myList = new List<string>() { "A", "B", "C" }; 
        /// var oldItem = "B";
        /// var newItem = "D";
        /// myList.ReplaceSingleItem(oldItem, newItem); //Replaces "B" with "D".  
        /// 
        /// for (var i = 0; i < myList.Count; i++)
        /// {
        ///     CMsgBox.Show(myList[i]); //Returns "A", "D", "C". 
        /// }
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="oldItem"></param>
        /// <param name="newItem"></param>
        /// <returns>Index of item replaced (for fyi sake).</returns>
        /// <remarks>
        /// Modified from: https://stackoverflow.com/a/38728879/722945  
        /// </remarks>
        /// Tested: 2019-07-07
        public static int ReplaceSingleItem<T>(this IList<T>source, T oldItem, T newItem)
        {
            if (source == null)
                CLogger.CreateLogEntry("Error with Ext_List->ReplaceSingleItem().  List param was null.", true, true);

            var index = source.IndexOf(oldItem);
            
            if (index != -1)
                source[index] = newItem;
            

            return index;
        }


        /// <summary>
        /// Quick method to determine if variable has value/more than zero rows.  
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        /// Tested: 2019-09-07
        public static bool HasValueRR(this IList source)
        {
            if (source == null)
                return false;

            if (source.Count == 0)
                return false;

            return true;
        }



    /// <summary>
    /// Similar to RemoveAll.  This has extra safety to handle NULL lists.  
    /// </summary>
    /// 
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="_predicate"></param>
    /// Tested: 2023-11-09
        public static void RemoveAllRR<T>(this List<T>source, Predicate<T> _predicate) //Had to use parameter List (instead of IList). List had method RemoveAll. IList did NOT have method RemoveAll.  
        {
            if (source == null)
                CLogger.CreateLogEntry("Error with Ext_List->ReplaceSingleItem().  List param was null.", true, true);

            source.RemoveAll(_predicate);

        }


    }
}
