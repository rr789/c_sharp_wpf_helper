﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System //Decimal uses "System" namespace.  
{
    public static class Ext_Decimal
    {

        /// <summary>
        /// Concise method to determine if number is zero.  Handles rounding automatically.
        /// Mainly used after subtracting two numbers, to tell if result is zero.  
        /// I ran into situations (especially with "double" numbers from form inputs)...C## adds a zillion digits to the end.  Ex: Form value of "2.0" converted to double can = "1.999999999".   
        /// 2021-09-22 This may not be needed with "Double" types.  So could possibly remove this extension.  
        /// </summary>
        /// <example>
        ///     (0.0040).IsZeroRR();                //Returns true;
        ///     (0.0050).IsZeroRR(2 or default);    //Returns true;  C# rounds down.  
        ///     (0.0050).IsZeroRR(3);               //Returns false
        ///     (0.0051).IsZeroRR(2 or default);    //Returns false.  
        /// </example>
        /// <param name="obj"></param>
        /// <param name="roundingDecimalPlaces">Optional, defaults to 2</param>
        /// <returns></returns>
        /// Tested: 2019-09-24
        public static bool IsZeroRR(this decimal obj, int roundingDecimalPlaces = 2)
        {
            decimal val = Math.Round((decimal)obj, roundingDecimalPlaces);
 
            if (Math.Abs(val) < 0.00001m) //TODO: 0.00001m is an arbitrary value.  Have it adjust by param roundingDecimalPlaces.    
                return true;

            return false;  
        }


        /// <summary>
        /// Little more concise method to round a number.  
        /// </summary>
        /// <example>
        ///     (0.123).RoundRR(1); //Returns 0.1. 
        ///     (0.123).RoundRR(2); //Returns 0.12. 
        ///     (0.123).RoundRR(3); //Returns 0.123.  
        ///     
        ///     (1.234).RoundRR();  //Returns 1.23.  Two decimals is default. 
        ///     (1.234).RoundRR(1); //Returns 1.2.
        /// </example>
        /// <param name="obj"></param>
        /// <param name="roundingDecimalPlaces">Two decimals is default. </param>
        /// <returns></returns>
        /// Tested: 2019-09-25
        public static decimal RoundRR(this decimal obj, int roundingDecimalPlaces = 2)
        {
            return Math.Round((decimal)obj, roundingDecimalPlaces);
        }


        /// <summary>
        /// Allows substitute number if value is zero.  Rounds to third decimal place.  
        /// </summary>
        /// <example>
        /// (0.001).SubstituteIfZero(10);   //Shows 0.001.  Default rounding is three digits. 
        /// (0.004).SubstituteIfZero(10);   //Shows 0.004.  Default rounding is three digits. 
        /// (0.001).SubstituteIfZero(10,2); //Shows 10.  Rounded to two digits.
        /// (0.004).SubstituteIfZero(10,2); //Shows 10.  Rounded to two digits.
        /// </example>
        /// <param name="obj"></param>
        /// <param name="replacementValue"></param>
        /// <returns></returns>
        public static decimal SubstituteIfZero( this decimal obj, decimal replacementValue, int roundingDigits= 3)
        {
            decimal original = obj;  

            //Three places for the heck of it.  
            if (original.IsZeroRR(roundingDigits))
                return replacementValue; 

            return original;
        }

    }
}
