﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using class_helper_rr;

namespace System //String uses system namespace.  
{
    public static class Ext_ArraySingle
    {
        /// <summary>
        /// Counts items in an array that have a non-null value.  Empty string "" counts as a value. 
        /// Improvement over native Length and GetLength() method which just counts slots allocated (does not care if value is set or not).  
        /// </summary>
        /// <example>
        ///     //Create empty array with four slots.  
        ///     string[] arr = new string[4];
        ///     arr.countItemsWithValue(); //Returns zero since no values were set.    
        /// 
        ///     //Set a couple values. 
        ///     arr[0] = "a"; 
        ///     arr[1] = "b";
        ///     arr.countItemsWithValue();//Returns two.  
        /// 
        ///     //Set value that skips the "c" (aka [2]) slot.
        ///     arr[3] = "d"; 
        ///     arr.countItemsWithValue(); //Returns three.  
        /// </example>
        /// <param name="str"></param>
        /// Tested: 2018-09-11
        public static int CountItemsWithValue<T>(this T[] arr)
        {
            int _count = 0;
            
            //Length returns "defined" length when created.  It does not care if each slot has a value. 
            for (var i=0;i<arr.Length; i++)   
            {
                if (arr[i] != null )
                    _count++;
            }

            return _count;
        }


        /// <summary>
        /// Returns first empty position. Index starts at zero.  
        /// If all positions are taken, then returns global error number.  
        /// </summary>
        /// <example>
        ///     //Define array.  Notice the third position for "c" (aka [2]) is not set.  
        ///     string[] arr = new string[4];
        ///     arr[0] = "a";
        ///     arr[1] = "b";
        ///     arr[3] = "d";
        ///     arr.getFirstEmptyPosition(); //Returns 2.
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="arr"></param>
        /// Tested: 2018-09-11
        public static int GetFirstEmptyPosition<T>(this T[] arr)
        {
            if (arr == null)
                return CGlobal.ErrorNumber_Int;

            //Length returns "defined" length when created.  It does not care if each slot has a value. 
            for (var i=0;i<arr.Length; i++) 
            {
                if (arr[i] == null)
                    return i;
            }
            
            return CGlobal.ErrorNumber_Int;
        }


        //
        /// <summary>
        /// Adds an item to the first open position/index in array.  
        /// This modifies the original array.  
        /// Note: To resize and add, use class CArraySingle instead since Array.Resize() makes a "copy" of the array.  See https://stackoverflow.com/q/42411693/722945  
        /// </summary>
        /// <example>
        ///     //Note we skipped "c" (aka position [2]);
        ///     string[] arr = new string[4];
        ///     arr[0] = "a";
        ///     arr[1] = "b";
        ///     arr[3] = "d";
        ///     
        ///     //Add value "c" to position [2].
        ///     arr.AddToFirstEmptyPosition("c");
        /// 
        ///     //Done.  Loop will show "c" is in position [2].  
        ///     for (var i=0;i<arr.Length;i++)
        ///     {
        ///         CMsgBox.Show("loop " + i + ": " + arr[i]);
        ///     }
        /// </example>
        /// <typeparam name="T"></typeparam>
        /// <param name="arr"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        /// Tested: 2019-07-06
        public static void AddToFirstEmptyPosition<T>(this T[] arr, T value)
        {
            int firstEmptyPosition = arr.GetFirstEmptyPosition();

            if (firstEmptyPosition != CGlobal.ErrorNumber_Int)
                arr[firstEmptyPosition] = value;
            else
                CMsgBox.Show("Error with Ext_ArraySingle->AddToFirstEmptyPosition().  Array is full.  Cannot add another item.");

        }








    }
}