﻿using class_helper_rr;
using rr_general.wpf_custom_control.treeview_item_rr;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;


//---------------------------------------------------------------------
namespace System.Windows.Controls //Same as ItemCollection/ItemsControl.
{
    public static class Ext_ReverseOrder
    {
        /// <summary>
        /// Modifies collection to reverse the order of child items.  First item becomes last, vice versa.  
        /// </summary>
        /// <example>
        /// myItemsControl.Items.ReverseOrder();
        /// </example>
        /// <param name="obj"></param>
        /// Tested: 2017-07-15.
        public static void ReverseOrder(this ItemCollection obj) //Define ItemCollection here, so ItemsControl is extended.  Defining ItemsControl here does not extend.    
        {
            if (obj == null)
                return;

            //Best to keep this static value, since loop below modifies collection.    
            int cnt = obj.Count;

            //Loop in reverse order since disconnecting children.  Otherwise VS throws error "GetItemAt() not valid range of values".  
            for (int i = cnt - 1; i >= 0; i--)
            {
                //Remove from parent collection.  Again, starts from end position.  
                var itm = obj.GetItemAt(i);
                obj.Remove(itm); //itm still exists, just removed from parent index reference.  

                //Add item to end.  End result: First item becomes last, vice versa.  
                obj.Add(itm);
            }
        }



    }
}