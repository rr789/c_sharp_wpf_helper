﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using class_helper_rr;
using WebView2;

namespace Microsoft.Web.WebView2.Wpf //Same as WebView2.  
{
    /// <summary>
    /// Couple handy extension for teh WebView2 browser.  
    /// Note: 
    /// 
    /// 
    /// 2023-08-22 replaces prior CWebBrowser which became unusable since it used old Internet Explorer.  
    /// </summary>
    public static class Ext_WebView2
    {

        /// <summary>
        /// (RR) Convenience function.  Can pass in a string without having to convert to "URI".  
        /// </summary>
        /// <example>
        /// //Load blank page. 
        /// browserObj.OpenUrl("about:blank");
        /// </example>
        /// <param name="url"></param>
        /// Tested: 2023-08-22
        public static void OpenUrl(this WebView2 obj, string url) 
        { 
            obj.Source = new Uri(url);
        }


        /// <summary>
        /// (RR) Convenience function.  Returns URL of current website as a "string".  No need to conver from "URI".  
        /// </summary>
        /// <returns></returns>
        /// Tested: 2023-08-22
        public static string GetUrl(this WebView2 obj) 
        { 
            return obj.Source.ToString(); 
        }


        //Fyi: To refresh website..use native Reload().  

   
        /// <summary>
        /// Returns domain name.  Ex: "google.com".  
        /// </summary>
        /// Tested: 2023-08-22
        public static string GetDomain(this WebView2 obj)
        {
            //From http://www.jonasjohn.de/snippets/csharp/extract-domain-name-from-url.htm
            string r = obj.Source.Host; 
            r = r.Replace("www.", ""); //Https urls include "www." (for whatever reason).  Remove it here.  Fyi: There are no "https" in the value (so no need to replace that).  
            return r;
        }



    }
}
