﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace System //Same namespace used by Int and long.
{
    /// <summary>
    /// For now, used for both 32bit int and 64bit long.  
    /// </summary>
    public static class Ext_Integer
    {
        /// <summary>
        /// Tests if value equals one of many parameters.  
        /// </summary>
        /// <example>
        /// int a = 40;
        /// a.EqualsAnyRR(10,20,40); //True
        /// a.EqualsAnyRR(10,20,4); //False
        /// </example>
        /// Tested: 2022-02-09
        public static bool EqualsAnyRR(this int val, params int[] needles)
        {
            return needles.Contains(val);
        }



        /// <summary>
        /// Tests if value equals one of many parameters.  
        /// </summary>
        /// <example>
        /// int a = 40;
        /// a.EqualsAnyRR(10,20,40); //True
        /// a.EqualsAnyRR(10,20,4); //False
        /// </example>
        /// Tested: 2022-02-09
        public static bool EqualsAnyRR(this long val, params long[] needles)
        {
            return needles.Contains(val);
        }


    }
}
