﻿using Newtonsoft.Json;
using class_helper_rr;
using rr_general.wpf_custom_control;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace System.Windows.Controls //Same as DataGridRow.
{
    public static class Extension_DataGridRow
    {
        /// <summary>
        /// Moves a single row up by one.  Handles both data sources (.items and .ItemsSource).    
        /// Will restore multiple row selections OK.  Moves only one row, but restores all row selections.         
        /// </summary>
        /// <example>
        /// //Get selected row.  
        /// DataGridRow row = (DataGridRow)(grid.ItemContainerGenerator.ContainerFromIndex(grid.SelectedIndex));
        /// row.moveUp<rowDataType>();
        /// </example>
        /// <typeparam name = "rowDataType" >Grid's ItemsSource type. </ typeparam >
        /// <param name="rowObj"></param>
        /// Tested: 2017-07-08.
        public static void moveUp<rowDataType>(this DataGridRow rowObj)
        {
            DataGridRR grid = rowObj.GetParent<DataGridRR>();

            //------------------------
            //Safeties.  
            //------------------------
            if (rowObj == null || grid == null)
                return;

            //Index of current row.  
            var index = rowObj.GetIndex();

            if (index == 0)
                return;

            //------------------------
            //Save rows selected.  
            //------------------------
            Dictionary<int, bool> dictSelected = new Dictionary<int, bool>(); //Int=Row Index, Bool=IsSelected.  

            for (var i = 0; i < grid.Items.Count; i++)
            {
                DataGridRow r = (DataGridRow)(grid.ItemContainerGenerator.ContainerFromIndex(i));
                dictSelected.Add(r.GetIndex(), r.IsSelected);
            }

            //------------------------
            //Move row.  
            //https://stackoverflow.com/a/39277980/722945
            //------------------------
            //For grid rows added via "grid.items".  Willy nilly way to add rows.  
            if (grid.ItemsSource == null)
            {
                var rows = grid.Items;
                var prevRow = rows[index - 1];
                rows.Remove(prevRow); //Will still keep prevRow in memory, to insert later.  Tested OK.  
                rows.Insert(index, prevRow); 
            }
            else //Grid rows added with true typed ItemsSource.  Proper way.  
            {
                var rows = grid.ItemsSource as ObservableCollection<rowDataType>;
                var prevRow = rows[index - 1];
                rows.Remove(prevRow); //Will still keep prevRow in memory, to insert later.  Tested OK.  
                rows.Insert(index, prevRow);
            }

            grid.UnselectAll();
            grid.UpdateLayout();//Need this here, otherwise "Restore rows" will throw null error.  



            //------------------------
            //Restore rows selected.    
            //------------------------
            for (var i = 0; i < grid.Items.Count; i++)
            {
                DataGridRow r = (DataGridRow)(grid.ItemContainerGenerator.ContainerFromIndex(i));
                
                //Index-1 = main row.  
                if (i == index - 1)
                    r.IsSelected = dictSelected[index];
                //Index row was moved-down (so main row could move-up). 
                else if (i == index)
                    r.IsSelected = dictSelected[index - 1];
                else
                    r.IsSelected = dictSelected[i];
            }



        }













        /// <summary>
        /// Moves a single row down by one.  Handles both data sources (.items and .ItemsSource).    
        /// Will restore multiple row selections OK.  Moves only one row, but restores all row selections.       
        /// </summary>
        /// <example>
        /// //Get selected row.  
        /// DataGridRow row = (DataGridRow)(grid.ItemContainerGenerator.ContainerFromIndex(grid.SelectedIndex));
        /// row.moveDown<rowDataType>();
        /// </example>
        /// <param name="rowObj"></param>
        /// Tested: 2017-07-08.
        public static void moveDown<rowDataType>(this DataGridRow rowObj)
        {
            DataGridRR grid = rowObj.GetParent<DataGridRR>();

            //------------------------
            //Safeties.  
            //------------------------
            if (rowObj == null || grid == null)
                return;

            //Index of current row.  
            var index = rowObj.GetIndex();

            //If record to move is already last.  
            if (index == grid.Items.Count - 1)
                return;

            //------------------------
            //Save rows selected.  
            //------------------------
            Dictionary<int, bool> dictSelected = new Dictionary<int, bool>(); //Int=Row Index, Bool=IsSelected.  

            for (var i = 0; i < grid.Items.Count; i++)
            {
                DataGridRow r = (DataGridRow)(grid.ItemContainerGenerator.ContainerFromIndex(i));
                dictSelected.Add(r.GetIndex(), r.IsSelected);
            }


            //------------------------
            //Move row.  
            //https://stackoverflow.com/a/39277980/722945
            //------------------------

            //For grid rows added via "grid.items".  Willy nilly way to add rows.  
            if (grid.ItemsSource == null)
            {
                var rows = grid.ItemsSource as ObservableCollection<rowDataType>;
                var nextRow = rows[index + 1];
                rows.Remove(nextRow); //Will still keep prevRow in memory, to insert later.  Tested OK.  
                rows.Insert(index, nextRow);
            }
            else //Grid rows added with true typed ItemsSource.  Proper way.  
            {
                var rows = grid.ItemsSource as ObservableCollection<rowDataType>;
                var nextRow = rows[index + 1];
                rows.Remove(nextRow); //Will still keep prevRow in memory, to insert later.  Tested OK.  
                rows.Insert(index, nextRow);
            }


            grid.UnselectAll();
            grid.UpdateLayout();//Need this here, otherwise "Restore rows" will throw null error.  


            //------------------------
            //Restore rows selected.    
            //------------------------
            for (var i = 0; i < grid.Items.Count; i++)
            {
                DataGridRow r = (DataGridRow)(grid.ItemContainerGenerator.ContainerFromIndex(i));

                //Index row was moved-up (so main row could move-down).    
                if (i == index)
                    r.IsSelected = dictSelected[index + 1];
                //Index+1 = main row.    
                else if (i == index + 1)
                    r.IsSelected = dictSelected[index];
                else
                    r.IsSelected = dictSelected[i];
            }




        }






        /// <summary>
        /// Returns hash value based on value of every column on the row.  Basically concatenates every value into a big string.  
        /// </summary>
        /// <remarks>
        /// Native DataGridRow.Item.GetHashCode alters value if row is removed/re-added.  This custom method below handles it OK.   
        /// </remarks>
        /// <param name="rowObj"></param>
        /// Tested: 2017-07-03 all passed.  
        public static string getHashRR(this DataGridRow rowObj)
        {
            DataGridRR grid = rowObj.GetParent<DataGridRR>();

            if (rowObj == null || grid == null)
                return null;

            string r = null;

            for (var i = 0; i < grid.Columns.Count; i++)
            {
                var cell = grid.Columns[i].GetCellContent(rowObj.Item);

                //DataGridTextColumn is TextBlock (I tested).  Have not tried DataGridCheckBoxColumn yet.  OK to skip over non-text columns...every grid will have PK column anyhow.    
                if (cell is TextBlock)
                    r += ((TextBlock)cell).Text;
            }

            return r;

        }














    }
}



