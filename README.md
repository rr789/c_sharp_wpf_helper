A hodgepodge of helpers for C# and WPF.  This is geared toward applications running in a trusted environment.  

# Database helpers

Connect to multiple databases with one connection library.

```csharp
//Setup connection.  Pick one of four connections below.
//Firebird embedded server. 
CDatabaseConnection conn = new CDatabaseConnection(
	_serverHost:  null //Always null for embedded.  
	, _dbName:      @"E:\sample_firebird_database.FDB" 
	, _username:    "SYSDBA" 
	, _password:    "my_password" 
	, _dbType:      CDatabaseConnection.DB_TYPE_FIREBIRD_EMBEDED_SERVER      
	, _port:        null //No port with embedded server.
	);
  
//Firebird connecting to external server.    
CDatabaseConnection conn = new CDatabaseConnection(
	_serverHost:  "localhost" //Always null for embedded.  
	, _dbName:      @"E:\sample_firebird_database.FDB" 
	, _username:    "SYSDBA" 
	, _password:    "my_password" 
	, _dbType:      CDatabaseConnection.DB_TYPE_FIREBIRD_EXTERNAL_SERVER   
	, _port:        3050
	);

//Mysql.
CDatabaseConnection conn = new CDatabaseConnection(
	_serverHost:  "localhost" 
  , _dbName:      "db_name" 
  , _username:    "username" 
  , _password:    "my_password" 
  , _dbType:      CDatabaseConnection.DB_TYPE_MYSQL 
  , _port:        3306
  );
  
//Oracle.
CDatabaseConnection conn2 = new CDatabaseConnection(
	_serverHost:  "192.168.1.12"
  , _dbName:      "orcl" 
  , _username:    "erp"
  , _password:    "my_password"
  , _dbType:      CDatabaseConnection.DB_TYPE_ORACLE
  , _port:        "1521"
  );

//Insert one record into db.  
conn.executeNonQuery("INSERT INTO TABLE1 (FIRSTNAME,LASTNAME) VALUES ('F SAMPLE', 'L SAMPLE')");

//Query records from db.  
var dt = conn.executeQuery("SELECT * FROM table1"); //Runs query, puts into CDataTable.  
dataGridView1.dataSourceRR(dt); //Links CDataGridView to CDataTable. 
MessageBox.Show("records found: " + dt.table.Rows.Count.ToString());

//Get scalar value (aka value in first row, first column).  Also used for getting value from a function.  
var oValue = conn.executeScalar("SELECT FIRSTNAME FROM table1 WHERE ID='2'"); //Query only returns one column, one row.    
MessageBox.Show(oValue.ToString()); //Shows the value (ex: "Jane").
```


Basic database query abstraction for create/read/update/delete operations.  Uses the connection library above.  One example.
```csharp
var query = new CDatabaseQuery(connectionObject); //connectionObject is from CDatabaseConnection.
query.tableName = "TABLE1";
query.valueArr = new Dictionary<string, string>() {   //Or can use the std syntax: valueArr["fieldname"] = "value";
	{ "FIELDNAME1", "VALUE1" },   
	{ "FIELDNAME2", "VALUE2" },
	{ "FIELDNAME3", "VALUE3" }
};
query.insertRecord(); //Returns "1" for inserting one row.
```
PS. CDatabaseQuery also sanitizes the inputs. 

# User settings
Use CDataStore to store application settings to User/AppData folder.  

# Directory helpers

CDirectoryHelper has concise methods to get/save files with one line of code.  

# Logging
CLogger provides basic application logging with exception handling.  

# Basic type helpers
Numerous helpful methods in CNumber, CString, CType.  

# WPF converters
Formatting classes for $$$ amounts, dates etc.  

A couple converters for basic IF/THEN functionality in WPF.  

# Combobox 
- Likes to use a list of ComboBoxItemRR items as dataset.  Eliminates need to set properties DisplayMemberPath or SelectedValuePathPass.  
- In-line search capability.  
- Reduced many pain-points. 

# Datagrid improvements
- Various user handling improvements.  Ex: No need to double/triple click on a cell to edit.  
- Popup configuration menu to show/hide columns.  
- Popup context menu for additional settings.  
- Save grid settings (column width, column order) to a user's harddrive automatically. 
- New event RowEditEndedRR.  Fires reliably after user has finished editing a row.  
- Use column types ending in "RR" to hook into a new "column loaded" event.  A little bit of a hack but it works.  

# DatePicker improvements
- Select all text on first mouseclick.
- Date 1900-01-01 visually appears as nothing/NULL.  
- Clears value if user manually entered a date in the past (AND) EnablePastDate = false.

# Hyperlink control
- Concise usercontrol to quickly add a hyperlink.  

# NumericStepper
- New control.  

# TextBox
- Optional adorner hint.
- Regex validation. 
- New event UserStopsTypingEvent fires when user stops typing.  Has appropriate delay for most scenarios.
- Select all text when focused. 
- New event ValueUpdatedEventRR only fires if value changed.  Superior to native "lost focus" method which fires every time.  


# Treeview control
- Create a hierarchical menu system.  
- Filter methods to only show nodes where name = search term.  

